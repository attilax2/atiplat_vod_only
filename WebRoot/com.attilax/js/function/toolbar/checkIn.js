$(function(){
    //签入所有组
    $("#btn_checkInAll").click(function(){
        //alert(ctx);
        //这里先把userId写死
        $.post(ctx + "weibo/toolBarManager!checkInAll", function(data,status){
			if("success"==status){
				//转成javascript对象，高版本的jquery不用转
				var userState = window["eval"]("(" + data + ")");
				/**
				 * 请求成功
				 * 把登录状态改为空闲
				 */
				setLoginState(userState.ret);
				
				//请求提示信息(成功or失败)
				alert(userState.message);
			}else{
				alert("url地址请求失败!");
			}
        });
    });
	
	//签入指定组
	$("#btn_checkInAsign").click(function(){
		
		window.open(ctx+"weibo/toolBarManager!showUserGroupAndSkill?userId=8", 'newwindow', 'height=400px, width=650px,toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no,resizable=no');

		/*
		function person(name,age){
			this.name=name;
			this.age=age;
		}
		person1 = new person("陈",28);
		person2 = new person("李",28);
		
		var arr = new Array();
		
		arr.push(person1);
		arr.push(person2);
		
		alert(arr.length);
		
		//alert(JSON.stringify(arr));
		
		//alert("hello");
		*/
		
	});
	
    //签出所有组
    $("#btn_checkOutAll").click(function(){
        //alert(ctx);
        //这里先把userId写死
        $.post(ctx + "weibo/toolBarManager!checkOutAll", function(data,status){
			if("success"==status){
				//转成javascript对象，高版本的jquery不用转
				var userState = window["eval"]("(" + data + ")");
				/**
				 * 请求成功
				 * 把登录状态改为登录未签入
				 */
				setLoginState(userState.ret);
				
				//请求提示信息(成功or失败)
				alert(userState.message);
			}else{
				alert("url地址请求失败!");
			}
        });
    });	
	
    //示忙
    $("#btn_showBusy").click(function(){
        //alert(ctx);
        //这里先把userId写死
        $.post(ctx + "weibo/toolBarManager!checkBusy", function(data,status){
			if("success"==status){
				//转成javascript对象，高版本的jquery不用转
				var userState = window["eval"]("(" + data + ")");
				/**
				 * 请求成功
				 * 把登录状态改为繁忙
				 */
				setLoginState(userState.ret);
				
				//请求提示信息(成功or失败)
				alert(userState.message);
			}else{
				alert("url地址请求失败!");
			}
        });
    });	
	
    //示闲
    $("#btn_showIdle").click(function(){
        //alert(ctx);
        //这里先把userId写死
        $.post(ctx + "weibo/toolBarManager!checkFree", function(data,status){
			if("success"==status){
				//转成javascript对象，高版本的jquery不用转
				var userState = window["eval"]("(" + data + ")");
				/**
				 * 请求成功
				 * 把登录状态改为繁忙
				 */
				setLoginState(userState.ret);
				
				//请求提示信息(成功or失败)
				alert(userState.message);
			}else{
				alert("url地址请求失败!");
			}
        });
    });			
	
    //注销
    $("#btn_logout").click(function(){
		var msg='是否退出系统 ！';
		if (!confirm(msg))
			return;
        $.post(ctx + "weibo/toolBarManager!logout", function(data,status){
			if("success"==status){
				//转成javascript对象，高版本的jquery不用转
				var userState = window["eval"]("(" + data + ")");						
				//请求提示信息(成功or失败)
				//alert(userState.message);
				window.location = ctx +'/login.jsp';
			}else{
				alert("url地址请求失败!");
			}
        });
    });	    
});
/**
 * 设置登录状态
 * @param {Object} loginState
 */
function setLoginState(loginState){
	    var show_message;
		if(loginState==0){
			show_message="登录未签入";
		}else if(loginState==1){
			show_message="空闲";
		}else if(loginState==2){
			show_message="处理中";
		}else if(loginState==3){
			show_message="离线";
		}else if(loginState==4){
			show_message="繁忙";
		}
		//设置登录状态文本的值
		$("#show_loginState").text(show_message);
}
