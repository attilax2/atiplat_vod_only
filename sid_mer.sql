/*
Navicat MySQL Data Transfer

Source Server         : loc
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : wxb_srv_mir

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-05-22 01:23:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sid_mer`
-- ----------------------------
DROP TABLE IF EXISTS `sid_mer`;
CREATE TABLE `sid_mer` (
  `realname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sid` varchar(255) DEFAULT NULL,
  `img1` varchar(255) DEFAULT NULL,
  `img2` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `stat` int(11) DEFAULT '0',
  `insertTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` datetime DEFAULT NULL,
  `delTime` datetime DEFAULT NULL,
  `isdel` int(11) DEFAULT '0',
  `oper` varchar(255) DEFAULT '',
  `oper_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sid_mer
-- ----------------------------
INSERT INTO `sid_mer` VALUES (null, null, '00upQ4/0521_171120_395.jpg', '', '1', '67637798', '0', '2016-05-22 01:11:21', null, null, '0', '', null);
