package System.Drawing;

 
 
import java.awt.image.BufferedImage;

import com.attilax.img.imgx;
import com.attilax.io.FileExistEx;

public class Bitmap   {

//	public Bitmap(int i, int j,int type) {
//		 super(i,j,type);
//	}

	public     int Width = 0;
	public int Height;
	public BufferedImage bi;

	public Bitmap(int i, int j) {
		//super(i,j,BufferedImage.TYPE_INT_RGB);
		 BufferedImage bi=new BufferedImage(i, j, BufferedImage.TYPE_INT_RGB);
		 imgx.setBackgroudColor(bi, new java.awt.Color(255, 255, 255));
		
		 this.bi=bi;
		 this.Width=i;this.Height=j;
	}

//	public Bitmap(BufferedImage bi) {
//		   this.bi=bi;
//	}

	public Bitmap() {
		// TODO Auto-generated constructor stub
	}

	public Bitmap(BufferedImage bi2) {
		this.bi=bi2;
		this.Width=bi2.getWidth();
		this.Height=bi2.getHeight();
	}

//	public Bitmap(BufferedImage bi2) {
//		// TODO Auto-generated constructor stub
//	}

	public Bitmap(String lineCharsPic) {
		BufferedImage bi2=imgx.toImg(lineCharsPic);
		this.bi=bi2;
		this.Width=bi2.getWidth();
		this.Height=bi2.getHeight();
	}

	public Color GetPixel(int x, int y) {
	 
		int rgb = bi.getRGB(x, y);
		java.awt.Color clr=new java.awt.Color(rgb);
		Color color = new  Color( rgb);
		color.G=clr.getGreen();
		color.R=clr.getRed();
		color.B=clr.getBlue();
		return color;
	}

	public void toFile(String string) throws FileExistEx {
		imgx.save(this.bi, string);
		
	}

	public void toFile_overwrite(String string) {
		imgx.save_overwrite(this.bi, string);
		
	}
	
//	public Bitmap() {
//		// TODO Auto-generated constructor stub
//	}

}
