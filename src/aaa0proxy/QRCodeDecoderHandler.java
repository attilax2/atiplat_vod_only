 
package aaa0proxy;
 

import java.awt.image.BufferedImage; 
import java.io.File; 
import java.io.IOException; 
 


import javax.imageio.ImageIO; 
 


import com.attilax.exception.ExUtil;

import jp.sourceforge.qrcode.QRCodeDecoder; 
import jp.sourceforge.qrcode.data.QRCodeImage; 
import jp.sourceforge.qrcode.exception.DecodingFailedException; 
 
/**
 * @blog http://sjsky.iteye.com
 * @author Michael
 */ 
public class QRCodeDecoderHandler { 
 
    /**
     * 解码二维码
     * @param imgPath
     * @return String
     */ 
    public String decoderQRCode(String imgPath) { 
 
    	String charsetName = "gbk";
        // QRCode 二维码图片的文件 
        return decoderQRCode(imgPath, charsetName); 
    }

    public String decoderQRCode(String imgPath, String charsetName) {
		File imageFile = new File(imgPath); 
 
        BufferedImage bufImg = null; 
        String decodedData = null; 
        try { 
            bufImg = ImageIO.read(imageFile); 
 
            QRCodeDecoder decoder = new QRCodeDecoder(); 
            byte[] decode = decoder.decode(new J2SEImage(bufImg));
			
			decodedData = new String(decode,charsetName); 
 
            // try { 
            // System.out.println(new String(decodedData.getBytes("gb2312"), 
            // "gb2312")); 
            // } catch (Exception e) { 
            // // TODO: handle exception 
            // } 
        } catch (IOException e) { 
            ExUtil.throwExV2(e);
        } catch (DecodingFailedException dfe) { 
        	  ExUtil.throwExV2(dfe);
        } 
        return decodedData;
	} 
 
    /**
     * @param args the command line arguments
     */ 
    public static void main(String[] args) { 
        QRCodeDecoderHandler handler = new QRCodeDecoderHandler(); 
        String imgPath = "c:\\3bf33a87e950352a5936aa0a5543fbf2b2118b59.jpg"; 
        String decoderContent = handler.decoderQRCode(imgPath,"gbk"); 
        System.out.println("解析结果如下："); 
        System.out.println(decoderContent); 
        System.out.println("========decoder success  !!!"); 
    } 
 
    class J2SEImage implements QRCodeImage { 
        BufferedImage bufImg; 
 
        public J2SEImage(BufferedImage bufImg) { 
            this.bufImg = bufImg; 
        } 
 
        public int getWidth() { 
            return bufImg.getWidth(); 
        } 
 
        public int getHeight() { 
            return bufImg.getHeight(); 
        } 
 
        public int getPixel(int x, int y) { 
            return bufImg.getRGB(x, y); 
        } 
 
    } 
} 
 
//运行结果如下（解码出的内容和之前输入的内容一致 ）：
//解析结果如下：
//Hello 大大、小小,welcome to QRCode!
//Myblog [ http://sjsky.iteye.com ]
//EMail [ sjsky007@gmail.com ]
//Twitter [ @suncto ]
//========decoder success!!!

