package aaaAtiDslParser;

 

 

	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.sql.Statement;
	import java.util.Properties;

	/**
	 * @author greatwqs
	 * @date 2011-8-12
	 */
	public class DerbyTest_File {

		//jdbc:derby:C:\\Users\\Administrator\\powerfulpos-database
		private final static String DB_URL = "jdbc:derby:D:\\derby\\ij_cmd_test_db;create=true";
		private final static String DERBY_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";

		public static void main(String[] args) {
			Connection conn = null;
			try {
				Class.forName(DERBY_DRIVER);
				Properties properties = new Properties();
				// properties.put("create", "false"); // 新建数据库
				// properties.put("user", "APP");     // 用户名
				// properties.put("password", "APP"); // 密码

				// properties.put("retreiveMessagesFromServerOnGetMessage", "true");
				conn = DriverManager.getConnection(DB_URL, properties);

				Statement stat = conn.createStatement();
				stat.execute("create table ijtest(id int primary key,txt varchar(20))");
				stat.execute("insert into ijtest(id,txt) values(1,'aa') ");
				
				ResultSet result = stat.executeQuery("SELECT id,txt FROM ijtest");

				while (result.next()) {
					System.out.println("序号 : " + result.getInt(1));
				}
				result.close();
				stat.close();
				conn.close();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// 内嵌模式数据库操作用完之后需要关闭数据库,这里没有执行数据库名称则全部关闭.
				try {
					DriverManager.getConnection("jdbc:derby:;shutdown=true");
				} catch (SQLException e) {
					e.getMessage();
				}
			}
			
			System.out.println("---ff");
		}
	}

 
