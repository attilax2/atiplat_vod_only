package aaaBrowser.model;

/**
 * 任务头实体类
 * 
 * @author moyan
 * @version [版本号, 2015年4月26日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class TaskModel
{
    private String taskid;
    
    private String tasktype;
    
    private String taskstatus;
    
    private String taskmessage;

    public String getTaskid()
    {
        return taskid;
    }

    public void setTaskid(String taskid)
    {
        this.taskid = taskid;
    }

    public String getTasktype()
    {
        return tasktype;
    }

    public void setTasktype(String tasktype)
    {
        this.tasktype = tasktype;
    }

    public String getTaskstatus()
    {
        return taskstatus;
    }

    public void setTaskstatus(String taskstatus)
    {
        this.taskstatus = taskstatus;
    }

    public String getTaskmessage()
    {
        return taskmessage;
    }

    public void setTaskmessage(String taskmessage)
    {
        this.taskmessage = taskmessage;
    }
    
    
}
