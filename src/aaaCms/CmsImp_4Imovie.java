package aaaCms;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Map;
import java.util.Set;

import com.attilax.core;
import com.attilax.fdb.ColsGener;
import com.attilax.fdb.FdbX;
import com.attilax.io.filex;
import com.attilax.lang.MapX;
import com.attilax.text.strUtil;

public class CmsImp_4Imovie extends CmsImpLocalFileVer2  {
	
	 

	 
	protected String recordDir;

	

	public CmsImp_4Imovie() {
		ColsGener colsGener = new ColsGener() {

			@Override 
			public Object exe(Object arg0) {
 				//final CmsImp_4Imovie cmsx = new CmsImp_4Imovie();
 			//	cmsx.
 				recordDir = (String)arg0;
 				 
				Map cols2 = MapX.newOrderMap();
				cols2.put("img", getPicPath());
				cols2.put("title", getTitle());
				cols2.put("txt", getTxtPath());
				cols2.put("file", getFilepath());
				return cols2;
			}
		};
		this.colsGener=colsGener;
	}
	
	public static void main(String[] args) {
		
	
	}
	
	
	Object getTitle() {
		String[] a=		this.recordDir.split("/");
		String tit = a[a.length-1];
		return tit;
	}

	Object getFilepath() {
		//
		String mvExt = ".mkv  .avi 		 .m2ts		 .ts		 .iso		 .rmvb		 .mp4";
		final Set<String> st = strUtil.toSet(mvExt, strUtil.dotChar);
		String[] list = new File(this.recordDir).list(new FilenameFilter() {

			@Override
			public boolean accept(File paramFile, String filename) {
				// path =----filename.ext
				String extname = filex.getExtName(filename);
				if (st.contains(extname))
					return true;
				else
					return false;
			}
		});
		try {
			int offsetStartIndex = maindir.length();

			return this.recordDir.substring(offsetStartIndex+1) + "/" + list[0];
		} catch (Exception e) {
			return "";
		}
	}

	Object getTxtPath() {
		String[] list = new File(this.recordDir).list(new FilenameFilter() {

			@Override
			public boolean accept(File paramFile, String filename) {
				// path =----filename.ext
				String extname = filex.getExtName(filename);
				if ("txt".contains(extname))
					return true;
				else
					return false;
			}
		});
		try {
			int offsetStartIndex = maindir.length();
			return this.recordDir.substring(offsetStartIndex+1) + "/" + list[0];
		} catch (Exception e) {
			return "";
		}
	}

	Object getPicPath() {
		String[] list = new File(recordDir).list(new FilenameFilter() {

			@Override
			public boolean accept(File paramFile, String filename) {
				// path =----filename.ext
				String extname = filex.getExtName(filename);
				if ("jpg".contains(extname) || "jpeg".contains(extname))
					return true;
				else
					return false;
			}
		});
		try {// .substring(3)
			if(this.envi.equals("bs"))
			{
				int offsetStartIndex = maindir.length();
				return this.recordDir.substring(offsetStartIndex+1) + "/" + list[0];
			}
			return this.recordDir + "/" + list[0];
		} catch (Exception e) {
			return "";
		}
	}



}
