/*
 * Christopher Deckers (chrriis@nextencia.net)
 * http://www.nextencia.net
 *
 * See the file "readme.txt" for information on usage and redistribution of
 * this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */
package aaaMoveMana;

import java.awt.AWTEvent;
import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.attilax.core;
import com.attilax.io.pathx;
import com.attilax.lang.CmdX;
import com.attilax.lang.GUIx;
import com.attilax.ui.MsgBox;
import com.attilax.util.PropX;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserAdapter;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserCommandEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserEvent;
import chrriis.dj.nativeswing.swtimpl.demo.examples.vlcplayer.AtiVLCPlayer;

/**
 * @author Christopher Deckers
 */
public class movemana {

	// public class Cmd1Listener extends WebBrowserAdapter {
	// @Override
	//
	//
	// }
	public	static  JWebBrowser webBrowser = new JWebBrowser();
	public static JComponent createContent() {
		JPanel contentPane = new JPanel(new BorderLayout());
		JPanel webBrowserPanel = new JPanel(new BorderLayout());
		// webBrowserPanel.setBorder(BorderFactory.createTitledBorder("--"));
		// JWebBrowser.useXULRunnerRuntime()
	
		String url = "http://www.baidu.com";

		url = "http://localhost/vod/11/jsjava.htm";
		url = PropX
				.getConfig(pathx.classPathParent() + "/cfg.properties", "movemana-url");
		webBrowser.navigate(url);
		// ati
		webBrowser.setDefaultPopupMenuRegistered(false);// �����Ҽ������˵�
		webBrowser.setBarsVisible(false);// ���������������
		webBrowser.setJavascriptEnabled(true);
		webBrowser.setAutoscrolls(false);
		// webBrowser.setv
		// webBrowser.set
		// webBrowser.addWebBrowserListener(listener)
		// webBrowser.addWebBrowserListener(new Cmd1Listener());
		webBrowser.addWebBrowserListener(new WebBrowserAdapter() {

			// �������ؽ���
			public void loadingProgressChanged(WebBrowserEvent e) {
				// ���������ʱ
				if (e.getWebBrowser().getLoadingProgress() == 100) {
					// String result = (String) webBrowser
					// .executeJavascriptWithResult("return '123';"
					// .toString());
					// System.out.println(result);
					//GUIx. hideMouse();
				}
			}

			// js java comm
			public void commandReceived(WebBrowserCommandEvent e) {
				String command = e.getCommand();
				System.out.println("  receve commd: " + command);
				Object[] parameters = e.getParameters();
				if ("cmd1".equals(command)) {
					String html = (String) parameters[0];
					System.out.println(html);
				}

				if ("play".equals(command)) {
					String html = (String) parameters[0];
					html = html.replaceAll("/vdx/", "");
					System.out.println(html);
					String playserver = PropX.getConfig(pathx.classPath()
							+ "/cfg.properties", "playserver");
					String url = PropX.getConfig(pathx.classPath()
							+ "/cfg.properties", "playserver")
							+ "/" + html;
					// p48
					if (playserver.trim().startsWith("\\\\"))
						url = playserver + "\\" + html;
					
					File f=new File(url);
					if(!f.exists())
					{
						MsgBox.show("文件可能不存在::"+url);
						return;
					}
					System.out.println(" will play:" + url);
					// will
					// play:z:/爱情类\五十度灰.未删减\五SD灰.未删减.Fifty.Shades.of.Grey.2015.DVD.X264.AAC.English.CHS.Mp4Ba
					// (2).mp4

					String player = PropX.getConfig(pathx.classPath()
							+ "/cfg.properties", "player");
					String cmd = "\"" + player + "\" \"" + url + "\"";
					System.out.println(cmd);
					CmdX.exec(cmd);
					// AtiVLCPlayer.getInstance().ini(new String[]{url});
				}else
				{
				
				
					jsHandler hdl=	(jsHandler) JsEventMap.get(command);
					hdl.exe(parameters);
				}
				
			//	, new jsHandler());

			}
		});

		// //ati end
		webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
		// webBrowserPanel.setv

		contentPane.add(webBrowserPanel, BorderLayout.CENTER);
		// Create an additional bar allowing to show/hide the menu bar of the
		// web browser.
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 4));
		JCheckBox menuBarCheckBox = new JCheckBox("Menu Bar",
				webBrowser.isMenuBarVisible());
		menuBarCheckBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				webBrowser.setMenuBarVisible(e.getStateChange() == ItemEvent.SELECTED);
			}
		});
		buttonPanel.add(menuBarCheckBox);
		buttonPanel.add(new JButton("exit..") {
			{

				this.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});

			}
		});
		buttonPanel.add(new JButton("test play..") {
			{

				this.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String url = PropX.getConfig(pathx.classPath()
								+ "/cfg.properties", "testMv");
						System.out.println(" will play:" + url);
						AtiVLCPlayer.getInstance().ini(new String[] { url });
					}
				});

			}
		});
		contentPane.setBackground(Color.black);// ati
		// ati p3g
		int showmenu = 1;
		try {
			showmenu = Integer.parseInt(PropX.getConfig(
					pathx.classPath() + "/cfg.properties", "showmenu").trim());
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (showmenu == 1)
			contentPane.add(buttonPanel, BorderLayout.SOUTH);
		return contentPane;
	}

	public static boolean canbeClose = false;
public static Map JsEventMap=new HashMap();
	/* Standard main method to try that test as a standalone application. */
	public static void main(String[] args) {
		
		JsEventMap.put("openFileBrowser", new jsHandler(webBrowser ));
		
		// System.setProperty("org.eclipse.swt.browser.XULRunnerPath",
		// "C:\\xulrunner");
		System.out.println("--");
	//	final ImageIcon icon = new ImageIcon(pathx.classPath(Ati4vod.class)
		//		+ "/mov.png");
		// setIconImage(image);//����setIconImage���������Ƕ�����JFrame�ĸ���Frame���еķ�����

		NativeInterface.open();
		UIUtils.setPreferredLookAndFeel();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("") {

					// 重写这个方法

					// @Override
//					protected void processWindowEvent(WindowEvent e) {
//						System.out.println(e.getID());
//						if (e.getID() == WindowEvent.WINDOW_CLOSING) {
//							// KeyEvent evt = (KeyEvent) e;
//							// if (evt.getKeyCode() == KeyEvent.VK_ESCAPE &&
//							// evt.isControlDown()) {
//							//
//							// }
//							// MsgBox.show("close  a");
//							if (canbeClose) {
//								System.out.println(" can be close..");
//								super.processWindowEvent(e);
//								return;
//							} else {
//								System.out.println(" can not close..");
//								return; // 直接返回，阻止默认动作，阻止窗口关闭
//							}
//
//						} else {
//							System.out.println("no close evert..");
//							super.processWindowEvent(e); // 该语句会执行窗口事件的默认动作(如：隐藏)
//						}
//					}

				};
			//	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane()
						.add(createContent(), BorderLayout.CENTER);
				frame.setSize(800, 600);
				frame.setLocationByPlatform(true);
			//	frame.setIconImage(icon.getImage());
		 		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); // todox max win
		//		frame.setUndecorated(true);
				frame.setVisible(true);
				// frame. addWindowListener(new WindowAdapter() {
				// private int oldValue; // 记忆原默认值
				// public void windowClosing(WindowEvent evt) {
				// MsgBox.show("close");
				// // if (notClose) { // 不能关闭时
				// // if (getDefaultCloseOperation() != 0) {
				// // oldValue = getDefaultCloseOperation();
				// // setDefaultCloseOperation(0);
				// // }
				// // // 提示不能关闭
				// // } else { // 恢复默认动作
				// // if (oldValue != 0 && getDefaultCloseOperation() == 0)
				// setDefaultCloseOperation(oldValue);
				// // }
				// }
				// });
			}
		});

//		core.execMeth_Ays(new Runnable() {
//
//			@Override
//			public void run() {
//				 String act="1";
//				 try {
//					   act =  PropX.getConfig(pathx.classPath()
//								+ "/cfg.properties", "act");
//				} catch (Exception e2) {
//					 
//				}
//				 try {
//					
//					 if(act.trim().equals("1"))
//						 GUIx.activeWin();
//					 else
//						 System.out.println("no act win");
//				 } catch (AWTException e) {
//				 // TODO Auto-generated catch block
//				 e.printStackTrace();
//				 }
//
//			}
//		}, "--act win");

		// && (arg0.isControlDown()))
		final Toolkit toolkit = Toolkit.getDefaultToolkit();
		toolkit.addAWTEventListener(new AWTEventListener() {
			@Override
			public void eventDispatched(AWTEvent e) {
				if (e.getID() == KeyEvent.KEY_PRESSED) {
					KeyEvent evt = (KeyEvent) e;
					// if (evt.getKeyCode() == KeyEvent.VK_ESCAPE &&
					// evt.isControlDown()) {
					if (evt.getKeyCode() == KeyEvent.VK_F7&& !evt.isAltDown()) {
						// frame.dispose();
						if (MsgBox.confirm("确认关闭")) {
							canbeClose = true;
							System.exit(0);
						}
					}
				}
			}
		}, AWTEvent.KEY_EVENT_MASK);
//		GUIx. hideMouse();
//		GUIx.hideMouseMulti();
		NativeInterface.runEventPump();

	}

}
