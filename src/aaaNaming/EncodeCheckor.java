package aaaNaming;

import java.io.UnsupportedEncodingException;

import com.attilax.io.filex;
import com.attilax.io.pathx;

public class EncodeCheckor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String f = pathx.classPath() + "/aaaNaming/names.txt";
		System.out.println(getEncode(f));
	}

	private static String getEncode(String f) {
		// TODO Auto-generated method stub
		String t = filex.read(f);
		String $zh = t;
		;
		String $s1 = iconv("UTF-8", "gb2312", $zh);
		String $s2 = iconv("gb2312", "UTF-8", $s1);
		if ($s2 .equals( $zh)) {
			return "utf-8";
		} // if utf code , convert2 gbk mode
		return "gbk";
	}

	private static String iconv(String encode1, String encode2, String str) {
		byte[] temp = null;
		try {
			temp = str.getBytes(encode1);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// 这里写原编码方式
		String newStr = null;
		try {
			newStr = new String(temp, encode2);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// 这里写转换后的编码方式

		return newStr;
	}

}
