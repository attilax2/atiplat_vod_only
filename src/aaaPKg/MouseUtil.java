package aaaPKg;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import com.attilax.concur.TaskUtil;
import com.attilax.exception.ExUtil;

public class MouseUtil {
	
	public static void main(String[] args) {
		MouseUtil mx = new MouseUtil();
	//	TaskUtil.sleep_sec_withCountdown(10);
		mx.mouseMove(650, 361);
		mx.mouseMove(1124, 704);
		System.out.println("--f");
	}

	public MouseUtil() {
		 
		try {
			robot = new Robot();
		} catch (AWTException e) {
			ExUtil.throwEx(e);
		}
	}
	Robot robot;

	public MouseUtil(Robot r) {
		this.robot=r;
	}

	public void click() {
		 robot.mousePress(KeyEvent.BUTTON1_MASK);
		  robot.mouseRelease(KeyEvent.BUTTON1_MASK);
		
	}

	public void mouseMove(int i, int j) {
		robot.mouseMove(i, j);
		
	}

}
