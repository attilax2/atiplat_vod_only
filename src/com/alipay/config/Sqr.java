/**
 * 
 */
package com.alipay.config;

/**
 * @author attilax 2016年11月21日 下午9:24:37
 */
public class Sqr {

	/**
	 * attilax 2016年11月21日 下午9:24:37
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("pp3");
		System.out.println(squareRoot(2));
	}

	public static double squareRoot(double n) {
		double x = 1;
		double temp = 1;
		int numTime=0;
		do {
			numTime++;
			temp = x; // 保存上一次计算的值
			x = 0.5 * (x + n / x); // 这个就是牛顿迭代法的基本公式
			System.out.println(numTime);
		} while (Math.abs(x - temp) > 0.00001); // 如果两次求值差的绝对值小于0.00001则结束循环
		return x;
	}
	
	
	public static double squareRoot(double n) {
		double x = 1;
		double temp = 1;
		do {
			temp = x; // 保存上一次计算的值
			x = 0.5 * (x + n / x); // 这个就是牛顿迭代法的基本公式
		} while (Math.abs(x - temp) > 0.00001); // 如果两次求值差的绝对值小于0.00001则结束循环
		return x;
	}

}
