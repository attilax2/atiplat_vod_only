package com.attilaax.encode;

// pbb import aaaBlogger.EditorForm; pbb

import java.io.UnsupportedEncodingException;

import com.attilax.exception.ExUtil;
import com.attilax.io.filex;
import com.attilax.io.pathx;

public class EncodeX {

	public static void main(String[] args) {
		// String s="window.document.getElementById('editor').value='\"aaa\"'";
		// s=filex.read("c:\\1.txt");
		// System.out.println(jsEncode( s));
//pbb
//		String js = filex.read(pathx.classPath(EditorForm.class)
//				+ "/editor__form.js");
//		String p2_txt = "\\'aaa\\'";
//		// String script = js.replace("@txt", p2_txt);
//		p2_txt = p2_txt.replace("\\", "\\\\");
//		System.out.println(p2_txt);
		System.out.println("f");
	}

	/**
	 * replace double quoue
	 * 
	 * @param p2_txt
	 * @return
	 */
	public static String jsEncode(String p2_txt) {
		// TODO Auto-generated method stub
		p2_txt = p2_txt.replace("\\", "\\\\");
		String doubleQuoto = "\"";
		String replaceAll = p2_txt.replace(doubleQuoto, "\\\"");
		replaceAll = replaceAll.replace("\r", "\\r");
		replaceAll = replaceAll.replace("\n", "\\n");

		return replaceAll;
	}

	public static String jsEncode_ReplaceSingleQuoue(String p2_txt) {
		return jsEncodeSingleQuoue(p2_txt);
	}

	public static String jsEncodeSingleQuoue(String p2_txt) {
		// TODO Auto-generated method stub

		p2_txt = p2_txt.replace("\\", "\\\\");
		String SingleQuoue = "\'";
		String replaceAll = p2_txt.replace(SingleQuoue, "\\\'");

		replaceAll = replaceAll.replace("\r", "\\r");
		replaceAll = replaceAll.replace("\n", "\\n");

		return replaceAll;
	}

	/**
	attilax    2016年11月21日  下午5:00:47
	 * @param name
	 * @return
	 */
	public static String gbk2utf(String name) {
		return enc1ToEnc2(name,"gbk","utf8");
		
	}

	private static String enc1ToEnc2(String name,String enc1,String enc2) {
		byte[] b = null;
		try {
			b = name.getBytes(enc1);
		} catch (UnsupportedEncodingException e1) {
			ExUtil.throwExV2(e1);
		}
		 
		try {
			return new String(b,enc2);
		} catch (UnsupportedEncodingException e) {
			ExUtil.throwExV2(e);
		}
		throw new RuntimeException("$null,param:"+name);
	}

	/**
	attilax    2016年11月21日  下午5:01:35
	 * @param name
	 * @return
	 */
	public static String utf2gbk(String name) {
		return enc1ToEnc2(name,"utf8","gbk");
	}

}
