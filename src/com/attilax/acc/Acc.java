package com.attilax.acc;

import java.math.BigDecimal;

/**
 * 
 * @author attilax
 *2016年4月16日 下午4:18:37
 */
public class Acc {

	public BigDecimal amount;
	public String uname;
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}

}
