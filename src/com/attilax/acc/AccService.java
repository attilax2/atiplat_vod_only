package com.attilax.acc;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

//qaa import aaaCasher.SqlParser;
import aaaCfg.IocX4casher;

import com.attilax.db.DBX;
import com.attilax.ioc.IocUtilV2;
import com.attilax.lang.AString;
import com.attilax.lang.text.strUtil;
import com.attilax.orm.AtiOrm;
import com.attilax.sql.SqlCheckor;
import com.google.inject.Inject;

/**
 * jobus
 * 
 * @author attilax
 *2016年10月24日 下午6:14:39
 */
public class AccService extends AccSrvAbs {
	
	public static void main(String[] args) {
		System.setProperty("prj", "jobus");
		   AccService accS=IocUtilV2.getBean(AccService.class);
	//	   System.out.println(accS.getNowMoney("aamaoaamao"));
		   accS.addAmount("uidxx14", new BigDecimal( "5.20"));
		//   accS.createAcc("acc_attilax",12.56);
		//   Query
	//	 int r=  accS.reduceAmount("uu", 12.56);
	//	 System.out.println("--ret :"+String.valueOf(r));
	}
	
	

	public void createAcc(String uname, double d) {
		BigDecimal money=new BigDecimal(d);
	String v_vali=	new SqlCheckor().getValideVal(money);
		 String   $sql = "insert into   ecs_users( "+
		            "   user_name,user_money,vali_val )values('@u@',  "+String.valueOf(d)+"" +
		           
		            ",'$vali$' )";
		 $sql= $sql.replace("@u@", uname).replace("$vali$", v_vali);
		  
		  System.out.println("---"+  $sql);
			
		  sqlCkSrv.valideValCheck4newRow(money, v_vali);
		int r=	dbx.execSql_retInt($sql);
		System.out.println("----ret int:"+String.valueOf(r));
		
	}



	@Inject
	DBX dbx;
	public int reduceAmount(String uid, Double i) {
		
		//DataLogicSales
		// TODO Auto-generated method stub
		
		
		 /* 鏇存柊鐢ㄦ埛淇℃伅 */
	  String   $sql = "UPDATE   ecs_users "+
	            " SET user_money = user_money - "+String.valueOf(i)+"" +
	           
	            " WHERE user_name = '"+uid+"' LIMIT 1";
	  
	  System.out.println("---"+  $sql);
		
	  
	int r=	dbx.execSql_retInt($sql);
	System.out.println("----ret int:"+String.valueOf(r));
		return r;
	}
	@Inject
	SqlCheckor sqlCkSrv;
	/**
	 * 
	 * @param uid
	 * @param i
	 * @return
	 */
public int addAmount(String uid, BigDecimal i) {
	iniAcc(uid);
	String sql = strUtil.fmt( "select * from ecs_users where user_name='$uid$'",uid);
	Map row=dbx.uniqueResult2row(sql);
	if(row==null)
		throw new CantFindAcc("uid="+uid);
	BigDecimal now=(BigDecimal) row.get("user_money");
	
//	SqlCheckor sqlCkSrv=
	if(row.get("vali_val")!=null)   //for compati  olddata no vali
	sqlCkSrv.valideValCheck4oldrow(row.get("user_money"),row.get("vali_val").toString() );
	
	BigDecimal  new_money=now.add(i);
	String new_money_vali= new SqlCheckor().getValideVal(new_money);
	new SqlCheckor().valideValCheck4newRow(new_money, new_money_vali);
	
		//DataLogicSales
		// TODO Auto-generated method stub
		
		
		 /* 鏇存柊鐢ㄦ埛淇℃伅 */
	  String sql2 = "UPDATE   ecs_users "+
	            " SET user_money =$newMoney$,vali_val='$vali$'"+
	           
	            " WHERE user_name = '"+uid+"' LIMIT 1";
	String   $sql = sql2.replace("$newMoney$", new_money.toString()).replace("$vali$",new_money_vali);
	  
	  System.out.println("---"+  $sql);
		
	  
	int r=	dbx.execSql_retInt($sql);
	if(r!=1)
		throw new AccEx(" acc ex ret is:"+String.valueOf(r) +" uid:"+uid);
	System.out.println("----ret int:"+String.valueOf(r));
		return r;
	}

@Inject
AtiOrm ormx;

	private void iniAcc(String uid) {
		 
		if(!ormx.setTable("ecs_users").existUniq("user_name",uid).existRzt)
		{
			createAcc(uid, 0);
		}
	}



	public double getNowMoney(String uid) {
		 String   $sql = "select * from   ecs_users "+ 
		           
		            " WHERE user_name = '"+uid+"' LIMIT 1";
		Map m= dbx.uniqueResult($sql);
		return Double.parseDouble( m.get("user_money").toString());
	}



	/**
	attilax    2016骞�4鏈�14鏃�  涓嬪崍2:25:45
	 * @param id
	 * @return
	 */
	public Acc getAcc(Object id) {
		// TODO Auto-generated method stub
		return null;
	}

}
