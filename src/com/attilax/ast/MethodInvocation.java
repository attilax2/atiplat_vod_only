package com.attilax.ast;

import java.util.List;

import com.attilax.exception.ExUtil;
import com.google.common.collect.Lists;

public class MethodInvocation  extends Expression  implements Cloneable {
	
//	  Expression EXPRESSIONn;  //ClassInstanceCreation [316+14]  new,,,SimpleName [230+8] static method
//String	NAME;
//	List ARGUMENTS;
	
	private static final String ARGUMENTS_PROPERTY = null;
	public Expression Exp = null;
	public String Name = null;
	
	public 	List arguments =Lists.newArrayList();
	
	 public Object clone() {   
	        try {   
	            return super.clone();   
	        } catch (CloneNotSupportedException e) {   
	           ExUtil.throwExV2(e);
	        }
			return null;   
	    }   

}
