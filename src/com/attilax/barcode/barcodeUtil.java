package com.attilax.barcode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import com.attilax.collection.Iflt;
import com.attilax.collection.Ireduce;
import com.attilax.collection.listUtil;
import com.attilax.io.filex;
//import com.attilax.util.BarCodeUtil;
import com.attilax.io.pathx;
import com.attilax.office.readExcel;
import com.attilax.util.Func_4SingleObj;
import com.attilax.util.dirx;

/** @author attilax */
@SuppressWarnings("all") public class barcodeUtil {

	/** @author attilax 老哇的爪�?
	 * @since 2014-4-24 上午10:17:17$
	 * 
	 * @param args */
	public static void main(String[] args) {
		
		
		// 上午10:17:17 2014-4-24
		// gene_by_dir();
		// gene_by_file("c:\\num.txt", "c:\\barcode4bj");
	    gene_by_file("c:\\bar.txt", "c:\\barOad-shohai");
	//   gene_by_file("c:\\bar2.txt", "c:\\barO8f sheohai");
		String dir = "D:\\Users\\attilax\\Documents\\Tencent Files\\1466519819\\FileRecv\\11档微信价\\";
		String f = "";
		dir="c:\\111\\";
		f="成都1412档微信价单品.xlsx";
		f="上海1412档微信价商品明细.xls";
		// f="11档微信价（武汉）.xlsx";
		// f="北京201411档微信价.xlsx";
		// f="成都1411档微信价.xls";
		// f="上海1411档微信价商品.xls";
		//f = "重庆11档微信价商品明细.xlsx";
		// String filename =dir+f;
// gene_by_excelFile(dir + f, "c:\\barO7u\\" + f);
		System.out.println("fff");

	}

	/** @author attilax 老哇的爪子
	 * @since o7d Wn5$
	 * 
	 * @param string
	 * @param string2 */
	private static void gene_by_excelFile(String file, String saveDir) {
		// attilax 老哇的爪子 Wn5 o7d
		readExcel xlsMain = new readExcel();
		List<Map> list = null;
		try {
			list = xlsMain.readXls(file);
		} catch (InvalidFormatException e) {
			// attilax 老哇的爪子 Wto o7d
			e.printStackTrace();
		} catch (IOException e) {
			// attilax 老哇的爪子 Wto o7d
			e.printStackTrace();
		}
		List<String> list2 = listUtil.reduceO6(list, new ArrayList<String>(), new Ireduce<Map, List>() {

			@Override public List $(Map o, List lastRetOBj) {
				// attilax 老哇的爪子 Wq40 o7d
				if (o.get("促销券号") == null && o.get("微信码") == null) return lastRetOBj;
				else {
					lastRetOBj.add(getValByTit(o));
					return lastRetOBj;
				}

			}

			private Object getValByTit(Map o) {
				// attilax 老哇的爪子 Ws5 o7d
				if (o.get("促销券号") == null) return o.get("微信码");
				return o.get("促销券号");

			}
		});

		// listUtil.filterO7(list, new Iflt<Map>() {
		//
		// @Override public boolean call(Map o) {
		// // attilax 老哇的爪子 V59o o7d
		// if (o.get("促销券号") == null && o.get("微信码") == null) return true;
		// return false;
		//
		// }
		// });
		gene_by_list(saveDir, list2);

	}

	/** @author attilax 老哇的爪子
	 * @since 2014-5-27 下午02:39:29$ */
	private static void gene_by_dir() {
		// attilax 老哇的爪子 下午02:39:29 2014-5-27
		String s = "C:\\baro5";

		String pathname = dirx.getParentPath(s);

		List<String> files = dirx.getFiles(s);
		for (String f : files) {
			String fname_noext = filex.getFileName_noExtName(f);
			String new_dir = s + "_barcodeResult/" + fname_noext;
			gene_by_file(f, new_dir);
		}
	}

	/** @author attilax 老哇的爪子
	 * @since 2014-5-27 下午02:48:21$
	 * 
	 * @param s barcodes_filePath
	 * @param dir outputDir */
	private static void gene_by_file(String s, String dir) {
		// String s = "C:\baro5";
		// String dir="c:\\barcodeotputO5j";
		List<String> li = filex.read2list(s);
		List<String> li2=listUtil.map_generic(li, new Func_4SingleObj<String, String>(){

			@Override public String invoke(String o) {
				// attilax 老哇的爪子  i_r_7   o9r 
				return  o.replaceAll(";", "");
				
			}});
		gene_by_list(dir, li2);
	}

	public static void gene_by_list(String dir, List<String> li) {
		int n = 0;
		for (String line : li) {
			// if(n==0){
			// n++;
			// continue;
			// }
			// String[] a = line.split("\t");
			// if(a.length==0)continue;
			String filename = line.trim();
			if (filename.trim().length() == 0) continue;
			String barcode = filename;
			// gene("c:\\barcodeotputO5", barcode, filename);
			n++;

			String file = dir + "\\" + filename + ".jpg";
			SampleBitmapBarcodeWithBean.geneBarcode(barcode, file);
			 
		}
	}

	/** @author attilax 老哇的爪�?
	 * @since 2014-4-24 上午10:28:41$
	 * 
	 * @param dir
	 * @param barcode2
	 * @param filename */
	private static void gene(String dir, String barcode2, String filename) {
		// 上午10:28:41 2014-4-24

		String barCode = barcode2;

		String strHeight = null;// request.getParameter("height");
		String strWidth = null;// request.getParameter("width");

		// response.setCharacterEncoding("UTF-8");
		// request.setCharacterEncoding("UTF-8");

		// response.setContentType("image/png");
		String file = dir + "\\" + filename + ".jpg";
		File f = new File(file);
		// tt(barCode, strHeight, strWidth, f);

	}// end gene

	private static void tt(String barCode, String strHeight, String strWidth, File f) {
		// // OutputStream out
		// OutputStream output = null;
		// try {
		// output = new FileOutputStream(f);
		// } catch (FileNotFoundException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// } // OutputStream out = new FileOutputStream(f);
		//
		// try {
		// if (strHeight != null && !"".equals(strHeight) && strWidth != null
		// && !"".equals(strWidth)) {
		// BarCodeUtil.makeBarCoder(barCode, Integer.parseInt(strHeight),
		// Integer.parseInt(strWidth), output);
		// } else {
		// BarCodeUtil.makeBarCoder(barCode, output);
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// try {
		// output.flush();output.close();
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

}
