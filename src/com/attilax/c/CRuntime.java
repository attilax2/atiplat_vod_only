package com.attilax.c;

import com.sun.jna.Native;
import com.sun.jna.Platform;

public class CRuntime {
	public static   CLibraryInterface INSTANCE = (CLibraryInterface) 
            Native.loadLibrary((Platform.isWindows() ? "msvcrt" : "c"), 
            		CLibraryInterface.class); 
}
