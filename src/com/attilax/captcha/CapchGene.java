/**
 * 
 */
package com.attilax.captcha;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;

import com.attilax.io.filex;
 

/**
 * @author attilax
 *2016年11月8日 下午4:31:52
 */
public class CapchGene {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		String f = "c:\\00capch\\"+filex.getUUidName()+".jpg";filex.createAllPath(f);
		new CapchGene().geneCapch(new FileOutputStream(new File(f)));
		System.out.println("--f");
	}

 
//    public void init() throws ServletException     
//    {     
//       // super.init();     
//    }     
    Color getRandColor(int fc,int bc)     
    {     
        Random random = new Random();     
        if(fc>255) fc=255;     
        if(bc>255) bc=255;     
        int r=fc+random.nextInt(bc-fc);     
        int g=fc+random.nextInt(bc-fc);     
        int b=fc+random.nextInt(bc-fc);     
        return new Color(r,g,b);     
    }     
    
    public void geneCapch(OutputStream os) throws IOException
    {     
     	 //设置字母的大小,大小     
    	 int width=600, height=200;     
      int fontSiz = 50;
	Font mFont = new Font("Times New Roman", Font.BOLD, fontSiz);  
//        response.setHeader("Pragma","No-cache");     
//        response.setHeader("Cache-Control","no-cache");     
//        response.setDateHeader("Expires", 0);     
        //表明生成的响应是图片     
   //     response.setContentType("image/jpeg");     
             // 100*18
       
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);     
             
        Graphics g = image.getGraphics();     
        
       
        
        
        //set boder
        g.setColor(new Color(102,102,102));     
        g.drawRect(0, 0, width-1, height-1);    
        
        
        //set bg  
        g.setColor(getRandColor(200,250));     
     //   g.fillRect(1, 1, width-1, height-1);   
        g.fillRect(0,0, width, height);   
        
        g.setFont(mFont);        
        g.setColor(getRandColor(160,200));     
    
        
        Random random = new Random();   
        //画随机线     
//        for (int i=0;i<155;i++)     
//        {     
//            int x = random.nextInt(width - 1);     
//            int y = random.nextInt(height - 1);     
//            int xl = random.nextInt(6) + 1;     
//            int yl = random.nextInt(12) + 1;     
//            g.drawLine(x,y,x + xl,y + yl);     
//        }     
//    
//        //从另一方向画随机线     
//        for (int i = 0;i < 70;i++)     
//        {     
//            int x = random.nextInt(width - 1);     
//            int y = random.nextInt(height - 1);     
//            int xl = random.nextInt(12) + 1;     
//            int yl = random.nextInt(6) + 1;     
//            g.drawLine(x,y,x - xl,y - yl);     
//        }     
    
        
   
        //生成随机数,并将随机数字转换为字母     
        String sRand="";     
        for (int i=0;i<6;i++)     
        {     
            int itmp = random.nextInt(26) + 65;     
            char ctmp = (char)itmp;     
            sRand += String.valueOf(ctmp);     
            g.setColor(new Color(20+random.nextInt(110),20+random.nextInt(110),20+random.nextInt(110)));     
            g.drawString(String.valueOf(ctmp),fontSiz*i+10,fontSiz);     
        }     
    
//        HttpSession session = request.getSession(true);     
//        session.setAttribute("rand",sRand);     
//        g.dispose();     
        ImageIO.write(image, "JPEG", os);     
    }
}
