/**
 * 
 */
package com.attilax.cert;

import java.io.File;
import java.util.Date;

import com.attilax.device.HardWareUtils;
import com.attilax.io.FileExistEx;
import com.attilax.io.filex;
import com.attilax.laisens.SnEx;
import com.attilax.secury.AesV2q421;
import com.attilax.util.DateUtil;

/**
 * @author attilax
 *2016年12月5日 下午11:30:20
 */
public class CertSrv4mv {
	
	public static void main(String[] args) throws FileExistEx {
		//new SnLissSrv().check();com  oWE9kBxPXL8OZDDsjc0P4Tlb3KDAX4IOhbatJpETUE6Oy3YPT4Oq9bNrb9hfx1Xd
	//	String cert=new SnLissSrv().geneCert();
		// new CertSrv4mv().check();
		 new CertSrv4mv().geneCert("c:\\sn3.txt");
		 System.out.println("--f");
	}
	
	
	/**
	attilax    2016年12月5日  下午11:33:54
	 * @return
	 */
	public String geneCert() {
		 String thisPc= new HardWareUtils().getSn();
		 String enc=AesV2q421.encrypt(thisPc);
		 System.out.println(enc);
		return enc;
	}
	
	public String geneCert(String f) throws FileExistEx {
		if(new File(f).exists())
			throw new FileExistEx(f);
		 String thisPc= new HardWareUtils().getSn();
		 String enc=AesV2q421.encrypt(thisPc);
		 System.out.println(enc);
		 filex.save(enc, f);
		return enc;
	}


	public   void check( ) throws CertEx {
	    @SuppressWarnings("deprecation")
		 String thisPc= new HardWareUtils().getSn();
	    
	    String certSn=filex.read_NSF("c:\\sn.txt");
	    certSn=certSn.trim();
	    String dec=AesV2q421.decrypt(certSn);
	    if(!thisPc.equals(dec) ) 
	    	throw new CertEx("certSn:"+dec+",nowSn:"+thisPc);
	}

}
