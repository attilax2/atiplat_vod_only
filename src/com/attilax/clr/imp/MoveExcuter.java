package com.attilax.clr.imp;

import com.attilax.clr.Executer;
import com.attilax.io.filex;
import com.attilax.io.pathx;

public class MoveExcuter implements Executer {
	public String dir;
	public String MovTargetDir;
	@Override
	public void exe(Object o) {
		String p=(String) o;
		String ori_name = p;
		p = noDirFix(p);
		p = MovTargetDir + "" + p;
		p=pathx.fixSlash(p);
		filex.createAllPath(p);
		filex.move(ori_name, p);
		System.out.println(" ok \"" + p + "\"");
		
	}
	
	/**
	 * get rlt path
	 * @param p
	 * @return
	 */
	private String noDirFix(String p) {
		p=pathx.fixSlash(p);
		dir=pathx.fixSlash(dir);
		p=p.replace(dir, "");
		return p;
	}

}
