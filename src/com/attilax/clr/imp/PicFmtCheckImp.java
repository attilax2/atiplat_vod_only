package com.attilax.clr.imp;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import javax.imageio.ImageIO;

import com.attilax.clr.IClrerPart;
import com.attilax.collection.CollX;
import com.attilax.ex.FileNotExistEx;
import com.attilax.ex.FmtEx;
import com.attilax.io.filex;
import com.google.common.collect.Sets;

public class PicFmtCheckImp extends MoveExcuter implements IClrerPart {

	@Override
	public boolean isGabFile(Object object) throws FileNotExistEx, FmtEx {
		String exts="jpg,jpeg,png,bmp";
		String[] ea=exts.split(",");
		Set<String> st=CollX.arr2set(ea);
		File f = new File((String) object);
		//try {
			String ex=filex.getExtName(object.toString());
			if(!st.contains(ex))
				 throw new FmtEx("fmt ex");
			//BufferedImage img = ImageIO.read(f);
//		} catch (Exception e) {
//			 throw new FmtEx(e.getMessage(),e);
//			//e.printStackTrace();
//		}
		return false;
	}
	
	@Override
	//not move 
	public void exe(Object o) {
	}

}
