/**
 * 
 */
package com.attilax.cn;

import java.util.Set;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.google.common.collect.Sets;

/**
 * @author attilax 2016年11月2日 下午10:47:11
 */
public class CncharX {

	/**
	 * attilax 2016年11月2日 下午10:47:11
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String s = "2016年11月2日  下午10:47:11";
		System.out.println(CncharX.cncharCount(s));
		System.out.println("--f");
	}

	public static Set set;

	/**
	 * attilax 2016年11月2日 下午10:47:50
	 * 
	 * @param s
	 * @return
	 */
	public static int cncharCount(String s) {
		iniCntxt();
		int n = 0;
		char[] ca = s.toCharArray();
		for (char c : ca) {
			if (set.contains(c))
				n++;
		}
		return n;
	}

	public static void iniCntxt() {
		if (set == null) {
			set=Sets.newHashSet();
			String f = pathx.classPath(CncharX.class) + "/2500.txt";
			String t = filex.read(f, "gbk");

			char[] ca = t.toCharArray();
			for (char c : ca) {
				set.add(c);
			}
		}
	}

}
