package com.attilax.csharp.System.Data.Linq;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataContext {
	public static Connection conn;
	public DataContext() {
		// 连接SQLite的JDBC

		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);

		}

		// 建立一个数据库名zieckey.db的连接，如果不存在就在当前目录下创建之

		try {
			  conn = DriverManager
					.getConnection("jdbc:sqlite:e:/moviebar.db");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

}
