package com.attilax.csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.attilax.exception.ExUtil;
import com.google.common.collect.Maps;

public class CSVUtils {
	public static String[] toCols(String row) {
//		String[] a2=row.split(":");
//		String dir=a2[0];
//		String files=a2[1];
		String[] files_arr=row.split(",");
		return files_arr;
	}
    /**
     * 导出
     * 
     * @param file csv文件(路径+文件名)，csv文件不存在会自动创建
     * @param dataList 数据
     * @return
     */
    public static boolean exportCsv(File file, List<String> dataList){
        boolean isSucess=false;
        
        FileOutputStream out=null;
        OutputStreamWriter osw=null;
        BufferedWriter bw=null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "GB2312"), 1024);
            if(dataList!=null && !dataList.isEmpty()){
                for(String data : dataList){
                    bw.append(data).append("\r");
                }
            }
            isSucess=true;
        } catch (Exception e) {
            isSucess=false;
        }finally{
            if(bw!=null){
                try {
                    bw.close();
                    bw=null;
                } catch (IOException e) {
                    e.printStackTrace();
                } 
            }
            if(osw!=null){
                try {
                    osw.close();
                    osw=null;
                } catch (IOException e) {
                    e.printStackTrace();
                } 
            }
            if(out!=null){
                try {
                    out.close();
                    out=null;
                } catch (IOException e) {
                    e.printStackTrace();
                } 
            }
        }
        
        return isSucess;
    }
    
    /**
     * 导入
     * 
     * @param file csv文件(路径+文件)
     * @return
     */
    public static List<String> importCsv(File file){
        List<String> dataList=new ArrayList<String>();
        
        BufferedReader br=null;
        try { 
            br = new BufferedReader(new FileReader(file));
            String line = ""; 
            while ((line = br.readLine()) != null) { 
                dataList.add(line);
            }
        }catch (Exception e) {
        }finally{
            if(br!=null){
                try {
                    br.close();
                    br=null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
 
        return dataList;
    }
    
    
    public static List<Map> read(String file,String cloNames){
    	
    	
        List<Map> dataList=new ArrayList<Map>();
        String[] cloNames_a=cloNames.split(",");
        
        BufferedReader br=null;
        try { 
            br = new BufferedReader(new FileReader(new File(file)));
            String line = ""; 
            while ((line = br.readLine()) != null) {
            	String[] a=line.split(",");
            	Map m=Maps.newHashMap();
            	for(int i=0;i<a.length;i++)
            	{
            		if(i>=cloNames_a.length)
            			continue;
            		String cloName=cloNames_a[i];
            		m.put(cloName, a[i]);
            	}
            
                dataList.add(m);
            }
        }catch (Exception e) {
        	ExUtil.throwExV2(e);
        }finally{
            if(br!=null){
                try {
                    br.close();
                    br=null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
 
        return dataList;
    }
}
