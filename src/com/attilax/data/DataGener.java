/**
 * 
 */
package com.attilax.data;

import java.util.Map;

import com.attilax.cfg.PropByColon;
import com.attilax.io.pathx;
import com.attilax.lang.MapX;
import com.attilax.lang.text.strUtil;
import com.attilax.time.timeUtil;
import com.attilax.util.randomx;
import com.google.common.collect.Maps;

/**
 * @author attilax
 *2016年4月29日 下午4:44:44
 */
public class DataGener {
	Map var_gener_map=Maps.newLinkedHashMap();
	private Map cfgMap;
	/**
	 * @param string
	 */
	public DataGener(String cfgFile) {
		Map m=new PropByColon(cfgFile).m;
		this.cfgMap=m;
	}

	/**
	attilax    2016年4月29日  下午4:44:44
	 * @param args
	 */
	public static void main(String[] args) {

String key="时间";key="姓名";
DataGener dataGener = new DataGener(pathx.classPath()+"/com/attilax/data/test_data_gene_rugur.txt");
String v=dataGener.gene(  key);
System.out.println(v);

	}

	/**
	 * key==学员姓名
	attilax    2016年4月29日  下午4:46:48
	 * @param v
	 * @return
	 */
	public String gene(String key) {
		 String v = MapX.getKeyBeContained(cfgMap, key).toString().trim();
	//	if( v.length()>0)
		 if(v.equals("$time"))
		return  timeUtil.getNowTime_NotIncluDate();
		 if(v.equals("$datetime"))
				return timeUtil.Now_CST();
		 
		 if(v.equals("$rdm"))
				return  String.valueOf(randomx.random(1, 100));
		 
		 if(v.equals("$pct"))
				return String.valueOf(randomx.random(1, 100));
		 if(v.equals("$tel"))
				return "13588888888";
		 if(v.equals("$tf"))
				return "是";
		 if(v.equals("$date"))
				return  timeUtil.date();
		 if(v.equals("$cardno"))
				return "6128 0000 6989 2531";
		 if(v.equals("$bank"))
				return "工行新乡分行逸致支行";
		 if(v.equals("$name"))
		 {
			String s="赵毅，阿提拉克斯-阿克巴 ,池安和,郝怡然，李一";
			s=strUtil.toEnChar(s);
			String[] a=s.split(",");
			int i=randomx.random(4);
			return a[i];
		 }
		 if(v.equals("$money"))
		 {
			 int i=randomx.random(10000);
			 return String.valueOf(i)+".00"; 
		 }
		 if(v.equalsIgnoreCase("age"))
		 {
			 int i=randomx.random(18,100);
			 return String.valueOf(i);
		 }
		 if(v.equals("$sex"))
		 { int i=randomx.random(1);
		 return "男,女".split(",")[i];
			 
		 }
			 if(v.equals("$area"))
			 {
				 return "xx自治区xx街区156号";
			 }
				 if(v.equals("$stat"))
				return "已完成";
		return v;
	}

}
