package com.attilax.dataService;

import java.util.Map;

import com.attilax.html.HtmlX;
import com.attilax.lang.Trigger;
import com.attilax.lang.text.RowParser;


/**
 * q319
 * com.attilax.dataService.meta_data_pars_from_txt_trigger
 * @author Administrator
 *
 */
public class meta_data_pars_from_txt_trigger extends Trigger {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	@SuppressWarnings("unchecked")
	public Object exec(Object object) {
		@SuppressWarnings("rawtypes")
		Map row=(Map) object;
		String txt= HtmlX.html2txtV2( row.get("copy_content").toString());
		Map part = new RowParser().parse(txt);
		part.put("salalei", part.get("薪资水平"));
		part.put("sala_mode", part.get("结算模式"));
		part.put("worktime", part.get("工作时间"));
		part.put("location", part.get("工作地点"));
		row.putAll(part);
		return null;
	//	 String txt=(String) object;
		 
		
	}

}
