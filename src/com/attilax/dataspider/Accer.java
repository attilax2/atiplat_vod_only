package com.attilax.dataspider;

import com.attilax.device.CpuUtil;

public class Accer extends BusPart {

	//public int freq;

	public int maxCpu;

	public Accer(Runnable upRun) {
		
		Runnable ra=()->{
			int cpu=new CpuUtil().getCpuRate_avg();
			if(cpu<maxCpu)
			{
				upRun.run();
			}
		};
	this.rx=upRun;
	}

}
