package com.attilax.dataSpider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.attilax.auto.web.WebDriverUtil;
import com.attilax.cmsPoster.BlogPubberMainform4bbs;
import com.attilax.html.htmlx;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.google.common.collect.Lists;

public class DoubanSpider extends DataSpider {
	String mainPageUrl = "https://book.douban.com/";
	public DoubanSpider() {
		System.setProperty("webdriver.firefox.bin", pathx.classPathParent()
				+ "\\Mozilla Firefox\\firefox.exe");
	}

	private String source;
	private String title;
	private String kewword;

	public static void main(String[] args) {
		String kw = "代码的未来";
		DoubanSpider be = new DoubanSpider();
		
		be.search(kw);
		be.clickFirst();
		
		be.processShowMainTxt();
		// System.out.println(extractText);
		// System.out.println(be.rzt);
		// filex.save(be.rzt, "c:\\h.txt");
		// System.out.println(htmlx.html2txt(be.rzt));
		System.out.println("---fff");

	}

	private void clickFirst() {
		String selector = "a[title=\"@t@\"]";
		selector=selector.replace("@t@", this.kewword);
		driver.findElement(By.cssSelector(selector)).click();
		source=driver.getPageSource();
	}

	private   void processShowMainTxt() {
	//	String read = source;
		System.out.println("##title:"+title);
		// String read = filex.read("c:\\h.txt");
		filex.save(source, "c:\\0Html\\"+filex.getUUidName()+".txt");
		
		
		String extractText = htmlx.html2txt_deDuliLine(source);
		
		String uuid=filex.getUUidName();
		filex.save(extractText, "c:\\0Html2txt\\"+uuid+".txt");
		
		List<String> li2=deDuliCapt(extractText);
		for (String capt : li2) {
			System.out.println(capt);
		}
		 
	}

	public List<String> deDuliCapt(String extractText) {
		List<String> li1=Lists.newArrayList();
		String[] a=extractText.split("\r\n");
		for (String line : a) {
			String trim = line.trim();
			if (trim.startsWith("第"))
			{
				//System.out.println(line.trim());
				if(!li1.contains(trim))
					li1.add(trim);
			}
			
		}
		 
		return li1;
	}

	private void search(String string) {
	this.kewword=string;
		// ByeduEncyc be = new ByeduEncyc();
		open_mainPage(mainPageUrl, null);
		inputKeywordNsubmit(string);
		String sou = driver.getPageSource();
		String title = driver.getTitle();
		this.source = sou;
		this.title=title;

		// System.out.println(driver.);

	}

	private void inputKeywordNsubmit(String string) {
		By ele = By.name("search_text");
		driver.findElement(ele).clear();
		driver.findElement(ele).sendKeys(string);
		String selector = "input[value=\"搜索\"][type=\"submit\"]";
		driver.findElement(By.cssSelector(selector)).click();

	}

	FirefoxDriver driver = null;
	public static Map<String, Object> propMap = new HashMap<String, Object>();

	public void open_mainPage(final String pubUrl, final Map blogMap) {
		System.out.println("");
		Runnable runnable = new Runnable() {

			@SuppressWarnings("static-access")
			@Override
			public void run() {
				WebDriverUtil wdu = WebDriverUtil.newx();

				driver = getDriverInstan(blogMap, wdu, driver);
				// try {
				// String baseUrl="http://write.blog.csdn.net/postedit";

				wdu.closeAlert(driver);
				// PropX px=(PropX) propMap.get(blogMap);
				// String pubUrl = (String) blogMap.get("pub_url");
				// String blog_index=px.getProperty("blog_index");

				driver.get(pubUrl);

				String sou = driver.getPageSource();
				String title = driver.getTitle();

				sou = driver.getPageSource();
				// JavascriptExecutor jse = (JavascriptExecutor) driver;
				// Object obj=
				// driver.executeScript("	return document.title; ",
				// driver.findElement(By.id("editor")));
				// title = (String) obj;
				// driver.getTitle();jeig yaosi manu op ..laoxsh outtime
				// value ..

				// publish(keyword, driver,blogMap);
				// } catch (Exception e) {
				// e.printStackTrace();
				// }

			}

		};
		runnable.run();
		// should ays first by blogger,,thern artis
		// core.execMeth_Ays(runnable, "threadName"+blogMap);

	}

	private FirefoxDriver getDriverInstan(final Map blogMap, WebDriverUtil wdu,
			FirefoxDriver driver) {
		try {
			// driver=(FirefoxDriver)
			// propMap.get(blogMap.get("pubber").toString()+"_driver");
			if (driver != null)
				if (wdu.isDisconn(driver))
					driver = null;
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (driver == null) {
			driver = new FirefoxDriver();
			// propMap.put(blogMap+"_driver",driver);
		}
		return driver;
	}

}
