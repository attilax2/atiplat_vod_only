package com.attilax.dataspider;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

public class TaskPool extends BusPart implements ExecutorService  {

	
	List<Runnable>  wait_que=  Collections.synchronizedList(  Lists.newArrayList());
	ExecutorService es;
	int MaxRun;
	AtomicInteger nowRunCount=new AtomicInteger(0);
	//public int MaxRunLimit;
	public int miniLimit;
	
	public TaskPool(int mini , int maxRunCount) {
		es=Executors.newFixedThreadPool(400,new MyThreadFactory("task_pool"));
		//  ExecutorService es = Executors.newFixedThreadPool(5, new MyThreadFactory("threadName"));  
		MaxRun=maxRunCount;
		miniLimit=mini;
	}
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public static final Logger logger = LoggerFactory
			.getLogger(TaskPool.class);
	public void run() {
		
		Runnable ra=()->{
			if (wait_que.size() == 0)
				return;

			if(nowRunCount.get()<MaxRun)
			{
			//	if(this.)
				 
					Runnable task= wait_que.get(0);		
					wait_que.remove(task);
					es.submit(task);
					nowRunCount.incrementAndGet();
					ThreadPoolExecutor es2=(ThreadPoolExecutor) es;
				 
					logger.info("--nowRunCount.incrementAndGet() and sumbit,es_id:"+es.toString());
				 
			}else  // ex limit max run
			{
				//nothing
			}
		};
		super.rx=ra;
		super.run();
//		if (runCount >= freq) { //run
//			
//			//re settimer
//			runCount=1;
//		}else
			
		
		return;
	}
	@Override
	public void execute(Runnable command) {
		wait_que.add(command);
		
	//	if(que.size())
		
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Runnable> shutdownNow() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isShutdown() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isTerminated() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean awaitTermination(long timeout, TimeUnit unit)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <T> Future<T> submit(Callable<T> task) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> Future<T> submit(Runnable task, T result) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Future<?> submit(Runnable task) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<Future<T>> invokeAll(
			Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T invokeAny(Collection<? extends Callable<T>> tasks)
			throws InterruptedException, ExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T invokeAny(Collection<? extends Callable<T>> tasks,
			long timeout, TimeUnit unit) throws InterruptedException,
			ExecutionException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}



	public void shouldUpPerf() {
		if(nowRunCount.get()>=MaxRun-3)
			MaxRun++;
		else  if (nowRunCount.get()<MaxRun)
		{
			
		}
		//else nothing.
		
	}

}
