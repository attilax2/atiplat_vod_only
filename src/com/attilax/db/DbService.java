/**
 * 
 */
package com.attilax.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import aaaCfg.ConnectionImp;

 
import com.attilax.sql.SqlService;
import com.attilax.time.timeUtil;

/**
 * only sql exe service ... and DBMeataServcie  another
 * @author attilax
 *2016年4月14日 下午12:40:27
 */
public class DbService extends SqlService {

	
	public Connection getConnection() {
		if(this.conn instanceof ConnectionImp)
			this.conn=null;
		
		if(this.conn!=null)
			return this.conn;
		// com.microsoft.sqlserver.jdbc.SQLServerDriver
	
		//System.out.println(PropX.getConfig(path, "jdbc.url"));
		
		
	
		try {

			Class.forName(dbcfg.getDriver());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Class.forName driver err,drive class name is :  "+dbcfg.getDriver(),e);
		}
	//	Connection conn;
//
//		conn = DriverManager.getConnection(dbcfg.getUrl(), dbcfg.getUser(), dbcfg.getPassword());
//		
//		try {
//
//			Class.forName("com.mysql.jdbc.Driver");
//		} catch (ClassNotFoundException e) {
//			throw new getConnEx("getconnex" + e.getMessage());
//		}
		Connection conn;
		try {
			conn = DriverManager.getConnection(
					dbcfg.getUrl(),
					dbcfg.getUser(),
					dbcfg.getPassword());
			System.out.println("get conn: "+ conn+" at:"+timeUtil.Now_CST());
			
		} catch (SQLException e) {
		 	throw new getConnEx("getconnex" + e.getMessage());
			
		}
		return conn;
	}
	
	public List executeQuery(String sql)
	{
		return super. executeQuery(sql);
		
	}
	
	public Map executeQuery_fetch_row(String sql)
	{
		return (Map) executeQuery(sql).get(0);
	}
	
	
	 
//	public Map uniqueResult(String sql)
//	{
//		return null;
//	}
	
	
	
	
}
