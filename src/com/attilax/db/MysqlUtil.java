package com.attilax.db;

import java.sql.Connection;

import com.attilax.ioc.IocUtilV2;

public class MysqlUtil {

	
	public  static Object	mysql_query(String sql,Object conn_obj)
	{
		 Connection conn=(Connection) conn_obj;
		if(sql.trim().startsWith("update"))
		{
			DbService dbs=IocUtilV2.getBean(DbService.class);
			return	dbs.exe(sql);
		}
		return null;
		
	}
}
