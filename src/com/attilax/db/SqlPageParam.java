/**
 * 
 */
package com.attilax.db;

/**
 * @author ASIMO
 *
 */
public class SqlPageParam {
	public int startIndex;
	public int pagesize;
	public int getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}
	public int getPagesize() {
		return pagesize;
	}
	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

}
