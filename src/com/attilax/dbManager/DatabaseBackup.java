package com.attilax.dbManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import com.attilax.cmd.CmdX;
import com.attilax.exception.ExUtil;
import com.attilax.io.filex;

/**
 * MySQL数据库的备份与恢复 缺陷：可能会被杀毒软件拦截
 * 
 * @author attilax q214
 * @version xxx
 */
public class DatabaseBackup {
	/** MySQL安装目录的Bin目录的绝对路径 */
	private String mysqlBinPath;
	/** 访问MySQL数据库的用户名 */
	private String username;
	/** 访问MySQL数据库的密码 */
	private String password;
	private String sqlfilePath;

	public DatabaseBackup(String mysqlBinPath, String username, String password) {
//		if (!mysqlBinPath.endsWith(File.separator)) {
//			mysqlBinPath = mysqlBinPath + File.separator;
//		}
		this.mysqlBinPath = mysqlBinPath;
		this.username = username;
		this.password = password;
	}

	/**
	 * 备份数据库
	 * 
	 * @param output
	 *            输出流
	 * @param dbname
	 *            要备份的数据库名
	 */
	public void backup(OutputStream output, String dbname) {
		String command = "  " + mysqlBinPath + " -u" + username + " -p"
				+ password + " --set-charset=utf8 " + dbname
				+ " --result-file=" + this.sqlfilePath + "\\"+dbname+"_back_"
				+ filex.getUUidName() + ".sql";
		System.out.println("--cmd :" + command);
		
	//	command="cmd /c dir";

		try {

			Process process = Runtime.getRuntime().exec(command);
			CmdX.redirectEcho2Screen(process);
		
			

		} catch (Exception e) {
			e.printStackTrace();
			ExUtil.throwEx(e);
		} 
	}


	/**
	 * 备份数据库，如果指定路径的文件不存在会自动生成
	 * 
	 * @param dest
	 *            备份文件的路径
	 * @param dbname
	 *            要备份的数据库
	 */
	public void backup(String dest, String dbname) {
		this.sqlfilePath = dest;

		OutputStream out = null;
		// new FileOutputStream(dest);
		backup(out, dbname);

	}

	/**
	 * 恢复数据库
	 * 
	 * @param input
	 *            输入流
	 * @param dbname
	 *            数据库名
	 */
	public void restore(InputStream input, String dbname) {
		String command = "cmd /c " + mysqlBinPath + "mysql -u" + username
				+ " -p" + password + " " + dbname;
		try {
			Process process = Runtime.getRuntime().exec(command);
			OutputStream out = process.getOutputStream();
			String line = null;
			String outStr = null;
			StringBuffer sb = new StringBuffer("");
			BufferedReader br = new BufferedReader(new InputStreamReader(input,
					"utf8"));
			while ((line = br.readLine()) != null) {
				sb.append(line + "/r/n");
			}
			outStr = sb.toString();
			OutputStreamWriter writer = new OutputStreamWriter(out, "utf8");
			writer.write(outStr);
			writer.flush();
			out.close();
			br.close();
			writer.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 恢复数据库
	 * 
	 * @param dest
	 *            备份文件的路径
	 * @param dbname
	 *            数据库名
	 */
	public void restore(String dest, String dbname) {
		try {
			InputStream input = new FileInputStream(dest);
			restore(input, dbname);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * C:\wamp\mysql\bin\mysqldump.exe root root wxb_site_new
	 * c:\wxb_site_new.sql
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Configuration config = HibernateSessionFactory.getConfiguration();
		String binPath = args[0];
		String userName = args[1];
		String pwd = args[2];
		DatabaseBackup bak = new DatabaseBackup(binPath, userName, pwd);
		bak.backup(args[4], args[3]);
		// bak.restore("c:/ttt.sql", "ttt");
		System.out.println("--f");
	}
}
