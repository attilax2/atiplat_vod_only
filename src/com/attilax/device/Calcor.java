package com.attilax.device;

import java.util.concurrent.atomic.AtomicInteger;

public class Calcor {

	private int resetNum;

	public Calcor(int resetNum) {
		this.resetNum=resetNum;
	}

	public void setInfo(int cpuuse) {
		if(cpuUseCount.get()>=this.resetNum)
			reset();
		cpuUseCount.incrementAndGet();
		cpuUseSums.addAndGet(cpuuse);
		cpuuse_avg.set(cpuUseSums.get() / cpuUseCount.get());

	}

	private void reset() {
		cpuUseCount.set(0);cpuUseSums.set(0);cpuuse_avg.set(0);
		
	}

	AtomicInteger cpuUseCount = new AtomicInteger(0);
	AtomicInteger cpuUseSums = new AtomicInteger(0);
	AtomicInteger cpuuse_avg = new AtomicInteger(100);

	public int getInt() {

		return cpuuse_avg.get();
	}

}
