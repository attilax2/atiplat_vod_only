package com.attilax.device;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;

import com.attilax.exception.ExUtil;
public class KeyboardUtil {

	public KeyboardUtil(Robot robot2) {
		this.robot=robot2;
	}

	public KeyboardUtil() {
		try {
			this.robot=new Robot();
		} catch (AWTException e) {
			ExUtil.throwExV2(e);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	// shift+ 鎸夐敭

    public static void keyPressWithShift(Robot r, int key) {

            r.keyPress(KeyEvent.VK_SHIFT);

            r.keyPress(key);

            r.keyRelease(key);

            r.keyRelease(KeyEvent.VK_SHIFT);

            r.delay(100);

    }



    // ctrl+ 鎸夐敭

    public static void keyPressWithCtrl(Robot r, int key) {

            r.keyPress(KeyEvent.VK_CONTROL);

            r.keyPress(key);

            r.keyRelease(key);

            r.keyRelease(KeyEvent.VK_CONTROL);

            r.delay(100);

    }



    // alt+ 鎸夐敭

    public static void keyPressWithAlt(Robot r, int key) {

            r.keyPress(KeyEvent.VK_ALT);

            r.keyPress(key);

            r.keyRelease(key);

            r.keyRelease(KeyEvent.VK_ALT);

            r.delay(100);

    }

    //鎵撳嵃鍑哄瓧绗︿覆

    public static void keyPressString(Robot r, String str){

            Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();//鑾峰彇鍓垏鏉�

            Transferable tText = new StringSelection(str);

            clip.setContents(tText, null); //璁剧疆鍓垏鏉垮唴瀹�

            keyPressWithCtrl(r, KeyEvent.VK_V);//绮樿创

            r.delay(100);

    }

     

    //鍗曚釜 鎸夐敭

    public static void keyPress(Robot r,int key){

            r.keyPress(key);

            r.keyRelease(key);

            r.delay(100);

    }

	private Robot robot;

	public KeyboardUtil keyPressString(String string) {
		keyPressString(this.robot, string);
		return this;
	}

	public   void keyPress(int vkEnter) {
		keyPress(this.robot,vkEnter);
		
	}

	public void keyPress_enter() {
		keyPress(robot, KeyEvent.VK_ENTER);
		
	}

	public void keyPressWithCtrl(int vkTab) {
		keyPressWithCtrl(robot,vkTab);
		
	}


}
