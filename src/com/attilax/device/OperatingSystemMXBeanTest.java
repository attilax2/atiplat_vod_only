package com.attilax.device;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;

public class OperatingSystemMXBeanTest {
/**
 * amd64
4
Windows 7
-1.0
6.1

 * @param args
 */
	public static void main(String []args) {
		OperatingSystemMXBean mxBean = ManagementFactory.getOperatingSystemMXBean();
		//mxBean.get
		System.out.println(mxBean.getArch());
		System.out.println(mxBean.getAvailableProcessors());
		System.out.println(mxBean.getName());
		System.out.println(mxBean.getSystemLoadAverage());
		System.out.println(mxBean.getVersion());
	}
}