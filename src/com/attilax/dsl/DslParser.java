package com.attilax.dsl;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import com.attilax.ast.AstBuilder;
import com.attilax.ast.AstBuilderSmpVer;
import com.attilax.ast.AstParser;
import com.attilax.ast.Expression;
 
import com.attilax.fsm.JavaTokener;
import com.attilax.fsm.Token;
import com.attilax.io.StreamUtil;
import com.attilax.json.AtiJson;

/**
 * com.attilax.dsl.DslParser
 * @author attilax
 *2016年11月2日 下午9:44:13
 */
public class DslParser {

	public static void main(String[] args) {
	 // t2();
		String dsl=args[0];
		Object r = new DslParser().parseV3(dsl);
		System.out.println(r);

	}

	private static void t22() {
		//   String s="new DslParser().m2(123)";
			System.out.println("");
			
		     System.out.println("aa");
		 //  String r=  (String) new DslParser().parse(s);
		 //  System.out.println(r);
			
			//new String[] {"aa","bb" }; 
			String a="  com.attilax.encry.RSACoder.main(['fixseed','c:/0k/pri.txt','c:/0k/pub.txt']) ";
			
			//	args=" aaaPKg.DslParser.m3(123).m4()";
				List<Token> tokens = new JavaTokener(a).getTokensV3();// er(args).getTokens();
				System.out.println(AtiJson.toJson(tokens));
			 	Expression buildAst = new AstBuilder().buildAstV2(tokens);
				 Object rzt = new AstParser().parse(buildAst);
				 if(rzt==null)
				 {
					 //  StringWriter stringWriter= new StringWriter();
				    //    PrintWriter writer= new PrintWriter(stringWriter);
//				 ByteArrayOutputStream bout=new ByteArrayOutputStream();
//				 PrintStream ps = new PrintStream(bout);
//			        System.out.println("--wanning..for debug out stacktrace");
//			        System.setOut(ps);
//			        byte[] buf=bout.toByteArray();
//			        StringBuffer buffer= stringWriter.getBuffer();
//			        return buffer.toString();
//				 String name="getMain_rzt";
					 
				 }
					 System.out.println();
				 System.out.println(rzt);
				
		//   t2(); t3();t4();t5();
		//	DslParser.m3("").m4();
	}
	
	public void m4() {
		System.out.println(" m4_m4");
		
	}

	private static void t2() {
		String args="  new aaaPKg .DslParser().m2(123) ";
		
	//	args=" aaaPKg.DslParser.m3(123).m4()";
		List<Token> tokens = new JavaTokener(args).getTokens();// er(args).getTokens();
		System.out.println(AtiJson.toJson(tokens));
		Expression buildAst = new AstBuilder().buildAst(tokens);
		Object rzt = new AstParser().parse(buildAst);
	//	System.out.println(AtiJson.toJson(tokens));		
		
		System.out.println("test dysn ok"+ rzt);
		
	}
	
	private static void t3() {
		String args=" aaaPKg.  DslParser.m3(123)  ";
		
	//	args=" aaaPKg.DslParser.m3(123).m4()";
		List<Token> tokens = new JavaTokener(args).getTokens();// er(args).getTokens();
		Expression buildAst = new AstBuilder().buildAst(tokens);
		Object rzt = new AstParser().parse(buildAst);
	//	System.out.println(AtiJson.toJson(tokens));		
		System.out.println("test static ok"+ rzt);
		
	}
	
	private static void t4() {
		String args=" new aaaPKg.DslParser().m3(11).m4().";
		
	//	args=" aaaPKg.DslParser.m3(123).m4()";
		List<Token> tokens = new JavaTokener(args).getTokens();// er(args).getTokens();
		Expression buildAst = new AstBuilder().buildAst(tokens);
		Object rzt = new AstParser().parse(buildAst);
	//	System.out.println(AtiJson.toJson(tokens));		
		System.out.println("test dysn meth chain ok"+ rzt);
		
	}
	
	private static void t5() {
		String args="  aaaPKg.DslParser.m3(11).m4().";
		
	//	args=" aaaPKg.DslParser.m3(123).m4()";
		List<Token> tokens = new JavaTokener(args).getTokens();// er(args).getTokens();
		Expression buildAst = new AstBuilder().buildAst(tokens);
		Object rzt = new AstParser().parse(buildAst);
	//	System.out.println(AtiJson.toJson(tokens));		
		System.out.println("test static meth chain ok"+ rzt);
		
	}

	public boolean redrectStdOut=false;
	@Deprecated
	public  Object parse(String args) {
		// TODO Auto-generated method stub   com.attilax.encry.RSACoder.main(['fixseed','c:/0k/pri.txt','c:/0k/pub.txt'])
		System.out.println("..start");
		//@SuppressWarnings("unchecked")
		//  com.attilax.ioc.IocFacV3_iocx_iocutil.getBean(com.attilax.order.RechargeOrderService.class).insert('20161024_16926254',5,1528)
		  StreamUtil sx = new StreamUtil();
		if(redrectStdOut)
		{
	   
				sx.RedirectToStrOut();
		}
		      //  System.out.println("--wanning..for debug out stacktrace");
		      
		       
		List<Token> tokens = new JavaTokener(args).getTokensV3();
		Expression buildAst = new AstBuilder().buildAstV2(tokens);
		Object rzt = new AstParser().parse(buildAst);
		if(rzt==null)
		{
			if(redrectStdOut)
			{
				 String rztFrmStdout=sx.getStr();
			     //restore std out
			     sx.restoreStdOut();
			     return rztFrmStdout;
			}
		}
		System.out.println(rzt);
		System.out.println("...finish");
		return rzt;
		
	}
	
	
	/**
	 * with type sys
	attilax    2016年10月24日  下午5:51:40
	 * @param args
	 * @return
	 */
	public  Object parse_typeSys(String args) {
		// TODO Auto-generated method stub   com.attilax.encry.RSACoder.main(['fixseed','c:/0k/pri.txt','c:/0k/pub.txt'])
		System.out.println("..start");
		//@SuppressWarnings("unchecked")
		//  com.attilax.ioc.IocFacV3_iocx_iocutil.getBean(com.attilax.order.RechargeOrderService.class).insert('20161024_16926254',5,1528)
	    //com.attilax.ioc.IocFacV3_iocx_iocutil.getBean(com.attilax.order.RechargeOrderService.class).insert('20161024_172851950':s,5:i,888:i)
		       
		List<Token> tokens = new JavaTokener(args).getTokensV4();
		Expression buildAst = new AstBuilder().buildAstV2(tokens);
		Object rzt = new AstParser().parse(buildAst);
	 		System.out.println(rzt);
		System.out.println("...finish");
		return rzt;
		
	}
	
	public  Object parseV3(String args) {
		// TODO Auto-generated method stub   com.attilax.encry.RSACoder.main(['fixseed','c:/0k/pri.txt','c:/0k/pub.txt'])
		System.out.println("..start");
		//@SuppressWarnings("unchecked")
		//  com.attilax.ioc.IocFacV3_iocx_iocutil.getBean(com.attilax.order.RechargeOrderService.class).insert('20161024_16926254',5,1528)
	    //com.attilax.ioc.IocFacV3_iocx_iocutil.getBean(com.attilax.order.RechargeOrderService.class).insert('20161024_172851950':s,5:i,888:i)
		       
		List<Token> tokens = new JavaTokener(args).getTokensV4();
		Expression buildAst = new AstBuilderSmpVer().buildAstV2(tokens);
		Object rzt = new AstParser().parse(buildAst);
	 		System.out.println(rzt);
		System.out.println("...finish");
		return rzt;
		
	}
	
	public String m2(String str)
	{
		return "ret"+str;
	}
	public static DslParser m3(String str)
	{
		return  new DslParser();
	}

}
