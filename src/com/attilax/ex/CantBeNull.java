package com.attilax.ex;

public class CantBeNull extends RuntimeException {

	public CantBeNull(String string) {
		super(string);
	}

}
