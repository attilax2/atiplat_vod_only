package com.attilax.fsm.camelScanner;

import com.attilax.fsm.Context;
import com.attilax.fsm.FinishState;
import com.attilax.fsm.State;
import com.attilax.fsm.Token;
import com.attilax.lang.text.strUtil;
import com.attilax.util.numUtil;

public class BigCaseLetterState extends State {

	@Override
	public void handle(String sampleParameter, Context context) {
		if(MoveNextisEnd(context) ) //or next cur move
			return;
		 char curChar=context.sa[context.curcharIndex];
		 if(strUtil.isBigLetter(curChar))
		 {
			 
			context.curToken.value=context.curToken.value+String.valueOf(curChar);
			 context.state=new BigCaseLetterState();
		 }
		 if(strUtil.isLowerLetter(curChar))
		 {
				context.curToken.value=context.curToken.value+String.valueOf(curChar);
			 context.state=new LowerLetterState();
		 }
		 if(numUtil.isNum(curChar))
		 {
			 context.tokenList.add(context.curToken);
			 context.curToken=new Token();
			 context.curToken.value=context.curToken.value+String.valueOf(curChar);
			 context.state=new NumState();
		 }
			 
		
	}

}
