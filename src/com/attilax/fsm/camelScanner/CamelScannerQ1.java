package com.attilax.fsm.camelScanner;

import java.util.ArrayList;
import java.util.List;

import com.attilax.fsm.Context;
import com.attilax.fsm.FinishState;
import com.attilax.fsm.Scanner;
import com.attilax.fsm.Token;
import com.attilax.json.JSONArray;
import com.attilax.json.JSONObject;

public class CamelScannerQ1 extends Scanner {

	public static void main(String[] args) {
		String s = "fld1=1,fld2='at''t,lax',fld3='val3'";
		s = "aaaaCamelJSONObject123forMac";
		s="com.sun.jna.platform.win32.Variant$VARIANT$_VARIANT$__VARIANT$BRECORD$ByReference.class";
		s="";
	//	s="a";
		@SuppressWarnings("rawtypes")
		List tokenList = new CamelScannerQ1().getTokenList(s);
		System.out.println(JSONArray.fromObject(tokenList).toString(2));
	}
	
	public List<String> getTokenList_retStr(String s)
	{
		if(s.length()==0)
			return new ArrayList<String>();
		List<String> li_r=new ArrayList<String>();
		List<Token> li=getTokenList(s);
		for (Token token : li) {
			li_r.add(token.value);
		}
		return li_r;
	}

	public List getTokenList(String s) {

		// DslPaserContext context = new DslPaserContext();

		Context context = new Context();

		context.setState(new iniState());

		int n = 0;

		while (!(context.state instanceof FinishState)) {

			// System.out.println(n);

			// ����

			context.request(s);

			n++;

			if (n > 200)

				break;

		}

//		for (Token tk : context.tokenList) {
//
//			// if(tk.value.trim().length()>0)
//
//			System.out.println(tk.value + "===");
//
//		}

		return (List) context.tokenList;

	}

}
