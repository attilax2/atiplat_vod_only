package com.attilax.fsm.camelScanner;

import com.attilax.fsm.Context;
import com.attilax.fsm.FinishState;
import com.attilax.fsm.State;
import com.attilax.fsm.Token;
import com.attilax.lang.text.strUtil;
import com.attilax.util.numUtil;

public class NumState extends State {

	@Override
	public void handle(String sampleParameter, Context context) {
		// TODO Auto-generated method stub
		
		
		if(MoveNextisEnd(context) )
			return;
		
		 char curChar=context.sa[context.curcharIndex];
		 if(strUtil.isBigLetter(curChar))
		 {
			 addCurTokenNnewToken(context, curChar);
			 
			 context.state=new BigCaseLetterState();
		 }
		 if(strUtil.isLowerLetter(curChar))
		 {
			 addCurTokenNnewToken(context, curChar);
			 
			 context.state=new LowerLetterState();
		 }
		 if(numUtil.isNum(curChar))
		 {
			 context.curToken.value=context.curToken.value+String.valueOf(curChar);
			// context.state=new NumState();
		 }
	}



}
