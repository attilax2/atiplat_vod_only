package com.attilax.gui;

import com.attilax.cmd.CmdX;
import com.attilax.concur.TaskUtil;
import com.attilax.device.KeyboardUtil;
import com.attilax.io.pathx;
import com.sun.glass.events.KeyEvent;

public class WinUtil {
	
	public static void main(String[] args) {
	 	new WinUtil().WinActivate("我的图书馆 - 360安全浏览器");
		new WinUtil().WinActivate("上传文档 - 360安全浏览器");
	 	
	 	
	   TaskUtil.sleep_sec(5);
		KeyboardUtil keyboardUtil = new KeyboardUtil();
		keyboardUtil.keyPress(KeyEvent.VK_F6);
		String string = "http://www.360doc.com/writedocument2.aspx";
	 	//string="xx";
		keyboardUtil.keyPressString(string);
	 	keyboardUtil.keyPress_enter();
	}
	
	
	/**
	 * 参考帮助文档中“命令行参数”这一节。
在传递命令行参数到AutoIt脚本中的时候，AutoIt会自动初始化一个特殊的数组 $CmdLine。
下面这些可以在AutoIt脚本中直接使用：
$CmdLine[0] ;参数的数量
$CmdLine[1] ;第一个参数 (脚本名称后面)
$CmdLine[2] ;第二个参数
	 * @param win_title
	 */
	public void WinActivate (String win_title)
	{
		String exe=pathx.prjPath()+"/autoit3\\AutoIt3_x64.exe";
		String scrpt=pathx.prjPath()+"\\win\\active.au3";
		//C:\prgrm\  C:\0workspace\AtiPlatf_cms\win\active.au3
//		atitit.细节决定成败的适合情形与缺点
	String cmd = exe +" "+scrpt +" \"%win_title%\"";
	cmd=cmd.replace("%win_title%", win_title);
	System.out.println(cmd);
	CmdX.	exec( cmd);
	}
	
	

}
