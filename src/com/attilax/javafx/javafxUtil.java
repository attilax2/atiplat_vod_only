package com.attilax.javafx;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class javafxUtil {

	public static StackPane getStackPaneFrmImg(Image image) {
	//	System.out.println(orcimg);
	    ImageView imageView = new ImageView();  
        imageView.setImage(image);  
		
		StackPane root = new StackPane();
		root.getChildren().add(imageView);
		return root;
	}

}
