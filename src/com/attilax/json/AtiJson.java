package com.attilax.json;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.reflect.ConstructorUtils;
 




import org.apache.commons.lang3.reflect.MethodUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import bsh.EvalError;
import bsh.Interpreter;

import com.attilax.collection.AArrays;
import com.attilax.io.pathx;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * com.attilax.json.AtiJson
 * @author attilax
 *2016年4月28日 下午9:29:16
 */
public class AtiJson {

	public static void main(String[] args) {
		 
		System.out.println(toJson(Lists.newArrayList("aa","bb")));

	}

	public static String toJson(Object li) {
		// TODO Auto-generated method stub
		
		if("1".equals("1"))
		{
				 
	//	Gson gson2 = new GsonBuilder().setPrettyPrinting().create();
	//	return gson2.toJson(li);
		return	com.alibaba.fastjson.JSON.toJSONString(li, true);
		}
		
		
	//	return gson(li);  
	
	//	Gson gson2 = new GsonBuilder().setPrettyPrinting().create();
		//return gson2.toJson(li);
		Interpreter i = new Interpreter();  // Construct an interpreter
//		try {
//			i.set("foo", 5);
//		} catch (EvalError e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}                    // Set variables
//		i.set("date", new Date() ); 
//
//	//	Date date = (Date)i.get("date");    // retrieve a variable

		// Eval a statement and get the result
		//i.eval("bar = foo*10");             
	//	System.out.println( i.get("bar") );
		 String pakg="com/attilax/json";
		// Source an external script file
	//	 List li=Lists.newArrayList("aa","cc");
		 try {
			i.set("li",li);
			String p = pathx.classPath()+"/"+pakg+"/tojson.bsh";
			String r=	(String) i.source(p);
			return r;
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}  
	 
	}

	private static String toJsonByJsonlib(Object li) {
		try {
			return net.sf.json.JSONObject.fromObject(li).toString(2);
		} catch (Exception e) {
			return net.sf.json.JSONArray.fromObject(li).toString(2);
		}
	
		
	}
	public static String gson(Object li) {
		String gson="com.google.gson.GsonBuilder";
	Object gsonbuilder;
	try {
		gsonbuilder = ConstructorUtils.invokeConstructor(Class.forName(gson),AArrays.emptyArr() );
		gsonbuilder=	MethodUtils.invokeMethod(gsonbuilder, "setPrettyPrinting", AArrays.emptyArr() );
		Object gson_Obj=MethodUtils.invokeMethod(gsonbuilder, "create", AArrays.emptyArr() );
		Object rzt=MethodUtils.invokeMethod(gson_Obj, "toJson",li);
		return rzt.toString();
	} catch (  Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	}
	return gson;
	}
	
	
	@SuppressWarnings("all")
	public static List fromJsonAsList(String t) {

		try {

			List m = new Gson().fromJson(t, List.class);
			return m;
		} catch (Exception e) {
			try {
				return JSONArray.fromObject(t);
			} catch (Exception e2) {
				Object result;
				try {
					Class<?> cls = Class.forName("org.json.JSONArray");
					Object o = ConstructorUtils.invokeConstructor(

					cls, t);
					result = o;
					return (List) result;
				} catch (Exception e1) {
					// TODO Auto-generated catch block
				//	e1.printStackTrace();
					throw new RuntimeException(e);

				}

				// org.json.JSONObject
				// return JsonUtil4jackjson.buildNormalBinder()(
				// this.obj);
			}

		}

	}


	@SuppressWarnings("all")
	public static Map fromJson(String t) {

		try {

			Map m = new Gson().fromJson(t, Map.class);
			return m;
		} catch (Exception e) {
			try {
				return JSONObject.fromObject(t);
			} catch (Exception e2) {
				Object result;
				try {
					Class<?> cls = Class.forName("org.json.JSONObject");
					Object o = ConstructorUtils.invokeConstructor(

					cls, t);
					result = o;
					return (Map) result;
				} catch (Exception e1) {
					// TODO Auto-generated catch block
				//	e1.printStackTrace();
					throw new RuntimeException(e);

				}

				// org.json.JSONObject
				// return JsonUtil4jackjson.buildNormalBinder()(
				// this.obj);
			}

		}

	}

}
