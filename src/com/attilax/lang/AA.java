/*
 * JSP generated by Resin Professional 4.0.22 (built Thu, 01 Sep 2011 01:42:07 PDT)
 */

package _jsp._elemt;
import javax.servlet.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import java.util.*;
import com.focusx.util.*;
import com.focusx.entity.*;
import org.apache.struts2.ServletActionContext;
import com.focusx.auth.authx;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.attilax.core;

public class _elemt_0list__jsp extends com.caucho.jsp.JavaPage
{
  private static final java.util.HashMap<String,java.lang.reflect.Method> _jsp_functionMap = new java.util.HashMap<String,java.lang.reflect.Method>();
  private boolean _caucho_isDead;
  private boolean _caucho_isNotModified;
  private com.caucho.jsp.PageManager _jsp_pageManager;
  
  public void
  _jspService(javax.servlet.http.HttpServletRequest request,
              javax.servlet.http.HttpServletResponse response)
    throws java.io.IOException, javax.servlet.ServletException
  {
    javax.servlet.http.HttpSession session = request.getSession(true);
    com.caucho.server.webapp.WebApp _jsp_application = _caucho_getApplication();
    com.caucho.jsp.PageContextImpl pageContext = _jsp_pageManager.allocatePageContext(this, _jsp_application, request, response, null, session, 8192, true, false);

    TagState _jsp_state = new TagState();

    try {
      _jspService(request, response, pageContext, _jsp_application, session, _jsp_state);
    } catch (java.lang.Throwable _jsp_e) {
      pageContext.handlePageException(_jsp_e);
    } finally {
      _jsp_state.release();
      _jsp_pageManager.freePageContext(pageContext);
    }
  }
  
  private void
  _jspService(javax.servlet.http.HttpServletRequest request,
              javax.servlet.http.HttpServletResponse response,
              com.caucho.jsp.PageContextImpl pageContext,
              javax.servlet.ServletContext application,
              javax.servlet.http.HttpSession session,
              TagState _jsp_state)
    throws Throwable
  {
    javax.servlet.jsp.JspWriter out = pageContext.getOut();
    final javax.el.ELContext _jsp_env = pageContext.getELContext();
    javax.servlet.ServletConfig config = getServletConfig();
    javax.servlet.Servlet page = this;
    javax.servlet.jsp.tagext.JspTag _jsp_parent_tag = null;
    com.caucho.jsp.PageContextImpl _jsp_parentContext = pageContext;
    response.setContentType("text/html");
    response.setCharacterEncoding("utf-8");
    com.focusx.dictionary.tag.SelectTag _jsp_SelectTag_0 = null;

    out.write(_jsp_string0, 0, _jsp_string0.length);
     

	String f = pathx.classPath(IocX.class) + "\\cate.txt";
	  Map m=  YamlAtiX.getMap(f);
		String cates=core.toJsonStrO88(m);		 
			 
    out.write(_jsp_string1, 0, _jsp_string1.length);
    out.print((cates));
    out.write(_jsp_string2, 0, _jsp_string2.length);
    
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"";

String targetUrl = request.getScheme()+"://"+request.getServerName()+"/";
String hostOaa=request.getServerName();

    out.write(_jsp_string3, 0, _jsp_string3.length);
    out.print((basePath ));
    out.write(_jsp_string4, 0, _jsp_string4.length);
    out.print((targetUrl));
    out.write(_jsp_string5, 0, _jsp_string5.length);
    out.print((basePath ));
    out.write(_jsp_string6, 0, _jsp_string6.length);
    out.print((path));
    out.write(_jsp_string7, 0, _jsp_string7.length);
    if("1"=="2"){
    out.write(_jsp_string8, 0, _jsp_string8.length);
    }
    out.write(_jsp_string9, 0, _jsp_string9.length);
    _jsp_SelectTag_0 = _jsp_state.get_jsp_SelectTag_0(pageContext, _jsp_parent_tag);
    _jsp_SelectTag_0.setDPcode("WJLX");
    _jsp_SelectTag_0.setTagId("materialType");
    int _jspEval3 = _jsp_SelectTag_0.doStartTag();
    int _jsp_end_4 = _jsp_SelectTag_0.doEndTag();
    if (_jsp_end_4 == javax.servlet.jsp.tagext.Tag.SKIP_PAGE)
      return;
    out.write(_jsp_string10, 0, _jsp_string10.length);
    //--include file="cate.html" 
    out.write(_jsp_string11, 0, _jsp_string11.length);
    if("1"=="2"){
    out.write(_jsp_string8, 0, _jsp_string8.length);
    }
    out.write(_jsp_string12, 0, _jsp_string12.length);
    _jsp_SelectTag_0 = _jsp_state.get_jsp_SelectTag_0(pageContext, _jsp_parent_tag);
    _jsp_SelectTag_0.setDPcode("appcate");
    _jsp_SelectTag_0.setTagId("applicationType");
    int _jspEval7 = _jsp_SelectTag_0.doStartTag();
    int _jsp_end_8 = _jsp_SelectTag_0.doEndTag();
    if (_jsp_end_8 == javax.servlet.jsp.tagext.Tag.SKIP_PAGE)
      return;
    out.write(_jsp_string13, 0, _jsp_string13.length);
    //--@include file="appcate.html" 
    out.write(_jsp_string14, 0, _jsp_string14.length);
     if( authx.hasReviewAuth2(6)) { 
    out.write(_jsp_string15, 0, _jsp_string15.length);
    }
    out.write(_jsp_string16, 0, _jsp_string16.length);
  }

  private com.caucho.make.DependencyContainer _caucho_depends
    = new com.caucho.make.DependencyContainer();

  public java.util.ArrayList<com.caucho.vfs.Dependency> _caucho_getDependList()
  {
    return _caucho_depends.getDependencies();
  }

  public void _caucho_addDepend(com.caucho.vfs.PersistentDependency depend)
  {
    super._caucho_addDepend(depend);
    _caucho_depends.add(depend);
  }

  protected void _caucho_setNeverModified(boolean isNotModified)
  {
    _caucho_isNotModified = true;
  }

  public boolean _caucho_isModified()
  {
    if (_caucho_isDead)
      return true;

    if (_caucho_isNotModified)
      return false;

    if (com.caucho.server.util.CauchoSystem.getVersionId() != 2727790410503961070L)
      return true;

    return _caucho_depends.isModified();
  }

  public long _caucho_lastModified()
  {
    return 0;
  }

  public void destroy()
  {
      _caucho_isDead = true;
      super.destroy();
    TagState tagState;
  }

  public void init(com.caucho.vfs.Path appDir)
    throws javax.servlet.ServletException
  {
    com.caucho.vfs.Path resinHome = com.caucho.server.util.CauchoSystem.getResinHome();
    com.caucho.vfs.MergePath mergePath = new com.caucho.vfs.MergePath();
    mergePath.addMergePath(appDir);
    mergePath.addMergePath(resinHome);
    com.caucho.loader.DynamicClassLoader loader;
    loader = (com.caucho.loader.DynamicClassLoader) getClass().getClassLoader();
    String resourcePath = loader.getResourcePathSpecificFirst();
    mergePath.addClassPath(resourcePath);
    com.caucho.vfs.Depend depend;
    depend = new com.caucho.vfs.Depend(appDir.lookup("elemt/elemt_list.jsp"), -4741836599561798504L, false);
    _caucho_depends.add(depend);
    depend = new com.caucho.vfs.Depend(appDir.lookup("common/global_v.jsp"), 3427326634490376515L, false);
    _caucho_depends.add(depend);
    depend = new com.caucho.vfs.Depend(appDir.lookup("com.attilax/web/dwrExCptr.html"), -9209526370734589089L, false);
    _caucho_depends.add(depend);
    depend = new com.caucho.vfs.Depend(appDir.lookup("WEB-INF/d.tld"), -3192857710841213059L, false);
    _caucho_depends.add(depend);
    _caucho_depends.add(new com.caucho.make.ClassDependency("com.focusx.dictionary.tag.SelectTag", 8114364502665258844L));
  }

  static {
    try {
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  final static class TagState {
    private com.focusx.dictionary.tag.SelectTag _jsp_SelectTag_0;

    final com.focusx.dictionary.tag.SelectTag get_jsp_SelectTag_0(PageContext pageContext, javax.servlet.jsp.tagext.JspTag _jsp_parent_tag) throws Throwable
    {
      if (_jsp_SelectTag_0 == null) {
        _jsp_SelectTag_0 = new com.focusx.dictionary.tag.SelectTag();
        _jsp_SelectTag_0.setPageContext(pageContext);
        if (_jsp_parent_tag instanceof javax.servlet.jsp.tagext.Tag)
          _jsp_SelectTag_0.setParent((javax.servlet.jsp.tagext.Tag) _jsp_parent_tag);
        else if (_jsp_parent_tag instanceof javax.servlet.jsp.tagext.SimpleTag)
          _jsp_SelectTag_0.setParent(new javax.servlet.jsp.tagext.TagAdapter((javax.servlet.jsp.tagext.SimpleTag) _jsp_parent_tag));
        else
          _jsp_SelectTag_0.setParent((javax.servlet.jsp.tagext.Tag) null);
      }

      return _jsp_SelectTag_0;
    }

    void release()
    {
      if (_jsp_SelectTag_0 != null)
        _jsp_SelectTag_0.release();
    }
  }

  public java.util.HashMap<String,java.lang.reflect.Method> _caucho_getFunctionMap()
  {
    return _jsp_functionMap;
  }

  public void caucho_init(ServletConfig config)
  {
    try {
      com.caucho.server.webapp.WebApp webApp
        = (com.caucho.server.webapp.WebApp) config.getServletContext();
      init(config);
      if (com.caucho.jsp.JspManager.getCheckInterval() >= 0)
        _caucho_depends.setCheckInterval(com.caucho.jsp.JspManager.getCheckInterval());
      _jsp_pageManager = webApp.getJspApplicationContext().getPageManager();
      com.caucho.jsp.TaglibManager manager = webApp.getJspApplicationContext().getTaglibManager();
      manager.addTaglibFunctions(_jsp_functionMap, "d", "/d-tags");
      com.caucho.jsp.PageContextImpl pageContext = new com.caucho.jsp.InitPageContextImpl(webApp, this);
    } catch (Exception e) {
      throw com.caucho.config.ConfigException.create(e);
    }
  }

  private final static char []_jsp_string0;
  private final static char []_jsp_string15;
  private final static char []_jsp_string5;
  private final static char []_jsp_string8;
  private final static char []_jsp_string3;
  private final static char []_jsp_string13;
  private final static char []_jsp_string4;
  private final static char []_jsp_string12;
  private final static char []_jsp_string9;
  private final static char []_jsp_string2;
  private final static char []_jsp_string14;
  private final static char []_jsp_string6;
  private final static char []_jsp_string10;
  private final static char []_jsp_string11;
  private final static char []_jsp_string1;
  private final static char []_jsp_string16;
  private final static char []_jsp_string7;
  static {
    _jsp_string0 = "\r\n\r\n    \r\n     \r\n      \r\n    \r\n      \r\n          \r\n     \r\n".toCharArray();
    _jsp_string15 = "\r\n                                    <a href=\"../elemt_review/review_edit.jsp?targetid={{itemO7.materialId}}&targettype=mtrl&datatypeNdecrib=\u7d20\u6750-{{itemO7.materialDescription}}&param=reviewMtrl,{{itemO7.materialId}}\" target=\"_blank\">\r\n                                    \u5ba1\u6838&nbsp;&nbsp;&nbsp;\r\n                                    </a>\r\n                                    ".toCharArray();
    _jsp_string5 = "';\r\n	var  basepathP29='".toCharArray();
    _jsp_string8 = "\r\n                <select></select>\r\n                ".toCharArray();
    _jsp_string3 = "\r\n \r\n<script type=\"text/javascript\">\r\n	var ctx = '".toCharArray();
    _jsp_string13 = "\r\n				<!--".toCharArray();
    _jsp_string4 = "';\r\n	var loadx = '".toCharArray();
    _jsp_string12 = " <!--o88-->\r\n                                                 ".toCharArray();
    _jsp_string9 = "\r\n                <!--o88 ati-->\r\n                ".toCharArray();
    _jsp_string2 = ";\r\n\r\n</script>\r\n<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\" ng-app=\"atiMod\">\r\n \r\n<head>\r\n<title>\u7d20\u6750\u7ba1\u7406</title>\r\n<!--o88.-->\r\n <script src=\"../com.attilax/util/datadic_js.jsp\"></script>\r\n <!--///-->\r\n<!--set ie8\u8bbe\u5b9a\u8981\u7528IE8\u6807\u51c6\u6a21\u5f0f\u6e32\u67d3\u7f51\u9875\uff0c\u800c\u4e0d\u4f1a\u4f7f\u7528\u517c\u5bb9\u7684\u6a21\u5f0f\u3002-->\r\n<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EmulateIE8\" />\r\n<meta http-equiv=\"keywords\" content=\"keyword1,keyword2,keyword3\">\r\n<meta http-equiv=\"description\" content=\"this is my page\">\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<link rel=\"stylesheet\" href=\"../resources/css/style.css\" type=\"text/css\"></link>\r\n   \r\n \r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n  <meta http-equiv=\"pragma\" content=\"no-cache\">\r\n	<meta http-equiv=\"cache-control\" content=\"no-cache\">\r\n	<meta http-equiv=\"expires\" content=\"0\">\r\n    \r\n".toCharArray();
    _jsp_string14 = "-->\r\n                          \r\n					      <td>\u5ba1\u6838\u72b6\u6001\uff1a</td>\r\n						  <td class=\"tdr\"><label for=\"revistat\"></label>\r\n						    <select name=\"revistat\" id=\"revistat\">\r\n						      <option value=\"-1\" selected=\"selected\">-------</option>\r\n						      <option value=\"1\">\u5df2\u901a\u8fc7</option>\r\n						      <option value=\"0\">\u672a\u901a\u8fc7</option>\r\n                               <option value=\"2\">\u5f85\u5ba1\u6838</option>\r\n			              </select></td>\r\n					  </tr>\r\n					</table>\r\n\r\n					<div class=\"toolBarDiv\">\r\n						<table cellpadding=0 cellspacing=0 width=\"100%\"  border=\"0\"  class=\"toolBar\">\r\n							<tr>\r\n						  		<td>\r\n						    		<table id=\"toolBar\" border=\"0\">\r\n						      			<tr>\r\n						      			  <td width=\"80\" class=\"coolButton\" ><img alt=\"\" width=\"78\" style=\"visibility:hidden\" /></td>\r\n											<td class=\"coolButton\" >\r\n												<div style=''>\r\n												  <input  type='button' id='id_ev_add' name='ev_add' class='bt00' value='\u65b0\u589e' onclick=\"link_add()\"  />\r\n												</div>\r\n											</td>\r\n											<td class=\"coolButton\" ><div style=''><input  type='button'  onclick=\"editSlktRow()\" id='id_ev_edit' name='ev_edit' class='bt00' value='\u7f16\u8f91' /></div> </td>\r\n											<td class=\"coolButton\" ><div style=''><input  type='button'   onclick=\"delxSlktRows()\" id='id_ev_del' name='ev_del' class='bt00' value='\u5220\u9664' /></div> </td>\r\n											<td class=\"coolButton\" ><div style=''><input  type='button' onclick=\"query(1)\" id='id_ev_query' name='ev_query' class='bt00' value='\u67e5\u8be2'/></div> </td>\r\n						            	</tr>\r\n						         	</table>\r\n						      	</td>\r\n						   	</tr>\r\n						</table>\r\n					</div>\r\n\r\n					<div class=\"form\">\r\n						<div class=\"l\">\r\n							<div class=\"r\">\r\n								<span>\u7d20\u6750\u7ba1\u7406</span>\r\n								<input type=\"hidden\" name=\"idsCheckVals\" id=\"idsCheckVals\" />\r\n							</div>\r\n						</div>\r\n					</div>\r\n\r\n					<table id=\"tabid1\" width=\"100%\" class=\"tab03\"   ng-controller=\"listCtrl\">\r\n						<thead>\r\n							<tr>\r\n								<th>\r\n                              \r\n                                  <input   type=\"checkbox\"  onclick=\"  setAllSelect(this)\"  value=\"{{itemO7.materialId}}\" />\r\n                                </th>\r\n								<th>\u7d20\u6750id</th>\r\n							 \r\n								<th> \u63cf\u8ff0</th>\r\n							 \r\n								<th>\u5206\u7c7b</th>\r\n							 \r\n								<th>\u5e94\u7528\u5206\u7c7b</th>\r\n							 \r\n								<th>\u64ad\u653e\u65f6\u957f</th>\r\n							 \r\n								<th>\u521b\u5efa\u65f6\u95f4</th>\r\n							 \r\n								<th>\u751f\u6548\u65f6\u95f4</th>\r\n							 \r\n								<th>\u5931\u6548\u65f6\u95f4</th>\r\n							 \r\n								<th style=\"display:none\">\u521b\u5efa\u4eba</th>\r\n								<th class=\"thr\">\u5ba1\u6838\u72b6\u6001</th>\r\n								<th class=\"thr\" width=\"260px\">\u64cd\u4f5c</th>\r\n						  </tr>\r\n					  </thead>\r\n						<tbody id=\"linkInfoTbody\">\r\n							<tr  ng-repeat=\"itemO7 in listO7\" >\r\n								<td>\r\n									<input name=\"idsCheck\" type=\"checkbox\" id=\"idsCheck\" value=\"{{itemO7.materialId}}\" />\r\n								</td>\r\n						 \r\n								<td>{{itemO7.materialId}}</td>\r\n                                	<td>{{itemO7.materialDescription}}</td>\r\n                                    	<td>{{itemO7.materialType|cateFmt}}</td>\r\n                                        <td>{{itemO7.applicationType|appCateFmt}} </td>  <td>{{itemO7.playtime|timefmtSecs}} </td>  <td>{{itemO7.createTime|timeFmtO7}} </td>  <td>{{itemO7.effectieTime|timeFmtO7}} </td>  <td>{{itemO7.failureTime|timeFmtO7}} </td>  <td  style=\"display:none\">{{itemO7.createUser}} </td>\r\n                                        <td class=\"tdr\"> {{itemO7.revi.state|stateFmt1}} </td>\r\n                                <td class=\"tdr\">\r\n                                \r\n									<a href=\"javascript:void(0)\" onclick=\"delx(this.id,this)\" id=\"{{itemO7.materialId}}\">\r\n										<img src=\"../resources/images/delete.gif\" onclick=\"\" />&nbsp;&nbsp;&nbsp;\r\n									</a>\r\n                                    \r\n                                <span id=\"RvwHide\" style=\"display:{{itemO7.revi.state|RvwHideFmt}}\">\r\n									<a href=\"elemt_edit.jsp?id={{itemO7.materialId}}\">\r\n										<img src=\"../resources/images/alter.gif\" />&nbsp;&nbsp;\r\n									</a>\r\n                              </span>\r\n                              	".toCharArray();
    _jsp_string6 = "';\r\n	\r\n</script>\r\n\r\n\r\n\r\n<script type=\"text/javascript\" src=\"../com.attilax/time/time.js\"></script>\r\n<script src=\"date.js\"></script>\r\n<script language=\"JavaScript\" type=\"text/javascript\" src=\"../js/jquery-1.8.0.min.js\"></script>\r\n\r\n\r\n\r\n <link rel=\"stylesheet\" type=\"text/css\" href=\"../js/themes/default/easyui.css\"/>  \r\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"../js/themes/icon.css\"/>  \r\n	    <link rel=\"stylesheet\" type=\"text/css\" href=\"../js/demo/demo.css\">\r\n\r\n\r\n \r\n<script language=\"JavaScript\" type=\"text/javascript\" src=\"../js/jquery.easyui.min.js\">\r\n</script>\r\n<script language=\"JavaScript\" type=\"text/javascript\" src=\"../js/locale/easyui-lang-zh_CN.js\"></script>\r\n \r\n<script src=\"js/angular.min.js\"></script>\r\n\r\n \r\n \r\n  <script type='text/javascript' src='../dwr/engine.js'></script>\r\n  <script type='text/javascript' src='../dwr/interface/elmtC.js'></script>\r\n\r\n \r\n  <script type='text/javascript' src='../dwr/interface/dwrC.js'></script>\r\n  <script type='text/javascript' src='../dwr/util.js'></script>\r\n  <script src=\"elemt_list_head.js\" type=\"text/javascript\"></script>\r\n  \r\n      \r\n <script>\r\n \r\n 	function errh(errorString, exception) {\r\n		logx(errorString);\r\n		if(errorString==\"\u6709\u8282\u76ee\u5355\u5173\u8054\u6570\u636e\uff0c\u4e0d\u80fd\u5220\u9664\")\r\n		alert(errorString);\r\n		\r\n	//	alert(exception);\r\n	//\u3000 \u3000 alert(errorString);\r\n	\u3000// 	 alert( JSON.stringify(exception));\r\n		//	eval(dwrxO9).getErr(				function(data){\r\n		//			 alert(data);\r\n		//			 window.open(\"".toCharArray();
    _jsp_string10 = "\r\n                                                <!-- ".toCharArray();
    _jsp_string11 = "-->\r\n                            \r\n							<td>\u521b\u5efa\u65f6\u95f4\uff1a</td>\r\n							<td class=\"tdr\">\r\n								<input type=\"text\" name=\"createTime\" value=\"\" class=\"input-text  easyui-datebox\" style=\"display:nonex\" />\r\n                                \r\n                              \r\n                                \r\n						  </td>\r\n						</tr>\r\n						<tr>\r\n						  <td>\u64ad\u653e\u65f6\u957f\uff1a</td>\r\n						  <td style=\"text-align:left;\"><span class=\"tdr\">\r\n						    <input name=\"playtime_start\"\r\n    required=\"required\" class=\"easyui-timespinner\" id=\"ss\" style=\"width:80px;\" value=\"00:00:01\" data-options=\"showSeconds:true\" />\r\n						  </span>---<span class=\"tdr\">\r\n						  <input name=\"playtime_end\"\r\n    required=\"required\" class=\"easyui-timespinner\" id=\"ss2\" style=\"width:80px;\" value=\"00:00:01\" data-options=\"showSeconds:true\" />\r\n						  </span></td>\r\n						  <td>\u5e94\u7528\u5206\u7c7b\uff1a</td>\r\n						  <td class=\"tdr\">\r\n						    ".toCharArray();
    _jsp_string1 = "\r\n<script>\r\nvar appcateO88=".toCharArray();
    _jsp_string16 = "\r\n                                    <a href=\"../elemt_review/review_list.jsp?targetid={{itemO7.materialId}}&targettype=mtrl&param=mtrlReviewHstry,{{itemO7.materialId}}\"   target=\"_blank\">\r\n                                    \u67e5\u770b\u5ba1\u6838\u8be6\u7ec6\u7ed3\u679c&nbsp;\r\n                                    </a>  \r\n                                    \r\n								</td>\r\n							</tr>\r\n			\r\n						</tbody>\r\n					</table>\r\n					<div class=\"splitPage\">\r\n					  <div align=\"right\" class='FormFont'>\r\n					    <script src=\"pagging.js\"></script>\r\n					    \u5171\u6709<span id=\"totalRows\">0</span>\u6761\u8bb0\u5f55&nbsp;&nbsp;&nbsp;&nbsp;\r\n                        <span id=\"firstPrePageBtnArea\">\r\n                        <a href=\"javascript:firstPage()\" id=\"firstPageBtn\">\u9996\u9875</a> &nbsp;<a href=\"javascript:prePage()\" id=\"prePageBtn\">\u4e0a\u9875</a></span>\r\n                        \r\n                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n                          <span id=\"nextLastPageBtnArea\">\r\n                        <a href=\"javascript:nextPage()\" id=\"nextPageBtn\">\u4e0b\u9875</a> &nbsp;<a href=\"javascript:lastPage()\" id=\"lastPageBtn\">\u5c3e\u9875</a></span>&nbsp;&nbsp;&nbsp;\u8f93\u5165\u9875\u7801\uff1a\r\n					    <input type=\"text\" id=\"pageItem\" size=\"4\" maxlength=\"5\" class=\"TextStyle\"onkeydown=\"if(event.keyCode==0xD) page_go()\">&nbsp;&nbsp;&nbsp;<a href=\"javascript:page_go()\">\u8df3\u8f6c</a>&nbsp;&nbsp;&nbsp;&nbsp;\u7b2c<span id=\"page_page_lab\">1</span>\u9875/\u5171<span id=\"totalPages\">10</span>\u9875\r\n                        <span style=\"display:none\">\r\n						  <input name=\"pagesize\" type=\"hidden\" id=\"pagesize\" value=\"10\" />\r\n						  <input name=\"page_page\" type=\"text\" id=\"page_page\" value=\"1\" />\r\n                          </span>\r\n				      <p/></div>\r\n						<!-- <div align=\"right\" class='FormFont'>\u5171\u67093\u6761\u8bb0\u5f55&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\u7b2c1\u9875/\u51711\u9875<p/> -->\r\n					</div>\r\n				</form>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<table width=\"100%\">\r\n					<tr>\r\n						<td align=\"left\" class=\"f3\"><img\r\n							src=\"../resources/images/center-lb.gif\" /></td>\r\n						<td align=\"right\" class=\"f3\"><img\r\n							src=\"../resources/images/center-rb.gif\" /></td>\r\n					</tr>\r\n				</table></td>\r\n		</tr>\r\n	</table>\r\n     <div id=\"loading\"></div> \r\n   <script>\r\n   function listCtrl($scope) { \r\n\r\n    $scope.listO7 =[ ] ; \r\n\r\n} \r\n\r\n   </script>\r\n      <script type='text/javascript' src='elemt_list.js'></script>\r\n    <script type='text/javascript' src='elemt_list_man.js'></script>\r\n   <script type=\"text/javascript\" defer=\"defer\" src=\"../resources/js/publicTbody2.js\"></script>\r\n</body>\r\n</html>\r\n".toCharArray();
    _jsp_string7 = "/com.attilax/util/dwrerr.jsp\");\r\n					 \r\n			  \r\n		//		});\r\n	}\r\n\r\ndwr.engine.setErrorHandler(errh);\r\n//dwr.engine.setErrorHandler(errh);\r\n//alert(\"set error handler ok\");\r\n//alert( dwr.engine.getErrorHandler());\r\n//dwr.engine.setErrorHandler(handler);\r\n \r\n \r\n </script>\r\n     <script type=\"text/javascript\" src=\"../com.attilax/core/core.js\"></script>\r\n     <script type=\"text/javascript\" src=\"../com.attilax/ui/checkbox.js\"></script>\r\n     \r\n     <script type=\"text/javascript\" src=\"../com.attilax/parte/parte.js\"></script>\r\n     \r\n     <script type=\"text/javascript\" src=\"../com.attilax/exception/ex.js\"></script>\r\n     \r\n</head>\r\n\r\n<body leftmargin=\"0\" topmargin=\"0\" scrolling=\"no\" style=\"padding: 0px;\">\r\n	<table width=\"100%\" height=\"100%\" id=\"center-table\">\r\n		<tr>\r\n			<td>\r\n				<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n					<tr>\r\n						<td width=\"0%\" align=\"left\" class=\"f1\"><img\r\n							src=\"../resources/images/center-lt.gif\" /></td>\r\n						<td width=\"0%\" align=\"right\" class=\"f1\"><img\r\n							src=\"../resources/images/center-rt.gif\" /></td>\r\n					</tr>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"3\" valign=\"top\" class=\"f2\">\r\n				<div class=\"current\">\r\n					<div class=\"l\">\r\n						<div class=\"r\">\r\n							<p>\r\n								\u5f53\u524d\u4f4d\u7f6e\uff1a\r\n									<a href=\"#\">\u89c6\u9891\u70b9\u64ad</a> >>\u7d20\u6750\u7ba1\u7406</p>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div class=\"form\">\r\n					<div class=\"l\" onclick=\"toggleElementByDiv('demo1_table2');\">\r\n						<div class=\"r\">\r\n							<span>\u67e5\u8be2</span>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<form id=\"formx\" name=\"formx\" action=\"\" method=\"post\">\r\n					<table width=\"100%\" class=\"tab01\" id=\"demo1_table2\">\r\n						<tr>\r\n							<td>\u63cf\u8ff0\uff1a</td>\r\n							<td style=\"text-align:left;\">\r\n								<input name=\"materialDescription\" type=\"text\" class=\"input-text\" id=\"materialDescription\" value=\"\" />\r\n								<input name=\"logicDel\" type=\"hidden\" id=\"logicDel\" value=\"8\" />\r\n							</td>\r\n							<td>\u7c7b\u578b\uff1a</td>\r\n							<td class=\"tdr\">\r\n							  ".toCharArray();
  }
}
