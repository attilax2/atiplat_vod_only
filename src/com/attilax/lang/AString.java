/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package com.attilax.lang;

import java.io.ObjectStreamField;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.attilax.lang.text.strUtil;
import com.attilax.vbs.Str;

 

public   class AString     {
	public static String[] cn_splitors = {"：",""};
	public static String[] en_splitors = {":",""};
	private   char[] value;
	public static void main(String[] args) {
//		String s="/aaabb";
//		System.out.println(LTrim(s, "/"));
		String x = "工作时间：08:00-22:00\r\n性别要求：男女不限";
		 x=replaceCnSplitor(x);
		System.out.println(AString.find("工作时间:.*", x)  );
	}
	private static String replaceCnSplitor(String x) {
		for (int i = 0; i < cn_splitors.length; i++) {
			try {
				x=x.replace(cn_splitors[i], en_splitors[i]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		return x;
	}
	private int hash;
	private static final long serialVersionUID = -6849794470754667710L;
	private static final ObjectStreamField[] serialPersistentFields = new ObjectStreamField[0];

	public static final Comparator<AString> CASE_INSENSITIVE_ORDER = new CaseInsensitiveComparator(
			 );
	private static final int HASHING_SEED;
	private transient int hash32;

	public AString() {
		this.hash32 = 0;

		this.value = new char[0];
	}
 

 

	public AString(char[] paramArrayOfChar, int paramInt1, int paramInt2) {
		this.hash32 = 0;

		if (paramInt1 < 0) {
			throw new StringIndexOutOfBoundsException(paramInt1);
		}
		if (paramInt2 < 0) {
			throw new StringIndexOutOfBoundsException(paramInt2);
		}

		if (paramInt1 > paramArrayOfChar.length - paramInt2) {
			throw new StringIndexOutOfBoundsException(paramInt1 + paramInt2);
		}
		this.value = Arrays.copyOfRange(paramArrayOfChar, paramInt1, paramInt1
				+ paramInt2);
	}

	 

	@Deprecated
	public AString(byte[] paramArrayOfByte, int paramInt1, int paramInt2,
			int paramInt3) {
		this.hash32 = 0;

		checkBounds(paramArrayOfByte, paramInt2, paramInt3);
		char[] arrayOfChar = new char[paramInt3];
		int i;
		if (paramInt1 == 0) {
			for (i = paramInt3; i-- > 0;)
				arrayOfChar[i] = (char) (paramArrayOfByte[(i + paramInt2)] & 0xFF);
		} else {
			paramInt1 <<= 8;
			for (i = paramInt3; i-- > 0;) {
				arrayOfChar[i] = (char) (paramInt1 | paramArrayOfByte[(i + paramInt2)] & 0xFF);
			}
		}
		this.value = arrayOfChar;
	}

	@Deprecated
	public AString(byte[] paramArrayOfByte, int paramInt) {
		this(paramArrayOfByte, paramInt, 0, paramArrayOfByte.length);
	}

	private static void checkBounds(byte[] paramArrayOfByte, int paramInt1,
			int paramInt2) {
		if (paramInt2 < 0)
			throw new StringIndexOutOfBoundsException(paramInt2);
		if (paramInt1 < 0)
			throw new StringIndexOutOfBoundsException(paramInt1);
		if (paramInt1 > paramArrayOfByte.length - paramInt2)
			throw new StringIndexOutOfBoundsException(paramInt1 + paramInt2);
	}

	
	/**
	 * o4e
	 * @param regEx
	 * @param str
	 */
	public static List find(String regEx, String str) {
		List li=new ArrayList<String>();
		Pattern p = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher m = p.matcher(str);
		boolean rs = m.find();
		System.out.println(rs);
//		System.out.println(m.groupCount());
//		for (int i = 1; i <= m.groupCount(); i++) {
//			System.out.println(m.group(i));
//		}
		 while (rs){
			 
			   String A=m.group();
			  
			   System.out.println(A);
			   li.add(A);
			   rs=m.find();
		 }
		 return li;
	}
//	public AString(byte[] paramArrayOfByte, int paramInt1, int paramInt2,
//			AString paramString) throws UnsupportedEncodingException {
//		this.hash32 = 0;
//
//		if (paramString == null)
//			throw new NullPointerException("charsetName");
//		checkBounds(paramArrayOfByte, paramInt1, paramInt2);
//		this.value = StringCoding.decode(paramString, paramArrayOfByte,
//				paramInt1, paramInt2);
//	}

//	public AString(byte[] paramArrayOfByte, int paramInt1, int paramInt2,
//			Charset paramCharset) {
//		this.hash32 = 0;
//
//		if (paramCharset == null)
//			throw new NullPointerException("charset");
//		checkBounds(paramArrayOfByte, paramInt1, paramInt2);
//		this.value = StringCoding.decode(paramCharset, paramArrayOfByte,
//				paramInt1, paramInt2);
//	}

//	public AString(byte[] paramArrayOfByte, AString paramString)
//			throws UnsupportedEncodingException {
//		this(paramArrayOfByte, 0, paramArrayOfByte.length, paramString);
//	}
//
//	public AString(byte[] paramArrayOfByte, Charset paramCharset) {
//		this(paramArrayOfByte, 0, paramArrayOfByte.length, paramCharset);
//	}

//	public AString(byte[] paramArrayOfByte, int paramInt1, int paramInt2) {
//		this.hash32 = 0;
//
//		checkBounds(paramArrayOfByte, paramInt1, paramInt2);
//		this.value = StringCoding
//				.decode(paramArrayOfByte, paramInt1, paramInt2);
//	}
//
//	public AString(byte[] paramArrayOfByte) {
//		this(paramArrayOfByte, 0, paramArrayOfByte.length);
//	}
//
//	public AString(StringBuffer paramStringBuffer) {
//		this.hash32 = 0;
//
//		synchronized (paramStringBuffer) {
//			this.value = Arrays.copyOf(paramStringBuffer.getValue(),
//					paramStringBuffer.length());
//		}
//	}

//	public AString(StringBuilder paramStringBuilder) {
//		this.hash32 = 0;
//
//		this.value = Arrays.copyOf(paramStringBuilder.getValue(),
//				paramStringBuilder.length());
//	}

	AString(char[] paramArrayOfChar, boolean paramBoolean) {
		this.hash32 = 0;

		this.value = paramArrayOfChar;
	}

	@Deprecated
	AString(int paramInt1, int paramInt2, char[] paramArrayOfChar) {
		this(paramArrayOfChar, paramInt1, paramInt2);
	}



	public AString(String string) {
		this.value = string.toCharArray();
		this.raw_obj=string;
	}
 

	/**
	 * @param string
	 * @param uid
	 */
	public AString(String string, String... vals) {
		
	    
		this.raw_obj=strUtil.replace(string, vals);
		
	}
	public int length() {
		return this.value.length;
	}
	String raw_obj="";
	public boolean isEmpty() {
		return (this.value.length == 0);
	}

	public char charAt(int paramInt) {
		if ((paramInt < 0) || (paramInt >= this.value.length)) {
			throw new StringIndexOutOfBoundsException(paramInt);
		}
		return this.value[paramInt];
	}

//	public int codePointAt(int paramInt) {
//		if ((paramInt < 0) || (paramInt >= this.value.length)) {
//			throw new StringIndexOutOfBoundsException(paramInt);
//		}
//		return Character.codePointAtImpl(this.value, paramInt,
//				this.value.length);
//	}
//
//	public int codePointBefore(int paramInt) {
//		int i = paramInt - 1;
//		if ((i < 0) || (i >= this.value.length)) {
//			throw new StringIndexOutOfBoundsException(paramInt);
//		}
//		return Character.codePointBeforeImpl(this.value, paramInt, 0);
//	}
//
//	public int codePointCount(int paramInt1, int paramInt2) {
//		if ((paramInt1 < 0) || (paramInt2 > this.value.length)
//				|| (paramInt1 > paramInt2)) {
//			throw new IndexOutOfBoundsException();
//		}
//		return Character.codePointCountImpl(this.value, paramInt1, paramInt2
//				- paramInt1);
//	}
//
//	public int offsetByCodePoints(int paramInt1, int paramInt2) {
//		if ((paramInt1 < 0) || (paramInt1 > this.value.length)) {
//			throw new IndexOutOfBoundsException();
//		}
//		return Character.offsetByCodePointsImpl(this.value, 0,
//				this.value.length, paramInt1, paramInt2);
//	}

	void getChars(char[] paramArrayOfChar, int paramInt) {
		System.arraycopy(this.value, 0, paramArrayOfChar, paramInt,
				this.value.length);
	}

	public void getChars(int paramInt1, int paramInt2, char[] paramArrayOfChar,
			int paramInt3) {
		if (paramInt1 < 0) {
			throw new StringIndexOutOfBoundsException(paramInt1);
		}
		if (paramInt2 > this.value.length) {
			throw new StringIndexOutOfBoundsException(paramInt2);
		}
		if (paramInt1 > paramInt2) {
			throw new StringIndexOutOfBoundsException(paramInt2 - paramInt1);
		}
		System.arraycopy(this.value, paramInt1, paramArrayOfChar, paramInt3,
				paramInt2 - paramInt1);
	}

	@Deprecated
	public void getBytes(int paramInt1, int paramInt2, byte[] paramArrayOfByte,
			int paramInt3) {
		if (paramInt1 < 0) {
			throw new StringIndexOutOfBoundsException(paramInt1);
		}
		if (paramInt2 > this.value.length) {
			throw new StringIndexOutOfBoundsException(paramInt2);
		}
		if (paramInt1 > paramInt2) {
			throw new StringIndexOutOfBoundsException(paramInt2 - paramInt1);
		}
		int i = paramInt3;
		int j = paramInt2;
		int k = paramInt1;
		char[] arrayOfChar = this.value;

		while (k < j)
			paramArrayOfByte[(i++)] = (byte) arrayOfChar[(k++)];
	}

//	public byte[] getBytes(AString paramString)
//			throws UnsupportedEncodingException {
//		if (paramString == null)
//			throw new NullPointerException();
//		return StringCoding.encode(paramString, this.value, 0,
//				this.value.length);
//	}
//
//	public byte[] getBytes(Charset paramCharset) {
//		if (paramCharset == null)
//			throw new NullPointerException();
//		return StringCoding.encode(paramCharset, this.value, 0,
//				this.value.length);
//	}
//
//	public byte[] getBytes() {
//		return StringCoding.encode(this.value, 0, this.value.length);
//	}

	public boolean equals(Object paramObject) {
		if (this == paramObject) {
			return true;
		}
		if (paramObject instanceof AString) {
			AString str = (AString) paramObject;
			int i = this.value.length;
			if (i == str.value.length) {
				char[] arrayOfChar1 = this.value;
				char[] arrayOfChar2 = str.value;
				int j = 0;
				while (i-- != 0) {
					if (arrayOfChar1[j] != arrayOfChar2[j])
						return false;
					++j;
				}
				return true;
			}
		}
		return false;
	}

	public boolean contentEquals(StringBuffer paramStringBuffer) {
		synchronized (paramStringBuffer) {
			return contentEquals(paramStringBuffer);
		}
	}

	 

	public boolean equalsIgnoreCase(AString paramString) {
		return (this == paramString);
	}

	public int compareTo(AString paramString) {
		int i = this.value.length;
		int j = paramString.value.length;
		int k = Math.min(i, j);
		char[] arrayOfChar1 = this.value;
		char[] arrayOfChar2 = paramString.value;

		int l = 0;
		while (l < k) {
			int i1 = arrayOfChar1[l];
			int i2 = arrayOfChar2[l];
			if (i1 != i2) {
				return (i1 - i2);
			}
			++l;
		}
		return (i - j);
	}

	public int compareToIgnoreCase(AString paramString) {
		return CASE_INSENSITIVE_ORDER.compare(this, paramString);
	}

	public boolean regionMatches(int paramInt1, AString paramString,
			int paramInt2, int paramInt3) {
		char[] arrayOfChar1 = this.value;
		int i = paramInt1;
		char[] arrayOfChar2 = paramString.value;
		int j = paramInt2;

		if ((paramInt2 < 0) || (paramInt1 < 0)
				|| (paramInt1 > this.value.length - paramInt3)
				|| (paramInt2 > paramString.value.length - paramInt3)) {
			return false;
		}
		while (paramInt3-- > 0) {
			if (arrayOfChar1[(i++)] != arrayOfChar2[(j++)]) {
				return false;
			}
		}
		return true;
	}
 

	public boolean startsWith(AString paramString, int paramInt) {
		char[] arrayOfChar1 = this.value;
		int i = paramInt;
		char[] arrayOfChar2 = paramString.value;
		int j = 0;
		int k = paramString.value.length;

		if ((paramInt < 0) || (paramInt > this.value.length - k)) {
			return false;
		}
		while (--k >= 0) {
			if (arrayOfChar1[(i++)] != arrayOfChar2[(j++)]) {
				return false;
			}
		}
		return true;
	}

	public boolean startsWith(AString paramString) {
		return startsWith(paramString, 0);
	}

	public boolean endsWith(AString paramString) {
		return startsWith(paramString, this.value.length
				- paramString.value.length);
	}

	public int hashCode() {
		int i = this.hash;
		if ((i == 0) && (this.value.length > 0)) {
			char[] arrayOfChar = this.value;

			for (int j = 0; j < this.value.length; ++j) {
				i = 31 * i + arrayOfChar[j];
			}
			this.hash = i;
		}
		return i;
	}

	public int indexOf(int paramInt) {
		return indexOf(paramInt, 0);
	}

	public int indexOf(int paramInt1, int paramInt2) {
		int i = this.value.length;
		if (paramInt2 < 0)
			paramInt2 = 0;
		else if (paramInt2 >= i) {
			return -1;
		}

		if (paramInt1 < 65536) {
			char[] arrayOfChar = this.value;
			for (int j = paramInt2; j < i; ++j) {
				if (arrayOfChar[j] == paramInt1) {
					return j;
				}
			}
			return -1;
		}
		return indexOfSupplementary(paramInt1, paramInt2);
	}

	private int indexOfSupplementary(int paramInt1, int paramInt2) {
		if (Character.isValidCodePoint(paramInt1)) {
			char[] arrayOfChar = this.value;
			int i = Character.highSurrogate(paramInt1);
			int j = Character.lowSurrogate(paramInt1);
			int k = arrayOfChar.length - 1;
			for (int l = paramInt2; l < k; ++l) {
				if ((arrayOfChar[l] == i) && (arrayOfChar[(l + 1)] == j)) {
					return l;
				}
			}
		}
		return -1;
	}

	public int lastIndexOf(int paramInt) {
		return lastIndexOf(paramInt, this.value.length - 1);
	}

	public int lastIndexOf(int paramInt1, int paramInt2) {
		if (paramInt1 < 65536) {
			char[] arrayOfChar = this.value;
			int i = Math.min(paramInt2, arrayOfChar.length - 1);
			for (; i >= 0; --i) {
				if (arrayOfChar[i] == paramInt1) {
					return i;
				}
			}
			return -1;
		}
		return lastIndexOfSupplementary(paramInt1, paramInt2);
	}

	private int lastIndexOfSupplementary(int paramInt1, int paramInt2) {
		if (Character.isValidCodePoint(paramInt1)) {
			char[] arrayOfChar = this.value;
			int i = Character.highSurrogate(paramInt1);
			int j = Character.lowSurrogate(paramInt1);
			int k = Math.min(paramInt2, arrayOfChar.length - 2);
			for (; k >= 0; --k) {
				if ((arrayOfChar[k] == i) && (arrayOfChar[(k + 1)] == j)) {
					return k;
				}
			}
		}
		return -1;
	}

	public int indexOf(AString paramString) {
		return indexOf(paramString, 0);
	}

	public int indexOf(AString paramString, int paramInt) {
		return indexOf(this.value, 0, this.value.length, paramString.value, 0,
				paramString.value.length, paramInt);
	}

	static int indexOf(char[] paramArrayOfChar1, int paramInt1, int paramInt2,
			char[] paramArrayOfChar2, int paramInt3, int paramInt4,
			int paramInt5) {
		if (paramInt5 >= paramInt2) {
			return ((paramInt4 == 0) ? paramInt2 : -1);
		}
		if (paramInt5 < 0) {
			paramInt5 = 0;
		}
		if (paramInt4 == 0) {
			return paramInt5;
		}

		int i = paramArrayOfChar2[paramInt3];
		int j = paramInt1 + paramInt2 - paramInt4;

		for (int k = paramInt1 + paramInt5; k <= j; ++k) {
			if ((paramArrayOfChar1[k] == i) || (++k > j)
					|| (paramArrayOfChar1[k] == i))
				;
			if (k <= j) {
				int l = k + 1;
				int i1 = l + paramInt4 - 1;
				for (int i2 = paramInt3 + 1; (l < i1)
						&& (paramArrayOfChar1[l] == paramArrayOfChar2[i2]);) {
					++l;
					++i2;
				}
				if (l == i1) {
					return (k - paramInt1);
				}
			}
		}
		return -1;
	}

//	public int lastIndexOf(AString paramString) {
//		return lastIndexOf(paramString, this.value.length);
//	}

//	public int lastIndexOf(AString paramString, int paramInt) {
//		return lastIndexOf(this.value, 0, this.value.length, paramString.value,
//				0, paramString.value.length, paramInt);
//	}

 

	public AString substring(int paramInt) {
		if (paramInt < 0) {
			throw new StringIndexOutOfBoundsException(paramInt);
		}
		int i = this.value.length - paramInt;
		if (i < 0) {
			throw new StringIndexOutOfBoundsException(i);
		}
		return new AString(this.value, paramInt, i);
	}

	public AString substring(int paramInt1, int paramInt2) {
		if (paramInt1 < 0) {
			throw new StringIndexOutOfBoundsException(paramInt1);
		}
		if (paramInt2 > this.value.length) {
			throw new StringIndexOutOfBoundsException(paramInt2);
		}
		int i = paramInt2 - paramInt1;
		if (i < 0) {
			throw new StringIndexOutOfBoundsException(i);
		}
		return new AString(this.value, paramInt1, i);
	}

 

	public AString concat(AString paramString) {
		int i = paramString.length();
		if (i == 0) {
			return this;
		}
		int j = this.value.length;
		char[] arrayOfChar = Arrays.copyOf(this.value, j + i);
		paramString.getChars(arrayOfChar, j);
		return new AString(arrayOfChar, true);
	}

	public AString trim() {
		int i = this.value.length;
		int j = 0;
		char[] arrayOfChar = this.value;

		while ((j < i) && (arrayOfChar[j] <= ' ')) {
			++j;
		}
		while ((j < i) && (arrayOfChar[(i - 1)] <= ' ')) {
			--i;
		}
		return (((j > 0) || (i < this.value.length)) ? substring(j, i) : this);
	}
 

	public char[] toCharArray() {
		char[] arrayOfChar = new char[this.value.length];
		System.arraycopy(this.value, 0, arrayOfChar, 0, this.value.length);
		return arrayOfChar;
	}

	

//	public static AString valueOf(char[] paramArrayOfChar) {
//		return new AString(paramArrayOfChar);
//	}
//
//	public static AString valueOf(char[] paramArrayOfChar, int paramInt1,
//			int paramInt2) {
//		return new AString(paramArrayOfChar, paramInt1, paramInt2);
//	}
//
//	public static AString copyValueOf(char[] paramArrayOfChar, int paramInt1,
//			int paramInt2) {
//		return new AString(paramArrayOfChar, paramInt1, paramInt2);
//	}
//
//	public static AString copyValueOf(char[] paramArrayOfChar) {
//		return new AString(paramArrayOfChar);
//	}

	

	public native AString intern();
 

	static {
		long l1 = System.nanoTime();
		long l2 = System.currentTimeMillis();
		int[] arrayOfInt1 = { System.identityHashCode(AString.class),
				System.identityHashCode(System.class), (int) (l1 >>> 32),
				(int) l1, (int) (l2 >>> 32), (int) l2,
				(int) (System.nanoTime() >>> 2) };

		int i = 0;

		for (int l : arrayOfInt1) {
			l *= -862048943;
			l = l << 15 | l >>> 17;
			l *= 461845907;

			i ^= l;
			i = i << 13 | i >>> 19;
			i = i * 5 + -430675100;
		}

		i ^= arrayOfInt1.length * 4;

		i ^= i >>> 16;
		i *= -2048144789;
		i ^= i >>> 13;
		i *= -1028477387;
		i ^= i >>> 16;

		HASHING_SEED = i;
	}

	private static class CaseInsensitiveComparator implements
			Comparator<AString>, Serializable {
		private static final long serialVersionUID = 8575799808933029326L;

		public int compare(AString paramString1, AString paramString2) {
			int i = paramString1.length();
			int j = paramString2.length();
			int k = Math.min(i, j);
			for (int l = 0; l < k; ++l) {
				char c1 = paramString1.charAt(l);
				char c2 = paramString2.charAt(l);
				if (c1 != c2) {
					c1 = Character.toUpperCase(c1);
					c2 = Character.toUpperCase(c2);
					if (c1 != c2) {
						c1 = Character.toLowerCase(c1);
						c2 = Character.toLowerCase(c2);
						if (c1 != c2) {
							return (c1 - c2);
						}
					}
				}
			}
			return (i - j);
		}
	}

	public AString Left(int i) {
		 
		return new AString(this.raw_obj.substring(0,i)) ;
	}
	public    String		 toString(  )
	{
		return  this.raw_obj;
	}
	public  static String	 Mid(String string,int  start, int length)
	{
		return string.substring(start, start+length);
	}
	
	public  static String		 LTrim( String charx)
	{
		return charx;
		//return  new string.trim();
	}
	public  static String		 RTrim( String string)
	{
		return string.trim();
	}
	public  static String		 Space( int  number)
	{
		String r="";
		for(int i=0;i<number;i++)
		{
			r=r+" ";
		}
		return  r;
	}
	  
	public  static String   Left(String string, int length)
	
	{
		return string.substring(0,length);
		
	}
	
public  static String   Right(String string, int length)
	
	{
	return string.substring(string.length()-length);
	}
public  static String   LCase(String string )

{
	return string.toLowerCase();
}
public  static String   UCase(String string )

{
	return string.toUpperCase();
}
	
/**
 *     Join(list, delimiter)
	    返回一个字符串，此字符串由包含在数组中的许多子字符串联接创建。
	    list：必选。包含要联接的子字符串一维数组。 
	    delimiter：可选。在返回字符串中用于分隔子字符串的字符。如果省略，将使用空字符 ("")。 如果 delimiter 是零长度字符串，则在同一列表中列出全部项，没有分界符。
	    
 * @param list
 * @param string
 * @return
 */
public  static String   Join(String[] list,String string )

{
	return string; 
 
}
	
/**
 *     返回字符串，此字符串与指定字符串顺序相反。
	    string：参数是要进行字符反向的字符串。如果 string 是零长度字符串 ("")，则返回零长度字符串。如果 string1 为 Null，则会出现错误。
 * @param string
 * @return
 */
public    String   StrReverse( String string )

{
	return string; 
 
}


public   String nextChar(String baseName) {
	byte[] ca=baseName.getBytes();
	int now=ca[0];
	now++;
	char c=(char) now;
	
	return String.valueOf(c);
}


public static String getRandomString(int length) { //length表示生成字符串的长度
    String base = "abcdefghijklmnopqrstuvwxyz0123456789";   
    Random random = new Random();   
    StringBuffer sb = new StringBuffer();   
    for (int i = 0; i < length; i++) {   
        int number = random.nextInt(base.length());   
        sb.append(base.charAt(number));   
    }   
    return sb.toString();   
 }  
public    String   random( int len )
{
//	String string;
	return getRandomString(len); 
 
}




public static String LTrim(String lastDir, String charx) {
	 String first=lastDir.substring(0, 1);
	 if(first.equals(charx))
		 
	return lastDir.substring(1);
	 return lastDir;
}
/**
attilax    2016年4月25日  下午5:10:06
 * @return
 */
public  String  toSysStr() {
	 
	return this.raw_obj;
}
}