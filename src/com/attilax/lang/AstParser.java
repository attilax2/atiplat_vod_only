package com.attilax.lang;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.attilax.fsm.JavaExpFsm;
import com.attilax.json.AtiJson;
import com.attilax.net.UrlEncode_del;
import com.google.common.collect.Lists;

public class AstParser {
	public Object obj;
	public Object rzt;

	public static void main(String[] args) throws UnsupportedEncodingException {

		System.out
				.println(URLEncoder
						.encode("new(com.attilax.util.connReduceDync).set_resfile(userPhone4jobusImp/uc_js.txt).joinNoutV2()",
								"utf8"));
		
		
		String s="new(com.attilax.orm.AtiOrmV2).queryAsRzt(\"select sum(rmb) sumx from recharge where accountId in (  select id as uid from account where promoter=888 ) \")";
	
		
		s=" new(com.attilax.agent.AgentRechargeService).getSubMemTotalsRecycleByAgentId(\"promoter:$pid$,fld2:v2\")";
		s="new(com.attilax.user.AgentService).login(admin,admin)";
		System.out.println( new AstParser().getExprsLiAst(s));
		
	//	"select sum(rmb) sumx from recharge where accountId in (  select id as uid from account where promoter=888 )"
		System.out.println("..");
		// System.setProperty("prj","jobus");
		// String
		// code="new(com.attilax.util.connReduceDync).set_resfile(userPhone4jobusImp/uc_js.txt).joinNoutV2() ".trim();
		// Ast astParser = new Ast();
		// List ast=astParser.getExprsLiAst(code);
		// System.out.println(AtiJson.toJson( astParser.parse(ast)));
	//	s="login(admin,admin)";

	}

	// public Object parse;
	public Object parse(List ast) {
		for (Object object : ast) {
			// String exp=(String) object;
			parseSingle(object);
		}
		return this.rzt;
	}

	private void parseSingle(Object exp) {
		Exprs e = (Exprs) (exp);
		e.obj = this.obj;

		this.rzt = e.calc();
		this.obj = e.obj;

	}

	@SuppressWarnings("all")
	public List getExprsLiAst(String code) {

		List li = new ArrayList();

		List tokens = new JavaExpFsm(code).getTokens();
		System.out.println(AtiJson.toJson(tokens));
		List<String> tokens_slice_li = Lists.newLinkedList();
		for (int i = 0; i < tokens.size(); i++) {
			
			// last
			// dot is expres splitor..if cur pos of tokens  is dot 
			if (tokens.get(i).toString().trim().equals(".")
					) {
				Exprs e = getExprsFrmTokenslice(tokens_slice_li);
				li.add(e);
			
				tokens_slice_li = Lists.newLinkedList();
				
				continue;
			}
			if( i == tokens.size()-1)
			{
				tokens_slice_li.add((String) tokens.get(i));
				Exprs e = getExprsFrmTokenslice(tokens_slice_li);
				li.add(e);
				break;
			}
		
			tokens_slice_li.add((String) tokens.get(i));

		}

		// Collections.addAll(li, a);
		return li;
	}

	private Exprs getExprsFrmTokenslice(List<String> tokens_slice_li) {
		Exprs e = new Exprs();
		
//		if (tokens_slice_li.size() == 5) {
//		
//		}
//		if (tokens_slice_li.size() == 4) {
//			e.method = (String) tokens_slice_li.get(0);
//			String params = tokens_slice_li.get(2);
//		//	e.params = params;  //e.parseParams(params.toString());
//			e.params = Lists.newLinkedList();
//			e.params.add(params);
//			//q716 must as a params..
//			//'new(com.attilax.agent.AgentRechargeService).getSubMemTotalsRecycleByAgentId("promoter:$pid$,fld2:v2")';
//			
//		}
		// last    log() mode ,empty param mode 
		if (tokens_slice_li.size() == 3) {
			e.method = (String) tokens_slice_li.get(0);
			e.params = Lists.newLinkedList();
		}else
		{		
			e.method = (String) tokens_slice_li.get(0);		 
			e.params = parseParams(tokens_slice_li);
		}

		return e;
	}

	@Deprecated
	public List parseParams(String pa_str) {
		String[] a=pa_str.split(",");
		List    li=	new ArrayList();
		Collections.addAll(li, a);
		return li;
	}
	/**
	attilax    2016年9月2日  下午12:12:01
	 * @param tokens_slice_li
	 * @return
	 */
	public List parseParams(List<String> tokens_slice_li) {
		List    li=	new ArrayList();
		for(int i=2;i<tokens_slice_li.size()-1;i++)
		{
			li.add(tokens_slice_li.get(i));
		}
		return li;
	}

}
