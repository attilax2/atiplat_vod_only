package com.attilax.lang;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.reflect.MethodUtils;

import com.attilax.exception.ExUtil;
import com.attilax.ioc.IocFacV3_iocx_iocutil;
import com.attilax.ioc.IocUtilV2;
import com.attilax.json.AtiJson;
import com.attilax.ref.refx;

public class Exprs {

	public String method;
	public List params;
	public Object obj;
	public Object rzt;
	public Exprs(String exp) {
		   int idx=exp.indexOf("(");
		   this.method=exp.substring(0, idx);
		   String pa_str=exp.substring(idx,exp.length()-1);
		   this.params=parseParams( pa_str );
	}

	public Exprs() {
		// TODO Auto-generated constructor stub
	}

	public List parseParams(String pa_str) {
		String[] a=pa_str.split(",");
		List    li=	new ArrayList();
		Collections.addAll(li, a);
		return li;
	}
	
	public String toString()
	{
		return AtiJson.toJson(this);
	}

	@SuppressWarnings("unchecked")
	public Object calc() {
		if(this.method.equals("new"))
		{
			if(Global.iocCurV2.get()!=null)
			{
				this.obj =  IocFacV3_iocx_iocutil.getBean( Global.iocCurV2.get(), this.params.get(0).toString());
			}else
				
			this.obj =  IocUtilV2.getBean(this.params.get(0).toString());
			return this.obj;
		}
		if(this.method.startsWith("set_"))
		{
			String prop=this.method.substring(4);
//			try {
//				BeanUtils.copyProperty(this.obj, prop, this.params.get(0));
//			} catch (IllegalAccessException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (InvocationTargetException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
				
				//todox q55  jeig ok ..
				refx.setPropertySafe(this.obj, prop, this.params.get(0));
				return this.obj;
			 
			
		}
		if(this.method.equals("exe"))
			System.out.println("dbg");
		Object invokeMethod_ret = null;
		if(this.method.toLowerCase().contains("nout"))
			System.out.println("Dbg");
		
		
		if(this.method.equals("getSubAgentTotal"))
			System.out.println("dbg");
		if(this.params.size()>0)  //q66
		{
		
			try {
				invokeMethod_ret =  MethodUtils.invokeMethod(this.obj, this.method, 	(Object[]) params.toArray(new Object[params.size()]));
			}catch(NoSuchMethodException e)
			{
				try {
					invokeMethod_ret = 	com.attilax.reflect.MethodUtils.invokeMethod(this.obj, this.method, 	(Object[]) params.toArray(new Object[params.size()]));
				} catch (Exception e1) {
					ExUtil.throwExV2(e1);
				}
			}
			
			catch ( IllegalAccessException
					| InvocationTargetException e) {
			 
				ExUtil.throwExV2(e);
			}
			//invokeMethod_ret = com.attilax.reflect.MethodUtils.invokeMethod(this.obj, this.method,
				//	new Object[0] );
			return  invokeMethod_ret;
		}
		
		
		try {
			
			invokeMethod_ret = com.attilax.reflect.MethodUtils.invokeMethod(this.obj, this.method,
					new Object[0] );
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return invokeMethod_ret;
		
	}

}
