package com.attilax.lang;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.attilax.core;
import com.attilax.cc.mainx;
import com.attilax.io.pathx;
import com.attilax.lang.process.ProcessX;
import com.attilax.util.PropX;

/**
 * @description Robot帮助类，实现基本的功能
 * @author Alexia
 * @date 2013/5/18
 *
 */
public class GUIx {
	
	public static void main(String[] args) throws AWTException {
	
	 	activeWin();
		System.out.println("PowerDVD.exe aaa".contains("PowerDVD.exe"));
		
		  
	}

	public static void hideMouseMulti() {
		core.execMeth_Ays(new Runnable() {

			@Override
			public void run() {
				 
					while (true) {
						try {
							Thread.sleep(30000);
							hideMouse();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					//END WHILE

			 

			}
		}, "");

	}
	public static void hideMouse()
	{
		try {
			  Robot rb = null;
				try {
					rb = new Robot();
				} catch (AWTException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					return;
				}
				  int x = 100;
				  try {
						x=Integer.parseInt( PropX.getConfig(pathx.classPath()+"/cfg.properties", "x"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				int y = 100;
				  try {
						y=Integer.parseInt( PropX.getConfig(pathx.classPath()+"/cfg.properties", "y"));
				} catch (Exception e) {
					// TODO: handle exception
				}
				 rb.mouseMove(x, y);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * 
	 * 
	 * //				  int x = 100;
//				  try {
//						x=Integer.parseInt( PropX.getConfig(pathx.classPath()+"/cfg.properties", "x"));
//				} catch (Exception e) {
//					// TODO: handle exception
//				}
//				int y = 100;
//				  try {
//						y=Integer.parseInt( PropX.getConfig(pathx.classPath()+"/cfg.properties", "y"));
//				} catch (Exception e) {
//					// TODO: handle exception
//				}
				//rb.mouseMove(x, y);
	 * 
	 * @throws AWTException
	 */
	public static void activeWin() throws AWTException {
		while(true)
		{
			try {
				boolean existPower=false;
				try {
					String playserver=PropX.getConfig(pathx.classPath()+"/cfg.properties", "actsleep");
					int actsleep=3000;
					try {
						actsleep=Integer.parseInt(playserver);
					} catch (Exception e) {
						// TODO: handle exception
					}
				start:	Thread.sleep(actsleep);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				List<String> set=ProcessX.	getProcessList();
			//	List<String> lst=
				for (String line : set) {
					if(line.contains("PowerDVD.exe") || line.contains("PowerDVDMovie"))
					{
					System.out.println("exist dvd ");
					existPower=true;
						break ;
					}
				}
				if(existPower)   // if sexist power dvd ,,b act select win..
					continue;
				System.out.println("---no exist dvd");
				  final Robot rb = new Robot();

				  clickMidKeyMouse(rb,50);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		
		}
	}

    /**
     * 鼠标单击（左击）,要双击就连续调用
     * 
     * @param r
     * @param x
     *            x坐标位置
     * @param y
     *            y坐标位置
     * @param delay
     *            该操作后的延迟时间
     */
    public static void clickLMouse(Robot r, int x, int y, int delay) {
        r.mouseMove(x, y);
        r.mousePress(InputEvent.BUTTON1_MASK);
        r.delay(10);
        r.mouseRelease(InputEvent.BUTTON1_MASK);
        r.delay(delay);

    }

    /**
     * 鼠标右击,要双击就连续调用
     * 
     * @param r
     * @param x
     *            x坐标位置
     * @param y
     *            y坐标位置
     * @param delay
     *            该操作后的延迟时间
     */
    public static void clickRMouse(Robot r, int x, int y, int delay) {
        r.mouseMove(x, y);
        r.mousePress(InputEvent.BUTTON3_MASK);
        r.delay(10);
        r.mouseRelease(InputEvent.BUTTON3_MASK);
        r.delay(delay);

    }
    
    public static void clickRMouse(Robot r,int delay) {
      //  r.mouseMove(x, y);
        r.mousePress(InputEvent.BUTTON3_MASK);
        r.delay(10);
        r.mouseRelease(InputEvent.BUTTON3_MASK);
        r.delay(delay);

    }
    
    
    public static void clickMidKeyMouse(Robot r,int delay) {
        //  r.mouseMove(x, y);
          r.mousePress(InputEvent.BUTTON2_MASK);
          r.delay(10);
          r.mouseRelease(InputEvent.BUTTON2_MASK);
          r.delay(delay);

      }

    /**
     * 键盘输入（一次只能输入一个字符）
     * 
     * @param r
     * @param ks
     *            键盘输入的字符数组
     * @param delay
     *            输入一个键后的延迟时间
     */
    public static void pressKeys(Robot r, int[] ks, int delay) {
        for (int i = 0; i < ks.length; i++) {
            r.keyPress(ks[i]);
            r.delay(10);
            r.keyRelease(ks[i]);
            r.delay(delay);
        }
    }

    /**
     * 复制
     * 
     * @param r
     * @throws InterruptedException
     */
    void doCopy(Robot r) throws InterruptedException {
        Thread.sleep(3000);
        r.setAutoDelay(200);
        r.keyPress(KeyEvent.VK_CONTROL);
        r.keyPress(KeyEvent.VK_C);
        r.keyRelease(KeyEvent.VK_CONTROL);
        r.keyRelease(KeyEvent.VK_C);
    }

    /**
     * 粘贴
     * 
     * @param r
     * @throws InterruptedException
     */
    void doParse(Robot r) throws InterruptedException {
        r.setAutoDelay(500);
        Thread.sleep(2000);
        r.mouseMove(300, 300);
        r.mousePress(InputEvent.BUTTON1_MASK);
        r.mouseRelease(InputEvent.BUTTON1_MASK);
        r.keyPress(KeyEvent.VK_CONTROL);
        r.keyPress(KeyEvent.VK_V);
        r.keyRelease(KeyEvent.VK_CONTROL);
        r.keyRelease(KeyEvent.VK_V);
    }

    /**
     * 捕捉全屏慕
     * 
     * @param r
     * @return
     */
    public Icon captureFullScreen(Robot r) {
        BufferedImage fullScreenImage = r.createScreenCapture(new Rectangle(
                Toolkit.getDefaultToolkit().getScreenSize()));
        ImageIcon icon = new ImageIcon(fullScreenImage);
        return icon;
    }

    /**
     * 捕捉屏幕的一个矫形区域
     * 
     * @param r
     * @param x
     *            x坐标位置
     * @param y
     *            y坐标位置
     * @param width
     *            矩形的宽
     * @param height
     *            矩形的高
     * @return
     */
    public Icon capturePartScreen(Robot r, int x, int y, int width, int height) {
        r.mouseMove(x, y);
        BufferedImage fullScreenImage = r.createScreenCapture(new Rectangle(
                width, height));
        ImageIcon icon = new ImageIcon(fullScreenImage);
        return icon;
    }

}