package com.attilax.lang;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Maps;

/**
 * com.attilax.lang.Global.*
 * @author Administrator
 *
 */
public class Global {
	/**
	 * main cfg file
	 */
	 
	public static Map<String, Object> propMap = new HashMap<String, Object>();
	public static Map  globalMap=Maps.newLinkedHashMap();
	public static ThreadLocal<String> mainCfg=new ThreadLocal<>();
	public static ThreadLocal<String> dbName=new ThreadLocal<>();
	public static String iocCur;
	public static ThreadLocal<String> iocCurV2=new ThreadLocal<>();
	 
	{
		//mainCfg.set("com.attilax.$def");
	}
	public static Map vars=Maps.newLinkedHashMap();
	public static ThreadLocal<Map> map=new ThreadLocal<>();
	{
	 	map.set(Maps.newLinkedHashMap());
	}
	public static ThreadLocal<HttpServletRequest> req=new ThreadLocal<>();
	public static ThreadLocal< HttpServletResponse> resp=new ThreadLocal<>();
	public static  ThreadLocal<String> blogName=new ThreadLocal<>();
	
	
	public static Object $GLOBALS(String key)
	{
	return	globalMap.get(key);
	}
	public static void $GLOBALS(String key,Object v)
	{
	 	globalMap.put(key, v);
	}

}
