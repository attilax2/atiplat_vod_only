package com.attilax.lang;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.attilax.atian.PinyinX;
import com.focusx.elmt.GvMaterial;

public class Trigger4elemt extends Trigger {
	
	public static void main(String[] args) {
	///	TriggerChain.addTrigger(HttpServletRequest,new Trigger4elemt());
		HttpSession ss = null;
		TriggerChain.addTrigger(ss,"elmt",new Trigger4elemt());
	}
	public void exec(Object object) {
	     GvMaterial gvm=(GvMaterial) object;
	     
	     if(gvm.getFilePath().contains(":"))
	    	 gvm.setFilePath(gvm.getFilePath().substring(3));
		//	ftp = ftp.substring(3);
	     if(gvm.getThumb().contains(":"))
	    	 gvm.setThumb(gvm.getThumb().substring(3));
	     
	     if(gvm.getTxt_file().contains(":"))
	    	 gvm.setTxt_file(gvm.getTxt_file().substring(3));
	     
	     
	     gvm.setMaterialType(gvm.getApplicationType());
	 	String pinyin = PinyinX.getSimple(gvm.getMaterialDescription());
	 	gvm.setMaterial_keyword(pinyin);
	 	if(gvm.getMaterialId()!=null)
	 		if(gvm.getMaterialId().equals(0))
	 			gvm.setMaterialId(null);
	}
}
