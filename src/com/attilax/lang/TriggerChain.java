package com.attilax.lang;

import javax.servlet.http.HttpSession;


/**
 *  com.attilax.lang.TriggerChain
 * @author Administrator
 *
 */
public class TriggerChain {
	
public static	ThreadLocal<Trigger> trigr=new ThreadLocal<Trigger>();

public static void main(String[] args) {
	TriggerChain.trigr.set(new Trigger4elemt());
	
	trigr.get().exec(new Object());
}

public static void addTrigger(HttpSession ss, String string,
		Trigger trigger4elemt) {
	 ss.setAttribute(string, trigger4elemt);
	
}

public static Trigger getTrigger(HttpSession ss, String string 
		 ) {
	return (Trigger) ss.getAttribute(string);
	
}

}
