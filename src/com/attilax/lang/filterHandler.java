package com.attilax.lang;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import aaaCfg.IocX;

import com.attilax.io.pathx;

public abstract class filterHandler {

	public abstract void doFilter(Object args, FilterChain nextChain) ;
	
	/**
	 * utf then gbk
	 * @param url2
	 * @return
	 */
	public File getFile(String url2) {
		
		url2=deMapDir(url2);
		
		
		
		//--------get file by utf8
		String url_decode = null;
		try {//
			url_decode = URLDecoder.decode(url2, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		 String file_server_DiskChar =	(String) Global.map.get().get("FileSeverDir");		
		String f =file_server_DiskChar + File.separator + url_decode;
		f=pathx.fixSlash2reversSplash(f);
		File file = new File(f);
		
		
		
		/////////////////then gbk
		boolean fas = file.exists();
		if (!fas) {
			f = getImgPath_gbk(url2,file_server_DiskChar);
			file = new File(f);

		}
		fas = file.exists();
		return file;
	}
//	public String getFileSeverDir_diskChar() {
//		String file_server_DiskChar="";//	 (String) IocX.cfgMap.get("uploadfiles");
//		 //ati p820 def pic server
//		 if( file_server_DiskChar==null || !new File(file_server_DiskChar).exists() )
//		 {
//				file_server_DiskChar="d:\\z";
//				if(!new File(file_server_DiskChar).exists())
//					file_server_DiskChar=pathx.webAppPath();
//		 }
//		return file_server_DiskChar;
//	}
	protected String deMapDir(String url_decode) {
		int idx2 = url_decode.indexOf("/");
		String url2 =url_decode.substring(idx2 + 1); // de webapp
		return url2;
	}

	protected String getImgPath_gbk(String url2, String file_server_DiskChar) {
		String url_decode_gbk;
		try {
			url_decode_gbk = URLDecoder.decode(url2, "gbk");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	 
		String f =file_server_DiskChar + File.separator + url_decode_gbk;
		return f;
	}

}
