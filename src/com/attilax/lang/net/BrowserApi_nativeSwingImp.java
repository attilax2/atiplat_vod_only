package com.attilax.lang.net;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.apache.commons.lang3.reflect.MethodUtils;

import aaaBlogger.EncodeX;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import com.attilax.core;
import com.attilax.lang.ParamX;
import com.attilax.ref.refx;

public class BrowserApi_nativeSwingImp {

	private JWebBrowser webBrowser;

	public BrowserApi_nativeSwingImp(JWebBrowser webBrowser) {
		this.webBrowser = webBrowser;
	}
	@SuppressWarnings("all")
	public void exe(Object[] parameters) {
	
		Map m = new ParamX().urlParams2Map((String) parameters[0]);
		String meth = (String) m.get("$method");
		//$method=aaaCms.CmsImpLocalFileVer.list&
		//$callback=cate_click_callback&http_param=select++*+from+gv_material+where+material_type+%3D+6 order+by++convert%28material_description+using+gbk%29 
		//&param=6
		String classname = refx.getClassName(meth);

		String meth_name = refx.getMethodName(meth);
		String callback_fun = (String) m.get("$callback");

		Object o;

		try {
			o = ConstructorUtils.invokeConstructor(

			Class.forName(classname), null);
			String ret = (String) MethodUtils.invokeMethod(o, meth_name,
					parameters[0]);
			System.out.println("");
			ret = EncodeX.jsEncodeSingleQuoue(ret);
			String javascript = callback_fun + "('" + ret + "')";
			System.out.println("executeJavascript:"+javascript);
			webBrowser.executeJavascript(javascript);
		} catch (Throwable e) {
			String string = EncodeX.jsEncodeSingleQuoue(core.toJsonStrO88(e));
			webBrowser.executeJavascript(callback_fun + "('" + string + "')");

		}
//		RuntimeException re = new RuntimeException("calljava.refImp() err ");
//		String string = EncodeX.jsEncodeSingleQuoue(core.toJsonStrO88(re));
//		webBrowser.executeJavascript(callback_fun + "('" + string + "')");
		// return core.toJsonStrO88(new
		// RuntimeException("calljava.refImp() err ") );

	}

	private Object refImp(String meth, List li) {
		String classname = refx.getClassName(meth);

		String meth_name = refx.getMethodName(meth);

		Object o;

		try {
			o = ConstructorUtils.invokeConstructor(

			Class.forName(classname), null);
			return MethodUtils.invokeMethod(o, meth_name, li.get(0));
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {

			return core.toJsonStrO88(e);

		}

		return core
				.toJsonStrO88(new RuntimeException("calljava.refImp() err "));

	}

}
