package com.attilax.lang.ui;

import java.awt.Cursor;
import java.awt.Frame;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.attilax.shbye.YejmXGUI;

public class Swingx {

	public static void setBackgroudImg(JFrame frame, String string) {
		//set bg pic 
		  ((JPanel) frame.getContentPane()).setOpaque(false);
		  ImageIcon img = new ImageIcon(string); // 添加图片
	        JLabel lblPicture = new JLabel(img);
	        frame.getLayeredPane().add(lblPicture, new Integer(Integer.MIN_VALUE));
	        lblPicture.setBounds(0, 0, img.getIconWidth(), img.getIconHeight());
	        
	        
	    //    rolloverIcon:

	     //   	鼠标经过按钮时显示图标对象
		
	}

	public static void NoTitleNboder(JFrame frame) {
		frame.setUndecorated(true);
		
	}

	public static void setBorderNull(JComponent object) {
		object.setBorder(BorderFactory.createEmptyBorder());
		
	}

	public static void setTransBtn(JButton btnNewButton) {
		//	btnNewButton.setFocusPainted(false);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
 	// 	btnNewButton.setIcon(new ImageIcon(pathx.classPath(IocX.class)+"\\transp4.png"));
		btnNewButton.setContentAreaFilled(false);
		
	}

	public static void miniWin(JFrame frame) {
		frame.setExtendedState(Frame.ICONIFIED);
		
	}

	public static void dragWin(JFrame frame) {
	new 	drager().dragWin(frame);
		
	}

	public static void midShow(JFrame frame) {
		frame.setLocationRelativeTo(null);
		
	}

}
