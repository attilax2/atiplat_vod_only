package com.attilax.lang.xml;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.thoughtworks.xstream.XStream;

public class XmlX {
	
	public static void main(String[] args) {
		Map m=new HashMap();
		m.put("img", "jpg..");
		Map m2=new HashMap();
		m2.put("img", "jpg22..");
		List li=new ArrayList();
		li.add(m);
		li.add(m2);
 	System.out.println(toXml2(li,"data","item"));
	//	System.out.println(toXml(li));
	}

	/**
	 * @param li
	 * @param rootListNodeName
	 * @param itemMapNodeName
	 * @return
	 */
	public static String toXml2(List li, String rootListNodeName, String itemMapNodeName) {
		//创建根节点...
		Element root = new Element(rootListNodeName);
		//将根节点添加到文档中...
		Document Doc  = new Document(root);
		for(int i = 0; i < li.size(); i++){
			
			Map m=(Map) li.get(i);
			//创建各种类水果的节点...
			Element Map_elements = new Element(itemMapNodeName);
			Set<Entry> entrySet=m.entrySet();
			for (Entry e : entrySet) {
				//.setAttribute(e.getKey().toString(), e.getValue().toString())
				//给各种水果节点加子节点...比如价格...
			//	Map_elements.add
				String val="";
				if(e.getValue()!=null)
				{
					
					val=e.getValue().toString();
				}
				Map_elements.setAttribute(e.getKey().toString(), val);
				Map_elements.addContent(new Element(e.getKey().toString()).setText( val));
			}
			
			root.addContent(Map_elements);
			
			
		}
		
		XMLOutputter XMLOut = new XMLOutputter(FormatXML());
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		try {
			XMLOut.output(Doc,bo);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw  new RuntimeException(e1);
		}
		String xmlStr;
		try {
			xmlStr = bo.toString("utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();throw  new RuntimeException(e);
		}
		
		return xmlStr;
	}
	public static Format FormatXML(){
		//格式化生成的xml文件，如果不进行格式化的话，生成的xml文件将会是很长的一行...
		Format format = Format.getCompactFormat();
		format.setEncoding("utf-8");
		format.setIndent(" ");
		return format;
	}
	
	/**
	 * <list>
  <map>
    <entry>
      <string>img</string>
      <string>jpg..</string>
    </entry>
  </map>
  <map>
    <entry>
      <string>img</string>
      <string>jpg22..</string>
    </entry>
  </map>
</list>

	 * @param easyuiFmtRzt
	 * @return
	 */

	public static String toXml(Object easyuiFmtRzt) {
		  XStream xstream = new XStream(); 
//	        xstream.alias("address", Address.class); 
//	        xstream.alias("addresses", Addresses.class); 
//	        xstream.alias("person", Person.class); 
//	        xstream.alias("persons", Persons.class); 
	        String xml = xstream.toXML(easyuiFmtRzt); 
		return xml;
	}

}
