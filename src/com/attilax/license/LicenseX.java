package com.attilax.license;

import java.util.Date;





import java.util.Map;
import java.util.prefs.Preferences;

import com.attilax.io.pathx;
import com.attilax.net.websitex;
import com.attilax.util.CstGettor;
//import com.attilax.laisens.CstGettor;
//import com.attilax.laisens.OverTimeEx;
import com.attilax.util.DateUtil;
import com.attilax.util.PropX;
import com.attilax.util.randomx;

/**
 * com.attilax.license.LicenseX
 * @author Administrator
 *
 */
public class LicenseX {

	public static void main(String[] args) throws OverTimeEx {
		// TODO Auto-generated method stub
//	new LicenseX().	checkOvertime("2015-03-12",CstGettor.getDateFrmNet());

		LicenseX licenseX = new LicenseX();
		System.out.println(licenseX.getPCid());
		 
		System.out.println( licenseX.isCanUse_byUsePercent("2016-05-01") );
		
		
	
	}
/**
	attilax    2016年9月6日  下午5:48:55
 * @param d 
	 * @return
	 */
	private int usePercent(String last_check_time) {
		//  String d="2016-09-01";
			 Date d= DateUtil.str2date(last_check_time, false);
			 if(new Date().getTime()<=d.getTime())
				 return 100;
			 int dayInterval = DateUtil.getDayInterval(d,new Date());
			 if(dayInterval>100) 
				 return 0;
			 dayInterval=100-dayInterval;
			return  dayInterval ;
			   
	}
	
	public boolean isCanUse_byUsePercent(String last_check_time) {
		 int use=usePercent(last_check_time);
		int rdm= randomx.random(1, 100);
		if(rdm<=use)
			return true;
		else
			return false;
	}
	//	public static void main(String[] args) throws OverTimeEx {
//		checkOvertime("2015-03-12",CstGettor.getDateFrmNet());
//		System.out.println("---");
//	}
	@Deprecated
	public   boolean isOvertime(String date_s) {
	    Date d= DateUtil.str2date(date_s, false);
	    if(d.getTime()<new Date().getTime())return true;
		return false;
	}
	/**
	 * 
	 * @param date_s   yyyy_mm_dd
	 * @return
	 * @throws OverTimeEx 
	 */
	public   void checkOvertime(String date_s,Date dt) throws OverTimeEx {
	    @SuppressWarnings("deprecation")
		Date d= DateUtil.str2date(date_s, false);
	    if(dt.getTime()>d.getTime()) 
	    	throw new OverTimeEx(" OverTimeEx");
	}
	
	public   void checkOvertime_frmRmt() throws OverTimeEx {
		Preferences pref = Preferences.userRoot().node(
				this.getClass().getName());
		String last_check_time=pref.get("now_check_time", "2014-01-01");
		if(isOvertime_3month(last_check_time))
		{
			try {
				   String pcid=getPCid();
				    PropX px=new PropX(pathx.webAppPath()+"/0cfg/cfg.properties");
				    String url=px.getProperty("timer_check_authcode_url");
				    url=new ParamX().varReplace(url, px.getValList(), "@");
				    String now_check_time=websitex.WebpageContent(url);		
					pref.put("now_check_time", now_check_time);
			} catch (Exception e) {
					throw  new OverTimeEx(e.getMessage());
			}
		 
		}
	}
	
	/**
	 * com.attilax.license.LicenseX.calcSn
	 * @param machi_id
	 * @return
	 */
	public String calcSn(String machi_id)
	{
		return "sn_"+machi_id;
	}
	
	public String calcSn_map(Map m)
	{
		return calcSn((String)m.get("machi_code"));
	}
	
	private boolean isOvertime_3month(String last_check_time) {
		 Date d= DateUtil.str2date(last_check_time, false);
		 if( DateUtil.getDayInterval(d,new Date())>90)
		   return true;
		return false;
	}
	public String getPCid()
	{
		
	return	HardWareUtils.getCPUSerial()+HardWareUtils.getMotherboardSN();
	}

}
