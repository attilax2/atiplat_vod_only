package com.attilax.license;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.attilax.core;
import com.attilax.lang.Closure2;
import com.attilax.text.regExpress;
import com.attilax.text.strUtil;

import static com.attilax.text.strUtil.*;

public class ParamX {

	public static void main(String[] args) {
	String s="boxsys=http://192.168.0.111/lime/com.attilax.php/lang/api.php?iocx=iocx_timer&method=setTime&no=1&timex=@timex&movie=@movie;";
Map m=new HashMap(){
	{
		this.put("movie","aaaaaaaaa");
		this.put("timex","6000");
	}
};
//System.out.println("----:"+  core.toJsonStrO88( new ParamX().findSqlParam(s))  );
	System.out.println( new ParamX(). sqlFmt(s,m));
	}



	private String sql;
	
	
	public List<String> findSqlParam(String sql)
	  {
		
		List<String> li=strUtil.  preg_match_all(regExpress.ParamNameExpress, sql);
		    //  print_r( $a);
		//     $GLOBALS["varsx"]["sql params"]=$a;
		  return li;
	  }
	  
	public String sqlFmt(String sql,Map s_GET)
	  {
		  List<String> params=findSqlParam(sql);
		  for (String p : params) {
			  String http_para_name= str_replace("@","",p);
			  if(s_GET.get(http_para_name) !=null) {
				String string = s_GET.get(http_para_name).toString();
				sql=str_replace(p,string,sql);
			}
			
		}
//		  foreach( $params as $p)
//		  {
//			
//		  }
		  return sql;  
		  
	  }
	
	/**
	 * 
	 * @param sql
	 * @param s_GET
	 * @param varHolder
	 * @return
	 */
	public String sqlFmt_percentChar(String sqlx,Map s_GET,final String varHolder_regExpress)
	  {
		this.sql=sqlx;
		  List<String> params=(List<String>) new Closure2() {

			@Override
			public Object execute(Object arg0) {
			
				List<String> li=strUtil.  preg_match_all(varHolder_regExpress, sql);
				return li;
			}
		}.execute(sql);
		  for (String p : params) {
			  String http_para_name= str_replace("%","",p);
			  if(s_GET.get(http_para_name) !=null) {
				String string = s_GET.get(http_para_name).toString();
				sql=str_replace(p,string,sql);
			}
			
		}
 
		  return sql;  
		  
	  }
	
	
	public String sqlFmt(String sqlx,Map s_GET,final String varCloserHolder)
	  {
		this.sql=sqlx;
		  List<String> params=(List<String>) new Closure2() {

			@Override
			public Object execute(Object arg0) {
				String reg_exp=regExpress.ParamNameExpress_containByPercentChar.replace("%", varCloserHolder);
				List<String> li=strUtil.  preg_match_all(reg_exp, sql);
				return li;
			}
		}.execute(sql);
		  for (String p : params) {
			  String http_para_name= str_replace(varCloserHolder,"",p);
			  if(s_GET.get(http_para_name) !=null) {
				String string = s_GET.get(http_para_name).toString();
				sql=str_replace(p,string,sql);
			}
			
		}

		  return sql;  
		  
	  }



	public Map urlParams2Map(String params) {
		Map o=new HashMap();
		String[] a=params.split("&");
		for(int i=0;i<a.length;i++)	
		{
			String itemx=a[i];
			String[] a2=itemx.split("=");
			String k=a2[0];
			String v = null;
			try {
				v = URLDecoder.decode(a2[1], "utf-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		o.put(k, v);
		}
		return o;
		 
	}

	public String varReplace(String url, Map valList, String string) {
		// TODO Auto-generated method stub
		return sqlFmt(url,valList,string);
	}

}
