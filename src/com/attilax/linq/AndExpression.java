package com.attilax.linq;

import java.util.Map;

public class AndExpression extends AExpression {

	public AndExpression(AExpression whereExpressAst, AExpression eq) {
		this.left=whereExpressAst;
		this.rit=eq;
	}
	
	public boolean checkSuitOk(Map row) {
		//	LikeExpression le=(LikeExpression) whereExpressAst;
		AExpression lft_exp=(AExpression) this.left;
		AExpression rit_exp=(AExpression) this.rit;
		return  lft_exp.checkSuitOk(row) && rit_exp.checkSuitOk(row);
		}

}
