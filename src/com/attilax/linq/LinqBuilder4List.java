package com.attilax.linq;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
@SuppressWarnings("all") 
public class LinqBuilder4List  extends LinqBuilder{

	public LinqBuilder4List(Object table, String op, AExpression whereExpressAst) {
		this.table=table;this.op=op;this.whereExpressAst=whereExpressAst;
	}

	public List<Map> exe() {
		// TODO Auto-generated method stub
		if(this.op.equals("select"))
		{
			List<Map> rzt_li = Lists.newArrayList();
			List<Map> tab=(List<Map>) table;
			for (Map row : tab) {
				if(suitExp(row,whereExpressAst))
					rzt_li.add(row);
			}
			return rzt_li;
			
		}
		return null;
	}

	private boolean suitExp(Map row, AExpression whereExpressAst) {
		if(whereExpressAst==null) //q326
			return true;
		return whereExpressAst.checkSuitOk(row);
		 
	//	return false;
	}

}
