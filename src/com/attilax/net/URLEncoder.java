package com.attilax.net;

import java.io.UnsupportedEncodingException;

import com.attilax.exception.ExUtil;

public class URLEncoder {

	public static String encodeDouble(String s) {
		 
		try {
			return  java.net.URLEncoder.encode(java.net.URLEncoder.encode(s, "utf-8"),"utf8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

//	public static String encodeDouble(String s) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	
	public static String encode(String sql) {
		 
		try {
			return java.net.URLEncoder.encode(sql, "utf8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			 ExUtil.throwEx(e); 
		}
		throw new RuntimeException("encode err");
	}

}
