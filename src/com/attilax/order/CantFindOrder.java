package com.attilax.order;

public class CantFindOrder extends Exception {

	public CantFindOrder(String order_id) {
		super(order_id);
	}

}
