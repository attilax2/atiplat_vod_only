/**
 * 
 */
package com.attilax.order;

/**
 * @author attilax
 *2016年4月21日 下午9:29:32
 */
public class FeeNotEquEx extends RuntimeException {

	/**
	 * @param string
	 */
	public FeeNotEquEx(String string) {
		super(string);
	}

}
