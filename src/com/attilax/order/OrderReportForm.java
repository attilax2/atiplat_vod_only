package com.attilax.order;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.attilax.io.pathx;
import com.attilax.ui.HTML5Form;

public class OrderReportForm extends HTML5Form  {
	
	
	public static void main(String[] args) {
		System.out.println(getUrl());
	}
	
	public OrderReportForm() {
		
		super(getUrl());
		setSize(960, 550);
	}

	private static String getUrl() {
		String s="http://localhost/ecsx/admin/order_stats.php?act=list";
		try {
			s=URLEncoder.encode(s, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//http://localhost/ecsx/admin/privilege.php?act=login
		//"http://localhost/ecsx/admin/order_stats.php?act=list"
		return "http://localhost/ecsx/admin/privilege.php?act=login&gourl="+s;
	}
	
	

}
