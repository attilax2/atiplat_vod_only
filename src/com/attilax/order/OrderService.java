package com.attilax.order;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import aaaCfg.IocX4casher;

import com.attilax.db.DBX;
import com.attilax.db.DbService;
import com.attilax.formatter.SqlFormatter;
import com.attilax.io.filex;
import com.attilax.sms.SmsService;
import com.google.inject.Inject;


/**
 * itfs abs and samp
 * @author attilax
 *2016年10月24日 下午12:34:26
 */
public class OrderService extends absService {

	
	@Inject
	DbService DbService1;
	public static void main(String[] args) {
//		final long time_intFmt = new Date().getTime() / 1000;
//		System.out.println(time_intFmt);
//		OrderService os = IocX4casher.getBean(OrderService.class);
//		os.insert(new HashMap() {
//			{
//				this.put("good_amount", 12.50);
//				this.put("add_time", time_intFmt);
//				this.put("confirm_time", time_intFmt);
//				this.put("order_sn",filex.getUUidName());
//			}
//		});
		System.out.println("--f");
	}

	@Inject
	DBX dx;

	public int insert(Map order) {
		String sql = "insert into ecs_order_info(order_sn,order_status,shipping_status,pay_status,goods_amount,add_time,confirm_time,agency_id,inv_type,tax,discount)values('@order_sn@',1,1,1,@good_amount@,@add_time@,@confirm_time@,0,0,0,0) ";

		sql = SqlFormatter.exe(sql, order);
		System.out.println(sql);
		// DBX IocX4casher.getBean(DBX.class)
		dx.execSql(sql);
		return 0;

	}
	
	
	@Inject 
	SmsService smsSrv;
	public Object refuse(String order_id)
	{
		 
		@SuppressWarnings("rawtypes")
		Map m = detail(order_id);
	
		String tel=(String) m.get("account");
		String s="亲爱的“$name$”同学，您好！您所报名的“$title$”职位被拒绝，由于您的自身条件不符合招聘要求或已招满等原因。转角遇到爱，如有疑问，请致电18706173108.【职达巴士】";
		s=s.replace("$name$", (CharSequence) m.get("realname")).replace("$title$", (CharSequence) m.get("copy_title"));
		System.out.println(s);
		String sql="update wxb_order set accept=0 where order_id='$id$'".replace("$id$", order_id);
		System.out.println("--accept sql:"+sql);
		DbService1.executeUpdate(sql);
		return	smsSrv.send(tel, s);
	//	 sql;
		
		
		
	}

	private Map detail(String order_id) {
		String sql="select * from `order` where order_id='"+order_id+"'";
	Map m=	DbService1.executeQuery_fetch_row(sql);
		return m;
	}
	/**
	 * 模板一
您的验证码为：XXXX请勿向任何人泄露。非本人操作请忽略本短信，如有疑问请致电18706173108.【职达巴士】

模板二
报名提醒：您发布的“XXXXXX”职位被“XXX”申请，请登录职达巴士商家进行签约。【职达巴士】

模板三
亲爱的“XXX”同学，您好！您所报名的“XXXXXX”职位被拒绝，由于您的自身条件不符合招聘要求或已招满等原因。转角遇到爱，如有疑问，请致电18706173108.【职达巴士】

模板四
亲爱的“XXX”同学，您好！您所报名的“XXXXXX”职位已被录用，请注意工作时间，按时上岗。【职达巴士】

模板五
好消息！您的资质已经被职达巴士审核通过，可以在职达巴士发布职位招聘人才了。赶快登陆职达巴士商家平台查看详情吧。【职达巴士】
	attilax    2016年4月14日  下午1:25:03
	 * @param order_id
	 * @return
	 */
	public Object accept(String order_id)
	{
		//亲爱的“XXX”同学，您好！您所报名的“XXXXXX”职位已被录用，请注意工作时间，按时上岗。【职达巴士】
		//String sql="select * from `order` where order_id='"+order_id+"'";
		Map m = detail(order_id);
		String tel=(String) m.get("account");
		String s="亲爱的“$name$”同学，您好！您好！您所报名的“$title$”职位已被录用，请注意工作时间，按时上岗。【职达巴士】";
		s=s.replace("$name$", (CharSequence) m.get("realname")).replace("$title$", (CharSequence) m.get("copy_title"));
		System.out.println(s);
		
		String sql="update wxb_order set accept=1 where order_id='$id$'".replace("$id$", order_id);
		System.out.println("--accept sql:"+sql);
		DbService1.executeUpdate(sql);
		return	smsSrv.send(tel, s);
	//	return sql;
		
	//	return sql;
		
	}

}
