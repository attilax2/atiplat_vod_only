package com.attilax.order;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 





import javax.servlet.http.HttpServletRequest;

 













import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import aaaCfg.IocX4casher;

import com.attilax.acc.Acc;
import com.attilax.acc.AccService;
//import com.attilax.bet.AmountCalcService;
import com.attilax.data.DataStoreService;
import com.attilax.db.DBX;
import com.attilax.db.DbService;
import com.attilax.function.Function;
import com.attilax.io.filex;
import com.attilax.ioc.IocFacV3_iocx_iocutil;
import com.attilax.ioc.IocXq214;
import com.attilax.json.AtiJson;
import com.attilax.lang.FunctinImp;
import com.attilax.log.LogSvr;
import com.attilax.math.ADecimal;
import com.attilax.orm.AOrm;
import com.attilax.sms.SmsService;
//import com.attilax.sql.DbService;
import com.attilax.store.StoreService;
import com.attilax.user.User;
import com.attilax.user.UserService;
import com.attilax.util.DataMapper;
import com.attilax.web.ReqX;
import com.google.inject.ImplementedBy;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import 	com.attilax.trigger.Trigger_after;


/**
 * v3 add refuse and accept
 *com.attilax.order.OrderService4jobus.refuse
 * @author attilax
 *2016年4月14日 下午12:36:44
 */
@Deprecated
public class OrderService4gamepay extends OrderService {

	public static void main(String[] args) {

		System.setProperty("apptype", "jobus");
		System.setProperty("prj", "jobus");
		System.setProperty("cfgfile", "pay.ini");
		OrderService4gamepay srv = IocFacV3_iocx_iocutil
				.getBean(OrderService4gamepay.class);

		
		 System.out.println("--f");
		
	}

	@Inject
	DataStoreService storeSvr;


	@Inject
	UserService userSvr;
	@Inject
	AccService accSvr;
	@Inject
	AmountCalcService amoutCalcSvr;
	@Inject
	private LogSvr logSvr;
	
//	 @Inject  @Named("order_service_dataMaper")
	// @ImplementedBy(FunctinImp.class)   should ostion in interface java hto..
//	public Function  dataMaper;
	@Inject

	public Trigger_after trig_bef;
 	@Inject
 	public Trigger_after trig_aft;
	
	
	
	public int insert(HttpServletRequest req) {
		return insert(ReqX.toMap(req));
	}

	public int insert(Map order) {
		if (userSvr == null)
			throw new RuntimeException("#userSvr_is_null");
		if (accSvr == null)
			throw new RuntimeException("#accSvr_is_null");
		if (amoutCalcSvr == null)
			throw new RuntimeException("#amoutCalcSvr_is_null");

		if (userSvr.isNotLogin()) {
			throw new RuntimeException(" not login 没登录,请先登录..#not_login");
		}

		User u = userSvr.getLoginUser();
		Acc a = accSvr.getAcc(u.id);

		BigDecimal needMoney = amoutCalcSvr.calc(order);

		if (new ADecimal(needMoney).biggerEqualThan(a.amount))
			throw new RuntimeException(
					"  amount not enough 金额不足够 ..#amount_not_enough ");

		// /...insert
		order.put("$op", "insert");
		order.put("order_id", filex.getUUidName());
		order.put("order_money", needMoney);
		trig_bef.apply(order);

		storeSvr.insert(order);

	
		int rzt = accSvr.reduceAmount(u.id.toString(), needMoney.doubleValue());
		logSvr.log(order);
		return rzt;

	}


	public List<Map> query(Map order) {

		if (userSvr.isNotLogin()) {
			throw new RuntimeException(" not login 没登录,请先登录..#not_login");
		}
		User u = userSvr.getLoginUser();

		return null;

		// /...insert
		// return accSvr.reduceAmount(u.id.toString(), needMoney.doubleValue());

	}
@Deprecated
	public String query2json(Map order) {

		return AtiJson.toJson(query(order));

		// /...insert
		// return accSvr.reduceAmount(u.id.toString(), needMoney.doubleValue());

	}

}
