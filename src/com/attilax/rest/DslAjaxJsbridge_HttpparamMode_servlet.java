package com.attilax.rest;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.struts2.dispatcher.StrutsRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import aaaCfg.IocX4nodb;




import com.attilax.core;
import com.attilax.collection.list;
import com.attilax.dsl.DslParser;
//import com.attilax.dsl.DslParser;
import com.attilax.exception.ExUtil;
import com.attilax.hre.UrlDslParser;
import com.attilax.hre.UrlDslParserV2;
import com.attilax.io.filex;
import com.attilax.ioc.IocFacV3_iocx_iocutil;
import com.attilax.ioc.IocUtilV2;
import com.attilax.ioc.IocXq214;
import com.attilax.json.AtiJson;
import com.attilax.lang.Global;
import com.attilax.net.requestImp;
import com.attilax.ref.refx;
import com.attilax.up.FileUploadService;
import com.attilax.web.ReqX;
import com.attilax.wechat.Order;
import com.attilax.wechat.WeChatPayUtil;
import com.attilax.wrmi.JsnaInvoker;
import com.attilax.wrmi.Wrmi;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

/**
 * com.attilax.rest.DslAjaxJsbridgeServlet
 * com.attilax.order.RechargeOrderService jobus not use ,,can be game use ..
 * /CommonServlet?$method= com.attilax.up.FileUploadService.upload
 * 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.process
 * 
 * @author Administrator
 * 
 *         / /jsnaServlet?$method=com.attilax.jsna.test.add&param1=2&param2=3
 *
 *         jeig he not support meth chain...deft use dyna new mode...
 *         v2 will add stati mode
 */

@WebServlet(name = "DslAjaxJsbridge_HttpparamMode_servlet_name", urlPatterns = "/DslAjaxJsbridge_HttpparamMode_servlet")
public class DslAjaxJsbridge_HttpparamMode_servlet implements Servlet {

	public static void main(String[] args) {
		System.setProperty("prj", "game");
		System.setProperty("agent_cfgfile", "cfg_game.properties");
		System.out.println("2333");
		requestImp ri=new requestImp();
		ri.setParam("iocx", "com.attilax.ioc.Ioc4agent");
		ri.setParam("meth", "com.attilax.db.DbServiceV4qb9.executeQuery");
		ri.setParam("p1", "select * from agt");
		new DslAjaxJsbridge_HttpparamMode_servlet().service(ri, null);
		logger.error("--e44rr10");
		// org.apache.xmlbeans
		// org.apache.xmlbeans
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig paramServletConfig) throws ServletException {

		// UrlDslParserx = IocXq214.getBean(UrlDslParser.class);

	}

	public static ThreadLocal<ServletResponse> resp = new ThreadLocal<ServletResponse>();

	@Inject
	UrlDslParser UrlDslParserx;

	@Override
	public void service(ServletRequest req, ServletResponse response) {
		try {
			HttpServletRequest req2 = (HttpServletRequest) req;
			Global.req.set(req2);

			String iocx = req.getParameter("iocx");
			String meth = req.getParameter("meth");
			String cls = refx.getClassName(meth);
			String meth_name = refx.getMethodName(meth);
			Class c = Class.forName(cls);
		//	Class<? extends Class> class1 = c.getClass();
			Object cls_obj = com.attilax.reflect.MethodUtils.invokeStaticMethod(iocx, "getBean", c);
			List<Object> params = Lists.newArrayList();
			for (int i = 1; i < 7; i++) {
				String pname = "p" + i;
				if (req.getParameter(pname) != null) {
					String p = req.getParameter(pname);
					String ptype = req.getParameter(pname + "type");
					if (ptype == null)
						ptype = "s";
					if (ptype.equals("s")) {
						params.add(p);
					} else {

					}
				}
			}
			Object[] p_a = params.toArray();
			Object rzt = MethodUtils.invokeMethod(cls_obj, meth_name, p_a);
			System.out.println(rzt);
			// Object r = new DslParser().parseV3(req2.getParameter("dsl"));
			if (req.getParameter("retFmt") != null) {
				if (req.getParameter("retFmt").equals("json"))
					response.getWriter().println(AtiJson.toJson(rzt)); // �벻Ҫ�޸Ļ�ɾ��
			} else
				response.getWriter().println(rzt.toString()); // �벻Ҫ�޸Ļ�ɾ��
		} catch (Exception e) {
			logger.error("--err", e);
			logger.error("--err2", AtiJson.toJson(e));
			filex.save_safe(core.getTrace(e), "c:\\0rechglog\\" + filex.getUUidName() + ".log");
			ExUtil.throwExV2(e);
		}

	}

	public static final Logger logger = LoggerFactory.getLogger(IocFacV3_iocx_iocutil.class);

}
