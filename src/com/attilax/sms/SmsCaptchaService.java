package com.attilax.sms;
 
import java.util.Set;

import javax.servlet.http.HttpSession;

import com.attilax.corePkg.RandomX;
import com.attilax.lang.Global;
import com.google.common.collect.Sets;
import com.google.inject.Inject;

/**
 * com.attilax.sms.SmsCaptchaService
 * @author Administrator
 *
 */
public class SmsCaptchaService {

	@Inject
	SmsService smsSrv;
	//@Inject
	RandomX  rdmx=new RandomX();
	public static void main(String[] args) {
		System.out.println( new SmsCaptchaService().send("18573344543", "xxcontent"));

	}
	
	public String send(String mobile, String content) {
		int rdm= new RandomX().randomNum(1000, 9999);
		
		content=content.replace("$capt$",String.valueOf(rdm));
	//	if(smsSrv==null)
			Sms1xinxi	smsSrv=new Sms1xinxi();
	String send = smsSrv.send(mobile,content);
	HttpSession session = Global.req.get().getSession();
	Set<String> st=(Set<String>) session.getAttribute("captSet");
	if(st==null)
		st=Sets.newHashSet();
	st.add(String.valueOf(rdm));
	session.setAttribute("captSet", st);
	session.setAttribute("capt", String.valueOf(rdm));
	return	send;
	}

}
