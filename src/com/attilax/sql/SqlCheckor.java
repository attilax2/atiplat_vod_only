/**
 * 
 */
package com.attilax.sql;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.management.RuntimeErrorException;

import com.attilax.json.AtiJson;
import com.attilax.lang.AString;
import com.attilax.lang.text.strUtil;
import com.attilax.secury.AesV2q421;
import com.attilax.secury.aesC47;
import com.google.inject.Inject;

/**
 * @author attilax
 *2016年4月19日 下午4:19:41
 */
public class SqlCheckor {
	  @Inject
		 SqlService sqlSrv;
	/**
	attilax    2016年4月19日  下午4:19:47
	 * @param string
	 * @param string2
	 */
	public void uniqCheck(String tb, String col,String val) {
		String s="select * from  "+tb+"   where "+col+"='$v$'";
		s=s.replace("$v$", val);
		List li=sqlSrv.executeQuery(s);
		if(li.size()>=1)
			throw new uniqCheckEx(tb+"."+col+"="+val);
		 
		
	}
	
	public static void main(String[] args) {
		String[] a=new String[]{"123"};
		String string= "where:uid='$uid$'";
	     
		System.out.println(new AString( "where:uid='$uid$'","123").toSysStr());
	}
	public void ForeitKeyCheck(String tb, String col,String val) {
		String s="select * from  "+tb+"   where "+col+"='$v$'";
		s=s.replace("$v$", val);
		List li=sqlSrv.executeQuery(s);
		if(li.size()>=1)
			throw new ForeitKeyCheckEx(tb+"."+col+"="+val);
		 
		
	}

	/**  valideValCheck before udate
	attilax    2016年4月25日  下午4:46:25
	 * @param whereExp 
	 * @param string
	 * @param string2
	 */
	public void valideValCheck4oldrow( String sql,String col,String valide_col) {
	 
//		Map row=sqlSrv.executeQueryFirstRow(sql);
//		String v=(String) row.get(col);
//		String valid=(String) row.get(valide_col);
//		if(! AesV2q421.encrypt(v, "ilovmole").equals(valid))
//		
//			throw new ValidValCheckEx("sql:"+sql);
		
	}
	
	public void valideValCheck4newRow(Object val,String valEncoded) {
		 
		 
		if(! AesV2q421.encrypt(val.toString(), "ilovmole").equals(valEncoded))
		
			throw new ValidValCheckEx("val.encode:"+val+"-"+valEncoded);
		
	}
	
	public String getValideVal(Object v)
	{
		return AesV2q421.encrypt(v.toString(), "ilovmole");
	}

	/**should str2 dec compare..cant dec2str comapre..cause to db and from db ,then num point maybe changte...but true val is not change..
	attilax    2016年4月25日  下午10:24:55
	 * @param object
	 * @param object2
	 */
	public void valideValCheck4oldrow(Object val, String valEncoded) {
		//8CBAC0UjkdSjLBFwKNB2iA==
		BigDecimal money=new BigDecimal(  AesV2q421.decrypt(valEncoded, "ilovmole"));
		if(money.compareTo(   (BigDecimal) val)!=0)
			
			throw new ValidValCheckEx("val.encode:"+val+"-"+valEncoded);
		
	}


}
