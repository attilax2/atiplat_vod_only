package com.attilax.sql;

import java.util.List;
import java.util.Map;

import com.attilax.io.filex;
import com.attilax.json.AtiJson;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class SqlJoinParser {

	public static void main(String[] args) {
		String f = "D:\\workspace\\atiPlatf_bet\\src\\com\\attilax\\order\\orderView.txt";
	System.out.println(AtiJson.toJson(new SqlJoinParser().parse(f)));	
		

	}

	public SqlJoinAst parse(String f) {
		List<String> li = filex.read2list_filtEmptyNstartSpace(f);
		List<String> li2 = Lists.newArrayList();
		for (String line : li) {
			if (line.trim().startsWith("#"))
				continue;
			li2.add(line);

		}
		String table = li2.get(0);
		SqlJoinAst sja = new SqlJoinAst();
		sja.table = table;
		sja.joinTables = Lists.newLinkedList();
		Map ast = Maps.newLinkedHashMap();
		ast.put("table", table);
		for (int i = 1; i < li2.size(); i++) {
			String[] a = li2.get(i).split(",");
			Map joinTable = Maps.newLinkedHashMap();
			joinTable.put("joinTable", a[0]);
			String joinCol_s = a[1];
			String[] a2 = joinCol_s.split("=");
			joinTable.put("onMainTableCol", a2[0]);
			joinTable.put("onJoinTableCol", a2[1]);
			sja.joinTables.add(joinTable);
		}
		return sja;
	}

}
