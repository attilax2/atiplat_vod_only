/**
 * 
 */
package com.attilax.sql;

/**
 * @author attilax
 *2016年4月18日 下午6:33:48
 */
public class SqlSecuryException extends RuntimeException {

	/**
	 * @param string
	 */
	public SqlSecuryException(String string) {
		 super(string);
	}

}
