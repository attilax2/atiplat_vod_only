package com.attilax.task;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import org.openqa.jetty.http.SSORealm;
//import org.sikuli.script.Screen;






import com.attilax.io.filex;
import com.attilax.io.pathx;
//import com.sun.javafx.print.Units;

public class TaskService {
	public  	ExecutorService fixedThreadPool;
	private int threadCount;
	private Callable task;
	private int pertask_sleeptime_millsecs;
	private Function task_getter;
	private int timeout_secs;
	public static void main(String[] args) {
		new filex().In_the_pc_machine_directory("c:\\0011windows")
		.written_to_the_file().comma()
		.the_content_is("om_mani_padme_hum").the_file_name_is_random()
		.the_extension_is("txt").exe_single();
		System.out.println("--f");
		Callable task_my = new Callable() {

			@Override
			public Object call() {
				new filex().In_the_pc_machine_directory("c:\\0011windows")
				.written_to_the_file().comma()
				.the_content_is("om_mani_padme_hum").the_file_name_is_random()
				.the_extension_is("txt").exe_single();
				  return null;
			 
			}
		};
		Function task_getter=new Function<Object,Callable>() {

			@Override
			public Callable apply(Object t) {
				
				return task_my;
			}
		};
		
		
		
		new TaskService().setTask(task_my).TasksCount(10).at_the_same_time().and_pertask_sleeptime_millsecs_is(100).timeout_secs(30).exe();
		
	}

	private TaskService timeout_secs(int i) {
		this.timeout_secs=i;
		return this;
	}

	private TaskService setTask(Callable task_my) {
	//	this.task_getter=task_my;
		this.task=task_my;
		return this;
	}

	private void exe() {
		fixedThreadPool	 = Executors.newFixedThreadPool(this.threadCount);
//		Callable apply = (Callable) this.task_getter.apply(null);
	//	FutureTask<Object> task = new FutureTask<Object>(apply);
		
		for (double i = 5; i >0; ) {
			try {
				FutureTask<Object> task = new FutureTask<Object>(this.task);
				
				fixedThreadPool.execute(task);
				check_time_out(task);
//				
				
			
			 
				Thread.sleep(this.pertask_sleeptime_millsecs);
			//	i++
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	private void check_time_out(FutureTask  task) {
		Runnable checkTimeout=new Runnable() {
			
			@Override
			public void run() {
				try {
					System.out.println("--start check task ,timeout:"+timeout_secs+ " taskid:"+task);
					Object r=task.get(timeout_secs,TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TimeoutException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					System.out.println( "--checkTimeout");
				//	task.cancel(true);
				//	task.
				} catch(CancellationException e)
				{
					System.out.println("--warnging");
					e.printStackTrace();
				}
				
			}
		};
		new Thread(checkTimeout).start();
	}

	private TaskService and_pertask_sleeptime_millsecs_is(int i) {
		this.pertask_sleeptime_millsecs=i;
		return this;
	}

	private TaskService at_the_same_time() {
		// TODO Auto-generated method stub
		return this;
	}

	private TaskService TasksCount(int i) {
		this.threadCount=i;
		
		return this;
	}
	//Timer timer=new Timer();
//	timer.schedule(new TimerTask() {
//
//		@Override
//		public void run() {
//			try {
//			
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		
//
//		}
//	}, 0, spanSec * 1000);

	public void start(Runnable task2) {
		new Thread(task2).start();
		
	}

	public String async_await(Callable object, int timeout_secs, String defVal) {
		FutureTask<String> task = new FutureTask<String>(object);
 
		try {
			return task.get(timeout_secs, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (TimeoutException e) {
			task.cancel(true);
		}
		return defVal;
	}
	
	public FutureTask async(Callable object, int timeout_secs ) {
		FutureTask<String> task = new FutureTask<String>(object);
		this.timeout_secs=timeout_secs;
		check_time_out(task);
return task;
		 
	}

	public <t> t await(FutureTask<t> async_task, int timeout_secs2, Object defVal) {
		try {
			return async_task.get(timeout_secs2, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (TimeoutException e) {
			async_task.cancel(true);
		}
		return (t) defVal;
	}

 

}
