/**
 * 
 */
package com.attilax.tmpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.attilax.cn.ChineseToPinYin;
import com.attilax.coll.ListX;
import com.attilax.collection.list;
import com.attilax.collection.listUtil;
import com.attilax.io.filex;
import com.attilax.json.AtiJson;
import com.attilax.lang.text.strUtil;
import com.csmy.my.center.util.zto.DigestUtil;
import com.csmy.my.center.util.zto.HttpUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @author attilax
 *2016年4月28日 下午6:05:21
 */
public class JspTmplUtil {

	/**
	attilax    2016年4月28日  下午6:05:22
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> cols=Lists.newLinkedList();
		cols.add("col1");cols.add("col2");
		ArrayList<Map> lists = new ArrayList<Map>() {
			{
				for (int i = 0; i < 10; i++) {
				//	final String tit =cols.get(i);
					Map m=geneOneRow(cols);
					add (m);

				}
			}
		};
		
		Map<String, Object> rootMap = new HashMap<String, Object>() {
			{
				put("cols",listUtil.join(cols, ",") );
				
				put("list", AtiJson.toJson( lists));
			//	Object mod;
				put("mod", "mode");

			}
		};
		
		String list="  [{\"col1\":\"c1_v\",\"col2\":\"c2_v\"}]";
		
		 List<Map> li2=(List<Map>) AtiJson.fromJsonAsList(list);
	//strUtil.toStr(object)	 
	 
			String traceString = HttpUtil. postV2("http://localhost:8080/tmpl/list.jsp", DigestUtil.UTF8,rootMap);
           System.out.println(traceString);
            filex.save_safe(traceString, "D:\\workspace\\AtiPlatf_cms\\WebRoot\\tmplout\\"+"测试"+".html");
           String[] cols2 = null;
           List<Map> li=(List<Map>) AtiJson.fromJson("");
           for (Map map : li) {
        	   
        	   for (String col : cols2)
        	   {
        		   System.out.println( map.get(col));
        	   }
			
		   }         

	}

	
	/** @author attilax 老哇的爪子
	 * @since o79 W429$
	 * 
	 * @param tits_a */

	/**
	attilax    2016年4月28日  下午9:50:00
	 * @param cols
	 * @return
	 */
	protected static Map geneOneRow(List<String> cols) {
		Map m=Maps.newLinkedHashMap();
		for ( String c : cols) {
			m.put(c, "测试数据");	
			}
		return m;
	}


	/** @param templateName 模板文件名称
	 * @param templateEncoding 模板文件的编码方式
	 * @param root 数据模型根对象 */
	public static void analysisTemplate(String Tmpl_pathname, String templateName, String templateEncoding, Map<?, ?> root, String file2save) {
		try {
			/** 创建Configuration对象 */
			Configuration config = new Configuration();
			/** 指定模板路径 */
			// = "templates";
			File file = new File(Tmpl_pathname);
			/** 设置要解析的模板所在的目录，并加载模板文件 */
			config.setDirectoryForTemplateLoading(file);
			/** 设置包装器，并将对象包装为数据模型 */
			config.setObjectWrapper(new DefaultObjectWrapper());

			/** 获取模板,并设置编码方式，这个编码必须要与页面中的编码格式一致 */
			Template template = config.getTemplate(templateName, templateEncoding);
			/** 合并数据模型与模板 */
			filex.createAllPath(file2save);
			OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file2save), "utf-8");
			// Writer out = new OutputStreamWriter(System.out);
			template.process(root, out);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}

	}
}
