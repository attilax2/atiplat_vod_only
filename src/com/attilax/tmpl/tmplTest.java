package com.attilax.tmpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;

import com.attilax.coll.ListX;
import com.attilax.collection.listUtil;
import com.attilax.data.DataGener;
import com.attilax.index.IndexService;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.json.AtiJson;
import com.attilax.lang.MapX;
import com.attilax.lang.text.strUtil;
import com.attilax.office.ExcelTest;
import com.attilax.office.excelUtil2007;
import com.csmy.my.center.util.zto.DigestUtil;
import com.csmy.my.center.util.zto.HttpUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class tmplTest {

	public static Map index1;
	public static void main(String[] args) {
		String tmpl_url = "http://localhost:8080/tmpl/list.jsp";  //gene list page.jsp
	//	tmpl_url="http://localhost/tmpl/list.jsp";
		System.out.println(tmpl_url);
	  List<Map> li = new excelUtil2007().toListMap("c:\\功能表.xlsx");
		
		li=new ExcelTest().clear(li);
		List<Map> funs_li=ListX.removeFirstRow(li);
		
		List<Map> funs_unq4idx=getLi4indx(funs_li);
		
		  index1=new IndexService().createIndex("funKeyIdx", "功能", funs_unq4idx);
		
		String cols=MapX.getKeysStr( li.get(0));
		
		int i=0;
		for (Map m : funs_li) {
			i++;
			//if(i>2)
			//	break;
			try {
				if(m.get("字段").toString().length()==0 || m.get("字段").toString().startsWith("//"))
					continue;
				String user=MapX.getStartWith(m,"使用用户类型").toString().replace("/", "-");
			
				String finename=filex.fileNameEncode(MapX.getStartWith(m,"上级菜单"))+"__"+MapX.getFileName(m,"功能")+"_"+user;
				 String savefile = "d:\\workspace\\AtiPlatf_cms\\WebRoot\\tmplout\\"+finename+".html";
				 Map rootmap=geneRootMap(m);
				String s=single(tmpl_url,savefile,rootmap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			
		}
		System.out.println("--f");
		
		
	}
	
	/**
	attilax    2016年5月3日  下午5:45:20
	 * @param funs_li
	 * @return
	 */
	private static List<Map> getLi4indx(List<Map> funs_li) {

		List<Map> li_r = Lists.newArrayList();
		for (Map map : funs_li) {
			try {
				String filedVal = (String) map.get("字段");
				if (filedVal.toString().trim().startsWith("//"))
					continue;
			 
				li_r.add(map);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		
		}

		return li_r;
	}

	private static Map geneRootMap(Map m) {
		String modename= m.get("功能").toString();
		 String cols=m.get("字段").toString().trim();
		 
		 if(cols.startsWith("//同上"))
		 {
			 String fun=(String) m.get("功能").toString().trim();
			 Map mfromIdx=(Map) index1.get(fun);
			 cols =(String) mfromIdx.get("字段");
		 }
			 
		 
		List<String>  col_li=strUtil.toList(cols);
		ArrayList<Map> data_lists = new ArrayList<Map>() {
			{
				for (int i = 0; i < 10; i++) {
				//	final String tit =cols.get(i);
					Map m=geneRootMap_geneOneRow(col_li);
					add (m);

				}
			}
		};
		final String cols_fina=cols;
		Map<String, Object> rootMap = new HashMap<String, Object>() {
			{
				put("cols", cols_fina );
				 
				put("list", AtiJson.toJson( data_lists));
			//	Object mod;
			
				put("mod", modename);

			}
		};
		return rootMap;
	}

	static DataGener dataGener = new DataGener(pathx.classPath()+"/com/attilax/data/test_data_gene_rugur.txt");

	protected static Map geneRootMap_geneOneRow(List<String> cols) {
		Map m=Maps.newLinkedHashMap();
		for ( String c : cols) {
			if(strUtil.contains(c,"姓名 时间  次数 电话 数量 金额 年龄  性别"))
				System.out.println(" dbg");
		//	if(c.contains("")||c.contains("姓名"))
			String v=dataGener.gene(c);
			if(StringUtils.isEmpty(v))
				v="测试数据...";
			m.put(c, v);	
			}
		return m;
	}

	private static String single(String tmpl_url, String savefile, Map rootMap) {
		
		
		
		
		
		String traceString = HttpUtil. postV2(tmpl_url, DigestUtil.UTF8,rootMap);
        System.out.println(traceString); 
        filex.save_safe(traceString, savefile);
		return traceString;
		
	}
}
