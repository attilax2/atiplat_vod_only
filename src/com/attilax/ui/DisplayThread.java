package com.attilax.ui;

import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Display;

public class DisplayThread extends Thread {
	private Display display;
	Object sem = new Object();

	public void run() {
		synchronized (sem) {
			display = Display.getDefault();
			sem.notifyAll();
		}
		swtEventLoop();
	}

	private void swtEventLoop() {
		
		  Display dis = Display.getDefault();
	        dis.syncExec(new Runnable() {  
	            public void run() {  
//	                Shell shell = SWT_AWT.new_Shell(displayThread.getDisplay(),  
//	                        canvas);  
//	                shell.setLayout(new FillLayout());  
//	                final BrowserPanel browser = new BrowserPanel(shell, SWT.NONE);  
//	                browser.setLayoutData(BorderLayout.CENTER);  
//	                browser.setUrl("http://localhost:8080/test2/index2.html");  
	            	 while (true) {  
	                     if (!display.readAndDispatch()) {  
	                     //    display.sleep();  
	                         display.sleep();System.out.println("----display.slp333");
	                     }  
	                 }  
	            	
	            }  
	        });  
		
	}

	public Display getDisplay() {
		try {
			synchronized (sem) {
				while (display == null)
					sem.wait();
				return display;
			}
		} catch (Exception e) {
			return null;
		}
	}

}
