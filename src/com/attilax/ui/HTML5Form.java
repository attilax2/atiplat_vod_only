package com.attilax.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;

import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
//import org.openqa.selenium.firefox.FirefoxDriver;

 

import com.attilax.Closure;
import com.attilax.core;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.ParamX;
import com.attilax.text.strUtil;
import com.attilax.util.PropX;
//import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

//import org.junit.*;

//import static org.junit.Assert.*;
//import static org.hamcrest.CoreMatchers.*;

//import org.openqa.selenium.*;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.remote.UnreachableBrowserException;
//import org.openqa.selenium.support.ui.Select;

public class HTML5Form extends BrowserForm {

	public HTML5Form(String string) {

		String urlgoto = string;
		super.atiBrowserExampleinstance.setUrlgoto(urlgoto);
		super.atiBrowserExampleinstance.gotoUrl();
//		regJsCallbackMeth("json", new Closure() {
//
//			@Override
//			public Object execute(Object arg0) throws Exception {
//
//				List li = (List) arg0;
//				JSONObject jo = JSONObject.fromObject(li.get(0));
//				String meth = jo.getString("method");
//				String app = jo.getString("app");
//
//				Closure hdl = (Closure) CallJava.eventMap.get(meth);
//				return hdl.execute(jo);
//			}
//		});

		regJsCallbackMeth("selectFile", new Closure() {

			@Override
			public Object execute(Object arg0) throws Exception {

				List li = (List) arg0;
				String params = (String) (li.get(0));
				Map mp = new ParamX().urlParams2Map(params);

				String f = (String) new FileSelector().exe();
				f = pathx.fixSlash(f);
				String control_id = (String) mp.get("control_id");
				mp.put("val", f);

				String js = "document.getElementById('@control_id').value='@val';";
				js = new ParamX().sqlFmt(js, mp);

				boolean rzt = atiBrowserExampleinstance.executeJsTxt(js);
				System.out.println("--f:" + f);

				String call = (String) mp.get("callback");
				String js2 = call + "('" + f + "');";
				boolean rzt2 = atiBrowserExampleinstance.executeJsTxt(js2);
				return f;
			}
		});

	}

	public static void main(String[] args) {
		HTML5Form ef = new HTML5Form(pathx.classPathParent_jensyegeor()
				+ "/ControlPanel/index.html");
		ef.setSize(1200, 700);
		ef.show();
		ef.threadRecycle();

	}

	public void regJsCallbackMeth_4jsonParam(String string, Closure closure) {
		super.regJsCallbackMeth_4jsonParam(string, closure);

	}

}
