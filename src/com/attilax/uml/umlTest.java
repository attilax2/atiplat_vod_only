package com.attilax.uml;

import java.io.IOException;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.eclipse.uml2.uml.resources.util.UMLResourcesUtil;
import org.eclipse.emf.common.util.URI;

public class umlTest {

	public static void main(String[] args) {

		Model epo2Model = createModel("epo2");

		org.eclipse.uml2.uml.Package barPackage = UMLFactory.eINSTANCE
				.createPackage();
		// PrimitiveType intPrimitiveType = createPrimitiveType(epo2Model,
		// "int");
		org.eclipse.uml2.uml.Class supplierClass = createClass(epo2Model,
				"Supplier", false);
		// createAttribute(supplierClass, "name", PrimitiveType, 0, 1);
		 String fileExtension = UMLResource.FILE_EXTENSION;//zosh .uml
		String f = "";//args[0];
		f="attilaxUmlOutputFolder";
		save(epo2Model, URI.createFileURI(f).appendSegment("UmlFileMainname").appendFileExtension(fileExtension)); 

	}

	protected static Model createModel(String name) {
		Model model = UMLFactory.eINSTANCE.createModel();
		model.setName(name);

		out("Model '" + model.getQualifiedName() + "' created.");

		return model;
	}

	private static void out(String string) {
		System.out.println(string);

	}

	protected static org.eclipse.uml2.uml.Package createPackage(
			org.eclipse.uml2.uml.Package nestingPackage, String name) {
		org.eclipse.uml2.uml.Package package_ = nestingPackage
				.createNestedPackage(name);

		out("Package '" + package_.getQualifiedName() + "' created.");

		return package_;
	}

	protected static PrimitiveType createPrimitiveType(
			org.eclipse.uml2.uml.Package package_, String name) {
		PrimitiveType primitiveType = (PrimitiveType) package_
				.createOwnedPrimitiveType(name);

		out("Primitive type '" + primitiveType.getQualifiedName()
				+ "' created.");

		return primitiveType;
	}

	protected static org.eclipse.uml2.uml.Class createClass(
			org.eclipse.uml2.uml.Package package_, String name,
			boolean isAbstract) {
		org.eclipse.uml2.uml.Class class_ = package_.createOwnedClass(name,
				isAbstract);

		out("Class '" + class_.getQualifiedName() + "' created.");

		return class_;
	}

	protected static Property createAttribute(
			org.eclipse.uml2.uml.Class class_, String name, Type type,
			int lowerBound, int upperBound) {
		Property attribute = class_.createOwnedAttribute(name, type,
				lowerBound, upperBound);

		out("Attribute '%s' : %s [%s..%s] created.", //
				attribute.getQualifiedName(), // attribute name
				type.getQualifiedName(), // type name
				lowerBound, // no special case for multiplicity lower bound
				(upperBound == LiteralUnlimitedNatural.UNLIMITED) ? "*" // special
																		// case
																		// for
																		// unlimited
																		// bound
						: upperBound);

		return attribute;
	}

	private static void out(String string, String qualifiedName,
			String qualifiedName2, int lowerBound, Object object) {
		// TODO Auto-generated method stub

	}

	protected static void save(org.eclipse.uml2.uml.Package package_, URI uri) {
		// Create a resource-set to contain the resource(s) that we are saving
		ResourceSet resourceSet = new ResourceSetImpl();

		// Initialize registrations of resource factories, library models,
		// profiles, Ecore metadata, and other dependencies required for
		// serializing and working with UML resources. This is only necessary in
		// applications that are not hosted in the Eclipse platform run-time, in
		// which case these registrations are discovered automatically from
		// Eclipse extension points.
		UMLResourcesUtil.init(resourceSet);

		// Create the output resource and add our model package to it.

		Resource resource = resourceSet.createResource(uri);
		resource.getContents().add(package_);

		// And save
		try {
			resource.save(null); // no save options needed
			out("Done.");
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	private static void err(String message) {
		// TODO Auto-generated method stub

	}
}
