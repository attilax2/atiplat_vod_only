package com.attilax.up;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.ProgressListener;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
 
import org.apache.log4j.Logger;
 
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
 














import com.attilax.core;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.json.AtiJson;
import com.attilax.util.WebCfgRead;
import com.attilax.web.UrlX;


/**
 *com.attilax.up.FileUploadService
 * @author Administrator
 *
 */
public class FileUploadService {
	
	public static void main(String[] args) {
		   ProcessInfo pri = new ProcessInfo();  
	       pri.setItemNum( 2);
	       pri.readSize = 3;
	      
	       core.log(   core.toJsonStr(pri));
	}
	Logger log = Logger.getLogger(FileUploadService.class);
	static final ThreadLocal<Integer> threadLocal4listerInvkTimes=new ThreadLocal<Integer>();;
  	String upFileSavePath ="";
	private boolean upfinish=false;
	
	String saveDir="00up_tmp_def";
	/**
	 * upload  上传文件
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	 

	public String upload(HttpServletRequest request 
			 )   {
		
		this.saveDir=getSaveDirFromUri(request);
		
		final HttpSession hs = request.getSession();
		//ModelAndView mv = new ModelAndView();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if(!isMultipart){
			//return mv;
			 throw new RuntimeException(" not a multi part content  q118");
		}
		// Create a factory for disk-based file items
		FileItemFactory factory = new DiskFileItemFactory();

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	//	int listtenerInvkTimes=0;
		threadLocal4listerInvkTimes.set(0);
	
		//note must befer  parseREq....beris not run lister
		 
		   setProcessLister(hs, upload);  //o8f
		   
		   /*
		   //"[name=29ce30384936772bd466c4f645411cfe.jpg, StoreLocation=C:\Users\ADMINI~1\AppData\Local\Temp\ upload_038c6ce1_23c4_447d_8303_9ff057eaeaaf_00000000.tmp, "
		   //size=31033 bytes, isFormField=false, FieldName=upfile]"
		    * 
		    */
		List items;
		try {
			items = upload.parseRequest(request);
		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// Parse the request
		// Process the uploaded items
		Iterator iter = items.iterator();
		
		while (iter.hasNext()) {
		    FileItem item = (FileItem) iter.next();
		    if (item.isFormField()) {
		        String name = item.getFieldName();
		        String value = item.getString();
		        if(name.equals("saveDir"))
		        	saveDir=value;
		        System.out.println("this is common feild!"+name+"="+value);
		    } else {
				System.out.println("this is file feild!");
				String fieldName = item.getFieldName();
				String fileName = item.getName();
				String contentType = item.getContentType();
				boolean isInMemory = item.isInMemory();
				long sizeInBytes = item.getSize();
				// String pathname = "c:\\000\\"+fileName;
				String ext = filex.getExtName(fileName);
				upFileSavePath = this.saveDir + "/" + filex.getUUidName() + "."
						+ ext;

				String pathname = pathx.webAppPath() + "/" + upFileSavePath;
				filex.createAllPath(pathname);
				filex.save_safe(pathname, "c:\\uplog.txt");
				core.log("---save file:" + pathname);
				File uploadedFile = new File(pathname);
				try {
					item.write(uploadedFile);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				this.upfinish = true;

		    }
		}
	 	String ret = "<script>window.parent.callback('@savepath@')</script>";
	 	ret=ret.replace("@savepath@", upFileSavePath);
	 	return   upFileSavePath;
	}


	/**
	attilax    2016年4月15日  下午4:21:41
	 * @param request
	 * @return
	 */
	private String getSaveDirFromUri(HttpServletRequest request) {
		String s=request.getQueryString();
		Map m=(Map) UrlX.getHeader_from_QueryStr(s);
		return (String) m.get("savepath");
	}


	private void setProcessLister(final HttpSession hs, ServletFileUpload upload) {
		upload.setProgressListener(new ProgressListener(){
			   public void update(long pBytesRead, long pContentLength, int pItems) {
				   ProcessInfo pri = new ProcessInfo();  
			       pri.setItemNum( pItems);
			       pri.readSize = pBytesRead;
			       pri.totalSize = pContentLength;
			  //     pri.show = pBytesRead+"/"+pContentLength+" byte";
			       pri.rate = Math.round(new Float(pBytesRead) / new Float(pContentLength)*100);
			 
			   //ati o81    
			       pri.show =String.valueOf(  pri.rate)+"%";
			   //    if(!upFileSavePath.equals(""))
			       pri.path=upFileSavePath;
			       /////end ati
			       hs.setAttribute("proInfo", pri);
			       int newtimes=threadLocal4listerInvkTimes.get()+1;				       
			       core.log("---- listerInvkTimes times:"+String.valueOf(newtimes));
			       core.log(   core.toJsonStr(pri));
			       threadLocal4listerInvkTimes.set(newtimes);
			      
			   }
			});
	}
	
	
	/**
	 * process 获取进度
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	//@RequestMapping(value = "/process.json", method = RequestMethod.GET)
	//@ResponseBody
	public Object process(HttpServletRequest request 
			) throws Exception {
		core.log("=========process.json");
		ProcessInfo pi = ( ProcessInfo)request.getSession().getAttribute("proInfo");
		
		//o8f todox cant get correct path  fix
		if(pi.getRate()==100 )
			if(!upfinish)
			{
				pi.setRate(99);
				pi.setPath(this.upFileSavePath);
			}
			else
				pi.setPath(this.upFileSavePath);
		return AtiJson.toJson( pi);
	}
	
	
	/**
	@author attilax 老哇的爪子
		@since  o81 2_53_52$

	 */
	private void kaka() {
		// attilax 老哇的爪子  2_53_52   o81 
		{}
		{}
		{}

	}
	
}
