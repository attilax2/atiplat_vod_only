package com.attilax.urldsl;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import com.attilax.json.AtiJson;
import com.attilax.user.NotLoginEx;

public class DslUtil {

	public static void appFldFun(Map m) {
		if(m.get("$fldAppFun" )==null || m.get("$fldAppFun" ).toString().trim().length()==0)
			return;
		String json_str=(String) m.get("$fldAppFun");
		Map fldFunMap=AtiJson.fromJson(json_str);
		Set<String> st = fldFunMap.keySet();
		for (String k : st) {
			 String script_tag=(String) fldFunMap.get(k);
			 Function fun=UrlDsl2SqlStoreService.scriptMapper.get(script_tag);
			 Object v=fun.apply(m.get(k));
			 m.put(k, v);
		}
		
	}
	
	

}
