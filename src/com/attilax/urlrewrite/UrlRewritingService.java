package com.attilax.urlrewrite;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.struts2.dispatcher.StrutsRequestWrapper;

import aaaCfg.IocX4nodb;

import com.attilax.core;
import com.attilax.hre.UrlDslParser;
import com.attilax.hre.UrlDslParserV2;
import com.attilax.ioc.IocXq214;
import com.attilax.lang.Ast;
import com.attilax.lang.Global;
import com.attilax.ref.refx;
import com.attilax.web.ReqX;
import com.attilax.web.UrlX;
import com.attilax.wrmi.JsnaInvoker;
import com.attilax.wrmi.Wrmi;
import com.google.inject.Inject;

/**
 * com.attilax.uti.global.IndexServlet
 * /CommonServlet?$method= com.attilax.up.FileUploadService.upload
 * 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.process
 * 
 * @author Administrator
 * 
 *         / /jsnaServlet?$method=com.attilax.jsna.test.add&param1=2&param2=3
 *
 */

//@  xxWebServlet(name = "indexServlet_name", urlPatterns = "/*")
 @WebFilter( urlPatterns = "/dsl/*",filterName="UrlRewritingService_filter_name" )
public class UrlRewritingService implements Filter {

	public static void main(String[] args) {
		System.out.println("--ff24d4665");

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(ServletConfig paramServletConfig) throws ServletException {
		
		UrlDslParserx = IocXq214.getBean(UrlDslParser.class);
		

	}

	public static ThreadLocal<ServletResponse> resp = new ThreadLocal<ServletResponse>();

	@Inject
	UrlDslParser UrlDslParserx;

	public void service(ServletRequest req, ServletResponse paramServletResponse)
			throws IOException, ServletException {
		//new%28com.attilax.util.connReduceDync%29.set_resfile%28uc_js.txt%29.joinNout%28%29
	// 	org.springframework.web.filter.CharacterEncodingFilter
	 Global.req.set((HttpServletRequest) req);
	 	Global.resp.set((HttpServletResponse) paramServletResponse);
		HttpServletRequest httpServletRequest = (HttpServletRequest) req;
		String uri=httpServletRequest.getRequestURI();
		if(uri.startsWith("/dsl/"))
		{
			String uri_decode=UrlX.decode(uri);
		//----/dsl/new%28com.attilax.util.connReduceDync%29.set_resfile%28userPhone4jobusImp%2Fuc_js.txt%29.joinNoutV2%28%29
 
		 		String code=uri_decode.substring(5);   //(String) reqMap.get("code");
		 		Ast astParser = new Ast();
 				List ast=astParser.getExprsLiAst(code);
//				System.out.println(AtiJson.toJson(   ));
			 
				String ret =(String)astParser.parse(ast);
			//	httpServletRequest.getRequestDispatcher(url2).forward(req, paramServletResponse);
				paramServletResponse.getWriter().println(ret);
				return;
		}else if(uri.startsWith("/dsl2/"))
		{
			String code=req.getParameter("$code");
			Ast astParser = new Ast();
				List ast=astParser.getExprsLiAst(code);
//			System.out.println(AtiJson.toJson(   ));
		 
			String ret =(String)astParser.parse(ast);
		//	httpServletRequest.getRequestDispatcher(url2).forward(req, paramServletResponse);
			paramServletResponse.getWriter().println(ret);
			return;
			
		}else
			FilterChainxx.doFilter(req, paramServletResponse);
 

	}
	FilterChain FilterChainxx;
	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		 
		FilterChainxx=arg2;
		service(arg0, arg1);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
