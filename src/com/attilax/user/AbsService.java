package com.attilax.user;

import java.util.List;
import java.util.Map;

import com.attilax.db.DBX;
import com.google.inject.Inject;

import aaaCasher.SqlParser;
import aaaCfg.IocX4casher;



public class AbsService {

	
	@Inject
	public DBX dbx;
	
	public static void main(String[] args) {
		AbsService absService = new AbsService();
		System.out.println(absService.Last("customers", "ID"));
		Map m=absService.Last_row("customers", "ID");
		System.out.println("--f");
	}

	/**
	 * by def order
	 * @param tabname
	 * @param col
	 * @return
	 */
	public String Last(String tabname, String col) {

		SqlParser sc = IocX4casher.getBean(SqlParser.class);
		String s = " select " + col + " from  " + tabname;
		List<Map> li = sc.exe_retLi(s);
		Map map = li.get(li.size() - 1);
		return (String) map.get(col);

	}
	
	public String Last_orderByColDesc(String tabname, String col) {

		SqlParser sc = IocX4casher.getBean(SqlParser.class);
		String s = " select " + col + " from  " + tabname +" order by "+col+ " desc limit 1";
	//	List<Map> li = sc.exe_retLi(s);
		//Map map = li.get(li.size() - 1);
		//return (String) map.get(col);
		return s;

	}
	
public Map Last_row(String tabname, String odrderDescCol) {
		
		String id=Last(tabname,odrderDescCol);
// select * from  customers where id='440f0a22-940c-42fe-be72-422ddf06922e'
		SqlParser sc = IocX4casher.getBean(SqlParser.class);
		String s = " select  * from  " + tabname +" order by "+odrderDescCol+ " desc limit 1";
//		String s = " select * from  " + tabname +" where id='@id@'";
//		s=s.replace("@id@", id);
		@SuppressWarnings("unchecked")
		List<Map> li = sc.exe_retLi(s);
	//	Map map = li.get(li.size() - 1);
		return  li.get(0);

	}
	
	
	@SuppressWarnings("all")
//	public Map Last_row(String tabname, String ID_col) {
//		
//		String id=Last(tabname,ID_col);
//// select * from  customers where id='440f0a22-940c-42fe-be72-422ddf06922e'
//		SqlParser sc = IocX4casher.getBean(SqlParser.class);
//		String s = " select * from  " + tabname +" where id='@id@'";
//		s=s.replace("@id@", id);
//		@SuppressWarnings("unchecked")
//		List<Map> li = sc.exe_retLi(s);
//	//	Map map = li.get(li.size() - 1);
//		return  li.get(0);
//
//	}

	public boolean isExist(String tabname, String idColName, Object val) {
		return false;

	}

	public void insert() {

	}

	public void update() {

	}

	public void delete() {

	}

}
