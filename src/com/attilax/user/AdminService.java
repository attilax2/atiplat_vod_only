/**
 * 
 */
package com.attilax.user;

import java.util.Map;

import com.attilax.db.DbServiceV3Q68;
import com.attilax.net.cookieUtil;
import com.attilax.secury.LoginException;
import com.attilax.token.TokenServiceV2;
import com.csmy.my.center.util.CTUtils;
import com.google.inject.Inject;

/**
 * @author attilax 2016年6月18日 下午9:53:53
 */
public class AdminService extends UserSrv_4game {
	
	public static void main(String[] args) {
		
	}

	@Inject
	DbServiceV3Q68 dbsvr;
	@Inject
	TokenServiceV2 tksvr;

	public Object login(String uname, String pwd) {
		Map m = getUserinfoByUid(uname);
		if (m == null)
			throw new LoginException("ex:user_not_exist:此用户不存在");
		String pwd_encode =CTUtils.encryptBasedDes( pwd);
		if (pwd_encode.equals(  m.get("password").toString())) {
			tksvr.setUtype("agent").setToken(uname, m.get("user_id").toString() ).setUrole("admin");
			 
	
		//	cookieUtil.add( "gameAppId_urole_agentUtype", uid.toString(),
			return m;
		}
		throw new LoginException("ex:pwd_err:密码错误");

	}

	public Map getUserinfoByUid(String uname) {
		String sql = "	select * from user_info where account='$uid$' ";
		sql = sql.replace("$uid$", uname);
		Map m = dbsvr.uniqueResultAsRow(sql);
		return m;
	}

}
