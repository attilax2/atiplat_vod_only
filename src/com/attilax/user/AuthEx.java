/**
 * 
 */
package com.attilax.user;

import com.attilax.ex.UidEmptyEx;

/**
 * @author attilax
 *2016年11月10日 下午3:42:27
 */
public class AuthEx extends Exception  {
	public AuthEx(String string) {
		super(string);
	}

	/**
	 * @param string
	 * @param e
	 */
	public AuthEx(String string, UidEmptyEx e) {
		super(string,e);
	}
}
