/**
 * 
 */
package com.attilax.user.ex;

/**
 * @author attilax
 *2016年11月10日 下午5:12:37
 */
public class UserEx extends Exception {

	/**
	 * @param message
	 * @param e
	 */
	public UserEx(String message, Exception e) {
		super(message,e);	}

}
