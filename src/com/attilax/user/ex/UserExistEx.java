/**
 * 
 */
package com.attilax.user.ex;

import com.attilax.sql.ex.DuplicateEntryEx;

/**
 * @author attilax
 *2016年11月10日 下午5:05:49
 */
public class UserExistEx extends Exception {

	/**
	 * @param message
	 * @param e
	 */
	public UserExistEx(String message, DuplicateEntryEx e) {
		super(message,e);
	}

}
