/**
 * 
 */
package com.attilax.user.ex;

/**
 * @author attilax
 *2016年11月10日 下午5:05:49
 */
public class UserNotExistEx extends Exception {

	/**
	 * @param string
	 */
	public UserNotExistEx(String string) {
		super(string);
	}

}
