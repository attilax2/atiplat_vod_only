package com.attilax.uti.global;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.struts2.dispatcher.StrutsRequestWrapper;

import aaaCfg.IocX4nodb;


//import com.attilax.core;
import com.attilax.hre.UrlDslParser;
import com.attilax.hre.UrlDslParserV2;
import com.attilax.io.pathx;
import com.attilax.ioc.IocXq214;
import com.attilax.lang.Global;
import com.attilax.ref.refx;
import com.attilax.util.PropX;
import com.attilax.web.ReqX;
import com.attilax.wrmi.JsnaInvoker;
import com.attilax.wrmi.Wrmi;
import com.google.inject.Inject;

/**
 * com.attilax.uti.global.IndexServlet
 * /CommonServlet?$method= com.attilax.up.FileUploadService.upload
 * 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.process
 * 
 * @author Administrator
 * 
 *         / /jsnaServlet?$method=com.attilax.jsna.test.add&param1=2&param2=3
 *
 */

//@  xxWebServlet(name = "indexServlet_name", urlPatterns = "/*")
//should use urlrewrite package htod ..
@Deprecated   
// urlPatterns = "/*",filterName="index_filter_name" )
public class UrlRewritingService implements Filter {

	public static void main(String[] args) {
		System.out.println("--ff24d4665");

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	public void init(ServletConfig paramServletConfig) throws ServletException {
		
		UrlDslParserx = IocXq214.getBean(UrlDslParser.class);
		

	}

	public static ThreadLocal<ServletResponse> resp = new ThreadLocal<ServletResponse>();

	@Inject
	UrlDslParser UrlDslParserx;

	public void service(ServletRequest req, ServletResponse paramServletResponse)
			throws IOException, ServletException {
		
	// 	org.springframework.web.filter.CharacterEncodingFilter
	 Global.req.set((HttpServletRequest) req);
	 	Global.resp.set((HttpServletResponse) paramServletResponse);
		HttpServletRequest httpServletRequest = (HttpServletRequest) req;
		String uri=httpServletRequest.getRequestURI();
		if(uri.equals("/"))
		{
			//q711
			if(System.getProperty("mvccfg")!=null)
			{
				String mvccfg=System.getProperty("mvccfg");
				 String cfg=pathx.webAppPath_jensyegeor()+"/"+mvccfg;
				 PropX px=new PropX(cfg);
				 String root_url = px.getProperty("/");
				 HttpServletResponse resp=(HttpServletResponse) paramServletResponse;
					resp.sendRedirect(root_url);
					return;
			}
			
			
			
				String true_url=	GlobalService.getIndex();
				if(true_url.equals("/"))
				{
					FilterChainxx.doFilter(req, paramServletResponse);
					return;
				}
				HttpServletResponse resp=(HttpServletResponse) paramServletResponse;
				resp.sendRedirect(true_url);
		//		httpServletRequest.getRequestDispatcher(true_url).forward(req, paramServletResponse);
				return;
		}else
			FilterChainxx.doFilter(req, paramServletResponse);
	//	paramServletResponse.getWriter().println(ret);
	//	filterChain.doFilter(request, response);
//		@SuppressWarnings("unused")
//		String p = httpServletRequest.getContextPath();
//		resp.set(paramServletResponse);
//		// StrutsRequestWrapper
//		// if( UrlDslParserx==null)
//		paramServletResponse.setContentType("text/html;charset=utf-8");
//
//		Map m = ReqX.toMap(req);// req.getParameterMap();
//
//		String ret = new Wrmi().exe(m);
//		// UrlDslParserx.exe((HttpServletRequest)req);
//		paramServletResponse.getWriter().println(ret);

	}
	FilterChain FilterChainxx;
	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		 
		FilterChainxx=arg2;
		service(arg0, arg1);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
