package com.attilax.util;

import java.util.List;
import java.util.Map;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.text.strUtil;
import com.attilax.dsl.VarUtil;
import com.google.common.collect.Maps;

public class CfgService {
	
	public static void main(String[] args) {
		String cfg=filex.read(pathx.appPath_webPrjMode()+"/spider.ini");
        CfgService cs=new CfgService().read(cfg);
        System.out.println(cs.m);
	}
	public Map m=Maps.newHashMap();
	public CfgService read(String cfg) {
		String[] a=cfg.split("\r\n");
	//	Map m=Maps.newHashMap();
		for (String line : a) {
			if(line.trim().length()==0)
				continue;
			if(line.trim().startsWith("//"))
				continue;
			if(line.contains("home"))
				System.out.println("dbg");
			line=replaceVar(line);
			String[] a2=line.split("=");
		//	m.put(a2[0].trim(),a2[1]);
			m.put(a2[0].trim().toLowerCase(),a2[1]);
			m.put(a2[0].trim(),a2[1]);
		}
		return this;
	}
	private String replaceVar(String line) {
		 List<String> vars=strUtil.find("\\$(.*)\\$", line);
		 
		 for (String string : vars) {
			 String varname=VarUtil.trim(string,"$");
			 
			line=line.replace( string , m.get(varname.trim().toLowerCase()).toString());
		}
		return line;
	}
	
	//String get

}
