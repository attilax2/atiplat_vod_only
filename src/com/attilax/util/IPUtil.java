package com.attilax.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.attilax.lang.text.strUtil;

public class IPUtil {
	
	public static void main(String[] args) {
		//System.out.println( getIp());
		String a= subnet_url("http://192.168.0.111/lime/aa.jsp");
		System.out.println(a);
	}
/**
 * ret subnet from url
 * @param url
 * @return
 */
	public static String  subnet_url(String url) {
		String[] a=strUtil.splitByMultiChar(url, "//,/");
		String host_port=a[1];
		
		return subnet(host_port);
	}

	public static String subnet(String host_port) {
		int lastIdx=host_port.lastIndexOf(".");
		return host_port.substring(0,lastIdx);
	}

	public static boolean isInternalIp(String ipAddress){    
        boolean isInnerIp = false;    
        long ipNum = getIpNum(ipAddress);    
        /**   
        私有IP：

               A类  10.0.0.0-10.255.255.255   
               B类  172.16.0.0-172.31.255.255   
               C类  192.168.0.0-192.168.255.255   
        **/   
        long aBegin = getIpNum("10.0.0.0");    
        long aEnd = getIpNum("10.255.255.255");    
        long bBegin = getIpNum("172.16.0.0");    
        long bEnd = getIpNum("172.31.255.255");    
        long cBegin = getIpNum("192.168.0.0");    
        long cEnd = getIpNum("192.168.255.255");    
        isInnerIp = isInnerIp(ipNum,aBegin,aEnd) || isInnerIp(ipNum,bBegin,bEnd) || isInnerIp(ipNum,cBegin,cEnd) || ipAddress.equals("127.0.0.1");   //访问本地localhost获取为127.0.0.1
        return isInnerIp;               
	}  
	
	private static long getIpNum(String ipAddress) {    
	     String [] ip = ipAddress.split("\\.");    
	     long a = Integer.parseInt(ip[0]);    
	     long b = Integer.parseInt(ip[1]);    
	     long c = Integer.parseInt(ip[2]);    
	     long d = Integer.parseInt(ip[3]);    
	    
	     long ipNum = a * 256 * 256 * 256 + b * 256 * 256 + c * 256 + d;    
	     return ipNum;    
	}  
	
	private static boolean isInnerIp(long userIp,long begin,long end){    
	      return (userIp>=begin) && (userIp<=end);    
}
	
	public static String getIp()
	{
		InetAddress ia = null;
		try {
			ia = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//	System.out.println();
		return ia.getHostAddress();
	}
	
	public static String subnet()
	{
	String ip=	getIp();
	int lastIdx=ip.lastIndexOf(".");
	return ip.substring(0,lastIdx);
	
	}
}
