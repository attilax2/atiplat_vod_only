package com.attilax.web;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.reflect.MethodUtils;

import aaaCfg.IocX4nodb;

import com.attilax.core;
import com.attilax.hre.UrlDslParserV2;
import com.attilax.ref.refx;

/**
 * /CommonServlet?$method= com.attilax.up.FileUploadService.upload
 * 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.process
 * @author Administrator
 *
 */

@WebServlet(name = "CommonServletName", urlPatterns = "/CommonServlet") 
public class CommonServlet implements Servlet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig paramServletConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	public static ThreadLocal<ServletResponse> 	 resp=new ThreadLocal<ServletResponse>();
	@Override
	public void service(ServletRequest req,
			ServletResponse paramServletResponse) throws IOException,
			ServletException {
		
		resp.set(paramServletResponse);
		String ret=new UrlDslParserV2().exe((HttpServletRequest)req);
		paramServletResponse.getWriter().println(ret);
		
	}

}
