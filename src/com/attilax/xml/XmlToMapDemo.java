package com.attilax.xml;
//测试类：
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
public class XmlToMapDemo {
 public static void main(String[] args) throws IOException,
   DocumentException {
  
  FileInputStream fis = new FileInputStream("d://a.xml");
  byte[] b = new byte[fis.available()];
  fis.read(b);
  String str = new String(b);
  
  Document doc = DocumentHelper.parseText(str);
  
  System.out.println(doc.asXML());
  long beginTime = System.currentTimeMillis();
  
  Map<String, Object> map = XmlUtils.Dom2Map(doc);
  System.out.println(map.toString());
  
  System.out.println("Use time:"+(System.currentTimeMillis()-beginTime));
 }
}