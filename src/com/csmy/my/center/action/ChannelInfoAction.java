package com.csmy.my.center.action;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csmy.my.center.service.AllInfoServer;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;

@Controller
@Scope("prototype")
public class ChannelInfoAction extends BaseActionSupport {

	public String channel_name;
	public String channel_uid;
	public String id;
	@Autowired
	private AllInfoServer allInfoServer;

	HttpServletRequest request = ServletActionContext.getRequest();
	HttpServletResponse response = ServletActionContext.getResponse();
	HttpSession session = request.getSession();

	/**
	 * 添加
	 * @return 是否成功
	 * 
	 * http://127.0.0.1:8080/llj_site/channe!addChannel.ct?
	 * channel_name=1123&channel_uid=68123991&SESSION_USER=eK8gZzb9+34=
	 */
	public String addChannel() {
		//Dto pDto=getPraramsAsDto(request);
		Dto pDto = new BaseDto();
		pDto.put("channel_name", channel_name);
		pDto.put("channel_uid", channel_uid);
		boolean statics = allInfoServer.AddChannelInfo(pDto);
		request.setAttribute("statics", statics);

		return SUCCESS;
	}

	/**
	 * 修改
	 * @return 是否成功
	 * 
	 * http://127.0.0.1:8080/llj_site/channe!upChannel.ct?channel_name=8686886&
	 * channel_uid=68123991&id=1&SESSION_USER=eK8gZzb9+34=
	 */
	public String upChannel() {
		//Dto pDto=getPraramsAsDto(request);
		Dto pDto = new BaseDto();
		pDto.put("channel_name", channel_name);
		pDto.put("channel_uid", channel_uid);
		pDto.put("channel_id", id);
		boolean statics = allInfoServer.UpdateChannelInfo(pDto);
		request.setAttribute("statics", statics);

		return SUCCESS;
	}

	/**
	 * 删除
	 * @return 是否成功
	 * 
	 * http://127.0.0.1:8080/llj_site/channe!deChannel.ct?channel_uid=68123991&id=6&SESSION_USER=eK8gZzb9+34=
	 */
	public String deChannel() {
		//Dto pDto=getPraramsAsDto(request);
		Dto pDto = new BaseDto();
		pDto.put("channel_uid", channel_uid);
		pDto.put("channel_id", id);
		boolean statics = allInfoServer.DeleteChannelInfo(pDto);
		request.setAttribute("statics", statics);

		return SUCCESS;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChannel_name() {
		return channel_name;
	}

	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}

	public String getChannel_uid() {
		return channel_uid;
	}

	public void setChannel_uid(String channel_uid) {
		this.channel_uid = channel_uid;
	}

}
