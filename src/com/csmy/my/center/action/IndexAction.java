package com.csmy.my.center.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csmy.my.center.module.UserInfo;
import com.csmy.my.center.service.ResourceInfoService;
import com.csmy.my.center.service.UserInfoService;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.cache.InitSystemCache;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;

import common.Logger;

/**
 * 首页action
 * @author wgp
 * @see 2013-01-15
 *
 */
@Controller
@Scope("prototype")
public class IndexAction extends BaseActionSupport {

	private static final long serialVersionUID = -7494234699032976084L;
	// 日志记录
	Logger log = Logger.getLogger(IndexAction.class);

	public PageModel pager = new PageModel();
	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private ResourceInfoService resourceInfoService;

	public static final String ERROR = "error";// 错误页面
	public static final String LEFTTREE = "left_tree";// 左侧树结构
	
	public static final String CODE = "code";// 字典管理页面
	public static final String ADDCODE = "addcode";// 增加字典页面
	public static final String EDITCODE = "editcode";// 修改字典页面
	public static final String SEECODE = "seecode";// 查看字典页面

	public static final String ADDPARAM = "addparam";// 增加参数页面
	public static final String EDITPARAM = "editparam";// 修改参数页面
	public static final String SEEPARAM = "seeparam";// 查看参数页面
	public static final String PARAMINFO = "paraminfo";// 参数管理页面

	HttpServletRequest request = ServletActionContext.getRequest();
	HttpServletResponse response = ServletActionContext.getResponse();
	HttpSession session = request.getSession();

	
	
	/**
	 * 获取系统坐标导航树结构
	 * @return
	 */
	public String getMainLeftTree(){
		try {
			String menuJsonTreeData = "";
			List<Dto> menuList = null;
			UserInfo userInfo = getUser(request);
			String role_id = userInfo.getRole_id();
			if(CTUtils.isNotEmpty(role_id)){
				Dto pDto = new BaseDto();
				pDto.put("role_id", role_id.replaceAll(",", "','"));
				menuList = CTReader.queryForList("role.getAllRolePermits", pDto);
				menuJsonTreeData = getMenuTreeJsonData(menuList, "module_code", "module_name", "parent_module", "link_url", "frmRight", request);
			}
			request.setAttribute("menuJsonTreeData", menuJsonTreeData);
			return LEFTTREE;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		
		
	}
	
	
	/**
	 * 字典参数管理
	 * @return
	 */
	public String getCodeInfo() {
		List<Dto> dList = null;
		try {
			//获取所有字典参数信息
			Dto dto = getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			/*if(CTUtils.isNotEmpty(queryParam)){
				if(CTUtils.isNotEmpty(page_query) && "1".equals(page_query)){
					queryParam = CCharseter(queryParam,"gbk");
				}else{
					queryParam = CCharseter(queryParam,"utf-8");
				}
			}*/
			dto.put("queryParam", queryParam);

			/**********************分页开始*************************/
			Integer offset = dto.getAsInteger("pager.offset");
			if (offset != null && offset != 0) {
				pageNo = offset / pageSize;
				start = (pageNo * pageSize);
			} else {
				start = 0;
			}
			dto.put("start", start);
			dto.put("limit", pageSize);
			/**********************分页结束*************************/

			dList = CTReader.queryForPage("admin.getCodeInfo", dto);
			Integer totalCount = (Integer) CTReader.queryForObject(
					"admin.getCodeInfoCount", dto);

			pager.setDatas(dList);
			pager.setPageNo(pageNo + 1);
			pager.setPageSize(pageSize);
			pager.setCount(totalCount);
			System.out.println("信息集合大小:" + pager.getPages());

			//返回参数到页面
			request.setAttribute("pm", pager);
			request.setAttribute("alisaname", queryParam);

			return CODE;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	/**
	 * 全局参数管理
	 * @return
	 */
	public String getParamInfo() {
		List<Dto> dList = null;
		try {
			//获取所有字典参数信息
			Dto dto = getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			/*if(CTUtils.isNotEmpty(queryParam)){
				if(CTUtils.isNotEmpty(page_query) && "1".equals(page_query)){
					queryParam = CCharseter(queryParam,"gbk");
				}else{
					queryParam = CCharseter(queryParam,"utf-8");
				}
			}*/
			dto.put("queryParam", queryParam);

			/**********************分页开始*************************/
			Integer offset = dto.getAsInteger("pager.offset");
			if (offset != null && offset != 0) {
				pageNo = offset / pageSize;
				start = (pageNo * pageSize);
			} else {
				start = 0;
			}
			dto.put("start", start);
			dto.put("limit", pageSize);
			/**********************分页结束*************************/

			dList = CTReader.queryForPage("admin.getParamInfo", dto);
			Integer totalCount = (Integer) CTReader.queryForObject(
					"admin.getParamInfoCount", dto);

			pager.setDatas(dList);
			pager.setPageNo(pageNo + 1);
			pager.setPageSize(pageSize);
			pager.setCount(totalCount);
			System.out.println("信息集合大小:" + pager.getPages());

			//返回参数到页面
			request.setAttribute("pm", pager);
			request.setAttribute("alisaname", queryParam);

			return PARAMINFO;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	/**
	 * 增加字典参数
	 * @return
	 */
	public String addCodeInfo() {
		//获取下拉数据
		Dto dto = getPraramsAsDto(request);
		request.setAttribute("enabledJsonData", getCombJsonData("ENABLED"));
		request.setAttribute("editmodeJsonData", getCombJsonData("EDITMODE"));
		request.setAttribute("isleafJsonData", getCombJsonData("LEAFTYPE"));
		request.setAttribute("menu_id", dto.getAsString("menu_id"));
		request.setAttribute("menu_value", dto.getAsString("menu_value"));
		request.setAttribute("action_value", dto.getAsString("action_value"));

		return ADDCODE;
	}

	/**
	 * 修改字典参数
	 * @return
	 */
	public String editCodeInfo() {

		//获取下拉数据
		Dto dto = getPraramsAsDto(request);
		request.setAttribute("enabledJsonData", getCombJsonData("ENABLED"));
		request.setAttribute("editmodeJsonData", getCombJsonData("EDITMODE"));
		request.setAttribute("isleafJsonData", getCombJsonData("LEAFTYPE"));
		request.setAttribute("menu_id", dto.getAsString("menu_id"));
		request.setAttribute("menu_value", dto.getAsString("menu_value"));
		request.setAttribute("action_value", dto.getAsString("action_value"));

		String listerTreeData = "";
		List<Dto> codeList = CTReader.queryForList("admin.getCodeTreeDatas");
		if (CTUtils.isNotEmpty(codeList)) {
			listerTreeData = getcCommJsonData(codeList, "code_id", "code_desc",
					"parent_id", "false", false, null, false);
		}
		request.setAttribute("codeTreeData", listerTreeData);
		//获取当前字典值
		Dto codeInfo = (Dto) CTReader.queryForObject("admin.getCodeById", dto);
		request.setAttribute("codeInfo", codeInfo);
		return EDITCODE;
	}

	/**
	 * 查看全局参数
	 * @return
	 */
	public String seeCodeInfo() {
		//获取下拉数据
		Dto dto = getPraramsAsDto(request);
		//获取当前全局值
		Dto codeInfo = (Dto) CTReader.queryForObject("admin.getCodeById", dto);
		codeInfo.put("enabled", getCodeName("ENABLED", codeInfo
				.getAsString("enabled")));
		codeInfo.put("edit_mode", getCodeName("EDITMODE", codeInfo
				.getAsString("edit_mode")));
		request.setAttribute("codeInfo", codeInfo);
		return SEECODE;
	}

	/**
	 * 增加全局参数
	 * @return
	 */
	public String addParamInfo() {
		//获取下拉数据
		Dto dto = getPraramsAsDto(request);

		return ADDPARAM;
	}

	/**
	 * 修改全局参数
	 * @return
	 */
	public String editParamInfo() {
		//获取当前全局值
		Dto dto = getPraramsAsDto(request);
		Dto paramInfo = (Dto) CTReader
				.queryForObject("admin.getParamByID", dto);
		request.setAttribute("paramInfo", paramInfo);
		return EDITPARAM;
	}

	/**
	 * 查看全局参数
	 * @return
	 */
	public String seeParamInfo() {
		//获取当前全局值
		Dto dto = getPraramsAsDto(request);
		Dto paramInfo = (Dto) CTReader
				.queryForObject("admin.getParamByID", dto);
		request.setAttribute("paramInfo", paramInfo);
		return SEEPARAM;
	}

	/**
	 * 操作字典信息
	 */
	public void operCodeInfo() {
		boolean temp = false;
		String err_msg = "false";
		try {
			Dto dto = getPraramsAsDto(request);
			if (CTUtils.isNotEmpty(dto)) {
				temp = resourceInfoService.operCodeInfo(dto);
				if (temp) {
					err_msg = "true";
					InitSystemCache.updateCache(CTConstants.CT_CODE_LIST, request.getSession().getServletContext());
				} else {
					err_msg = "false";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//处理权限更新操作
			renderText(err_msg);
		}
	}

	/**
	 * 操作全局信息
	 */
	public void operParamInfo() {
		boolean temp = false;
		String err_msg = "false";
		try {
			Dto dto = getPraramsAsDto(request);
			if (CTUtils.isNotEmpty(dto)) {
				temp = resourceInfoService.operParamInfo(dto);
				if (temp) {
					err_msg = "true";
					//重新加载全局参数
					InitSystemCache.updateCache(CTConstants.CT_PARAM_LIST, request.getSession().getServletContext());
				} else {
					err_msg = "false";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//处理权限更新操作
			renderText(err_msg);
		}
	}

	public PageModel getPager() {
		return pager;
	}

	public void setPager(PageModel pager) {
		this.pager = pager;
	}

}
