package com.csmy.my.center.dao;

 
import org.springframework.stereotype.Repository;

import com.attilax.log.Logger;
import com.csmy.my.center.util.dataconvert.Dto;

@SuppressWarnings("unchecked")
@Repository
public class AllInfoDao  extends DaoImpl{

	
	
	private static final long serialVersionUID = -7977535368743775681L;
	private Logger log = Logger.getLogger(AllInfoDao.class);
			//
	
	
	/**
	 * 添加渠道
	 * @param pDto
	 * @return
	 */
	public boolean AddChannelInfo(Dto pDto) {
		boolean statics=false;
		try{
			
			insert("resource.addChannel", pDto);
			statics =true;
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		return statics;
		
	}
	
	
	/**
	 * 修改渠道
	 * @param pDto
	 * @return
	 */
	public boolean UpdateChannelInfo(Dto pDto) {
		boolean statics=false;
		try{
			
			update("resource.upChannel", pDto);
			statics =true;
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		return statics;
		
	}
	
	
	/**
	 * 删除渠道
	 * @param pDto
	 * @return
	 */
	public boolean DeleteChannelInfo(Dto pDto) {
		boolean statics=false;
		try{
			delete("resource.deChannel", pDto);
			statics =true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return statics;
	}
	
	
}
