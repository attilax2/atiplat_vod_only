package com.csmy.my.center.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.UniqueID;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.idgenerator.IdGenerator;

@SuppressWarnings("unchecked")
@Repository
public class GoodInfoDao extends DaoImpl {

	private static final long serialVersionUID = -7977535368743775681L;
	private Logger log = Logger.getLogger(GoodInfoDao.class);

	/**
	 * 操作商品数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operGoodInfo(Dto pDto) {

		boolean temp = false;
		try {
			String optype = pDto.getAsString("optype");
			// 处理业务逻辑
			if (CTUtils.isNotEmpty(optype)) {
				if ("add_type".equals(optype)) {
					String parent_id = pDto.getAsString("parent_id");
					pDto.put("type_id", IdGenerator.getTypeIdGenerator(parent_id));
					insert("good.addGoodTypeInfo", pDto);
					temp = true;
				}
				if ("add_copy".equals(optype)) {
					pDto.put("createtime", CTUtils.getCurrentTime());
					insert("good.addGoodCopy", pDto);
					temp = true;
				}
				if ("edit_copy".equals(optype)) {
					update("good.editGoodCopy", pDto);
					temp = true;
				}
				if ("delete_copy".equals(optype)) {
					 
					delete("good.deleteGoodCopy", pDto);
					temp = true;
				}
				if ("edit_type".equals(optype)) {
						update("good.editGoodTypeInfo", pDto);
					temp = true;
				}

				if ("delete_type".equals(optype)) {
					delete("good.deleteGoodTypeInfoByID", pDto);
					temp = true;
				}
				
				if ("add_good".equals(optype)) {
					if(CTUtils.isEmpty(pDto.getAsString("enabled"))){
				  	   pDto.put("enabled", "0");
					}
					pDto.put("toped", "0");//置顶
					pDto.put("recomed", "0");//推荐
					pDto.put("create_time", CTUtils.getCurrentTime());
					insert("good.addGoodInfo", pDto);
					//处理套餐信息
					int len=0;
					String sku_title = pDto.getAsString("sku_title");
					String sku_cost = pDto.getAsString("sku_cost");
					String sku_price = pDto.getAsString("sku_price");
					String sku_pmoney = pDto.getAsString("sku_pmoney");
					String service_money = pDto.getAsString("service_money");
					
					if(CTUtils.isNotEmpty(sku_title)&&CTUtils.isNotEmpty(sku_price)){
						String title[] = sku_title.split("\\|");
						String cost[] = sku_cost.split("\\|");
						String price[] = sku_price.split("\\|");
						String pmoney[] = sku_pmoney.split("\\|");
						String smoney[] = service_money.split("\\|");
						
						//先删除信息
						delete("good.deleteGoodSkuByGoodID",pDto);
						
						len = title.length;
						Dto perDto = null;
						for (int i=0;i<len;i++) {
							
							perDto = new BaseDto();
							perDto.put("order_no", i);
							perDto.put("sku_name", title[i]);
							perDto.put("good_id", pDto.getAsString("good_id"));
							perDto.put("sku_cost", cost[i]);
							perDto.put("sku_price", price[i]);
							perDto.put("sku_pmoney", pmoney[i]);
							perDto.put("service_money", smoney[i]);
							perDto.put("sku_id", UniqueID.getUniqueID(8, 0));
							
							//加入数据
							insert("good.addGoodSkuInfo",perDto);
						}
					}
					
					temp = true;
				}
				
				if ("edit_good".equals(optype)) {
					insert("good.editGoodInfo", pDto);
					//获取sku
					List<Dto> skuList = queryForList("good.getGoodSkuInfo", pDto);
					//现有的sku
					String kustr = StringUtil.getStrFromList(skuList, "sku_id", ",");
					//处理套餐信息
					String sku_str = pDto.getAsString("sku_str");
					if(CTUtils.isNotEmpty(sku_str)){
						Dto perDto = null;
						String skus[] = sku_str.split("\\$");
						for (int j = 0; j < skus.length; j++) {
							String sk[] = skus[j].split("\\|");
							String sku_id = sk[0];
							
							perDto = new BaseDto();
							perDto.put("order_no", j);
							perDto.put("sku_name", sk[1]);
							perDto.put("good_id", pDto.getAsString("good_id"));
							perDto.put("sku_cost", sk[2]);
							perDto.put("sku_price", sk[3]);
							perDto.put("sku_pmoney", sk[4]);
							perDto.put("service_money", sk[5]);
							if(sku_id.equals("newid")){//新增数据
								sku_id = UniqueID.getUniqueID(8, 0);
								perDto.put("sku_id", sku_id);
								insert("good.addGoodSkuInfo",perDto);
							}else{//更新数据
								perDto.put("sku_id", sku_id);
								update("good.editGoodSkuInfo",perDto);
								//处理删除的
								kustr = kustr.replaceAll(sku_id, "");
							}
						}
						
						if(CTUtils.isNotEmpty(kustr)){
							String kus[] = kustr.split(",");
							for (String kuid : kus) {
								Dto dlDto = new BaseDto();
								if(CTUtils.isNotEmpty(kuid)){
									dlDto.put("sku_id", kuid);
									delete("good.deleteGoodSkuBySkuID",dlDto);
								}
								
							}
						}
					}
					temp = true;
				}
				
				if ("delete_good".equals(optype)) {
					String good_id = pDto.getAsString("good_id");
					if(CTUtils.isNotEmpty(good_id)){
						good_id = good_id.replaceAll(",", "','");
						pDto.put("good_id", good_id);
					}
					//删除套餐
					delete("good.deleteGoodSkuByGoodID", pDto);
					delete("good.deleteGood", pDto);
					temp = true;
				}
				
				//置顶操作
				if("toped".equals(optype)){
					pDto.put("toped_time", CTUtils.getCurrentTime());
					String good_id = pDto.getAsString("good_id");
					if(CTUtils.isNotEmpty(good_id)){
						good_id = good_id.replaceAll(",", "','");
						pDto.put("good_id", good_id);
					}
					update("good.goodTop", pDto);
					temp = true;
				}
				
				//推荐操作
				if("recomed".equals(optype)){
					pDto.put("recomed_time", CTUtils.getCurrentTime());
					String good_id = pDto.getAsString("good_id");
					if(CTUtils.isNotEmpty(good_id)){
						good_id = good_id.replaceAll(",", "','");
						pDto.put("good_id", good_id);
					}
					update("good.goodRecom", pDto);
					temp = true;
				}
				
				//上下加操作
				if("upStore".equals(optype)){
					String good_id = pDto.getAsString("good_id");
					if(CTUtils.isNotEmpty(good_id)){
						good_id = good_id.replaceAll(",", "','");
						pDto.put("good_id", good_id);
					}
					update("good.goodUpStore", pDto);
					temp = true;
				}
			}
		} catch (Exception e) {
			//{copy_id=83467585_cid, optype=delete_copy, now=1459357841919}
			temp = false;
			e.printStackTrace();
		}
		return temp;
	}
}