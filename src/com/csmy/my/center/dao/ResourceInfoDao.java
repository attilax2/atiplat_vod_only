package com.csmy.my.center.dao;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.UniqueID;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.idgenerator.IDHelper;
import com.csmy.my.center.util.idgenerator.IdGenerator;

/**
 * 资源维护dao
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class ResourceInfoDao extends DaoImpl {

	private static final long serialVersionUID = -7977535368743775681L;
	private Logger log = Logger.getLogger(ResourceInfoDao.class);

	// 连接全局对象
	protected Connection con = null;
	// 语句集全局对象
	protected Statement sta = null;
	// 结果集全局对象
	protected ResultSet rs = null;
	
	
	/**
	 * 操作字典数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operCodeInfo(Dto pDto) {

		boolean temp = false;
		try {
			String optype = pDto.getAsString("optype");
			// 处理业务逻辑
			if (CTUtils.isNotEmpty(optype)) {
				if ("add".equals(optype)) {
					// 增加字典信息
					pDto.put("codeid", IDHelper.getCodeID());
					insert("admin.addCodeInfo", pDto);
					temp = true;
				}

				if ("edit".equals(optype)) {
					// 修改字典信息
					update("admin.editCodeInfo", pDto);
					temp = true;
				}
				
				if ("delete".equals(optype)) {
					String action_code = pDto.getAsString("code_id");
					delete("admin.deleteCodeInfo", pDto);
					StringUtil.xprint("删除字典["+action_code+"]的信息成功！");
					temp = true;
				}
			}
		} catch (Exception e) {
			temp = false;
			StringUtil.xprint("对不起！操作字典参数信息失败！");
			e.printStackTrace();
		}
		return temp;
	}
	
	/**
	 * 操作订单数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operOrderInfo(Dto pDto) {

		boolean temp = false;
		try {
			String optype = pDto.getAsString("optype");
			// 处理业务逻辑
			if (CTUtils.isNotEmpty(optype)) {
				if ("add_log".equals(optype)) {
					// 增加订单日志信息
					pDto.put("createtime", CTUtils.getCurrentTime());
					insert("order.addOrderLog", pDto);
					temp = true;
				}
				if ("add_slog".equals(optype)) {
					// 增加订单客服日志信息
					pDto.put("createtime", CTUtils.getCurrentTime());
					insert("order.addServerLog", pDto);
					temp = true;
				}
				if ("add_plog".equals(optype)) {
					// 增加付款日志信息
					pDto.put("createtime", CTUtils.getCurrentTime());
					insert("order.addPayMoneyLog", pDto);
					//修改结算单的状态
					pDto.put("stateid", "1");
					update("order.updateOrderMoney",pDto);
					//添加订单日志信息
					List<Dto> orderList = queryForList("order.getDPayOrderList", pDto);
					if(CTUtils.isNotEmpty(orderList)){
						for (Dto logDto : orderList) {
							logDto.put("log_txt", "已支付、已结清");
							logDto.put("createtime", CTUtils.getCurrentTime());
							insert("order.addOrderLog", logDto);
						}
					}
					temp = true;
				}
				if ("add_splog".equals(optype)) {
					// 增加付款日志信息
					pDto.put("createtime", CTUtils.getCurrentTime());
					if(CTUtils.isEmpty(pDto.getAsString("remark"))){
						pDto.put("remark","结算客户提成");
					}
					insert("order.addSPayMoneyLog", pDto);
					//修改结算单的状态
					pDto.put("stateid", "1");
					update("order.updateSOrderMoney",pDto);
					temp = true;
				}
				if ("reback".equals(optype)) {
					//撤销支付
					delete("order.deletePayLog", pDto);
					//修改结算单的状态
					pDto.put("stateid", "0");
					update("order.updateOrderMoney",pDto);
					temp = true;
				}
				if ("order_count".equals(optype)) {
					// 增加订单结算流水信息
					insert("order.addOrderCount", pDto);
					temp = true;
				}
				if ("sorder_count".equals(optype)) {
					// 增加客服订单结算流水信息
					insert("order.addSOrderCount", pDto);
					temp = true;
				}

				if ("edit_order".equals(optype)) {
					// 修改订单信息
					pDto.put("address", pDto.getAsString("address"));
					pDto.put("city", pDto.getAsString("city"));
					pDto.put("area", pDto.getAsString("area"));
					pDto.put("province", pDto.getAsString("province"));
					pDto.put("buyer_name", pDto.getAsString("buyer_name"));
					update("order.updateOrder", pDto);
					temp = true;
				}
				
				if ("mod_order".equals(optype)) {
					// 修改订单信息
					update("order.updateOrder", pDto);
					temp = true;
				}
				
				if ("check_order".equals(optype)) {//订单结算
					// 修改订单信息
					String ocid = pDto.getAsString("ocid");
					String state_id = pDto.getAsString("stateid");
					// 获取订单流水集合信息
					Dto orMemberInfo = null;
					List<Dto> ocList = null;
					if (CTUtils.isNotEmpty(ocid)) {
						ocid = ocid.replaceAll(",", "','");
						pDto.put("ocid", ocid);
						if (state_id.equalsIgnoreCase("3")) {// 处理生成打款单据
							//获取结算单
							ocList = queryForList("order.getOrderCountList",pDto);
							//获取分成比例 
							String mpercent = CTUtils.getParamValue("MEM_PERCENT");
							StringUtil.xprint("全局分成比例："+mpercent);
							if(CTUtils.isEmpty(mpercent)){
								mpercent = "0.05";//如果没有设置就给所有上级奖励5%
							}
							double perLevel =Double.valueOf(mpercent);
							StringUtil.xprint("默认分成比例：" + perLevel);
							
							//处理多级分成
							String pmoney_level = CTUtils.getParamValue("PMONEY_LEVEL_SET");
							if(CTUtils.isNotEmpty(pmoney_level)){
								
								if (!StringUtil.checkListBlank(ocList)) {
									Dto orDto = null;
									String order_id = null;
									for (Dto oDto : ocList) {
										order_id = oDto.getAsString("order_id");
										double orderPMoney = Double.valueOf(oDto.getAsString("order_pmoney"));//分成
										//获取当前会员信息
										orMemberInfo = (BaseDto) queryForObject("system.getMemberInfo", oDto);
										int level_count = 0;//计数器
										String levels [] = null;
										String plevels[] = null;
										String level_code = null;
										if (CTUtils.isNotEmpty(orMemberInfo)) {
											level_code = orMemberInfo.getAsString("level_code");
											levels = level_code.split("\\|");
											if(levels!=null && levels.length>0){//处理层级分成关系
												Dto sDto = null;
												Dto perMember = null;
												if(CTUtils.isNotEmpty(pmoney_level)){
													plevels = pmoney_level.split("\\|");
													StringUtil.xprint("设置分成比例为："+pmoney_level);
												}
												for (int i = levels.length ; i>0 ; i--) {//添加分成数据
													sDto = new BaseDto();
													sDto.put("visit_code", levels[i-1]);
													perMember = (BaseDto) queryForObject("system.getMemberInfo", sDto);
													//保存数据
													orDto = new BaseDto();
	        										orDto.put("user_id", perMember.getAsString("memeber_id"));// 推荐人ID
	        										orDto.put("state", "0");// 未支付
	        										orDto.put("ocid", oDto.getAsString("ocid"));// 流水号
	        										orDto.put("createtime", CTUtils.getCurrentTime());// 返款时间
	        										
													if(level_count==0){
														orDto.put("visit_money","0.0");
														orDto.put("order_money",orderPMoney);
		        										orDto.put("own_money",orderPMoney);
		        										orDto.put("rm_type","0");
													}
													
	                                                if(level_count==1){//第一级上级
	                                                	if(CTUtils.isNotEmpty(plevels[0])){
	                                                		perLevel = Double.valueOf(plevels[0]);
	                                                		StringUtil.xprint("第一级上级分成比例为："+perLevel);
	                                                	}
	                                                	orDto.put("visit_money",orderPMoney * perLevel);
	            										orDto.put("own_money",orderPMoney * perLevel);
	            										orDto.put("order_money","0.0");
	            										orDto.put("rm_type","1");
													}
	                                                
	                                                if(level_count==2){//第二级上级
	                                                	if(CTUtils.isNotEmpty(plevels[1])){
	                                                		perLevel = Double.valueOf(plevels[1]);
	                                                		StringUtil.xprint("第二级上级分成比例为："+perLevel);
	                                                	}
	                                                	orDto.put("visit_money",orderPMoney * perLevel);
	            										orDto.put("own_money",orderPMoney * perLevel);
	            										orDto.put("order_money","0.0");
	            										orDto.put("rm_type","1");
													}
	                                                
	                                                if(level_count==3){//第三级上级
	                                                	if(CTUtils.isNotEmpty(plevels[2])){
	                                                		perLevel = Double.valueOf(plevels[2]);
	                                                		StringUtil.xprint("第三级上级分成比例为："+perLevel);
	                                                	}
	                                                	orDto.put("visit_money",orderPMoney * perLevel);
	            										orDto.put("own_money",orderPMoney * perLevel);
	            										orDto.put("order_money","0.0");
	            										orDto.put("rm_type","1");
													}
	                                               
	        										insert("order.addOrderMoney", orDto);
	        										
	        										level_count++;
	        										if(level_count>3)break;//控制级别为3级
												}
											}
										}
										
										//计入日志
										Dto logDto = new BaseDto();
										logDto.put("log_txt", "已结算返款、待支付");
										logDto.put("createtime", CTUtils.getCurrentTime());
										logDto.put("order_id", order_id);
										insert("order.addOrderLog", logDto);
									}
								}
								
								
							}else{//默认只处理两级
								
								if (!StringUtil.checkListBlank(ocList)) {
									Dto orDto = null;
									Dto omDto = null;
									String order_id = null;
									for (Dto oDto : ocList) {
										order_id = oDto.getAsString("order_id");
										double orderPMoney = Double.valueOf(oDto.getAsString("order_pmoney"));//分成
										orMemberInfo = (BaseDto) queryForObject("order.getORMemeberByOrderID", oDto);
										// 如果此订单有上级推荐人 则增加一条邀请奖励金额
										if (CTUtils.isNotEmpty(orMemberInfo)) {

											orDto = new BaseDto();
											orDto.put("user_id", orMemberInfo.getAsString("memeber_id"));// 推荐人ID
											orDto.put("visit_money",orderPMoney * perLevel);// 计算奖励金额
											orDto.put("own_money",orderPMoney * perLevel);// 应返款数
											orDto.put("order_money","0.0");
											orDto.put("state", "0");// 未支付
											orDto.put("ocid", oDto.getAsString("ocid"));// 流水号
											orDto.put("createtime", CTUtils.getCurrentTime());// 返款时间
											// 增加记录
											insert("order.addOrderMoney", orDto);

										}

										// 生成订单返款单
										omDto = new BaseDto();
										omDto.put("user_id", oDto.getAsString("user_id"));// 当前用户编号
										omDto.put("order_money", orderPMoney);// 订单金额
										omDto.put("own_money", orderPMoney);// 应返款数
										omDto.put("visit_money", "0.0");
										omDto.put("state", "0");// 未支付
										omDto.put("ocid", oDto.getAsString("ocid"));// 流水号
										omDto.put("createtime", CTUtils.getCurrentTime());// 返款时间
										// 增加记录
										insert("order.addOrderMoney", omDto);
										
										//计入日志
										Dto logDto = new BaseDto();
										logDto.put("log_txt", "已结算返款、待支付");
										logDto.put("createtime", CTUtils.getCurrentTime());
										logDto.put("order_id", order_id);
										insert("order.addOrderLog", logDto);
									}
								}
								
							}//处理层级关系结束
						}
					}

					//处理订单流水状态
					update("order.updateOrderCount", pDto);
					temp = true;
				}
				
				
				/*if ("check_order".equals(optype)) {//订单结算
					// 修改订单信息
					String ocid = pDto.getAsString("ocid");
					String state_id = pDto.getAsString("stateid");
					//获取分成比例
					String mpercent = CTUtils.getParamValue("MEM_PERCENT");
					StringUtil.xprint("全局分成比例："+mpercent);
					if(CTUtils.isEmpty(mpercent)){
						mpercent = "0.05";//默认值0.05
					}
					double pcent =Double.valueOf(mpercent);
					StringUtil.xprint("分成比例：" + pcent);
					
					// 获取订单流水集合信息
					Dto orMemberInfo = null;
					List<Dto> ocList = null;
					if (CTUtils.isNotEmpty(ocid)) {
						ocid = ocid.replaceAll(",", "','");
						pDto.put("ocid", ocid);
						if (state_id.equalsIgnoreCase("3")) {// 处理生成打款单据
							ocList = queryForList("order.getOrderCountList",pDto);
							if (!StringUtil.checkListBlank(ocList)) {
								Dto orDto = null;
								Dto omDto = null;
								String order_id = null;
								for (Dto oDto : ocList) {
									order_id = oDto.getAsString("order_id");
									double orderPMoney = Double.valueOf(oDto.getAsString("order_pmoney"));//分成
									orMemberInfo = (BaseDto) queryForObject("order.getORMemeberByOrderID", oDto);
									// 如果此订单有上级推荐人 则增加一条邀请奖励金额
									if (CTUtils.isNotEmpty(orMemberInfo)) {

										orDto = new BaseDto();
										orDto.put("user_id", orMemberInfo.getAsString("memeber_id"));// 推荐人ID
										orDto.put("visit_money",orderPMoney * pcent);// 计算奖励金额
										orDto.put("own_money",orderPMoney * pcent);// 应返款数
										orDto.put("order_money","0.0");
										orDto.put("state", "0");// 未支付
										orDto.put("ocid", oDto.getAsString("ocid"));// 流水号
										orDto.put("createtime", CTUtils.getCurrentTime());// 返款时间
										// 增加记录
										insert("order.addOrderMoney", orDto);

									}

									// 生成订单返款单
									omDto = new BaseDto();
									omDto.put("user_id", oDto.getAsString("user_id"));// 当前用户编号
									omDto.put("order_money", orderPMoney);// 订单金额
									omDto.put("own_money", orderPMoney);// 应返款数
									omDto.put("visit_money", "0.0");
									omDto.put("state", "0");// 未支付
									omDto.put("ocid", oDto.getAsString("ocid"));// 流水号
									omDto.put("createtime", CTUtils.getCurrentTime());// 返款时间
									// 增加记录
									insert("order.addOrderMoney", omDto);
									
									
									//计入日志
									Dto logDto = new BaseDto();
									logDto.put("log_txt", "已结算返款、待支付");
									logDto.put("createtime", CTUtils.getCurrentTime());
									logDto.put("order_id", order_id);
									insert("order.addOrderLog", logDto);
								}
							}
						}
					}

					//处理订单流水状态
					update("order.updateOrderCount", pDto);
					temp = true;
				}*/
				
				
				if ("check_sorder".equals(optype)) {//客服订单结算
					String ocid = pDto.getAsString("scid");
					String state_id = pDto.getAsString("stateid");
					// 获取流水集合信息
					List<Dto> ocList = null;
					if (CTUtils.isNotEmpty(ocid)) {
						ocid = ocid.replaceAll(",", "','");
						pDto.put("scid", ocid);
						if (state_id.equalsIgnoreCase("3")) {// 处理生成打款单据
							ocList = queryForList("order.getSOrderCountList",pDto);
							if (!StringUtil.checkListBlank(ocList)) {
								Dto omDto = null;
								for (Dto oDto : ocList) {
									double orderSMoney = Double.valueOf(oDto.getAsString("server_pmoney"));//分成
									// 生成订单返款单
									omDto = new BaseDto();
									omDto.put("user_id", oDto.getAsString("user_id"));// 当前用户编号
									omDto.put("own_money", orderSMoney);// 应返款数
									omDto.put("server_money", orderSMoney);//结算分成
									omDto.put("state", "0");// 未支付
									omDto.put("scid", oDto.getAsString("scid"));// 流水号
									omDto.put("createtime", CTUtils.getCurrentTime());// 返款时间
									// 增加记录
									insert("order.addSOrderMoney", omDto);
								}
							}
						}
					}

					//处理结算流水状态
					update("order.updateSOrderCount", pDto);
					temp = true;
				}
				
				
				if ("delete_order".equals(optype)) {
					String order_id = pDto.getAsString("order_id");
					if(CTUtils.isNotEmpty(order_id)){
						//删除订单日志信息
						delete("order.deleteOrderLog", pDto);
						StringUtil.xprint("删除订单["+order_id+"]的日志信息成功！");
						//删除订单流水
						delete("order.deleteOrderCount", pDto);
						StringUtil.xprint("删除订单["+order_id+"]的流水信息成功！");
						//删除订单信息
						delete("order.deleteOrder", pDto);
						StringUtil.xprint("删除订单["+order_id+"]的信息成功！");
						temp = true;
					}
				}
				
				if ("batch_delete".equals(optype)) {
					String order_id = pDto.getAsString("order_id");
					if(CTUtils.isNotEmpty(order_id)){
						order_id = order_id.replaceAll(",", "','");
						pDto.put("order_id", order_id);
						//删除订单日志信息
						delete("order.deleteOrderLog", pDto);
						StringUtil.xprint("删除订单["+order_id+"]的日志信息成功！");
						//删除订单流水
						delete("order.deleteOrderCount", pDto);
						StringUtil.xprint("删除订单["+order_id+"]的流水信息成功！");
						//删除订单信息
						delete("order.deleteOrder", pDto);
						StringUtil.xprint("删除订单["+order_id+"]的信息成功！");
						temp = true;
					}
				}
			}
		} catch (Exception e) {
			temp = false;
			StringUtil.xprint("对不起！操作订单信息失败！");
			e.printStackTrace();
		}
		return temp;
	}
	
	/**
	 * 操作内容数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operContentInfo(Dto pDto) {

		boolean temp = false;
		try {
			String optype = pDto.getAsString("optype");
			// 处理业务逻辑
			if (CTUtils.isNotEmpty(optype)) {
				if ("add_news".equals(optype)) {
					// 增加正文
					pDto.put("click_num", "0");
					pDto.put("news_state", "0");
					pDto.put("create_time", CTUtils.getCurrentTime());
					insert("system.addSiteNews", pDto);
					temp = true;
				}

				if ("edit_news".equals(optype)) {
					// 修改正文
					update("system.editSiteNews", pDto);
					temp = true;
				}
				
				if ("del_news".equals(optype)) {
					String news_id = pDto.getAsString("news_id");
					delete("system.deleteSiteNews", pDto);
					StringUtil.xprint("删除公告["+news_id+"]的信息成功！");
					temp = true;
				}
				
				//清空日志信息
				if ("clear".equals(optype)) {
					update("system.clearLog");
					temp = true;
				}
				
				
			}
		} catch (Exception e) {
			temp = false;
			StringUtil.xprint("对不起！操作公告信息失败！");
			e.printStackTrace();
		}
		return temp;
	}
	
	
	/**
	 * 操作全局数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operParamInfo(Dto pDto) {

		boolean temp = false;
		try {
			String optype = pDto.getAsString("optype");
			// 处理业务逻辑
			if (CTUtils.isNotEmpty(optype)) {
				if ("add".equals(optype)) {
					// 增加全局参数信息
					pDto.put("paramid", IDHelper.getParamID());
					insert("admin.addParamInfo", pDto);
					temp = true;
				}

				if ("edit".equals(optype)) {
					// 修改全局参数信息
					update("admin.editParamInfo", pDto);
					temp = true;
				}
				
				if ("delete".equals(optype)) {
					String paramkey = pDto.getAsString("paramkey");
					delete("admin.deleteParamInfo", pDto);
					StringUtil.xprint("删除全局参数["+paramkey+"]的信息成功！");
					temp = true;
				}
			}
		} catch (Exception e) {
			temp = false;
			StringUtil.xprint("对不起！操作全局参数信息失败！");
			e.printStackTrace();
		}
		return temp;
	}
	
	
	
	/**
	 * 操作菜单数据
	 * 
	 * @param pDto
	 * @return
	 */
	public Dto operMenuInfo(Dto pDto) {

		Dto reDto = null;
		try {
			
			reDto = new BaseDto();
			String optype = pDto.getAsString("optype");
			// 处理业务逻辑
			if (CTUtils.isNotEmpty(optype)) {
				
				// 增加菜单信息
				if ("add".equals(optype)) {
					String parent_id = pDto.getAsString("parent_id");
					//验证菜单是否已经存在
					Dto mDto = (BaseDto)queryForObject("resource.checkMenuExsit", pDto);
					if(CTUtils.isNotEmpty(mDto)){
						reDto.put("SUC", "false");
						reDto.put("MSG", "对不起！菜单[<font color=red>"+pDto.getAsString("menu_name")+"</font>]已经存在！");
					}else{
						String menu_code = IdGenerator.getMenuIdGenerator(parent_id);
						pDto.put("menu_id", UniqueID.getUniqueID(8, 0));
						pDto.put("menu_code", menu_code);
						//增加一条菜单数据
						insert("resource.addMenuInfo", pDto);
						reDto.put("SUC", "true");
						reDto.put("MSG", "恭喜你！添加菜单信息成功！");
					}
				}

				// 修改菜单信息
				if ("edit".equals(optype)) {
					
					String menu_code = pDto.getAsString("menu_code");
					String parent_id = pDto.getAsString("parent_id");
					//验证菜单是否已经存在
					Dto mDto = (BaseDto)queryForObject("resource.checkMenuExsit", pDto);
					if(CTUtils.isNotEmpty(mDto)&& !menu_code.equals(mDto.getAsString("module_code"))){
						reDto.put("SUC", "false");
						reDto.put("MSG", "对不起！菜单[<font color=red>"+pDto.getAsString("menu_name")+"</font>]已经存在！");
						return reDto;
					}
					
					String menu_id = "";
					//处理上级关系 如果当前选择上级和原来上级不同需要更新
					String old_parent = pDto.getAsString("old_pid");
					if(!old_parent.equals(parent_id)){
						//要清除现在的菜单对应的权限信息
						delete("resource.deleteMenuPermit", pDto);
						
						//从新计算当前菜单编号
						menu_id = IdGenerator.getMenuIdGenerator(parent_id);//新编号
						StringUtil.xprint("======修改后的主键编号为："+menu_id);
						pDto.put("menu_id", menu_id);
						
						//更新下级节点的编号和上级编号
						String u_id = "";
						Dto ouDto = new BaseDto();
						Dto uDto = new BaseDto();
						ouDto.put("menu_code", menu_code);
						List<Dto> childList = queryForList("resource.getAllChildsByPID", ouDto);
						for(Dto oDto : childList) {
							String o_id = oDto.getAsString("module_code");
							String o_pid = oDto.getAsString("parent_module");
							if(CTUtils.isNotEmpty(o_id) && CTUtils.isNotEmpty(o_pid)){
								u_id = o_id.replaceAll(o_pid, menu_id);//替换旧的编号为新的编号前缀
								uDto.put("menu_id", u_id);
								uDto.put("menu_code", o_id);
								uDto.put("parent_id", menu_id);
								update("resource.editMenuInfo", uDto);
							}
						}
					}
					
					update("resource.editMenuInfo", pDto);
					reDto.put("SUC", "true");
					reDto.put("MSG", "恭喜你！修改菜单信息成功！");
				}

				if ("delete".equals(optype)) {
					
					String menu [] = null;
					String menuID = pDto.getAsString("menuid");
					if(CTUtils.isNotEmpty(menuID)){
						menu = menuID.split(",");
						for (String mid : menu) {
							pDto.put("menu_code", mid);
							//解除菜单功能和角色更新
							delete("resource.deleteMenuPermit", pDto);
							StringUtil.xprint("解除菜单["+mid+"]的角色权限成功！");
							// 删除菜单信息
							delete("resource.deleteMenuInfo", pDto);
							StringUtil.xprint("删除菜单["+mid+"]的基本信息成功！");
						}
					}
					
					reDto.put("SUC", "true");
					reDto.put("MSG", "恭喜你！删除菜单信息成功！");
				}
				
			}
				
				
		} catch (Exception e) {
			reDto.put("SUC", "false");
			reDto.put("MSG", "对不起！操作菜单信息失败！");
			e.printStackTrace();
		}finally{
			return reDto;
		}
	}

	
	
	
	
	/**
	 * 判断数据库是否支持批处理
	 * @param con
	 * @return
	 */
	public static boolean supportBatch(Connection con) {
		try {
			// 得到数据库的元数据
			DatabaseMetaData md = con.getMetaData();
			return md.supportsBatchUpdates();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 执行一批SQL语句
	 * @param con	数据库的连接
	 * @param sqls	待执行的SQL数组
	 * @return
	 */
	public static int[] goBatch(Connection con, String[] sqls) {
		if (sqls == null) {
			return null;
		}
		Statement sm = null;
		try {
			sm = con.createStatement();
			// 将所有的SQL语句添加到Statement中
			for (int i = 0; i < sqls.length; i++) {
				sm.addBatch(sqls[i]);
			}
			// 一次执行多条SQL语句
			return sm.executeBatch();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(sm!=null){
				try {
					sm.clearBatch();
					sm.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
}