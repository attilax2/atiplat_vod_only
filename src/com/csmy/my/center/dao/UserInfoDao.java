package com.csmy.my.center.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.module.UserExtendInfo;
import com.csmy.my.center.module.UserInfo;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.UniqueID;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.exception.G4Exception;

@SuppressWarnings("unchecked")
@Repository
public class UserInfoDao extends DaoImpl {

	private static final long serialVersionUID = -7977535368743775681L;
	private Logger log = Logger.getLogger(UserInfoDao.class);

	/**
	 * 验证用户登录信息
	 * 
	 * @param pDto
	 * @return
	 */
	public UserInfo checkLogin(Dto pDto) throws G4Exception {
		//com.opensymphony.xwork2.ognl.OgnlValueStackFactory
		// 获取用户信息
		String user_id = null;// 用户编号
		UserExtendInfo extendInfo = null;
		UserInfo userInfo = (UserInfo) queryForObject("admin.getUserByAcount",pDto);
		if (userInfo != null) {
			user_id = userInfo.getUser_id();
			if (CTUtils.isNotEmpty(user_id)) {
				log.debug("开始封装用户基本属性..........");
				pDto.put("user_id", user_id);
				
				/***************** 获取用户对应的角色信息 **********************/
				// 加载用户扩展信息
				log.debug("获取用户扩展信息..........");
				extendInfo = (UserExtendInfo) queryForObject("admin.getUserExtendInfoById", pDto);
				if (CTUtils.isNotEmpty(extendInfo)) {
					userInfo.setUserExtendInfo(extendInfo);
				}
			}
		}

		return userInfo;
	}
	
	/**
	 * 验证用户登录信息
	 * 
	 * @param pDto
	 * @return
	 */
	public MemeberInfo checkMLogin(Dto pDto) throws G4Exception {
		return (MemeberInfo)queryForObject("admin.getMemeberByAcount",pDto);
	}
	
	/**
	 * 会员注册
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean userRegister(Dto pDto) throws G4Exception {
		pDto.put("memeber_id", UniqueID.getUniqueID(8, 0));
		insert("system.addMemInfo", pDto);//增加用户信息
		return true;
	}


	/**
	 * 操作用户数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operUserInfo(Dto pDto) {

		boolean temp = false;
		try {
			String optype = pDto.getAsString("optype");
			// 处理业务逻辑
			if (CTUtils.isNotEmpty(optype)) {
				if ("add".equals(optype)) {
					// 增加用户信息
					String user_id = UniqueID.getUniqueID(8, 0);
					pDto.put("password", CTUtils.encryptBasedDes(pDto.getAsString("password")));
					pDto.put("user_id", user_id);
					pDto.put("login_num","0");
					pDto.put("login_time",CTUtils.getCurrentTime());
					if("0".equals(pDto.getAsString("sex"))){
						pDto.put("user_pic","/images/upic/boy1.jpg");//默认男士头像
					}else{
						pDto.put("user_pic","/images/upic/girl1.jpg");//默认女士头像
					}
					
					insert("system.addUserInfo", pDto);//增加用户信息
					insert("system.addUserExtendInfo", pDto);//增加用户扩展信息
					
//					//处理用户角色
//					String role_id = pDto.getAsString("role_id");
//					if(CTUtils.isNotEmpty(role_id)){
//						Dto perDto = null;
//						String rids[] = role_id.split(",");
//						for (String rid : rids) {
//							perDto = new BaseDto();
//							perDto.put("role_id", rid);
//							perDto.put("user_id", user_id);
//							insert("addUserRole", perDto);
//						}
//					}

					temp = true;
				}

				if ("edit".equals(optype)) {
					// 修改用户信息
					pDto.put("password", CTUtils.encryptBasedDes(pDto.getAsString("password").trim()));
					update("system.editUserInfo", pDto);
					update("system.updateUserExtendInfo", pDto);//用户扩展信息
					temp = true;
				}
				
				if ("edit_mem".equals(optype)) {
					// 修改用户信息
					update("system.updateMemInfo", pDto);

					temp = true;
				}
				
				if ("reset_pwd".equals(optype)) {
					// 重置密码信息
					pDto.put("password",CTUtils.encryptBasedDes(StringUtil.getAttrFromPro("ct_user_password", null)));
					update("system.updateMemInfo", pDto);

					temp = true;
				}
				
				if ("set_recom".equals(optype)) {
					// 设置开启邀请
					pDto.put("use_recom","1");
					update("system.updateMemInfo", pDto);

					temp = true;
				}

				if ("delete_mem".equals(optype)) {
					// 删除会员信息
					delete("system.deleteMemInfo", pDto);
					temp = true;
				}
				
				if ("delete".equals(optype)) {
					// 删除用户信息
					delete("system.deleteUserInfo", pDto);
					delete("system.deleteUserExtendInfo", pDto);
					delete("system.deleteUserRoleInfo", pDto);
					temp = true;
				}

				// 修改密码
				if ("change".equals(optype)) {
					String pwd  = pDto.getAsString("new_psw");
					if(CTUtils.isNotEmpty(pwd)){
						pDto.put("new_psw", CTUtils.encryptBasedDes(pwd));
					}
					update("admin.updateAdminPsw", pDto);
					temp = true;
				}
				
				if ("lock_user".equals(optype)) {
					update("system.lockMember", pDto);
					temp = true;
				}
				
				if ("add_cust".equals(optype)) {
					pDto.put("customer_id", UniqueID.getUniqueID(8, 0));
					if(CTUtils.isEmpty(pDto.getAsString("level"))){
					   pDto.put("level", "0");
					}
					String login_pwd = pDto.getAsString("login_pwd");
					if(CTUtils.isEmpty(login_pwd)){
						login_pwd = StringUtil.getAttrFromPro("ct_user_password", null);
					}
					pDto.put("login_pwd", CTUtils.encryptBasedDes(login_pwd));
					pDto.put("createtime", CTUtils.getCurrentTime());
					insert("resource.addCustomer", pDto);
					temp = true;
				}
				
				if ("edit_cust".equals(optype)) {
					String login_pwd = pDto.getAsString("login_pwd");
					if(CTUtils.isNotEmpty(login_pwd)){
					   pDto.put("login_pwd", CTUtils.encryptBasedDes(login_pwd.trim()));
					}
					update("resource.editCustomer", pDto);
					temp = true;
				}
				
				if ("delete_cust".equals(optype)) {
					//删除商户
					delete("resource.deleteCustomer", pDto);
					//清除商品和商户的关系
					update("good.clearCustID", pDto);
					temp = true;
				}
				
			}
		} catch (Exception e) {
			temp = false;
			e.printStackTrace();
		}
		return temp;
	}
	
	/**
	 * 操作角色数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operRoleInfo(Dto pDto) {

		boolean temp = false;
		try {
			String optype = pDto.getAsString("optype");
			// 处理业务逻辑
			if (CTUtils.isNotEmpty(optype)) {
				if ("add".equals(optype)) {
					pDto.put("role_id", UniqueID.getUniqueID(8, 0));
					insert("role.addRoleInfo", pDto);
					temp = true;
				}

				if ("edit".equals(optype)) {
					update("role.editRoleInfo", pDto);
					temp = true;
				}
				
				if ("delete".equals(optype)) {
					update("role.deleteUserRole", pDto);
					update("role.deleteRolePermit", pDto);
					update("role.deleteRoleInfo", pDto);
					temp = true;
				}
				
				if ("setPermit".equals(optype)) {
					//设置权限数据
					String opermit_code = "";
					String permit_code = pDto.getAsString("permit_code");
					List<Dto> permitList = queryForList("role.getRolePermits", pDto);
					if(CTUtils.isNotEmpty(permitList)){
						opermit_code = StringUtil.getStrFromList(permitList, "module_code", ",");
					}
					
					//如果已经存在授权则要判断
					if(CTUtils.isNotEmpty(opermit_code)){
						String[] new_arr = permit_code.split(",");
						String[] old_arr = opermit_code.split(",");
						
						Dto backDto = StringUtil.getArrStr(new_arr, old_arr);
						String add_ids = backDto.getAsString("add_id");
						String del_ids = backDto.getAsString("del_id");
						
						//如果存在删除
						if(CTUtils.isNotEmpty(del_ids)){
							del_ids = del_ids.replaceAll(",", "','");
							Dto inDto = new BaseDto();
							inDto.put("module_id", del_ids);
							inDto.put("role_id", pDto.getAsString("role_id"));
							delete("role.deleteRolePermits", inDto);
						}
						
						//如果存在增加
						if(CTUtils.isNotEmpty(add_ids)){
							String [] aids = add_ids.split(",");
							for (String aid : aids) {
								Dto inDto = new BaseDto();
								inDto.put("module_id", aid);
								inDto.put("role_id", pDto.getAsString("role_id"));
								insert("role.addRolePermit", inDto);
							}
						}
					}else{//直接新增
						String[] new_arr = permit_code.split(",");
						for (String aid : new_arr) {
							Dto inDto = new BaseDto();
							inDto.put("module_id", aid);
							inDto.put("role_id", pDto.getAsString("role_id"));
							insert("role.addRolePermit", inDto);
						}
					}
					
					temp = true;
				}
				
			}
		} catch (Exception e) {
			temp = false;
			e.printStackTrace();
		}
		return temp;
	}
}