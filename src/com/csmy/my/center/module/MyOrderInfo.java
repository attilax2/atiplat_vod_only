package com.csmy.my.center.module;

import java.util.ArrayList;
import java.util.List;


public class MyOrderInfo implements java.io.Serializable {

	private static final long serialVersionUID = 388755927948190934L;
	
	private String zds;
	private String zje;
	private String zyf;
	private List<OrderInfo> order = new ArrayList<OrderInfo>();

	public String getZds() {
		return zds;
	}

	public void setZds(String zds) {
		this.zds = zds;
	}

	public String getZje() {
		return zje;
	}

	public void setZje(String zje) {
		this.zje = zje;
	}

	public String getZyf() {
		return zyf;
	}

	public void setZyf(String zyf) {
		this.zyf = zyf;
	}

	public List<OrderInfo> getOrder() {
		return order;
	}

	public void setOrder(List<OrderInfo> order) {
		this.order = order;
	}

	
}
