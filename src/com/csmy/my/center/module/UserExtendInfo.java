package com.csmy.my.center.module;

/**
 * 用户扩展信息实体类
 * 
 * @author wgp
 * 
 */
public class UserExtendInfo implements java.io.Serializable {

	private static final long serialVersionUID = 80336062968038706L;
	private String user_id;
	private String sex;
	private String vip_card;
	private String nick_name;
	private String machine_id;
	private String email;
	private String qq_num;
	private String telephone;
	private String user_pic;
	private String login_num;
	private String actived;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getVip_card() {
		return vip_card;
	}

	public void setVip_card(String vip_card) {
		this.vip_card = vip_card;
	}

	public String getNick_name() {
		return nick_name;
	}

	public void setNick_name(String nick_name) {
		this.nick_name = nick_name;
	}

	public String getMachine_id() {
		return machine_id;
	}

	public void setMachine_id(String machine_id) {
		this.machine_id = machine_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQq_num() {
		return qq_num;
	}

	public void setQq_num(String qq_num) {
		this.qq_num = qq_num;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getUser_pic() {
		return user_pic;
	}

	public void setUser_pic(String user_pic) {
		this.user_pic = user_pic;
	}

	public String getLogin_num() {
		return login_num;
	}

	public void setLogin_num(String login_num) {
		this.login_num = login_num;
	}

	public String getActived() {
		return actived;
	}

	public void setActived(String actived) {
		this.actived = actived;
	}

}
