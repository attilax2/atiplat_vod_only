package com.csmy.my.center.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csmy.my.center.dao.GoodInfoDao;
import com.csmy.my.center.util.dataconvert.Dto;

/**
 * 商品信息业务类
 * 
 * @author wgp
 * @since 2013-01-15
 * 
 */
@Controller
@Scope("prototype")
@SuppressWarnings("unchecked")
public class GoodInfoService {
	@Autowired
	private GoodInfoDao  goodInfoDao;
	private Logger logger = Logger.getLogger(GoodInfoService.class);
	
	/**
	 * 操作菜肴数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operGoodInfo(Dto pDto) {
		return goodInfoDao.operGoodInfo(pDto);
	}
}
