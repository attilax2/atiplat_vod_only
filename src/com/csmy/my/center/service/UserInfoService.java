package com.csmy.my.center.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csmy.my.center.dao.UserInfoDao;
import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.module.UserInfo;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.exception.G4Exception;

/**
 * 用户信息业务类
 * 
 * @author wgp
 * @since 2013-01-15
 * 
 */
@Controller
@Scope("prototype")
@SuppressWarnings("unchecked")
public class UserInfoService {
	@Autowired
	private UserInfoDao  userInfoDao;
	private Logger logger = Logger.getLogger(UserInfoService.class);
	
	/**
	 *  根据登录名和密码获取用户
	 * @param pDto
	 * @return
	 * @throws G4Exception
	 */
	public MemeberInfo checkMLogin(Dto pDto) throws G4Exception {
		return userInfoDao.checkMLogin(pDto);
	}
	
	/**
	 * 会员注册
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean userRegister(Dto pDto) throws G4Exception {
		return userInfoDao.userRegister(pDto);
	}
	/**
	 *  根据登录名和密码获取用户
	 * @param pDto
	 * @return
	 * @throws G4Exception
	 */
	public UserInfo checkLogin(Dto pDto) throws G4Exception {
		return userInfoDao.checkLogin(pDto);
	}
	
	/**
	 * 操作用户数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operUserInfo(Dto pDto) {
		return userInfoDao.operUserInfo(pDto);
	}

	/**
	 * 操作角色数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operRoleInfo(Dto pDto) {
		return userInfoDao.operRoleInfo(pDto);
	}
}
