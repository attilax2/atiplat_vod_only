package com.csmy.my.center.test;

import java.io.File;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import com.csmy.my.center.util.ExcelUtils;
import com.csmy.my.center.util.FileUtils;

public class TestExcel {

	/**
	 * 测试excel读取
	 * @param args
	 */
	public static void main(String[] args) {
		ExcelUtils ex = new ExcelUtils();
		FileUtils.appendMethodB("E://幼儿食谱.sql",ex.readExcel2(new File("E://幼儿食谱.xls")));
	}
	
	
	public static void readExcel(){
	        try {
	            Workbook book = Workbook.getWorkbook(new File("D:/视频文字内容/日本料理.xls"));
	            Sheet [] sheets = book.getSheets();
	            System.out.println("sheets="+sheets.length);
	            int rows = 0;
	            int cols = 0;
	            Cell cell1 = null;
	            String result = null;
	            for (Sheet sheet : sheets) {
	            	 rows = sheet.getRows();
	            	 cols = sheet.getColumns();
	            	 System.out.println("rows="+rows);
	            	 System.out.println("cols="+cols);
	            	 for (int i = 0; i < rows; i++) {
	            		 for (int j = 0; j < cols; j++) {
	            			 cell1 = sheet.getCell(i, j);
	            			 if(cell1!=null){
	            			    result = cell1.getContents();
	            			    System.out.println("result="+result);
	            			 }
	            		 }
					}
	            	 book.close();
				}
	        } catch (Exception e) {
	            System.out.println(e);
	        }
	}


}
