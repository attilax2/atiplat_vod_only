package com.csmy.my.center.util;

/**
 * 常量表
 * 
 * @author wgp
 * @since 2013-01-15
 */
public interface CTConstants {
	
	/**
	 * 系统默认编码格式
	 */
	public static String SYS_PAGE_ENCODE="UTF-8";
	/**
	 * XML文档风格<br>
	 * 0:节点属性值方式
	 */
	public static final String XML_Attribute = "0";

	/**
	 * XML文档风格<br>
	 * 1:节点元素值方式
	 */
	public static final String XML_Node = "1";

	/**
	 * 字符串组成类型<br>
	 * number:数字字符串
	 */
	public static final String S_STYLE_N = "number";

	/**
	 * 字符串组成类型<br>
	 * letter:字母字符串
	 */
	public static final String S_STYLE_L = "letter";

	/**
	 * 字符串组成类型<br>
	 * numberletter:数字字母混合字符串
	 */
	public static final String S_STYLE_NL = "numberletter";

	/**
	 * 格式化(24小时制)<br>
	 * FORMAT_DateTime: 日期时间
	 */
	public static final String FORMAT_DateTime = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * 格式化(12小时制)<br>
	 * FORMAT_DateTime: 日期时间
	 */
	public static final String FORMAT_DateTime_12 = "yyyy-MM-dd hh:mm:ss";

	/**
	 * 格式化<br>
	 * FORMAT_DateTime: 日期
	 */
	public static final String FORMAT_Date = "yyyy-MM-dd";

	/**
	 * 格式化(24小时制)<br>
	 * FORMAT_DateTime: 时间
	 */
	public static final String FORMAT_Time = "HH:mm:ss";
	
	/**
	 * 格式化(12小时制)<br>
	 * FORMAT_DateTime: 时间
	 */
	public static final String FORMAT_Time_12 = "hh:mm:ss";

	/**
	 * 换行符<br>
	 * \n:换行
	 */
	public static final String ENTER = "\n";

	/**
	 * 异常信息统一头信息<br>
	 * 非常遗憾的通知您,程序发生了异常
	 */
	public static final String Exception_Head = "\n非常遗憾的通知您,程序发生了异常.\n" + "异常信息如下:\n";

	/**
	 * Ext表格加载模式<br>
	 * \n:非翻页排序加载模式
	 */
	public static final String EXT_GRID_FIRSTLOAD = "first";

	/**
	 * Excel模板数据类型<br>
	 * number:数字类型
	 */
	public static final String ExcelTPL_DataType_Number = "number";

	/**
	 * Excel模板数据类型<br>
	 * number:文本类型
	 */
	public static final String ExcelTPL_DataType_Label = "label";

	/**
	 * HTTP请求类型<br>
	 * 1:裸请求
	 */
	public static final String PostType_Nude = "1";

	/**
	 * HTTP请求类型<br>
	 * 0:常规请求
	 */
	public static final String PostType_Normal = "0";

	/**
	 * Ajax请求超时错误码<br>
	 * 999:Ajax请求超时错误码
	 */
	public static final int Ajax_Timeout = 999;
	
	/**
	 * Ajax请求非法错误码<br>
	 * 998:当前会话userid和登录时候的userid不一致(会话被覆盖)
	 */
	public static final int Ajax_Session_Unavaliable = 998;
	
	/**
	 * 交易状态:成功
	 */
	public static final Boolean TRUE = new Boolean(true);
	
	/**
	 * 交易状态:失败
	 */
	public static final Boolean FALSE = new Boolean(false);
	
	/**
	 * 交易状态:成功
	 */
	public static final String SUCCESS = "1";
	
	/**
	 * 交易状态:失败
	 */
	public static final String FAILURE = "0";

	/**
	 * 分页查询分页参数缺失错误信息
	 */
	public static final String ERR_MSG_QUERYFORPAGE_STRING = "您正在使用分页查询,但是你传递的分页参数缺失!如果不需要分页操作,您可以尝试使用普通查询:queryForList()方法";
	
	/**
	 * Flash图标色彩数组
	 */
	public static String[] CHART_COLORS = {"AFD8F8","F6BD0F","8BBA00","008E8E","D64646","8E468E","588526","B3AA00","008ED6","9D080D","A186BE","1EBE38"};
	
	
	/**
	 * 后台登录用户session标识
	 */
	public static String SESSION_USER = "SESSION_USER";
	/**
	 * 会员登录用户session标识
	 */
	public static String SESSION_MUSER = "SESSION_MUSER";
	/**
	 * 商户登录用户session标识
	 */
	public static String SESSION_CUSER = "SESSION_CUSER";
	/**
	 * 容器路劲
	 */
	public static String SESSION_PATH = "SESSION_PATH";
	
	/**
	 * 登录用户验证码标识
	 */
	public static String SESSION_CODE = "RANDOMCODEKEY";
	
	/**
	 * 手机验证码
	 */
	public static String MOBILE_CHECK_CODE = "MOBILE_CHECK_CODE";
	
	/**
	 * 当前选择的菜单
	 */
	public static String MENU_SELECTED_INDEX = "MENU_INDEX";
	
	/**
	 * 当前选择的菜单
	 */
	public static String CMENU_SELECTED_INDEX = "CMENU_INDEX";
	
	/**
	 * 所有子系统标识
	 */
	public static String SESSION_SYSTEM = "systems";
	
	/**
	 * 当前登录系统标识
	 */
	public static String SYSTEM_CODE = "systemCode";
	
	
	
	/**
	 * 用户角色标识
	 */
	public static final String STAFF_ROLE = "staff_r";
	
	/**
	 * 用户职位标识
	 */
	public static final String STAFF_POSITION = "staff_p";
	
	/**
	 * 用户部门标识
	 */
	public static final String STAFF_DEPT = "staff_d";
	
	/**
	 * 系统组织机构标识
	 */
	public static final String CT_ORGA_LIST = "ct_orga_list";
	
	/**
	 * 系统组织机构标识
	 */
	public static final String CT_ORGA = "ct_orga";
	
	/**
	 * 用户特权标识
	 */
	public static final String STAFF_PERMIT = "staff_pt";
	
	
	/**
	 * 角色权限标识
	 */
	public static final String ROLE_PERMIT = "r_permit";

	/**
	 * 职位权限标识
	 */
	public static final String POSITION_PERMIT = "p_permit";
	
	/**
	 * 角色数据权限标识
	 */
	public static final String ROLE_DATA= "r_data";

	/**
	 * 职位数据权限标识
	 */
	public static final String POSITION_DATA = "p_data";
	
	/**
	 * 数据权限分存字段
	 */
	public static final String DATA_SAFE_CODE = "permit_code";
	
	/**
	 * 增加按钮权限标识值
	 */
	public static final String ACTION_ADD = "ADD";
	
	/**
	 * 删除按钮权限标识值
	 */
	public static final String DELETE_ADD = "DEL";
	
	/**
	 * 修改按钮权限标识值
	 */
	public static final String UPDATE_ADD = "EDT";
	
	/**
	 * 查询按钮权限标识值
	 */
	public static final String QUERY_ADD = "QUE";
	
	/**
	 * 全局内存用户对应角色标识值
	 */
	public static final String CT_STAFF_ROLE_LIST = "CT_STAFF_ROLE_LIST";
	/**
	 * 全局内存用户对应部门标识值
	 */
	public static final String CT_STAFF_DEPARTMENT_LIST = "CT_STAFF_DEPARTMENT_LIST";
	
	/**
	 * 全局内存角色验证权限标识值
	 */
	public static final String CT_ROLE_PERMIT_LIST = "CT_ROLE_PERMIT_LIST";
	/**
	 * 全局内存验证权限标识值
	 */
	public static final String CT_ALL_PERMIT_SET = "CT_ALL_PERMIT_SET";
	
	/**
	 * 全局内存职位验证权限标识值
	 */
	public static final String CT_POSITION_PERMIT_LIST = "CT_POSITION_PERMIT_LIST";
	
	/**
	 * 全局内存角色数据权限标识值
	 */
	public static final String CT_ROLE_DATA_LIST = "CT_ROLE_DATA_LIST";
	
	/**
	 * 全局内存职位数据权限标识值
	 */
	public static final String CT_POSITION_DATA_LIST = "CT_POSITION_DATA_LIST";
	
	/**
	 * 全局内存角色菜单集合标识值
	 */
	public static final String CT_ROLE_MENU_LIST = "CT_ROLE_MENU_LIST";
	
	/**
	 * 全局内存职位菜单集合标识值
	 */
	public static final String CT_POSITION_MENU_LIST = "CT_POSITION_MENU_LIST";
	
	/**
	 * 全局内存总数据对象标识值
	 */
	public static final String USER_PERMIT_LIST = "USER_PERMIT_LIST";
	
	/**
	 * 全局内存字典表标识值
	 */
	public static final String CT_CODE_LIST = "CT_CODE_LIST";
	/**
	 * 全局内存全局参数表标识值
	 */
	public static final String CT_PARAM_LIST = "CT_PARAM_LIST";
	/**
	 * 全局内存角色表标识值
	 */
	public static final String CT_ROLE_LIST = "CT_ROLE_LIST";
	/**
	 * 全局内存字典表标识值
	 */
	public static final String CT_DEPART_LIST = "CT_DEPART_LIST";
	
	/**
	 * 全局内存子系统标识值
	 */
	public static final String CT_SYSTEM_LIST = "CT_SYSTEM_LIST";
	
	/**
	 * 全局内存子系统根节点标识值
	 */
	public static final String CT_SYSTEM_ROOT_MENU_LIST = "CT_SYSTEM_ROOT_MENU_LIST";
	
	/**
	 * 全局内存菜单标识值（按照Code分存）
	 */
	public static final String CT_MENU_LIST = "CT_MENU_LIST";
	/**
	 * 全局内存按钮标识值（按照Code分存）
	 */
	public static final String CT_ACTION_LIST = "CT_ACTION_LIST";
	
	/**
	 * 全局内存菜单表标识值(system_code分存)
	 */
	public static final String CT_ALL_MENU_LIST = "CT_ALL_MENU_LIST";
	
	/**
	 * 全局内存菜单表标识值
	 */
	public static final String CT_ALL_MENU = "CT_ALL_MENU";
	
	/**
	 * 全局SESSION标识值
	 */

	public static final String CT_USER_SESSION = "CT_USER_SESSION";
	
	/**
	 * 系统默认密码
	 */

	public static final String CT_USER_PASSWORD = "ct_user_password";
	
	/**
	 * 系统分页条数
	 */

	public static final int PAGER_SIZE = 20;
	
	/**
	 * 系统前端分页条数
	 */

	public static final int PAGER_MSIZE = 20;

}