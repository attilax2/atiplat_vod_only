package com.csmy.my.center.util;

public class IndexCache {
	public static String FOOD_ID = "food_id";// 菜肴编号;
	public static String FOOD_NAME = "food_name";// 菜肴名称;
	public static String FOOD_TITLE = "food_title";// 食物标题;
	public static String FOOD_CODE = "food_code";// 菜名简码（与视频简码对应）;
	public static String FOOD_PIC_SM = "food_pic_sm";// 菜肴小预览图;
	public static String FOOD_PIC_BG = "food_pic_bg";// 菜肴大预览图;
	public static String FOOD_TAG = "food_tag";// 菜肴标签;
	public static String TYPE_ID = "type_id";// 类型编号;
	public static String TYPE_NAME = "type_name";// 类型名称;
	public static String DEPART_ID = "depart_id";// 菜系编号;
	public static String DEPART_NAME = "depart_name";//菜系名称
	public static String MATERIAL = "material";// 采用的食材;
	public static String SEASONING = "seasoning";// 采用调料;
	public static String FOOD_TASTE = "food_taste";// 口味;
	public static String COOK_DISC = "cook_disc";// 具体做法;
	public static String COOK_METHOD = "cook_method";// 制作工艺;
	public static String COOK_TIME = "cook_time";// 制作需要的时间 默认 分钟为单位;
	public static String FOOD_FUNC = "food_func";// 拥有功效;
	public static String FOOD_TABOO = "food_taboo";// 搭配禁忌;
	public static String APPLICABLE = "applicable";// 适用人群;
	public static String DIFF_LEVEL = "diff_level";// 难度级别;
	public static String FOOD_TIPS = "food_tips";// 制作温馨提示;
	public static String EAT_TYPE = "eat_type";// 食用时间 早餐 中餐 晚餐;
	public static String ORDER_NO = "order_no";// 排序编号;
	public static String VIDEO_ID = "video_id";// 视频编号;
	public static String VIDEO_PATH = "video_path";//视频地址
	public static String VAGUE_SEARCH = "vague_search";//模糊查询
}
