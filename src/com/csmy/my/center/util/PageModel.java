package com.csmy.my.center.util;

import java.util.List;

public class PageModel {
	private int offset = 0;
	// 每页记录数
	private int pageSize = 20;
	// 当前页号,从1开始
	private int pageNo;
	// 记录条数
	private int count;
	// 集合对象
	private List datas;
	// 总页数
	private int pages;
	// 总的统计数
	private int totalCount;
	// 统计图片的路径
	private String chartImg;
	// 统计图片的路径
	private String chartMap;

	// 显示尾页最大页数(即超过多少页后，禁用分页栏的尾页标签)
	private int maxPages = 1000;

	// 统计语句
	private String countSql;

	public PageModel() {

	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List getDatas() {
		return datas;
	}

	public void setDatas(List datas) {
		this.datas = datas;
	}

	public int getPages() {
		if (count == 0) {
			return 0;
		} else {
			pages = (count / ((float) pageSize) > count / pageSize ? count
					/ pageSize + 1 : count / pageSize);
			return pages;
		}

	}

	public void setPages() {
		this.pages = getPages();
	}

	public String getChartImg() {
		return chartImg;
	}

	public void setChartImg(String chartImg) {
		this.chartImg = chartImg;
	}

	public int getMaxPages() {
		return maxPages;
	}

	public void setMaxPages(int maxPages) {
		this.maxPages = maxPages;
	}

	public String getCountSql() {
		return countSql;
	}

	public void setCountSql(String countSql) {
		this.countSql = countSql;
	}

	public String getChartMap() {
		return chartMap;
	}

	public void setChartMap(String chartMap) {
		this.chartMap = chartMap;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

}