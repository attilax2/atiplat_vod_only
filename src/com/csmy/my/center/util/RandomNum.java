package com.csmy.my.center.util;

import java.util.Random;

public class RandomNum {

	/** 
	 * 方法说明： 
	 * 该函数获得随机数字符串 
	 * @param iLen  int:需要获得随机数的长度 
	 * @param iType int:随机数的类型： 
	 * '0 ':表示仅获得数字随机数； 
	 * '1 '：表示仅获得字符随机数； 
	 * '2 '：表示获得数字字符混合随机数 
	 * @since   1.0.0 
	 */
	public static final String CreateRadom(int iLen, int iType) {
		String strRandom = "";//随机字符串 
		Random rnd = new Random();
		if (iLen < 0) {
			iLen = 5;
		}
		if ((iType > 2) || (iType < 0)) {
			iType = 2;
		}
		switch (iType) {
		case 0:
			for (int iLoop = 0; iLoop < iLen; iLoop++) {
				strRandom += Integer.toString(rnd.nextInt(10));
			}
			break;
		case 1:
			for (int iLoop = 0; iLoop < iLen; iLoop++) {
				strRandom += Integer.toString((35 - rnd.nextInt(10)), 36);
			}
			break;
		case 2:
			for (int iLoop = 0; iLoop < iLen; iLoop++) {
				strRandom += Integer.toString(rnd.nextInt(36), 36);
			}
			break;
		}
		//System.out.println(strRandom);
		return strRandom.trim();
	}
	
	public static final String NewCreateRadom(int iLen, int iType) {
		String returNum = null;
		while(true){
			returNum = CreateRadom(iLen,iType);
			if(iType==0 && returNum!=null && !"".equals(returNum)){
				if(returNum.charAt(0)!='0'){
					break;
				}
			}
		}
		System.out.println("最后返回随机"+iLen+"位数："+returNum);
		return returNum;
	}
	public static void main(String []args){
		for(int i=0;i<=100;i++){
			CreateRadom(8,2);
		}
		//CreateRadom(8,0);
		//CreateRadom(8,1);
		//CreateRadom(8,2);
	}
}
