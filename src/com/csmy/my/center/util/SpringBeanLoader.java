package com.csmy.my.center.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.attilax.io.filex;
import com.csmy.my.center.util.properties.PropertiesFactory;
import com.csmy.my.center.util.properties.PropertiesFile;
import com.csmy.my.center.util.properties.PropertiesHelper;

/**
 * SpringBwan加载器<br>
 * (1)、使用此加载器可以获得一个Spring容器的ApplicationContext实例,通过此实例你就可以方便的使用getBean()
 * 方法获取SpringBean.<br>
 * (2)、您也可以直接通过我们提供的getSpringBean()方法获得SpringBean。
 * 
 * @author wgp
 * @since 2012-09-06
 */
public class SpringBeanLoader{
	private static Log log = LogFactory.getLog(SpringBeanLoader.class);
	private static ApplicationContext applicationContext;

	static {
		try {
			initApplicationContext();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 初始化ApplicationContext对象
	 * @throws Exception 
	 */
	private static void initApplicationContext() throws Exception {
	    PropertiesHelper pHelper = PropertiesFactory.getPropertiesHelper(PropertiesFile.APP);
	    String forceLoad = pHelper.getValue("forceLoad", ArmConstants.FORCELOAD_N);
		try {
			if (forceLoad.equalsIgnoreCase(ArmConstants.FORCELOAD_N)) {
				log.info("系统正在初始化服务容器...");
			}
			applicationContext = new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml"});
			if (forceLoad.equalsIgnoreCase(ArmConstants.FORCELOAD_N)) {
				log.info("容器初始化成功啦，您的托管Bean已经被实例化。");
			}
		} catch (Exception e) {
			log.error("服务容器初始化失败.");
			log.error(CTConstants.Exception_Head + "初始化服务容器发生错误,请仔细检查您的配置文件喔!\n" + e.getMessage());
			e.printStackTrace();
			System.exit(0);
			throw e;
		}
	}

	/**
	 * 返回ApplicationContext对象
	 * 
	 * @return ApplicationContext 返回的ApplicationContext实例
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * 获取一个SpringBean服务
	 * 
	 * @param pBeanId
	 *            Spring配置文件名中配置的SpringID号
	 * @return Object 返回的SpringBean实例
	 */
	public static Object getSpringBean(String pBeanId) {
		Object springBean = null;
		try {
			springBean = applicationContext.getBean(pBeanId);
		} catch (NoSuchBeanDefinitionException e) {
			log.error(CTConstants.Exception_Head + "Spring配置文件中没有匹配到ID号为:[" + pBeanId + "]的SpringBean组件,请检查!");
			log.error(e.getMessage());
		}catch(Exception e)
		{filex.saveLog("bean id:"+pBeanId,  "c:\\0e");
			filex.saveLog(e, "c:\\0e");
		}
		return springBean;
	}
}
