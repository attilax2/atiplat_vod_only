package com.csmy.my.center.util;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.properties.PropertiesFactory;
import com.csmy.my.center.util.properties.PropertiesFile;
import com.csmy.my.center.util.properties.PropertiesHelper;


/**
 * 
 * <p>
 * 版权所有（c）2009-2012 湖南美音网络技术有限公司 * 保留所有权利。<br>
 * </p>
 * 
 * @description 详细描述
 * 
 * <pre>
 *  
 *  类功能描述                    创建人                     创建时间
 * ---------------------------------------------------------------------------
 * 字符串操作类                    Jackie	                   2009.12.8
 * ---------------------------------------------------------------------------
 * </pre>
 * 
 * @history 修改历史
 * 
 * <pre>
 * 修改内容描述                   修改人                     修改时间
 * ---------------------------------------------------------------------------
 * 
 * 
 * ---------------------------------------------------------------------------  * 
 * </pre>
 * 
 * @author <a href="mailto:inter520008@163.com">wangguiping<王桂平></a>
 * @project 工程名称： TestJ2EE
 * @date java类创建时间：2009-12-8 下午01:44:11
 * @version 1.0
 */
@SuppressWarnings("unchecked")
public class StringUtil {

	/**
	 * 将传入的的html格式转换成txt文本格式； 换行\r\n没有进行特殊处理
	 * 
	 * @param inputString
	 *            html字符串
	 * @return txt文本字符串
	 */
	public static String Html2Text(String inputString) {
		String htmlStr = inputString; // 含html标签的字符串
		String textStr = "";
		java.util.regex.Pattern p_script;
		java.util.regex.Matcher m_script;
		java.util.regex.Pattern p_style;
		java.util.regex.Matcher m_style;
		java.util.regex.Pattern p_html;
		java.util.regex.Matcher m_html;

		try {
			String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>"; // 定义script的正则表达式{或<script[^>]*?>[\\s\\S]*?<\\/script>
			// }
			String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>"; // 定义style的正则表达式{或<style[^>]*?>[\\s\\S]*?<\\/style>
			// }
			String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式

			p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
			m_script = p_script.matcher(htmlStr);
			htmlStr = m_script.replaceAll(""); // 过滤script标签

			p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
			m_style = p_style.matcher(htmlStr);
			htmlStr = m_style.replaceAll(""); // 过滤style标签

			p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
			m_html = p_html.matcher(htmlStr);
			htmlStr = m_html.replaceAll(""); // 过滤html标签

			textStr = htmlStr;

		} catch (Exception e) {
			System.err.println("Html2Text: " + e.getMessage());
		}

		return textStr;// 返回文本字符串
	}

	/**
	 * 判断字符串src中是否包含flag特定字符串
	 * 
	 * @param src
	 *            源字符串，需要查询的字符串
	 * @param flag
	 *            标签值
	 * @return 布尔值，如果有则返回true，反之返回false
	 */
	public static boolean contains(String src, String flag) {
		if (src == null || flag == null)
			return false;
		return src.indexOf(flag) >= 0;
	}

	/**
	 * 判断字符串src中是否不包含flag特定字符串
	 * 
	 * @param src
	 *            源字符串，需要查询的字符串
	 * @param flag
	 *            标签值
	 * @return 布尔值，如果没有则返回true，反之返回false
	 */
	public static boolean notContains(String src, String flag) {
		if (src == null || flag == null)
			return false;
		return src.indexOf(flag) < 0;
	}

	/**
	 * 如果对象为空则，转换为目标字符串def，反之则将对象toString();
	 * 
	 * @param str
	 *            对象
	 * @param def
	 *            目标字符串def
	 * @return 转换后的字符串
	 */
	public static String nvl(Object str, String def) {
		return (str == null || "".equals(str.toString())) ? def : str
				.toString();
	}

	/**
	 * 将对象进行空值判断，如果对象为空，则处理后返回“”字符串对象，反之则返回对象toString()
	 * 
	 * @param str
	 *            需要处理的对象
	 * @return 转换后的字符串
	 */
	public static String nvl(Object str) {
		return nvl(str, "");
	}

	/**
	 * 判断字符串对象是否为空值，如果为空值则返回 <code>true</code>,反之则<code>false</code>。
	 * 
	 * @param str
	 *            字符串对象
	 * @return 布尔值
	 */
	public static boolean isEmpty(String str) {
		return str == null || "".equals(str);
	}

	/**
	 * @see #isEmpty(String)刚好与之相反
	 * @param str
	 *            字符串对象
	 * @return 布尔值
	 */
	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	/**
	 * 判断字符串对象是否为空值或者空字符串， 如果是则返回<code>true</code>,反之则<code>false</code>。
	 * 
	 * @param str
	 *            字符串对象
	 * @return 布尔值
	 */
	public static boolean isBlank(String str) {
		return str == null || "".equals(str.trim());
	}

	/**
	 * 判断字符串对象是否为空值或者空字符串， 如果是则返回<code>true</code>,反之则<code>false</code>。
	 * 
	 * @param str
	 *            字符串对象
	 * @return 布尔值
	 */
	public static boolean isNotBlank(String str) {
		return !isBlank(str);
	}

	/**
	 * 如果是null就返回零。反之就返回toString()方法。
	 * 
	 * @param obj
	 * @return
	 */
	public static String nvlzero(Object obj) {
		if (obj == null)
			return "0";
		else
			return obj.toString();
	}

	/**
	 * 将包含格式的字符串，用具体的参数字符串根据序号进行替换操作后返回；
	 * 
	 * <pre>
	 * String result = MessageFormat.format(
	 *  &quot;At {1,time} on {1,date}, there was {2} on planet {0,number,integer}.&quot;,
	 *  planet, new Date(), event);
	 *  输出为：
	 *   At 12:30 PM on Jul 3, 2053, there was a disturbance in the Force on planet 7.
	 * </pre>
	 * 
	 * @param formatStr
	 *            格式化的字符串
	 * @param args
	 *            参数集合
	 * @return 转换后的字符串
	 */
	public static String format(String formatStr, Object... args) {
		try {
			return MessageFormat.format(formatStr, args);
		} catch (Exception e) {
			return formatStr;
		}
	}

	/**
	 * 将字符串数组用分隔符号分割开来，并且拼成一个完整的字符串
	 * 
	 * @param target
	 *            字符串数组
	 * @param separator
	 *            分割符
	 * @return 处理后的字符串
	 */
	public static String join(String[] target, String separator) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < target.length; i++) {
			if (i != 0)
				sb.append(separator);
			sb.append(StringUtil.nvl(target[i], ""));
		}
		return sb.toString();
	}
	
	/**
	 * 将字符串数组用分隔符号分割开来，并且拼成一个完整的字符串
	 * 
	 * @param target
	 *            字符串数组
	 * @param separator
	 *            分割符
	 * @return 处理后的字符串
	 */
	public static String joinListStr(List<String> target, String separator) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < target.size(); i++) {
			if (i != 0)
				sb.append(separator);
			sb.append(StringUtil.nvl(target.get(i), ""));
		}
		return sb.toString();
	}

	/**
	 * 将带分隔符的字符串转换成数组
	 * 
	 * @param src
	 *            带分隔符的字符串
	 * @param delim
	 *            分隔符
	 * @return 字符串数组
	 */
	public static String[] toArray(String src, String delim) {
		if (StringUtil.isEmpty(src))
			return null;
		StringTokenizer st = new StringTokenizer(src, delim);
		String sts[] = new String[st.countTokens()];
		int j = 0;
		while (st.hasMoreElements()) {
			sts[j] = (String) st.nextElement();
			j++;
		}
		return sts;

	}

	public static String[] match(String str, String rgExp) {
		List<String> finds = new ArrayList<String>();
		try {
			Pattern regex = Pattern.compile(rgExp);
			Matcher regexMatcher = regex.matcher(str);
			while (regexMatcher.find()) {
				finds.add(regexMatcher.group());
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		String[] result = new String[finds.size()];
		return finds.toArray(result);
	}

	public static String parseSql(String sql, Map param) {
		Pattern p = Pattern.compile("#([^#]*)#|\\$([^$]*)\\$");
		Matcher m = p.matcher(sql);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			if (m.group(1) != null) {
				m.appendReplacement(sb, "'"
						+ StringUtil.nvl(param.get(m.group(1)), "") + "'");

			}
			if (m.group(2) != null) {
				m.appendReplacement(sb, StringUtil.nvl(param.get(m.group(2)),
						""));
			}
		}
		m.appendTail(sb);
		return sb.toString();
	}

	public static String joinString(String key, String value, String deli,
			String seperate) {
		return new StringBuffer(seperate).append(key).append(deli)
				.append(value).toString();
	}

	/**
	 * 两个Double数相减
	 * 
	 * @param v1
	 * @param v2
	 * @return Double
	 */
	public static Double sub(Double v1, Double v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.subtract(b2).doubleValue();
	}
	
	/**
	 * 将List转换为数组
	 * @param clList
	 * @return
	 */
	public static String[] ListToArr(List clList) {
		String [] arr = new String[clList.size()];
		for (int i = 0; i < clList.size(); i++) {
			arr[i] = clList.get(i).toString();
		}
		return arr;
	}

	/**
	 * 两个Double数相加
	 * 
	 * @param v1
	 * @param v2
	 * @return Double
	 */
	public static Double add(Double v1, Double v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		return b1.add(b2).doubleValue();
	}

	/**
	 * 通用打印方法 必须指定类型
	 */
	public static void printObj(String type, Object obj) {
		if (type.equals("String")) {
			System.out.println(obj.toString());
		}
		if (type.equals("List")) {
			List list = (ArrayList) obj;
			Iterator it = list.iterator();
			while (it.hasNext()) {
				System.out.println(it.next());
			}
		}
		if (type.equals("Set")) {
			Set kset = (HashSet) obj;
			Iterator it = kset.iterator();
			while (it.hasNext()) {
				System.out.println(it.next());
			}
		}
		if (type.equals("Array")) {
			String[] arr = (String[]) obj;
			for (int i = 0; i < arr.length; i++) {
				System.out.println(arr[i]);
			}
		}
		if (type.equals("Boolean")) {
			System.out.println(obj);
		}
		if (type.equals("Map")) {
			Map kmap = (HashMap) obj;
			Set key = kmap.keySet();
			Set entry = kmap.entrySet();

			/*
			 * Iterator itk = key.iterator(); while(itk.hasNext()){
			 * System.out.println("打印Map key："+itk.next()); }
			 */
			Iterator itv = entry.iterator();
			while (itv.hasNext()) {
				System.out.println(String.valueOf(itv.next()));
			}

		}
	}

	/**
	 * 打印共用方法
	 */
	public static void xprint(Object obj) {
		if (obj != null && obj instanceof String) {// 打印String
			printObj("String", obj);
		}
		if (obj != null && obj instanceof List) {// 打印List
			printObj("List", obj);
		}
		if (obj != null && obj instanceof Map) {// 打印Map
			printObj("Map", obj);
		}
		if (obj != null && obj instanceof Set) {// 打印Set
			printObj("Set", obj);
		}
		if (obj != null && obj instanceof Array) {// 打印数组
			printObj("Array", obj);
		}
		if (obj != null && obj instanceof Boolean) {// 打印数组
			printObj("Boolean", obj);
		}
	}

	/**
	 * 将GBK字符编码格式转换为UTF-8格式
	 * 
	 * @package com.csmy.util
	 * @class StringUtil.java
	 * @describe
	 * @param chenese
	 * @return
	 * @author weixiong
	 * @date May 25, 2011
	 */
	public static byte[] gbk2utf8(String chenese) {
		char c[] = chenese.toCharArray();
		//UTF-8字节码的长度
		int max=chenese.getBytes().length*2-chenese.length();
		byte[] fullByte = new byte[max];
		int count=0;
		for (int i = 0; i < c.length; i++) {
			int m = (int) c[i];
			String word = Integer.toBinaryString(m);
			//如果为字母数字下划线等，不进行转码操作
			if (m >= 0 && m <= 127) {
				fullByte[count] = Integer.valueOf(word, 2).byteValue();
				count++;
				continue;
			}

			StringBuffer sb = new StringBuffer();
			int len = 16 - word.length();
			// 补零
			for (int j = 0; j < len; j++) {
				sb.append("0");
			}
			sb.append(word);

			sb.insert(0, "1110");
			sb.insert(8, "10");
			sb.insert(16, "10");

			String s1 = sb.substring(0, 8);
			String s2 = sb.substring(8, 16);
			String s3 = sb.substring(16);

			byte b0 = Integer.valueOf(s1, 2).byteValue();
			byte b1 = Integer.valueOf(s2, 2).byteValue();
			byte b2 = Integer.valueOf(s3, 2).byteValue();
			byte[] bf = new byte[3];
			bf[0] = b0;
			fullByte[count] = bf[0];
			count++;
			bf[1] = b1;
			fullByte[count] = bf[1];
			count++;
			bf[2] = b2;
			fullByte[count] = bf[2];
			count++;

		}
		return fullByte;
	}
	
	public static void cont() {
		
		Dto cDto = null;
		String siteid="";//网站编号
		Dto rDto = new BaseDto();
		Dto wDto =null;
		List rList = null;
		List rList1 = null;
		
		List cList = new ArrayList();
		Dto bDto = new BaseDto();
		bDto.put("siteID", 1);
		cList.add(bDto);
		
		Dto bDto1 = new BaseDto();
		bDto1.put("siteID", 1);
		cList.add(bDto1);
		
		Dto bDto2 = new BaseDto();
		bDto2.put("siteID", 2);
		cList.add(bDto2);
		
		Dto bDto3 = new BaseDto();
		bDto3.put("siteID", 2);
		cList.add(bDto3);
		
		Dto bDto4 = new BaseDto();
		bDto4.put("siteID", 3);
		cList.add(bDto4);
		
		List reList = new ArrayList();
		if(cList!=null && cList.size()>0){
			for (int i = 0; i < cList.size(); i++) {
				cDto = (BaseDto)cList.get(i);
				siteid = cDto.getAsString("siteID");
				if(rDto.containsKey(siteid)){//已经存在
					rList = rDto.getAsList(siteid);
					rList.add(siteid);
					rDto.put(siteid, rList);
				}else{//不存在
				    rList1 = new ArrayList();
				    rList1.add(siteid);
				    rDto.put(siteid, rList1);
				}
			}
			    //循环遍历所有对象获取总数
			    List kList = null;
				java.util.Set key = rDto.keySet();
				java.util.Set val = rDto.entrySet();
				Iterator itv = key.iterator();
				while (itv.hasNext()) {
					wDto = new BaseDto();
					String itn = String.valueOf(itv.next());
					int num = Integer.parseInt(itn);
					kList = (List)rDto.get(itn);
					System.out.println("统计后的结果kList=="+kList);
					wDto.put("name", itn);
					wDto.put("value", kList.size());
					if(num%2==0){
						wDto.put("color", "B3AA00");
					}
					if(num%2==1){
						wDto.put("color", "AFD8F8");
					}
					System.out.println("统计后的结果=="+wDto);
					reList.add(wDto);
					System.out.println("统计后的结果2=="+reList);
				}
		 }
		
	}
	
	
	/**
	 * 检查list是否为空
	 * @param list
	 * @return
	 */
	public static boolean checkListBlank(List list) {
		if(list!=null && list.size()>0){
			return false;
		}else{
			return true;
		}
	}
	/**
	 * 合并集合中重复项
	 * @param cList
	 */
	public static List<Dto> cutListRepDto(List<Dto> cList,String scode) {
		
		StringUtil.xprint("去除重复前集合大小为："+cList.size());
		String code = "";
		Dto cDto = new BaseDto();
		List<Dto> rList = null;
		if(CTUtils.isNotEmpty(cList)){
			for (Dto dto : cList) {
				code = dto.getAsString(scode);
				if(!cDto.containsKey(code)){//去除重复项
					cDto.put(code, dto);
				}
			}
			//重新组合集合项目
			if(CTUtils.isNotEmpty(cDto)){
				String next_id = null;
				rList = new ArrayList<Dto>();
				Iterator iterator = cDto.keySet().iterator();
				while (iterator.hasNext()) {
					next_id = (String)iterator.next();
					rList.add((BaseDto)cDto.get(next_id));
				}
			}
			StringUtil.xprint("去除重复后集合大小为："+rList.size());
		}
		return rList;
	}
	

	/**
	 * 检查list是否存在某个元素
	 * 
	 * @param list
	 * @param obj
	 * @return
	 */
	public static boolean checkListExist(List<Dto> list, String target,String obj) {
		boolean temp = false;
		if (list != null && list.size() > 0) {
			String tg = "";
			for (Dto dto : list) {
				tg = dto.getAsString(target);
				if (tg.equals(obj)) {
					temp = true;
					break;
				}
			}
		}
		return temp;
	}
	
	
	/**
	 * 检查dto是否为空
	 * @param list
	 * @return
	 */
	public static boolean checkDtoBlank(Dto dto) {
		if(dto!=null && dto.size()>0){
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * 去掉指定字符串中重复的项
	 * @param str 字符串
	 * @param split 分隔符
	 * @return
	 */
	public static List cutRepItem(String str,String split) {
		String [] arr = null;
		List cList = new ArrayList();
		try {
			if(isNotBlank(str)){
				arr = str.split(split);//分隔字符
				for(int i = 0; i < arr.length; i++) {
					if(!cList.contains(arr[i])){
						cList.add(arr[i]);
					}
				}
			}
		} catch (Exception e) {
			xprint("对不起！去掉字符串重复项失败！");
			e.printStackTrace();
		}
		return cList;
	}
	
	/**
	 * 从list里面获取指定字段的值返回连接串
	 * @param list 结果集
	 * @param ch 获取字段
	 * @param link 连接符号
	 * @return
	 */
	public static String getStrFromList(List list,String ch,String link) {
		Dto pDto = null;
		int count = 0;
		StringBuffer sb = new StringBuffer("");
		if(!checkListBlank(list)){//不为空继续
			for (int i = 0; i < list.size(); i++) {
				pDto = (BaseDto)list.get(i);
				if(isNotBlank(pDto.getAsString(ch))){
					count++;
					if(count<list.size()){
					  sb.append(pDto.getAsString(ch)).append(link);
					}else{
					  sb.append(pDto.getAsString(ch));
					}
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * 打印bean对象
	 * @param model
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public static void printBean(Object model) throws IllegalArgumentException, SecurityException,
	IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		xprint("◆◆◆打印bean对象开始◆◆◆◆◆◆◆◆◆◆◆◆");
		 Field[] field = model.getClass().getDeclaredFields();   
		 for(int i=0;i<field.length;i++){
			 String nameStr =  field[i].toString();
			 nameStr = nameStr.substring(nameStr.lastIndexOf(".")+1, nameStr.length());
			 Object value = model.getClass().getMethod("get" + UpperCaseField(nameStr)).invoke(model);
			 if(value!=null){
		         xprint("◆◆◆"+nameStr+"==>"+value.toString());
			 }else{
				 xprint("◆◆◆"+nameStr+"==>null");
			 }
		 }
		 xprint("◆◆◆打印bean对象结束◆◆◆◆◆◆◆◆◆◆◆◆");
	}
	
	/**
	 * 打印对个bean对象
	 * @param modelList
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public static void printBeanList(List modelList) throws IllegalArgumentException, SecurityException, 
	IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		int i=0;
	   if(!checkListBlank(modelList)){
		   for (Object model : modelList) {
			   i++;
			   xprint("◇◇◇◇◇◇◇◇◇◇◇◇打印第["+i+"]个bean对象开始◇◇◇◇◇◇◇◇◇◇◇◇");
			   printBean(model);
			   xprint("◇◇◇◇◇◇◇◇◇◇◇◇打印第["+i+"]个bean对象结束◇◇◇◇◇◇◇◇◇◇◇◇");
			   System.out.println("");
		   }
	   }	
	}
	
	// 转化字段首字母为大写   
	public static String UpperCaseField(String fieldName) {   
        fieldName = fieldName.replaceFirst(fieldName.substring(0, 1), fieldName   
                .substring(0, 1).toUpperCase());   
        return fieldName;   
    }   

	
	public static String getContainterPath() {
		String TOMCAT_ROOT = "";
		String CONFIGPATH = StringUtil.class.getResource("/").getPath();
		//获取TOMCAT的根路径
		int index = CONFIGPATH.indexOf("/webapps");
		if (index > 0 && index < CONFIGPATH.length()) {
		    TOMCAT_ROOT = CONFIGPATH.substring(0, index);
		}
		return TOMCAT_ROOT;
	}
	
	public static String getContainPath() {
		String TOMCAT_ROOT = "";
		String CONFIGPATH = StringUtil.class.getResource("/").getPath();
		//获取TOMCAT的根路径
		int index = CONFIGPATH.indexOf("/webapps");
		if (index > 0 && index < CONFIGPATH.length()) {
		    TOMCAT_ROOT = CONFIGPATH.substring(0, index+8);
		}
		return TOMCAT_ROOT;
	}
	
	
	/**
	 * 获取图片路径
	 * @param request
	 * @param type 类型 1 ext目录 2 表示 image目录
	 * @return
	 */
	public static String getImagePath(HttpServletRequest request,String type){
		String rePath = "";
		String basePath = request.getContextPath()+"/";
		if(isNotBlank(type) && "1".equals(type)){
			rePath = basePath+"/resource/image/ext"+"/";
		}else{
			rePath = basePath+"/resource/image"+"/";
		}
		return rePath;
	}
	
	/**
	 * 将数据转换为code-list的信息返回Dto结构
	 * Dto<String ,List> 机构
	 * @param cList
	 * @param per_code
	 * @return
	 */
	public  static Dto convertListToDto(List cList,String per_code) {
		Dto rDto2 = new BaseDto();
		if(!checkListBlank(cList)){
			Dto rDto = null;
			String earch_code = null;
			for (int i = 0; i < cList.size(); i++) {
				rDto = (BaseDto)cList.get(i);
				if(!checkDtoBlank(rDto)){
					earch_code = rDto.getAsString(per_code);
					if(CTUtils.isNotEmpty(earch_code)){
						rDto2.put(earch_code, rDto);//重新组合数据
					}
				}
			}
		}
		return rDto2;
	}
	
	/**
	 * 对list集合进行冒泡排序
	 * @param dList 排序集合
	 * @param sort_code 排序字段
	 * @param sort_type asc or desc
	 * @return
	 */
	public static List<Dto> sortDtoList(List<Dto> dList,String sort_code,String sort_type){
		Dto temp = null;
		try {
            
			Dto mDto = null;
			Dto nDto = null;
			
			Integer sort_id1 = null;
			Integer sort_id2 = null;
			
			for(int i=0;i<dList.size()-1;i++) {
			  for(int j=1;j<dList.size()-i;j++) {	
				    mDto = dList.get(j-1);
				    nDto = dList.get(j);
				    
					sort_id1 = mDto.getAsInteger(sort_code);
					sort_id2 = nDto.getAsInteger(sort_code);
				    
					if("asc".equals(sort_type)){
						if(sort_id1.compareTo(sort_id2)>0){
							temp = dList.get(j-1);
							dList.set((j-1), dList.get(j)); 
							dList.set(j, temp);
						}
				  }else{
					    if(sort_id1.compareTo(sort_id2)<0){
						    temp = dList.get(j-1);
						    dList.set((j-1), dList.get(j)); 
						    dList.set(j, temp);
					    }
				  }
			   }
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dList;
		
	}
	
	
	
	
	
	public static void main(String[] args) {
		
		StringUtil.xprint("getContainterPath="+getContainterPath());
		
		Dto dlDto = new BaseDto();
		dlDto.put("acode", "1");
		dlDto.put("scode", "1");
		
		Dto dlDto1 = new BaseDto();
		dlDto1.put("acode", "2");
		dlDto1.put("scode", "2");
		
		
		Dto dlDto2 = new BaseDto();
		dlDto2.put("acode", "3");
		dlDto2.put("scode", "3");
		
		List<Dto> cList = new ArrayList<Dto>();
		cList.add(dlDto2);
		cList.add(dlDto);
		cList.add(dlDto1);
		
		List<Dto> clsList = sortDtoList(cList,"scode","desc");
		for (Dto dto : clsList) {
			StringUtil.xprint("dddd="+dto);
		}
		
		
		StringUtil.xprint("psd="+CTUtils.encryptBasedDes("888888"));
	}
	
	
	public static String getFilePath(String realPath,String index) {
		String file_path = "";
         if(CTUtils.isNotEmpty(realPath)){
        	 file_path = realPath.substring(realPath.indexOf(index),realPath.length());
         }
         StringUtil.xprint("file_path="+file_path);
         return file_path;
	}
	
	/**
	 * 获取属性对应的值
	 * @param key
	 * @param pFile
	 * @return
	 */
	public static String getAttrFromPro(String key,String pFile){
		if(isEmpty(pFile))pFile = PropertiesFile.APP;
		PropertiesHelper pHelper = PropertiesFactory.getPropertiesHelper(pFile);
	    return pHelper.getValue(key, ArmConstants.FORCELOAD_N);
	}
	
	
	//比较两个数字 判断是新增数据还是减少数据
	public static Dto getArrStr(String[] arr1, String[] arr2) {

		Dto backDto = new BaseDto();
		List<String> add_arr = new ArrayList<String>();
		List<String> mus_arr = new ArrayList<String>();
		if (arr1 != null && arr2 != null) {
			//先比较新的数据在老数据中是否存在    
			for (int i = 0; i < arr1.length; i++) {
				if (!checkItemExist(arr1[i], arr2)) {
					//判断新增加的数据加入数组
					add_arr.add(arr1[i]);
				}
			}

			//再比较老数据在新的数据中是否存在    
			for (int j = 0; j < arr2.length; j++) {
				if (!checkItemExist(arr2[j], arr1)) {
					//判断减少的数据加入数组
					mus_arr.add(arr2[j]);
				}
			}

			//转换参数
			String add_id = joinListStr( add_arr, ",");
			String del_id = joinListStr( mus_arr, ",");
			
			backDto.put("add_id", add_id);
			backDto.put("del_id", del_id);
			
			xprint("add_id="+add_id);
			xprint("del_id="+del_id);
		}
		return backDto;
	}

	//判断某个元素是否已经存在与数组
	public static boolean checkItemExist(String vl, String[] arr) {
		boolean temp = false;
		if (arr != null && arr.length != 0) {
			for (int i = 0; i < arr.length; i++) {
				if (arr[i].equals(vl)) {
					temp = true;
				}
			}
		}
		return temp;
	}

}
