package com.csmy.my.center.util.base;
/**
 * 渠道
 * @author Administrator
 *
 */
public class Channel {
	

	public String channel_id;//id
	public String channel_name;//渠道名称
	public String channel_uid;// 用户ID
	public String channel_txt;//渠道描述
	
	public String getChannel_id() {
		return channel_id;
	}
	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}
	public String getChannel_name() {
		return channel_name;
	}
	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}
	public String getChannel_uid() {
		return channel_uid;
	}
	public void setChannel_uid(String channel_uid) {
		this.channel_uid = channel_uid;
	}
	public String getChannel_txt() {
		return channel_txt;
	}
	public void setChannel_txt(String channel_txt) {
		this.channel_txt = channel_txt;
	}
	

	
}
