package com.csmy.my.center.util.cache;

import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.csmy.my.center.dao.IReader;
import com.csmy.my.center.filter.SystemInitListener;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.SpringBeanLoader;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;

/**
 * 系统缓存初始化
 * @author jackie
 *
 */
public class InitSystemCache {
	private static Log log = LogFactory.getLog(SystemInitListener.class);
	private static boolean success = true;
	private static IReader CTReader = null;
	//全局参数
	public static Dto reDto = null;
	static{
		CTReader = (IReader) SpringBeanLoader.getSpringBean("CTReader");
	}
	
	@SuppressWarnings("unchecked")
	public static Dto initCache() {
		
		try {
			reDto = new BaseDto();
			StringUtil.xprint("开始加载数据库中所有公用数据到缓存中...");
			//加载系统字典信息
			if(success){
				//获取字典参数名称集合
				Dto intDto = null;
				Dto codeDto = null;
				List<Dto> valueList = null;
				List<Dto> codeList = CTReader.queryForList("admin.getCodeNameList");
				//加入全局缓存
				if(!StringUtil.checkListBlank(codeList)){
					String code_name = "";
					codeDto = new BaseDto();
					for(Dto cd : codeList) {
						intDto = new BaseDto();
						code_name = cd.getAsString("code_name");
						intDto.put("code_name", code_name);
						valueList = CTReader.queryForList("admin.getCodeValueList",intDto);
						if(CTUtils.isNotEmpty(valueList)){
							codeDto.put(code_name, valueList);
						}
					}
					
					reDto.put(CTConstants.CT_CODE_LIST,codeDto);
					StringUtil.xprint("=======共加载【"+codeDto.size()+"】条字典参数！=======");
				}else{
					StringUtil.xprint("=====<<<<<<<<<<<<当前系统还没有任何字典参数！>>>>>>>>>>>=======");
				}
			}
			
			//处理全局参数
			if(success){
				Dto paramDto = null;
				List<Dto> paramList = CTReader.queryForList("admin.getParamCodeList");
				if(!StringUtil.checkListBlank(paramList)){
					paramDto = new BaseDto();
					for (Dto dto : paramList) {
						paramDto.put(dto.getAsString("param_key"), dto);
					}
					reDto.put(CTConstants.CT_PARAM_LIST,paramDto);
					StringUtil.xprint("=======共加载【"+paramDto.size()+"】条全局参数！=======");
				}else{
					StringUtil.xprint("=====<<<<<<<<<<<<当前系统还没有任何全局参数！>>>>>>>>>>>=======");
				}
			}
			
			//处理角色信息
			if(success){
				Dto roleDto = null;
				List<Dto> roleList = CTReader.queryForList("role.getSystemRole");
				if(!StringUtil.checkListBlank(roleList)){
					roleDto = new BaseDto();
					for (Dto dto : roleList) {
						roleDto.put(dto.getAsString("role_code"), dto);
					}
					reDto.put(CTConstants.CT_ROLE_LIST,roleDto);
					StringUtil.xprint("=======共加载【"+roleDto.size()+"】条角色信息！=======");
				}else{
					StringUtil.xprint("=====<<<<<<<<<<<<当前系统还没有任何全局参数！>>>>>>>>>>>=======");
				}
			}
			
			StringUtil.xprint("加载数据库中所有公用数据到缓存中结束...");
			
			
		} catch (Exception e) {
			success = false;
			StringUtil.xprint("------------加载数据到缓存失败!----------------");
			e.printStackTrace();
		}
		
		return reDto;
	}
	
	
	//更新缓存信息
	public static boolean updateCache(String key,ServletContext servletContext) {
		boolean temp = false;
		try {
			
			Dto reDto = initCache();
			if((CTConstants.CT_CODE_LIST).equals(key)){
				StringUtil.xprint("更新字典参数到缓存成功...");
				temp = true;
				servletContext.setAttribute(CTConstants.CT_CODE_LIST, reDto.get(CTConstants.CT_CODE_LIST));
			}
			if((CTConstants.CT_PARAM_LIST).equals(key)){
				StringUtil.xprint("更新全局参数到缓存成功...");
				temp = true;
				servletContext.setAttribute(CTConstants.CT_PARAM_LIST, reDto.get(CTConstants.CT_PARAM_LIST));
			}
			
		} catch (Exception e) {
			temp = false;
			e.printStackTrace();
			StringUtil.xprint("更新缓存失败...");
		}
		return temp;
	}
}
