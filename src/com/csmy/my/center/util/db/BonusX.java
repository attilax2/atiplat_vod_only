package com.csmy.my.center.util.db;

import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.cache.InitSystemCache;
import com.csmy.my.center.util.dataconvert.Dto;

public class BonusX {

	public double get_bonusPercent() {
		 
		
		Dto paramDto =	(Dto) InitSystemCache.reDto.get(CTConstants.CT_PARAM_LIST);
		Dto dto2=(Dto) paramDto.get("MEM_PERCENT");
		String bonusPercent=dto2.getAsString("param_value");
		return Double.parseDouble(bonusPercent);
	}

}
