package com.csmy.my.center.util.db;
import java.sql.*;
/**
 * 数据库工具类
 * @author Administrator
 *
 */
public class DBUtil {
	/**
	 * 当前正文数
	 */
	public static int currentCount = 0;
	
	/**
	 * 得到上次统计正文数
	 * @return
	 */
	public static int getLastCount() {
		return currentCount;
	}
	
	
//	public static int setCurrentCount(int clientID){
//		currentCount=getContentCount(clientID);
//		return currentCount;
//	}
	
	/**
	 * 查出当前正文数
	 * @return int
	 */
//	public static int getContentCount(int clientID) {
//		Connection con = null;
//		Statement stm = null;
//		ResultSet rs = null;
//		try {
//			con = DBConnector.getMysqlConnect();
//			stm = con.createStatement();
//			rs = stm.executeQuery("select count(distinct content_id) cnt from content_info_keyword where client_id="+clientID);
//			if (rs.next()) {
//				return rs.getInt("cnt");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			DBConnector.close(rs, stm, con);
//		}
//		return 0;
//	}
}
