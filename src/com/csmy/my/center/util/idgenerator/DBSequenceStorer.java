package com.csmy.my.center.util.idgenerator;


import com.csmy.my.center.dao.Dao;
import com.csmy.my.center.util.SpringBeanLoader;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.id.SequenceStorer;
import com.csmy.my.center.util.id.StoreSequenceException;

/**
 * ID数据库逻辑存储器
 * @author wgp
 * @since 2012-03-21
 */
public class DBSequenceStorer implements SequenceStorer{
	
	private static Dao ctDao = (Dao)SpringBeanLoader.getSpringBean("CTDao");
	
	/**
	 * 返回当前最大序列号
	 */
	public long load(String pIdColumnName) throws StoreSequenceException {
		Dto dto = new BaseDto();
		dto.put("fieldname", pIdColumnName);
		dto = (BaseDto)ctDao.queryForObject("c.getEaSequenceByFieldName", dto);
		Long maxvalue = dto.getAsLong("maxid");
		return maxvalue.longValue();
	}
	
	/**
	 * 写入当前生成的最大序列号值
	 */
	public void  updateMaxValueByFieldName(long pMaxId, String pIdColumnName) throws StoreSequenceException {
		Dto dto = new BaseDto();
		dto.put("maxid", String.valueOf(pMaxId));
		dto.put("fieldname", pIdColumnName);
		ctDao.update("c.updateMaxValueByFieldName", dto);
	}
}
