package com.csmy.my.center.util.idgenerator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.csmy.my.center.dao.Dao;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.SpringBeanLoader;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.id.SequenceStorer;
import com.csmy.my.center.util.id.fomater.DefaultSequenceFormater;
import com.csmy.my.center.util.id.generator.DefaultIDGenerator;
import com.csmy.my.center.util.id.sequence.DefaultSequenceGenerator;

/**
 * ID生成器
 * @author jackie
 * @since 2013-01-19
 */
public class IdGenerator {
	private static Log log = LogFactory.getLog(IdGenerator.class);
	private static int catche = 1;
	private static Dao ctDao = (Dao)SpringBeanLoader.getSpringBean("CTDao");
	
	/**
	 * 字段名称
	 */
	private String fieldname;
	
	public IdGenerator(String pFieldName){
		setFieldname(pFieldName);
	}
	
	public IdGenerator(){
	}
	
	/**
	 * 获取ID生成器实例
	 * @return DefaultIDGenerator
	 */
	@SuppressWarnings("unchecked")
	public DefaultIDGenerator getDefaultIDGenerator(){
		Dto dto = new BaseDto();
		dto.put("fieldname", getFieldname());
		dto = (BaseDto)ctDao.queryForObject("c.getEaSequenceByFieldName", dto);
		DefaultIDGenerator idGenerator = new DefaultIDGenerator(); 
		DefaultSequenceFormater sequenceFormater = new DefaultSequenceFormater(); 
		sequenceFormater.setPattern(dto.getAsString("pattern"));
		DefaultSequenceGenerator sequenceGenerator = new DefaultSequenceGenerator(getFieldname());
		SequenceStorer sequenceStorer = new DBSequenceStorer();
		sequenceGenerator.setSequenceStorer(sequenceStorer);
		sequenceGenerator.setCache(catche);
		idGenerator.setSequenceFormater(sequenceFormater);
		idGenerator.setSequenceGenerator(sequenceGenerator);
		return idGenerator;
	}
	
    /**
     * 食品分类编号ID生成器(自定义)
     * @param Parentid 菜单编号的参考编号
     * @return
     */
	public static String getTypeIdGenerator(String pParentid){
		String maxSubTypeId = (String)ctDao.queryForObject("c.getMaxSubTypeId", pParentid);
		String typeID = null;
		if(CTUtils.isEmpty(maxSubTypeId)){
			typeID = "01";
		}else{
			int length = maxSubTypeId.length();
			String temp = maxSubTypeId.substring(length-2, length);
			int intTypeID = Integer.valueOf(temp).intValue() + 1;
			if(intTypeID > 0 && intTypeID < 10){
				typeID = "0" + String.valueOf(intTypeID);
			}else if(10 <= intTypeID && intTypeID <= 99){
				typeID = String.valueOf(intTypeID);
			}else if(intTypeID > 99){
				log.error(CTConstants.Exception_Head + "生成分类编号越界了.同级兄弟节点编号为[01-99]\n请和您的系统管理员联系!");
			}else{
				log.error(CTConstants.Exception_Head + "生成分类编号发生未知错误,请和开发人员联系!");
			}
		}
		return pParentid + typeID;
	}
	
	  /**
     * 菜单编号ID生成器(自定义)
     * @param Parentid 菜单编号的参考编号
     * @return
     */
	public static String getMenuIdGenerator(String pParentid){
		String maxSubMenuId = (String)ctDao.queryForObject("c.getMaxSubMenuId", pParentid);
		String menuId = null;
		if(CTUtils.isEmpty(maxSubMenuId)){
			menuId = "01";
		}else{
			int length = maxSubMenuId.length();
			String temp = maxSubMenuId.substring(length-2, length);
			int intMenuId = Integer.valueOf(temp).intValue() + 1;
			if(intMenuId > 0 && intMenuId < 10){
				menuId = "0" + String.valueOf(intMenuId);
			}else if(10 <= intMenuId && intMenuId <= 99){
				menuId = String.valueOf(intMenuId);
			}else if(intMenuId > 99){
				log.error(CTConstants.Exception_Head + "生成菜单编号越界了.同级兄弟节点编号为[01-99]\n请和您的系统管理员联系!");
			}else{
				log.error(CTConstants.Exception_Head + "生成菜单编号发生未知错误,请和开发人员联系!");
			}
		}
		return pParentid + menuId;
	}
	
	
    /**
     * 菜系编号ID生成器(自定义)
     * @param Parentid 菜单编号的参考编号
     * @return
     */
	public static String getDepartIdGenerator(String pParentid){
		String maxSubTypeId = (String)ctDao.queryForObject("c.getMaxSubDepartId", pParentid);
		String typeID = null;
		if(CTUtils.isEmpty(maxSubTypeId)){
			typeID = "01";
		}else{
			int length = maxSubTypeId.length();
			String temp = maxSubTypeId.substring(length-2, length);
			int intTypeID = Integer.valueOf(temp).intValue() + 1;
			if(intTypeID > 0 && intTypeID < 10){
				typeID = "0" + String.valueOf(intTypeID);
			}else if(10 <= intTypeID && intTypeID <= 99){
				typeID = String.valueOf(intTypeID);
			}else if(intTypeID > 99){
				log.error(CTConstants.Exception_Head + "生成菜系编号越界了.同级兄弟节点编号为[01-99]\n请和您的系统管理员联系!");
			}else{
				log.error(CTConstants.Exception_Head + "生成菜系编号发生未知错误,请和开发人员联系!");
			}
		}
		return pParentid + typeID;
	}
	
	public String getFieldname() {
		return fieldname;
	}
	public void setFieldname(String fieldname) {
		this.fieldname = fieldname;
	}
}
