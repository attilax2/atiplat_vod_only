package com.csmy.my.center.util.mail;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.StringUtil;

/**
 * 发送邮件公共服务类
 * @author jackie
 * @version DATE 2014-2-1
 */
public class MailService {
	private static byte key = 0X12;
	private static byte offset = 0;
	static MailSenderInfo mailInfo = null;
	static SimpleMailSender sms = new SimpleMailSender();
	static String sendAddress = "U+hxJGKJQU1LDW9VFSpRbjHS25+GGtvu";
	static String sendPassword = "U+hxJGKJQU2HWdee76TANw==";
	/**
	 * 获得邮件设置信息
	 * @return Properties
	 */
	private static String path;
	static {
		path = MailService.class.getResource("/").getPath() + "mail.properties";
		path = path.substring(1);
	}
	public static Properties getMailProperties() {
		Properties prop = null;
		try {
			prop = new Properties();
			InputStream ins = MailService.class.getResourceAsStream("/mail.properties");
			prop.load(ins);
			ins.close();
			ins = null;
			return prop;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 设置properties信息
	 * @return Properties
	 */
	public static void setMailProperties(String key,String value) {
		Properties prop = null;
		try {
			prop = getMailProperties();
			OutputStream out = new FileOutputStream(path);
			prop.setProperty(key, value);
			prop.store(out, "-------");
			out.close();
			out = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 发送邮件
	 * @return
	 */
	public static boolean sendMail(String recmail,String title,String content) {
		try {
			System.out.println("发送内容:"+content);
			Properties prop = getMailProperties();
			String from = prop.getProperty("sendAddress");
			String password = prop.getProperty("sendPassword");
			// 如果发送的邮件为空，则采用系统指定的邮箱地址
			if (from == null || "".equals(from.trim())) {
				from = new String(CTUtils.decryptBasedDes(sendAddress));
				password = new String(CTUtils.decryptBasedDes(sendPassword));
			}else{
				from = new String(CTUtils.decryptBasedDes(from));
				password = new String(CTUtils.decryptBasedDes(password));
			}
			
			
			StringUtil.xprint("sendAddress="+from);
			StringUtil.xprint("sendPassword="+password);
			
			mailInfo = new MailSenderInfo();
			mailInfo.setFromAddress(from);
			mailInfo.setPassword(password);
			mailInfo.setValidate(true);
			mailInfo.setMailServerHost(prop.getProperty("sendProtocol"));
			mailInfo.setMailServerPort(prop.getProperty("mailPort"));
			mailInfo.setSubject(title);
			mailInfo.setContent(content);
			mailInfo.setUserName(from);
			
			if(CTUtils.isNotEmpty(recmail)){
				StringUtil.xprint("发送到用户邮件提醒地址...");
				mailInfo.setToAddress(recmail);
				sms.sendTextMail(mailInfo);
			}
			
			StringUtil.xprint("发送到后台配置的邮件提醒地址...");
			String[] toAddress = prop.getProperty("receiveAddress").split(";");
			if(CTUtils.isNotEmpty(toAddress)){
			    for (String str : toAddress) {
					mailInfo.setToAddress(str);
					sms.sendTextMail(mailInfo);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			mailInfo = null;
		}
		return true;
	}
	
	/**
	 * 发送邮件
	 * @return
	 */
	public static boolean sendHtmlMail(List<String> recmail,String title,String content) {
		try {
			System.out.println("发送内容:"+content);
			System.out.println("目标邮箱地址:"+recmail);
			Properties prop = getMailProperties();
			String from = prop.getProperty("sendAddress");
			String password = prop.getProperty("sendPassword");
			
			// 如果发送的邮件为空，则采用系统指定的邮箱地址
			if (from == null || "".equals(from.trim())) {
				from = new String(CTUtils.decryptBasedDes(sendAddress));
				password = new String(CTUtils.decryptBasedDes(sendPassword));
			}else{
				from = new String(CTUtils.decryptBasedDes(from));
				password = new String(CTUtils.decryptBasedDes(password));
			}
			
			StringUtil.xprint("sendAddress="+from);
			StringUtil.xprint("sendPassword="+password);
			
			mailInfo = new MailSenderInfo();
			mailInfo.setFromAddress(from);
			mailInfo.setPassword(password);
			mailInfo.setValidate(true);
			mailInfo.setMailServerHost(prop.getProperty("sendProtocol"));
			mailInfo.setMailServerPort(prop.getProperty("mailPort"));
			mailInfo.setSubject(title);
			mailInfo.setContent(content);
			mailInfo.setUserName(from);
			
			//发送邮件到指定账号
			if(!StringUtil.checkListBlank(recmail)){
				for (String toAddr : recmail) {
					mailInfo.setToAddress(toAddr);
					sms.sendHtmlMail(mailInfo);
				}
			}
			
			//后台配置的邮件地址
			StringUtil.xprint("发送到后台配置的邮件提醒地址...");
			String[] toAddress = prop.getProperty("receiveAddress").split(";");
			if(CTUtils.isNotEmpty(toAddress)){
			    for (String str : toAddress) {
			    	StringUtil.xprint("发送到后台配置的邮件为："+str);
					mailInfo.setToAddress(str);
					sms.sendHtmlMail(mailInfo);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			mailInfo = null;
		}
		return true;
	}
	
	/**
	 * 加密算法
	 * @param src
	 * @param dest
	 */
	public static byte[] encrypt(byte[] src) {
		int len = src.length;
		byte[] dest = new byte[len];
		for(int i = 0;i < len;i++) {
			dest[i] = (byte)((src[i] ^ key) + offset);
		}
		return dest;
	}
	
	/**
	 * 解密算法
	 * @param src
	 * @param dest
	 */
	public static byte[] deEncrypt(byte[] src) {
		int len = src.length;
		byte[] dest = new byte[len];
		for(int i = 0;i < len;i++) {
			dest[i] = (byte)((src[i] - offset) ^ key);
		}
		return dest;
	}
	
	public static void main1(String[] args){
         //这个类主要是设置邮件   
//		System.out.println(getMailProperties());
//		System.out.println(deEncrypt(((String)getMailProperties().get("sendAddress")).getBytes()));
		byte[] src = "qakes`|| &\"#".getBytes();
		byte[] src1 = encrypt(src);
		byte[] src2 = deEncrypt(src1);
		System.out.println(new String (src1));
		System.out.println(new String (src2));
		
    }
	
	public static void main(String[] args) {
		System.out.println(new String(encrypt(sendAddress.getBytes())));
		System.out.println(new String(encrypt(sendPassword.getBytes())));
//		System.out.println(deEncrypt(((String)getMailProperties().get("sendAddress")).getBytes()));
//		System.out.println(deEncrypt(((String)getMailProperties().get("sendPassword")).getBytes()));
	}

}