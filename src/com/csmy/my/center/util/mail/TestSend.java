package com.csmy.my.center.util.mail;

import javax.mail.MessagingException;

/**
 * 发送邮件测试类
 * @author jackie
 * @version DATE 2014-2-1
 */
public class TestSend {

public static void main(String[] args) throws MessagingException{   
         //这个类主要是设置邮件   
      MailSenderInfo mailInfo = new MailSenderInfo();    
      mailInfo.setMailServerHost("smtp.163.com");    
      mailInfo.setMailServerPort("25");    
      mailInfo.setValidate(true);    
      mailInfo.setUserName("test@163.com");    
      mailInfo.setPassword("******");//您的邮箱密码    
      mailInfo.setFromAddress("test@163.com");    
      mailInfo.setToAddress("123@qq.com");    
      mailInfo.setSubject("设置邮箱标题 美音网格");    
      mailInfo.setContent("设置邮箱内容 长沙美音网格有限公司");    
         //这个类主要来发送邮件   
      SimpleMailSender sms = new SimpleMailSender();   
      sms.sendTextMail(mailInfo);//发送文体格式    
      sms.sendHtmlMail(mailInfo);//发送html格式   
    }

}