package com.csmy.my.center.util.properties;

/**
 * Properties文件类型
 * @author wgp
 * @since 2012-08-2
 */
public interface PropertiesFile {
	/**
	 * Properties文件类型<br>
	 * EREDBOS对应global.eredbos.properties属性文件
	 */
	public static final String G4 = "g4";
	
	/**
	 * Properties文件类型<br>
	 * EREDBOS对应global.myconfig.properties属性文件
	 */
	public static final String APP = "app";
	
	/**
	 * Properties文件类型<br>
	 * EREDBOS对应cache_objects.properties属性文件
	 */
	public static final String CACHE = "cache";
}
