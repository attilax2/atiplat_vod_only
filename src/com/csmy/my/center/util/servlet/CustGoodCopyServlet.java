package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import m.global;

import com.attilax.ioc.IocXq214;
import com.attilax.lang.Global;
import com.attilax.token.TokenService;
import com.attilax.web.ReqX;
import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;
import com.google.inject.Inject;

public class CustGoodCopyServlet extends HttpServlet {
	@Inject
	TokenService tokSvr;
	private static final long serialVersionUID = 2698031791465920323L;
	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		//处理商品信息
		String param = "";
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		CustomerInfo customerInfo = RequestUtil.getCUser(req);
//		if(CTUtils.isEmpty(customerInfo)){
//			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
//			return;
//		} 
		
		if(tokSvr==null)
			tokSvr=IocXq214.getBean(TokenService.class);
		tokSvr.setModule("merMod");
		System.out.println("---");
	//	tokSvr.clrToken(resp);
		if(!tokSvr.isHasToken(req))
		{
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login?envi=cp&utype=mer&ver=1");return;
		}
			
			//处理列表
			if(reqPath.indexOf("list")!=-1){
				list(req, resp, paramDto, customerInfo);
			}
			
			//处理增加
			if(reqPath.indexOf("add")!=-1){
				add(req, resp, paramDto);
			}
			
			//处理保存
			if(reqPath.indexOf("save")!=-1){
				save(req, resp, paramDto, customerInfo);
			}
			
			//处理编辑
			if(reqPath.indexOf("edit")!=-1){
				edit(req, resp, paramDto, customerInfo);
			}
			
			//处理删除
			if(reqPath.indexOf("trash")!=-1){
				del(paramDto, customerInfo);
			}
			
			
		 
		
	}

	private void del(Dto paramDto, CustomerInfo customerInfo) {
		//处理添加逻辑
		TokenService ts=new TokenService();
		ts.setModule("merMod");
	String uid=	ts.getuid(Global.req.get());
		boolean temp = false;
		String copy_id = paramDto.getAsString("copy_id");
		if(CTUtils.isNotEmpty(copy_id)){//修改
			copy_id = copy_id.replaceAll(",", "','");
			temp = JdbcTemplateTool.deleteGoodCopy(copy_id,uid);
			if(temp){
				RequestUtil.renderText("true");
			}else {
				RequestUtil.renderText("false");
			}
		}
	}

	private void edit(HttpServletRequest req, HttpServletResponse resp,
			Dto paramDto, CustomerInfo customerInfo) throws IOException {
		boolean temp = false;
		String copy_id = paramDto.getAsString("copy_id");
		if(CTUtils.isNotEmpty(copy_id)){//修改
			paramDto.put("customer_id", customerInfo.getCustomer_id());
			paramDto.put("copy_title", paramDto.getAsString("copy_title"));
			paramDto.put("copy_content",paramDto.getAsString("copy_content"));
			temp = JdbcTemplateTool.editGoodCopy(paramDto);
			if(temp){
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_copy");
				resp.sendRedirect(RequestUtil.getBasePath(req)+"/cust/goods/copy/list");
			}
		}
	}

	private void save(HttpServletRequest req, HttpServletResponse resp,
			Dto paramDto, CustomerInfo customerInfo) throws ServletException,
			IOException {
		boolean temp = false;
		String copy_id = paramDto.getAsString("copy_id");
		req.setAttribute("tp", paramDto.getAsString("tp"));
		if(CTUtils.isNotEmpty(copy_id)){//修改
			List<Dto> copyList = JdbcTemplateTool.getGoodCopyList(paramDto).getDatas();
			if(!StringUtil.checkListBlank(copyList)){
				Dto copyInfo = copyList.get(0);
				req.setAttribute("copyInfo", copyInfo);
			}
			req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_copy");
			req.getRequestDispatcher("/customer/add_cust_good_copy.jsp").forward(req, resp);
		}else {//添加
			TokenService ts=new TokenService();
			ts.setModule("merMode");
			paramDto.put("customer_id", ts.getuid(Global.req.get()));
			paramDto.put("copy_title", paramDto.getAsString("copy_title"));
			String content = paramDto.getAsString("copy_content");
			content = CTUtils.rhtml(content);
			paramDto.put("copy_content",content);
			temp = JdbcTemplateTool.addGoodCopy(paramDto);
			if(temp){
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_copy");
				resp.sendRedirect(RequestUtil.getBasePath(req)+"/cust/goods/copy/list");
			}
		}
	}

	private void add(HttpServletRequest req, HttpServletResponse resp,
			Dto paramDto) throws ServletException, IOException {
		req.setAttribute("tp", paramDto.getAsString("tp"));
		if(paramDto.getAsString("tp").equals("0")){
			
		   req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_copy_add1");
		   
		}else if(paramDto.getAsString("tp").equals("1")){
			
		   req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_copy_add2");
		   
		}else{
			
		   req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_copy_add3");
		   
		}
		 req.setAttribute("tp", paramDto.getAsString("tp"));
		req.getRequestDispatcher("/customer/add_cust_good_copy.jsp").forward(req, resp);
	}

	private void list(HttpServletRequest req, HttpServletResponse resp,
			Dto paramDto, CustomerInfo customerInfo) throws ServletException,
			IOException {
		//处理列表数据
		String searchValue = paramDto.getAsString("searchText");
		paramDto.put("alisaname", searchValue);
		
		paramDto.put("customer_id", tokSvr.getuid(req));
		//paramDto.put("page_size", 20);
		//paramDto = RequestUtil.getPageDto2(paramDto);
		int pageSize = 0;
		String page_size = CTUtils.getParamValue("PAGE_MSIZE");
		if(CTUtils.isNotEmpty(page_size)){
			pageSize = Integer.valueOf(page_size);
		}else{
			pageSize = CTConstants.PAGER_MSIZE;
		}
		paramDto.put("pageSize", pageSize);
		PageModel pager = JdbcTemplateTool.getGoodCopyList(paramDto);
		req.setAttribute("pm", pager);
		
		//回传参数
		req.setAttribute("pageSize", pageSize);
		req.setAttribute("alisaname", searchValue);
		req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_copy");
		String envi=ReqX.getParameter(req, "envi",""); 
		if(StringUtil.isNotEmpty(envi))
			envi="_"+envi;
		req.getRequestDispatcher("/customer/custom_good_copy"+envi+".jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
