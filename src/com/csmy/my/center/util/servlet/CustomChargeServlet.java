package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class CustomChargeServlet extends HttpServlet {

	private static final long serialVersionUID = -6245638097989631195L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String param = "";
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		CustomerInfo customerInfo = RequestUtil.getCUser(req);
		if(reqPath.indexOf("list")!=-1){
			//处理列表数据
			String searchValue = CTUtils.CharsetFilter(paramDto.getAsString("searchText"));
			paramDto.put("alisaname", searchValue);
			paramDto.put("customer_id", customerInfo.getCustomer_id());
			PageModel pager = JdbcTemplateTool.getCustChargeList(paramDto);
			req.setAttribute("pm", pager);
			
			req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "pay_charge");
			req.getRequestDispatcher("/customer/custom_charge.jsp").forward(req, resp);
		}
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
