package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.DefaultEditorKit.CutAction;

import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.SenderQueryUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.ZHToEN;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.db.JdbcTemplateTool;
import com.sun.org.apache.bcel.internal.classfile.PMGClass;

public class CustomOrderServlet extends HttpServlet {

	private static final long serialVersionUID = 711765943689137716L;

	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {


		//处理商品信息
		String param = "";
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		//处理分页条件过滤
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		String buyer_name = paramDto.getAsString("buyer_name");
		String good_name = paramDto.getAsString("good_name");
		String account = paramDto.getAsString("account");
		//good_name = CTUtils.CharsetFilter(good_name);
		//buyer_name = CTUtils.CharsetFilter(buyer_name);
		
		//paramDto.put("good_name", good_name);
		//paramDto.put("buyer_name", buyer_name);
		CustomerInfo customerInfo = RequestUtil.getCUser(req);
		if(CTUtils.isEmpty(customerInfo)){
			CTUtils.getParamToReq(req);
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		}else{
			//处理列表
			if(reqPath.indexOf("list")!=-1){
				
				param = RequestUtil.getParamFromURl(reqPath, temp_path);
				StringUtil.xprint("获取的参数=="+param);
				String queryType = paramDto.getAsString("queryType");
				if(StringUtil.isNotEmpty(param) && param.equals("0"))param = null;

				if(CTUtils.isNotEmpty(paramDto.getAsString("state_id"))){
					param = paramDto.getAsString("state_id");
				}
				if(CTUtils.isNotEmpty(queryType)&&queryType.equals("1")){
					good_name = CTUtils.CharsetFilter(good_name);
					paramDto.put("good_name", good_name);
					
					buyer_name = CTUtils.CharsetFilter(buyer_name);
					paramDto.put("buyer_name", buyer_name);
					
					account = CTUtils.CharsetFilter(account);
					paramDto.put("account", account);
				}
				//处理列表数据
				int pageSize = 0;
				String page_size = CTUtils.getParamValue("PAGE_MSIZE");
				if(CTUtils.isNotEmpty(page_size)){
					pageSize = Integer.valueOf(page_size);
				}else{
					pageSize = CTConstants.PAGER_MSIZE;
				}
				paramDto.put("pageSize", pageSize);
				String searchValue = paramDto.getAsString("searchText");
				paramDto.put("alisaname", searchValue);
				paramDto.put("state_id", param);
				paramDto.put("customer_id", customerInfo.getCustomer_id());
				PageModel pager = JdbcTemplateTool.getCTOrderList(paramDto);
				req.setAttribute("pm", pager);
				//获取合计数据
				Dto counDto = JdbcTemplateTool.getCTOrderCount(paramDto);
				req.setAttribute("counDto", counDto);
				
				if (CTUtils.isEmpty(param)) {
					param = "";
				}
				//回传参数
				req.setAttribute("state_id", param);
				req.setAttribute("pageSize", pageSize);
				req.setAttribute("alisaname", searchValue);
				req.setAttribute("order_id", paramDto.getAsString("order_id"));
				req.setAttribute("good_name", good_name);
				req.setAttribute("buyer_name", buyer_name);
				req.setAttribute("buyer_phone", paramDto.getAsString("buyer_phone"));
				req.setAttribute("account", paramDto.getAsString("account"));
				req.setAttribute("end_date", paramDto.getAsString("end_date"));
				req.setAttribute("start_date", paramDto.getAsString("start_date"));
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "order_"+param);
				req.getRequestDispatcher("/customer/custom_order.jsp").forward(req, resp);
			}
			
			//处理结算列表
			if(reqPath.indexOf("count")!=-1){
				
				param = RequestUtil.getParamFromURl(reqPath, temp_path);
				StringUtil.xprint("获取的参数=="+param);

				//处理列表数据
				String searchValue = paramDto.getAsString("searchText");
				paramDto.put("alisaname", searchValue);
				paramDto.put("c_state", param);
				paramDto.put("o_state", "7");//只处理已签收的订单
				paramDto.put("customer_id", customerInfo.getCustomer_id());
				
				int pageSize = 0;
				String page_size = CTUtils.getParamValue("PAGE_MSIZE");
				if(CTUtils.isNotEmpty(page_size)){
					pageSize = Integer.valueOf(page_size);
				}else{
					pageSize = CTConstants.PAGER_MSIZE;
				}
				paramDto.put("pageSize", pageSize);
				PageModel pager = JdbcTemplateTool.getCustOrderCountList(paramDto);
				req.setAttribute("pm", pager);
				
				//回传参数
				req.setAttribute("state_id", param);
				req.setAttribute("pageSize", pageSize);
				req.setAttribute("alisaname", searchValue);
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "pay_count_"+param);
				req.setAttribute("pay_date", CTUtils.getParamValue("ALIPAY_DATE_PERIOD"));
				req.getRequestDispatcher("/customer/custom_ocount.jsp").forward(req, resp);
			}
			
			//处理返款列表
			if(reqPath.indexOf("money")!=-1){
				
				StringUtil.xprint("获取的参数=="+paramDto);
				//处理列表数据
				String searchValue = paramDto.getAsString("searchText");
				paramDto.put("alisaname", searchValue);
				paramDto.put("customer_id", customerInfo.getCustomer_id());
				//paramDto.put("page_size", 20);
				//paramDto = RequestUtil.getPageDto2(paramDto);
				int pageSize = 0;
				String page_size = CTUtils.getParamValue("PAGE_MSIZE");
				if(CTUtils.isNotEmpty(page_size)){
					pageSize = Integer.valueOf(page_size);
				}else{
					pageSize = CTConstants.PAGER_MSIZE;
				}
				paramDto.put("pageSize", pageSize);
				PageModel pager = JdbcTemplateTool.getCustOrderMoneyList(paramDto);
				req.setAttribute("pm", pager);
				
				//回传参数
				req.setAttribute("stateid", paramDto.getAsString("stateid"));
				req.setAttribute("alisaname", searchValue);
				req.setAttribute("pageSize", pageSize);
				req.setAttribute("pay_date", CTUtils.getParamValue("ALIPAY_DATE_PERIOD"));
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "pay_money");
				req.getRequestDispatcher("/customer/custom_omoney.jsp").forward(req, resp);
			}
			
			//处理订单导入
			if(reqPath.indexOf("import")!=-1){
				req.setAttribute("state", 2);
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "order_2");
				req.getRequestDispatcher("/customer/importCustOrder.jsp").forward(req, resp);
			}
			
			//处理订单签收
			if(reqPath.indexOf("bsign")!=-1){
				Dto mDto = new BaseDto();
				mDto.put("state_id", "4");
				mDto.put("pageSize", "100000");
				mDto.put("customer_id", customerInfo.getCustomer_id());
				List<Dto> orderList = JdbcTemplateTool.getCTOrderList(mDto).getDatas();
				if(CTUtils.isNotEmpty(orderList)){
					req.setAttribute("onum", orderList.size());
				}else{
					req.setAttribute("onum", "0");
				}
				req.setAttribute("state", 4);
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "order_4");
				req.getRequestDispatcher("/customer/batchSignOrder.jsp").forward(req, resp);
			}
			
			//处理订单删除
			if(reqPath.indexOf("delete")!=-1){
				boolean temp = false;
				String order_id = paramDto.getAsString("order_id");
				if(CTUtils.isNotEmpty(order_id)){
					order_id = order_id.replaceAll(",", "','");
					temp = JdbcTemplateTool.deleteOrder(order_id);
				    if(temp){
					   RequestUtil.renderText("true");
				    }else{
					   RequestUtil.renderText("false");
				    }
				}
			}
			
			//处理订单编辑
			if(reqPath.indexOf("modify")!=-1){
				boolean temp = false;
				String sku_id = paramDto.getAsString("sku_id");
				String order_id = paramDto.getAsString("order_id");
				if(CTUtils.isNotEmpty(sku_id)){//进入编辑
					if(CTUtils.isNotEmpty(order_id)){
						temp = JdbcTemplateTool.updateOrder(paramDto);
					    if(temp){
						   RequestUtil.renderText("true");
					    }else{
						   RequestUtil.renderText("false");
					    }
					}
				}else{//访问页面
					
					//获取订单详情
					List<Dto> skuList = null;
					Dto orderInfo = new BaseDto();
					List<Dto> corderList = JdbcTemplateTool.getCOrderList(paramDto);
					if(!StringUtil.checkListBlank(corderList)){
						orderInfo = corderList.get(0);
					}
					//获取商品套餐信息
					skuList = JdbcTemplateTool.getGoodSkuList(orderInfo.getAsString("good_id"));
					req.setAttribute("skuList", skuList);
					req.setAttribute("orderInfo", orderInfo);
					req.setAttribute("senderList", CTUtils.getCodeList("SENDER_TYPE"));
					req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "order_0");
					req.getRequestDispatcher("/customer/editOrderInfo.jsp").forward(req, resp);
					
				}
			}
			
			//处理返款列表
			if(reqPath.indexOf("paylog")!=-1){
				
				param = RequestUtil.getParamFromURl(reqPath, temp_path);
				StringUtil.xprint("获取的参数=="+param);

				//处理列表数据
				String searchValue = paramDto.getAsString("searchText");
				paramDto.put("alisaname", searchValue);
				paramDto.put("customer_id", customerInfo.getCustomer_id());
				//paramDto.put("page_size", 20);
				//paramDto = RequestUtil.getPageDto2(paramDto);
				int pageSize = 0;
				String page_size = CTUtils.getParamValue("PAGE_MSIZE");
				if(CTUtils.isNotEmpty(page_size)){
					pageSize = Integer.valueOf(page_size);
				}else{
					pageSize = CTConstants.PAGER_MSIZE;
				}
				paramDto.put("pageSize", pageSize);
				PageModel pager = JdbcTemplateTool.getCustomPayLog(paramDto);
				req.setAttribute("pm", pager);
				
				//回传参数
				req.setAttribute("alisaname", searchValue);
				req.setAttribute("pageSize", pageSize);
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "pay_log");
				req.getRequestDispatcher("/customer/custom_paylog.jsp").forward(req, resp);
			}
			
			//处理财务支付
			if(reqPath.indexOf("gopay")!=-1){
				boolean temp = false;
				String um_id = paramDto.getAsString("um_id");
				String alipay_code = paramDto.getAsString("alipay_code");
				if(CTUtils.isNotEmpty(alipay_code)){//进入支付流程
					//添加支付日志
					temp = JdbcTemplateTool.addOrderPayLog(paramDto);
					if(temp){
						temp = JdbcTemplateTool.updateOrderMoney("1", um_id);
					}
					
				    if(temp){
					   RequestUtil.renderText("true");
				    }else{
					   RequestUtil.renderText("false");
				    }
					
					
				}else{
					
					//获取所有参数信息
					int count = 0;
					int fcount = 0;
					List<Dto> umList = null;
					Dto payMoneyDto = null;
					String um_idString = "";
					String um_ids = paramDto.getAsString("um_id");
					if(CTUtils.isNotEmpty(um_ids)){
						double payMoney = 0d;
						double smoney = 0d;
						um_ids = um_ids.replaceAll(",", "','");
						paramDto.put("um_id", um_ids);
						umList = JdbcTemplateTool.getCustOrderMoneyList(paramDto).getDatas();
						payMoneyDto = (BaseDto)umList.get(0);
						for (Dto dto2 : umList) {
							count++;
							if(count<umList.size()){
								um_idString +=dto2.getAsString("um_id")+",";
							}else{
								um_idString +=dto2.getAsString("um_id");
							}
							
							smoney = Double.valueOf(dto2.getAsString("own_money"));
							if(smoney<0)fcount++;
							payMoney += smoney;//实际应该打款数
						}
						
						if(count>1){
							payMoneyDto.put("payNum", count);
							payMoneyDto.put("payType", "合并付款");
						}else{
							payMoneyDto.put("payNum", 1);
							payMoneyDto.put("payType", "普通付款");
						}
						if(fcount>0){
							payMoneyDto.put("payFNum", fcount);
						}
						payMoneyDto.put("payMoney", payMoney);
						payMoneyDto.put("um_ids", um_idString);
					}
					req.setAttribute("um_id", um_id);
					req.setAttribute("moneyInfo", payMoneyDto);
					req.getRequestDispatcher("/customer/goPayMoney.jsp").forward(req, resp);
				}
			}
			
			//处理审核
			if(reqPath.indexOf("check")!=-1){
				boolean temp = false;
				String ocid = paramDto.getAsString("ocid");
				String state_id = paramDto.getAsString("state_id");
				if(CTUtils.isNotEmpty(state_id) && CTUtils.isNotEmpty(ocid)){
				   temp = JdbcTemplateTool.updateOrderCount(state_id, ocid);
				   if(temp){
					   RequestUtil.renderText("true");
				   }else{
					   RequestUtil.renderText("false");
				   }
				}
			}
			
			//批量审核订单
			if(reqPath.indexOf("bchange")!=-1){
				
				boolean temp = false;
				Dto intDto = new BaseDto();
				String oids = paramDto.getAsString("order_id");
				String remark = paramDto.getAsString("remark");
				String check_state = paramDto.getAsString("check_state");
				if(CTUtils.isNotEmpty(oids)&&CTUtils.isNotEmpty(check_state)){//修改
					String o_id [] = oids.split(",");
					for (String order_id : o_id) {
						
						intDto.put("order_id", order_id);
						intDto.put("order_remark", CTUtils.isEmpty(remark)?"批量审核":remark);
						intDto.put("order_id", order_id);
						
						if(check_state.equals("1")){//审核有效
							intDto.put("state", "2");//已审核
							if(CTUtils.isNotEmpty(remark)){
								intDto.put("log_txt", "已审核、待发货 "+"("+remark+")");
							}else{
								intDto.put("log_txt", "已审核、待发货 ");
							}
						}else if(check_state.equals("2")){//待跟进
							intDto.put("state", "9");//待跟进
							if(CTUtils.isNotEmpty(remark)){
							    intDto.put("log_txt", "待跟进 "+"("+remark+")");
							}else{
								intDto.put("log_txt", "待跟进 ");
							}
						}else{//无效取消
							intDto.put("state", "3");//已取消
							if(CTUtils.isNotEmpty(remark)){
							    intDto.put("log_txt", "已取消 "+"("+remark+")");
							}else{
								intDto.put("log_txt", "已取消 ");
							}
						}
						
						//开始处理订单信息
						temp = JdbcTemplateTool.updateOrderCountSate(intDto);
						if(temp){
							intDto.put("optype", "add_log");
							JdbcTemplateTool.addOrderLog(intDto.getAsString("order_id"), intDto.getAsString("log_txt"));
						}
					}
					
					//反馈信息
					if(temp){
					   RequestUtil.renderText("true");
				    }else{
					   RequestUtil.renderText("false");
				    }
					
				}else{//初始进入页面
					req.setAttribute("order_id", oids);
					req.getRequestDispatcher("/customer/batchCheckOrder.jsp").forward(req, resp);
				}
			}
			
			if(reqPath.indexOf("getexpress")!=-1){//获取物流详情
 				String gs = paramDto.getAsString("company");
 				String dh = paramDto.getAsString("number");
 				if(CTUtils.isNotEmpty(gs) && CTUtils.isNotEmpty(dh)){
 					//通过公司编号获取公司名称
 					String gsspell = null;
 					//String gs_name = CTUtils.getCodeName("SENDER_TYPE", gs);
 					try {
 						gsspell = ZHToEN.getPingyin(gs).toLowerCase();
					} catch (BadHanyuPinyinOutputFormatCombination e) {
						e.printStackTrace();
					}
					StringUtil.xprint("物流简称为："+gsspell);
					StringUtil.xprint("物流单号为："+dh);
 				    String wlString = SenderQueryUtil.getWuliu(dh, gsspell);
 				    StringUtil.xprint("物流信息为："+wlString);
 				    if(CTUtils.isNotEmpty(wlString)){
 				    	RequestUtil.renderText(wlString);
 				    }else{
 				    	RequestUtil.renderText("");
 				    }
 				}
 			}
			
		}
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
