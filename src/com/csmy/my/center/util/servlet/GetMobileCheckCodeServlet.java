package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.sms.SMS_Sender;

public class GetMobileCheckCodeServlet extends HttpServlet{
	
	private static final long serialVersionUID = -1487280593775608457L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String reqPath = req.getRequestURI();
		try {
			
			if(reqPath.indexOf("vcode")!=-1){
				
				//获取验证码
				String vcode = "";
				String rcode = "";
				String msg = "";
				String mobile = req.getParameter("mobile");
				if(CTUtils.isNotEmpty(mobile)){
				   Dto reDto = SMS_Sender.getSmsCode(mobile);
				   msg = reDto.getAsString("msg");
				   rcode = reDto.getAsString("rcode");
				   vcode = reDto.getAsString("vcode");
				   if(rcode.endsWith("2")){//发送成功
					   RequestUtil.renderText("1#"+msg);
					   //将验证码放入session中
					   StringUtil.xprint("获取到的手机验证码为："+vcode);
					   req.getSession().setAttribute(CTConstants.MOBILE_CHECK_CODE, vcode);
				   }else{
					   RequestUtil.renderText("0#"+msg);
				   }
				}
			}
			
            if(reqPath.indexOf("check")!=-1){
				
            	String vcode = req.getParameter("vcode");
            	if(CTUtils.isNotEmpty(vcode)){
            		if(vcode.equals(req.getSession().getAttribute(CTConstants.MOBILE_CHECK_CODE))){
            			RequestUtil.renderText("1");//验证码相同
            		}else{
            		    RequestUtil.renderText("0");//验证码相同
            		}
            	}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
