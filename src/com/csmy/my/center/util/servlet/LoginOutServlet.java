package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.attilax.ioc.IocXq214;
import com.attilax.sso.LoginX;
import com.attilax.token.TokenService;
import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.google.inject.Inject;

public class LoginOutServlet extends HttpServlet {
	
	private static final long serialVersionUID = 6682050543261677542L;
	@Inject
	TokenService tokSvr;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

         //退出登录
		HttpSession session = req.getSession();

		MemeberInfo memeber = (MemeberInfo) session.getAttribute(CTConstants.SESSION_MUSER);
		if (memeber != null) {
			session.removeAttribute(CTConstants.SESSION_MUSER);
			session.invalidate();
			StringUtil.xprint("=======当前登录会员session成功！======");
		}
		CTUtils.getParamToReq(req);
		
		//q32
		//q32
		if(tokSvr==null)
			tokSvr=IocXq214.getBean(TokenService.class);
		tokSvr.setModule("userMod");
		System.out.println("---");
	 tokSvr.clrToken(resp);
		//
	 	if(req.getParameter("afterLogoutGotoUrl")!=null)
	 		resp.sendRedirect(RequestUtil.getBasePath(req)+"/"+req.getParameter("afterLogoutGotoUrl"));
	 	else
	 		resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	

}
