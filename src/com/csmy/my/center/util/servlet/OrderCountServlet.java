package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class OrderCountServlet extends HttpServlet {
	
	private static final long serialVersionUID = -162960597648789296L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
          
		 //获取参数
		 String param = null;
		 String reqPath = req.getRequestURI();
         Dto paramDto = RequestUtil.getPraramsAsDto(req);
		 String nowURl = req.getServletPath();
		 String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
         if(CTUtils.isEmpty(RequestUtil.getUser(req))){
        	 CTUtils.getParamToReq(req);
			 resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		  } else {
			  
			//处理列表数据
			 int pageSize = 0;
			 String page_size = CTUtils.getParamValue("PAGE_MSIZE");
			 if(CTUtils.isNotEmpty(page_size)){
				pageSize = Integer.valueOf(page_size);
			 }else{
				pageSize = CTConstants.PAGER_MSIZE;
			 }
			 if(reqPath.indexOf("log")!=-1){//结算流水
				 //获取结算流水列表数据
				 param = RequestUtil.getParamFromURl(reqPath, temp_path);
				 if(CTUtils.isEmpty(param))param = "1";
				 paramDto.put("state", param);
				 paramDto.put("pageSize", pageSize);
				 paramDto.put("user_id", RequestUtil.getUser(req).getMemeber_id());
				 PageModel pager = JdbcTemplateTool.getOrderCountList(paramDto);
				 
				 req.setAttribute("pm", pager);
				 req.setAttribute("cur", param);
				 req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "orlist");
				 CTUtils.getParamToReq(req);
				 req.setAttribute("pageSize", pageSize);
				 req.getRequestDispatcher("/wxb/orderCountLog.jsp").forward(req, resp);
			 }
			 
             if(reqPath.indexOf("list")!=-1){//结算返款
            	 param = RequestUtil.getParamFromURl(reqPath, temp_path);
            	 paramDto.put("pageSize", pageSize);
            	 paramDto.put("user_id", RequestUtil.getUser(req).getMemeber_id());
            	 PageModel pager = JdbcTemplateTool.getOrderPayMoneyList(paramDto);
            	 req.setAttribute("pm", pager);
            	 req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "returns");
            	 CTUtils.getParamToReq(req);
            	 req.setAttribute("pageSize", pageSize);
				 req.getRequestDispatcher("/wxb/orderCountList.jsp").forward(req, resp);
			 }
		    
		 }
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	

	
}
