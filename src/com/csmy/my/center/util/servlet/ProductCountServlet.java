package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;
/**
 * 商品统计处理
 * @author jialiduo
 *
 */
public class ProductCountServlet extends HttpServlet {

	private static final long serialVersionUID = 6362408299068705073L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//处理商品信息
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		MemeberInfo memeberInfo = RequestUtil.getUser(req);
		if(CTUtils.isEmpty(memeberInfo)){
			CTUtils.getParamToReq(req);
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		}else{
			//按照商品推广统计
			if(reqPath.indexOf("goods")!=-1){
				//获取所有商户信息
				List<Dto> cusList = JdbcTemplateTool.getCustomerList();
				req.setAttribute("customerList", cusList);
				paramDto.put("good_name", CTUtils.specialStrFilter(paramDto.getAsString("good_name")));
				paramDto.put("user_id", memeberInfo.getMemeber_id());
				Dto proDto = JdbcTemplateTool.getProductCountList(paramDto);
				req.setAttribute("goodDto", proDto);
				req.setAttribute("end_date", paramDto.getAsString("end_date"));
				req.setAttribute("good_name", paramDto.getAsString("good_name"));
				req.setAttribute("customer_id", paramDto.getAsString("customer_id"));
				req.setAttribute("start_date", paramDto.getAsString("start_date"));
				req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "pcount");
				CTUtils.getParamToReq(req);
				req.getRequestDispatcher("/wxb/prduct_count.jsp").forward(req, resp);
			}
			
			//按照渠道推广统计
			if(reqPath.indexOf("channel")!=-1){

				paramDto.put("user_id", memeberInfo.getMemeber_id());
				paramDto.put("channel_name", CTUtils.specialStrFilter(paramDto.getAsString("channel_name")));
				Dto proDto = JdbcTemplateTool.getChannelCountList(paramDto);
				req.setAttribute("goodDto", proDto);
				req.setAttribute("end_date", paramDto.getAsString("end_date"));
				req.setAttribute("channel_name", paramDto.getAsString("channel_name"));
				req.setAttribute("start_date", paramDto.getAsString("start_date"));
				req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "ccount");
				CTUtils.getParamToReq(req);
				req.getRequestDispatcher("/wxb/channel_count.jsp").forward(req, resp);
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
