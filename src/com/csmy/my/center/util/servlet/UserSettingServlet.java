package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class UserSettingServlet extends HttpServlet {

	private static final long serialVersionUID = -2802526769808196379L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		  //进入用户设置
		String returnMsg="";
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		String memeber_id = paramDto.getAsString("memeber_id");
		if(CTUtils.isEmpty(req.getSession().getAttribute(CTConstants.SESSION_MUSER))){
			
			CTUtils.getParamToReq(req);
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
			
		}else if(StringUtil.isNotEmpty(memeber_id)){
			//处理修改数据逻辑
			boolean bool = JdbcTemplateTool.updateMemeberInfo(paramDto);
			if(bool){
				returnMsg = "0#修改会员信息成功";
				//修改会员session信息
				MemeberInfo memberInfo = (MemeberInfo)(req.getSession().getAttribute(CTConstants.SESSION_MUSER));
				memberInfo.setName(paramDto.getAsString("name"));
				memberInfo.setPay_account(paramDto.getAsString("pay_account"));
				memberInfo.setEmail(paramDto.getAsString("email"));
				memberInfo.setQq_num(paramDto.getAsString("qq_num"));
				memberInfo.setPhone(paramDto.getAsString("phone"));
				req.getSession().removeAttribute(CTConstants.SESSION_MUSER);
				req.getSession().setAttribute(CTConstants.SESSION_MUSER, memberInfo);
				
			}else{
				returnMsg = "1#修改会员信息出错";
			}
			RequestUtil.renderText(returnMsg);
		} else {
		   CTUtils.getParamToReq(req);
		   req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "setting");
		   req.getRequestDispatcher("/wxb/userInfo.jsp").forward(req, resp);
		
		}
		
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
