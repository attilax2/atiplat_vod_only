package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class showWenAnServlet extends HttpServlet {

	private static final long serialVersionUID = -3538348052416404355L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		//处理文案信息
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		String copy_id = getParamFromURl(reqPath, temp_path);
		if(CTUtils.isNotEmpty(copy_id)){
			//获取文案信息
			paramDto.put("copy_id", copy_id);
			List<Dto> cpList = JdbcTemplateTool.getGoodCopyList(paramDto).getDatas();
			req.setAttribute("copyInfo", cpList.get(0));
			req.getRequestDispatcher("/wxb/wenan/showWenAn.jsp").forward(req, resp);
		}
	}

	/**
	 * 获取参数
	 * @param url
	 * @return
	 */
	private String getParamFromURl(String url,String surl) {
		String param=null;
		if(StringUtil.isNotEmpty(url)){
			String tempUrl = url.substring(url.lastIndexOf(surl), url.length());
			String params[] = tempUrl.split("\\/");
			if(params.length==3){
			   param = params[2];
			}else{
			   param = params[1];
			}
		}
		StringUtil.xprint("param="+param);
		return param;
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		    doGet(req, resp);
	}

}
