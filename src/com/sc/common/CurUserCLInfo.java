package com.sc.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.csmy.my.center.util.db.DBConnect;

public class CurUserCLInfo {
	/**
	 * 获取当前登录人的平台提成
	 * @param usercode
	 * @return
	 */
	public String getCurUserPttc(String usercode)
	{
		String pttc = "0%";
		ResultSet rs = null;
		Statement sta = null;
		Connection c = DBConnect.getMysqlConnect();
		try 
		{
			sta = c.createStatement();
			String qrySql = "select pttc from sys_code d where d.field_code='CULEVEL' and d.code_value=(select c.level from wxb_customer c where c.login_name='"+usercode+"') ";
			rs = sta.executeQuery(qrySql);
			if(rs.next())
			{
				pttc = rs.getString("pttc");
			}
			else
			{
				pttc = "0%";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("当前用户的提成为"+pttc);
		return pttc;
	}
	
	/**
	 * 获取当前登录人的平台提成
	 * @param usercode
	 * @return
	 */
	public String getCurUserSpsl(String usercode)
	{
		String retr = "";
		String spsl = "0";
		ResultSet rs = null;
		ResultSet rs1 = null;
		Statement sta = null;
		int h = 0;
		Connection c = DBConnect.getMysqlConnect();
		try 
		{
			sta = c.createStatement();
			String qrySql = "select spsl from sys_code d where d.field_code='CULEVEL' and d.code_value=(select c.level from wxb_customer c where c.login_name='"+usercode+"') ";
			rs = sta.executeQuery(qrySql);
			if(rs.next())
			{
				spsl = rs.getString("spsl");
			}
			else
			{
				spsl = "0";
			}
			
			String havSql = "select count(1)as sl from wxb_good g where g.customer_id in (select r.customer_id from wxb_customer r where r.login_name='"+usercode+"')";
			rs1 = sta.executeQuery(havSql);
			
			if(rs1.next())
			{
			   h = rs1.getInt("sl");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		retr = spsl+"#"+h;
		return retr;
	}

}
