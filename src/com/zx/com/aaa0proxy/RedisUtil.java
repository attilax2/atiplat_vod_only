package aaa0proxy;

import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisUtil {

	public static void main(String[] args) {
		System.out.println("--in");
		   JedisPool jedisPool = new JedisPool("localhost", 6379);  
	        Jedis jedis = null;  
	        try {  
	            jedis = jedisPool.getResource(); 
	          Set<String>  keys= jedis.keys("*");
	          for (String k : keys) {
				System.out.println(k);
				System.out.println(jedis.get(k));
				System.out.println("----");
			}
	          //  jedis.set("rediskey1", "redisvalue1");  
	         //   jedis.set("rediskey2", "redisvalue2");  
	          //  System.out.println(jedis.get("rediskey1"));  
	       //     System.out.println(jedis.get("rediskey2"));  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        } finally {  
	            if (jedis != null)  
	                jedis.close();  
	        }  
	        jedisPool.destroy();  
	    }  

 

}
