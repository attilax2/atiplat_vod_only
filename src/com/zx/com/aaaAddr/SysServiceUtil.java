package aaaAddr;

import java.util.List;
import java.util.Map;

import jdk.nashorn.internal.ir.Terminal;

import com.attilax.json.AtiJson;
import com.attilax.lang.YamlAtiX;
import com.attilax.lang.text.strUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.registry.RegistryKey;
import com.registry.RegistryValue;

/** aaaAddr.SysServiceUtil.getList
 * @author digdeep@126.com
 */
public class SysServiceUtil {
	// "HKEY_LOCAL_MACHINE"
	public static final RegistryKey LOCALMACHINE = RegistryKey
			.getRootKeyForIndex(RegistryKey.HKEY_LOCAL_MACHINE_INDEX);

	// "HKEY_LOCAL_MACHINESystemCurrentControlSetControlTerminal ServerWinStationsRDP-Tcp"
	public static final RegistryKey rdpKey = null;

	// = new RegistryKey(LOCALMACHINE,
	// "\System\CurrentControlSet\Control\Terminal
	// Server\WinStations\RDP-Tcp\");

	/**
	 * 获得 windows server 中的 terminal server 的端口 也就是远程桌面服务的端口
	 * 
	 * @return
	 */
	public static long getTerminalServerPort() {
		long port = -1;

		if (rdpKey.hasValues()) {
			RegistryValue portNumber = rdpKey.getValue("PortNumber");
			System.out.println(portNumber.toString()); // Name: PortNumber Type:
														// REG_DWORD Value: 3389
			String value = portNumber.toString();
			port = Long.valueOf(value.substring(value.lastIndexOf(": ") + 2));
			System.out.println("port: " + port);
		}

		return port;
	}

	public static void main(String[] args) {

		SysServiceUtil sysx = new SysServiceUtil();
		List li = sysx.getList();
		System.out.println(AtiJson.gson(li));

	}

	/**
	 * attilax 2016年4月12日 下午2:35:10
	 * 
	 * @return
	 */
	@SuppressWarnings("all")
	public List getList() {
		// getTerminalServerPort();
		String roottype = "1:man,2:auto,3:manType3,4:disable";
		Map roottype_m = YamlAtiX.getMap_fromTxt(roottype);
		RegistryKey k = new RegistryKey(LOCALMACHINE,
				"SYSTEM\\CurrentControlSet\\services");
		//todox  drektly use fullpath se ,get retu is empty in get subkey..zihao use jeig locamacie var mode..
		// System.out.println( AtiJson.toJson(k));
		/*
		 * 
		 * { "hKey": -2147483647, "name": "services", "path":
		 * "HKEY_CURRENT_USER\\HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet",
		 * "handles": [], "created": false, "level": 4, "lastError": 0,
		 * "view64": 256 }
		 */
		// k.g
		List<RegistryKey> li = k.getSubKeys();
		// k.getSubKeys(".NETFramework")
		// k.getSubKeyNames()
		// k.getNumberOfSubKeys()
		System.out.println(li.size());
		List<Map> li_r = Lists.newLinkedList();
		int i = 0;
		for (RegistryKey registryKey : li) {
			try {
				i++;
				System.out.println(i);
				// System.out.println( AtiJson.toJson(registryKey));
				System.out.println(registryKey.getName());
				if (registryKey.getName().equals("AcpiPmi"))
					System.out.println("dbg");
				Map m = Maps.newLinkedHashMap();
				if (registryKey.getValue("Description") != null) {
					RegistryValue v = registryKey.getValue("Description");

					byte[] byteData = v.getByteData();
					// Object s = strUtil.from_wz_encode( byteData,"gbk");
					Object s = strUtil.from_wz_encode(byteData,
							"UnicodeLittleUnmarked");
					m.put("Description", s);
				}

				m.put("name", registryKey.getName());
				if (registryKey.getValue("DisplayName") != null) {

					RegistryValue v = registryKey.getValue("DisplayName");
					// v.getName() ==DisplayName
					// v.toString()
					byte[] byteData = v.getByteData();
					Object s = strUtil.from_wz_encode(byteData,
							"UnicodeLittleUnmarked");

					m.put("DisplayName", s);

				}
				if (registryKey.getValue("ImagePath") != null)
					m.put("ImagePath", strUtil.from_wz_encode(registryKey
							.getValue("ImagePath").getByteData(),
							"UnicodeLittleUnmarked"));

			
					m.put("Start", getReg_dword(registryKey.getValue("Start") ));
					if(m.get("Start")!=null)
						m.put("StartTxt", roottype_m.get(m.get("Start")));
				 
				// //if DelayedAutostart=1 and auto ,then auto delaye start
			 
					m.put("DelayedAutostart",getReg_dword(registryKey.getValue("DelayedAutostart") ));
							 
				li_r.add(m);
			} catch (Exception e) {
				e.printStackTrace();
			}

			//
		}
		System.out.println("--ok");

		return li_r;
	}

	/**
	attilax    2016年4月12日  下午4:59:53
	 * @param string
	 * @return
	 */
	private Object getReg_dword(RegistryValue  regVal) {
		if (regVal != null) {
			
			byte[] a=	regVal
				.getByteData();
			
	
		return 	strUtil.from(a[0]);
		}
		return regVal;
	}

}