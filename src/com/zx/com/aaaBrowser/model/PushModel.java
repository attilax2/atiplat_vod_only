package aaaBrowser.model;

public class PushModel
{
    private String ciid;
    
    private String title;
    
    private String catid;
    
    private String keyword;
    
    private String price;
    
    private String amount;
    
    private String thumb;
    
    private String content;
    
    private String ciuid;
    
    private String cisiteid;
    
    private String ciacturl;
    
    private String cipostedid;
    
    private String username;
    
    private String password;
    
    
    public String getCiid()
    {
        return ciid;
    }

    public void setCiid(String ciid)
    {
        this.ciid = ciid;
    }

    public String getTitle()
    {
        return title;
    }
    
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    public String getCatid()
    {
        return catid;
    }
    
    public void setCatid(String catid)
    {
        this.catid = catid;
    }
    
    public String getKeyword()
    {
        return keyword;
    }
    
    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }
    
    public String getPrice()
    {
        return price;
    }
    
    public void setPrice(String price)
    {
        this.price = price;
    }
    
    public String getAmount()
    {
        return amount;
    }
    
    public void setAmount(String amount)
    {
        this.amount = amount;
    }
    
    public String getThumb()
    {
        return thumb;
    }
    
    public void setThumb(String thumb)
    {
        this.thumb = thumb;
    }
    
    public String getContent()
    {
        return content;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public String getCiuid()
    {
        return ciuid;
    }
    
    public void setCiuid(String ciuid)
    {
        this.ciuid = ciuid;
    }
    
    public String getCisiteid()
    {
        return cisiteid;
    }
    
    public void setCisiteid(String cisiteid)
    {
        this.cisiteid = cisiteid;
    }
    
    public String getCiacturl()
    {
        return ciacturl;
    }
    
    public void setCiacturl(String ciacturl)
    {
        this.ciacturl = ciacturl;
    }
    
    public String getCipostedid()
    {
        return cipostedid;
    }
    
    public void setCipostedid(String cipostedid)
    {
        this.cipostedid = cipostedid;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
    
    
    
}
