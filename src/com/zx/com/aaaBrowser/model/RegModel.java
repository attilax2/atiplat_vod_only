package aaaBrowser.model;

public class RegModel
{
    private String crid;
    private String truename;
    private String groupid;
    private String email;
    private String area;
    private String company;
    private String telephone;
    private String regyear;
    private String regcity;
    private String   business;
    private String  regunit;
    private String  capital;
    private String  cruid;
    private String  crsiteid;
    private String  error;
    private String address;
    private String content;
    private String catid;
    private String type;
    private String validated;
    private String croccupytime;
    private String crloginurl;//登录地址
    private String crdetailurl;//完善资料地址
    private String cracturl;//注册地址
     
    private String  username;
    
    private String password;
    public String getCrid()
    {
        return crid;
    }
    public void setCrid(String crid)
    {
        this.crid = crid;
    }
  
   
    public String getTruename()
    {
        return truename;
    }
    public void setTruename(String truename)
    {
        this.truename = truename;
    }
    public String getGroupid()
    {
        return groupid;
    }
    public void setGroupid(String groupid)
    {
        this.groupid = groupid;
    }
    public String getEmail()
    {
        return email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
    public String getArea()
    {
        return area;
    }
    public void setArea(String area)
    {
        this.area = area;
    }
    public String getCompany()
    {
        return company;
    }
    public void setCompany(String company)
    {
        this.company = company;
    }
    public String getTelephone()
    {
        return telephone;
    }
    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }
    public String getRegyear()
    {
        return regyear;
    }
    public void setRegyear(String regyear)
    {
        this.regyear = regyear;
    }
    public String getRegcity()
    {
        return regcity;
    }
    public void setRegcity(String regcity)
    {
        this.regcity = regcity;
    }
    public String getBusiness()
    {
        return business;
    }
    public void setBusiness(String business)
    {
        this.business = business;
    }
    public String getRegunit()
    {
        return regunit;
    }
    public void setRegunit(String regunit)
    {
        this.regunit = regunit;
    }
   
    public String getCapital()
    {
        return capital;
    }
    public void setCapital(String capital)
    {
        this.capital = capital;
    }
    public String getCruid()
    {
        return cruid;
    }
    public void setCruid(String cruid)
    {
        this.cruid = cruid;
    }
    public String getCrsiteid()
    {
        return crsiteid;
    }
    public void setCrsiteid(String crsiteid)
    {
        this.crsiteid = crsiteid;
    }
    public String getError()
    {
        return error;
    }
    public void setError(String error)
    {
        this.error = error;
    }
    public String getAddress()
    {
        return address;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }
    public String getContent()
    {
        return content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }
    public String getCatid()
    {
        return catid;
    }
    public void setCatid(String catid)
    {
        this.catid = catid;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public String getValidated()
    {
        return validated;
    }
    public void setValidated(String validated)
    {
        this.validated = validated;
    }
    public String getCroccupytime()
    {
        return croccupytime;
    }
    public void setCroccupytime(String croccupytime)
    {
        this.croccupytime = croccupytime;
    }
    public String getCrloginurl()
    {
        return crloginurl;
    }
    public void setCrloginurl(String crloginurl)
    {
        this.crloginurl = crloginurl;
    }
    public String getCrdetailurl()
    {
        return crdetailurl;
    }
    public void setCrdetailurl(String crdetailurl)
    {
        this.crdetailurl = crdetailurl;
    }
    public String getCracturl()
    {
        return cracturl;
    }
    public void setCracturl(String cracturl)
    {
        this.cracturl = cracturl;
    }
    public String getUsername()
    {
        return username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }
    public String getPassword()
    {
        return password;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    
} 
