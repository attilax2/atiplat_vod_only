package aaaClicker;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import com.attilax.io.pathx;

public class P4n {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
   System.setProperty("webdriver.firefox.bin", pathx.classPathParent()+"\\Mozilla Firefox\\firefox.exe");

    driver = new FirefoxDriver();
    baseUrl = "http://guang.zhe800.com/guangbaobao";
	driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testP4n() throws Exception {
	  System.out.println("-- wile open ");
    driver.get(baseUrl  );
    System.out.println("will click");
    driver.findElement(By.linkText("春款时尚单肩斜挎包女士包包")).click();
    Thread.sleep(20000);
  }

  @After
  public void tearDown() throws Exception {
	  System.out.println(" quit");
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
