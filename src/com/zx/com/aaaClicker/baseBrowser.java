package aaaClicker;

import java.util.List;
import java.util.Map;

import com.attilax.collection.CollX;
import com.attilax.io.filex;
import com.attilax.io.pathx;

public class baseBrowser {

	public List<String> getProxysLi(Map<String, String> PropMap) {
		List<String> li = filex.read2list_filtEmptyNstartSpace(pathx
				.classPathParent() + "\\proxy.txt");
		// p916
		if (PropMap.get("proxy").equals("0")) {

			try {
				int maxOpenCountNoProxy = 1;
				maxOpenCountNoProxy = Integer.parseInt(System
						.getProperty("ati.maxOpenCountNoProxy"));
				li = CollX.geneItems(maxOpenCountNoProxy);
			} catch (Exception e) {
				li = CollX.geneItems(300);
			}

		}// end p916
		final List<String> li2 = li;
		return li2;
	}

}
