/**
 * 
 */
package aaaCms;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;
import java.util.Map;

import com.attilax.io.filex;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author attilax
 *2016年4月4日 下午5:25:16
 */
public class Folder2Row {

	/**
	attilax    2016年4月4日  下午5:25:16
	 * @param args
	 */
	public static void main(String[] args) {
		 String s="Z:\\动作类\\超能失控";
		 
          System.out.println(   new Folder2Row().toRow(s)  );
	}

	/**
	attilax    2016年4月4日  下午5:28:16
	 * @param s  data_folder
	 */
	@SuppressWarnings("unchecked")
	public Map toRow(String s) {
		
	//	List<Map> li = Lists.newArrayList();
		
		Map m = Maps.newLinkedHashMap();
		 File folder=new File(s);
			File[] fs = folder.listFiles();
			if(fs==null)
				return m;
			for (File file : fs) {
			
				try {
					m.put("data_id", folder.getName());
					
					if(isThumb( file.getName()))
						m.put("thumb",  ""+folder.getName()+"/"+ file.getName());
					if(isDesc(file.getName()))
						m.put("desc", getDesc(s+"/"+ file.getName()));
					
					
					if(isBigPic(file.getName()))
						m.put("bigpic",  folder.getName()+"/"+ file.getName());
				
					if(isMov(""))
						m.put("path", "");
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				 
			}
			m.put("bigpic",m.get("thumb"));
			return m;
		
	}

	/**
	attilax    2016年4月4日  下午6:02:38
	 * @param string
	 * @return
	 */
	private Object getDesc(String path) {
		// TODO Auto-generated method stub
	//	System.out.println(new File);
		Boolean b=new File(path).exists();
		String read = filex.read(path, "gbk");
		return read;
	}

	/**
	attilax    2016年4月4日  下午5:51:02
	 * @param string
	 * @return
	 */
	private boolean isMov(String string) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	attilax    2016年4月4日  下午5:50:20
	 * @param name
	 * @return
	 */
	private boolean isThumb(String filename) {
		String extname = filex.getExtName(filename).toLowerCase();
		if ("jpg".contains(extname) || "jpeg".contains(extname) || "png".equals(extname))
		{
			if(!filename.contains("bigpic"))
				return true;
		}
	 
			return false;
	}

	/**
	attilax    2016年4月4日  下午5:50:18
	 * @param string
	 * @return
	 */
	private boolean isDesc(String filename) {
		String extname = filex.getExtName(filename).toLowerCase();
		System.out.println(extname+":/"+ filename);
		if(extname.equals("txt"))
			return true;
		return false;
	}

	/**
	attilax    2016年4月4日  下午5:46:58
	 * @param name
	 * @return
	 */
	private boolean isBigPic(String filename) {
		 
		 
					String extname = filex.getExtName(filename).toLowerCase();
					if ("jpg".contains(extname) || "jpeg".contains(extname) || "png".equals(extname))
					{
						if(filename.contains("bigpic"))
							return true;
					}
				 
						return false;
			 
		 
		 
	}

}
