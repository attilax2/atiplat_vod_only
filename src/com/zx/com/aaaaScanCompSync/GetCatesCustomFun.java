package aaaaScanCompSync;

import java.util.List;

import com.attilax.io.pathx;
import com.attilax.lang.text.strUtil;
import com.attilax.linq.CustomFun;
import com.attilax.secury.propertyReader;
import com.attilax.util.PropX;

public class GetCatesCustomFun extends CustomFun {

	@Override
	public Object ext(Object obj) {
		List<Object> li=(List<Object>) obj;
		String cateExisted_s=(String) li.get(0);
		if(cateExisted_s==null)
			cateExisted_s="";
		String playUrl=(String) li.get(1);
		String mycate=playUrl.split("/")[0];
		if(mycate==null)
			mycate="";
		String mycate_smp=mycate.replace("类", "");
		if(mycate_smp==null)
			mycate_smp="";
		String convert_himedia_cate=get_convert_himedia_cate(mycate);
	 //  String[] cateExisted=obj.toString().split(" ");
		//剧情 喜剧 家庭 奇幻
		String str = mycate+" "+mycate_smp+" "+convert_himedia_cate+" "+cateExisted_s.trim();
		return strUtil.deduli(str," ");
	}

	private String get_convert_himedia_cate(String mycate) {
		try {
			 String f=pathx.webAppPath()+"/cate_map2himedia.txt";
			  PropX px=new PropX(f, "gbk");
			//  propertyReader pr=new propertyReader();	  
				String property = px.getProperty(mycate);
				if(property==null) return "";
				return property;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	 
	}

}
