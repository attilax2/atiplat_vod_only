package aaaaScanCompSync;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import com.attilax.Closure;
import com.attilax.cmsPoster.ItemImp;
import com.attilax.cmsPoster.Traver;
import com.attilax.io.dirx;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.office.excelUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;


/**
 * aaaaScanCompSync.Scanner
 * @author Administrator
 *
 */@SuppressWarnings("all")
public class Scanner {
	public static int i=0;
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		//  Auto-generated method stub
		
		final List<Map> li=Lists.newArrayList();
		 dirx.traveQ21(args[0],new Closure<String, Object>() {

			@Override
			public Object execute(String f2) throws Exception {
				f2=pathx.fixSlash(f2);
				File f=new File(f2);
				if(f.length()>10000000)//10m
				{
					System.out.println(f);
					i++;
					if(i>1000000)
						throw new StopException();
					
					Map m=Maps.newLinkedHashMap(); 
					String mainName=filex.getFileName_mainname_noExtName_nopath(f2);
				   String  dirName=filex.getDirName(f2,2);
					m.put("dirName", dirName);
					m.put("mainName", mainName);
					double lenGb = Double.valueOf(f.length()) /1000000000;
					DecimalFormat    df   = new DecimalFormat("######0.00");  
				String s=	df.format(lenGb);
				// Math.round( lenGb)
					m.put("lenGB",Double.parseDouble(s));
					m.put("lenxMB", f.length()/1000000);
					m.put("len", f.length());m.put("path", f2);
				 
					li.add(m);
				}
				return null;
			}
		});
			
			 
		
	String outputFilePath = pathx.webAppPath()+"/"+filex.getUUidName()+".xls";
	System.out.println("--------out:"+outputFilePath);
	new 	excelUtil().toExcel(li, outputFilePath);
	System.out.println("--f");
		
//	filex
//		dirx.traveDirPa1(strPath, closure);

	}

}
