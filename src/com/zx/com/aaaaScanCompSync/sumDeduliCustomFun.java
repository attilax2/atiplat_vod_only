package aaaaScanCompSync;

import java.util.List;

import com.attilax.lang.text.strUtil;
import com.attilax.linq.CustomFun;
import com.attilax.linq.reduceFun;

public class sumDeduliCustomFun extends CustomFun implements reduceFun {

	@Override
	public Object ext(Object obj) {
		List<Object> li=(List<Object>) obj;
		String cateExisted_s=(String) li.get(0);
		String lastVal=(String) li.get(1);
		return strUtil.deduli(cateExisted_s+" "+lastVal," ");
	}

}
