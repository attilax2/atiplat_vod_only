 package aaaaaCompiler;
  import javax.tools.*;
import javax.tools.JavaFileManager.Location;

import com.attilax.io.pathx;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
  public class Compiler {
    public static void main(String[] args) throws Exception{
    T();
    	String sFullFileName=pathx.appPath()+"/aaaaaCompiler/Target.java";
		String sOutputPath="c:\\0out";
	//	new Compiler().compileFile(sFullFileName, sOutputPath)
		//利用URLClassLoader去实例化一个Class类  类文件可以放在任意位置，这样就很方便了
   	 URL[] urls=new URL[]{new URL("file:/"+"./bin/")};
   	 URLClassLoader classLoader=new URLClassLoader(urls);
         Class class1=classLoader.loadClass("aaaaaCompiler.Target");
   	 Method method=class1.getDeclaredMethod("main",String[].class);
   	 String[] args1={null};
    	 method.invoke(class1.newInstance(),args1); 
    }


	private static void T() throws FileNotFoundException {
//		String fullQuanlifiedFileName = "compile" + java.io.File.separator +
//		        "Target.java";     
		String fullQuanlifiedFileName=pathx.prjPath()+"/src/aaaaaCompiler/Target.java";
		  JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
 
	//	  FileOutputStream err = new FileOutputStream("err.txt");
// com.sun.tools.javac.api.JavacTool @2133c8f8  ==compiler  ,
		  String string = "-Xlint:deprecation ";
		  string ="-d c:\\00output";
		int compilationResult = compiler.run(null, null, null,"-d","c:\\00output", fullQuanlifiedFileName);
 
		  if(compilationResult == 0){
		    System.out.println("Done");
		  } else {
		    System.out.println("Fail");
		  }
	}
    
    
    /**
     * Author: Jiangtao He; Email: ross.jiangtao.he@gmail.com
     * @param sFullFileName: the java source file name with full path
     * @param sOutputPath: the output path of java class file
     * @return bRet: true-compile successfully, false - compile unsuccessfully
     * Description: Compile java source file to java class with getTask
     *     method, it can specify the class output path and catch diagnostic
     *     information
     * @throws IOException 
     */
    public boolean compileFile(String sFullFileName, String sOutputPath) throws IOException
    {
        boolean bRet = false;
        // get compiler
        JavaCompiler oJavaCompiler = ToolProvider.getSystemJavaCompiler();

        // define the diagnostic object, which will be used to save the
        // diagnostic information
        DiagnosticCollector<JavaFileObject> oDiagnosticCollector = new DiagnosticCollector<JavaFileObject>();

        // get StandardJavaFileManager object, and set the diagnostic for the
        // object
        StandardJavaFileManager oStandardJavaFileManager = oJavaCompiler
                .getStandardFileManager(oDiagnosticCollector, null, null);

        // set class output location
        Location oLocation = StandardLocation.CLASS_OUTPUT;
        try
        {
            oStandardJavaFileManager.setLocation(oLocation, Arrays
                    .asList(new File[] { new File(sOutputPath) }));

            // get JavaFileObject object, it will specify the java source file.
            Iterable<? extends JavaFileObject> oItJavaFileObject = oStandardJavaFileManager
                    .getJavaFileObjectsFromFiles(Arrays.asList(new File(
                            sFullFileName)));

            // compile the java source code by using CompilationTask's call
            // method
            bRet = oJavaCompiler.getTask(null, oStandardJavaFileManager,
                    oDiagnosticCollector, null, null, oItJavaFileObject).call();

            //print the Diagnostic's information
            for (Diagnostic oDiagnostic : oDiagnosticCollector
                    .getDiagnostics())
            {
                System.out.println("Error on line: "
                        + oDiagnostic.getLineNumber() + "; URI: "
                        + oDiagnostic.getSource().toString());
            }
        }
        catch (IOException e)
        {
            //exception process
            System.out.println("IO Exception: " + e);
            throw e;
        }
        finally
        {
            //close file manager
            if (null != oStandardJavaFileManager)
            {
                oStandardJavaFileManager.close();
            }
        }
        return bRet;
    }
  }