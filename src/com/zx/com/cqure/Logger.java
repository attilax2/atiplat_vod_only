/* 
   Cifs Password Scanner
   Copyright (C) Patrik Karlsson 2004
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

package cqure;

import java.io.*;

public class Logger {

    private boolean m_bLogToScreen = true;
    private PrintStream m_oOut = null;
    private PrintStream m_oOutDebug = System.out;

    public static final int LOG_DEBUG = 5;
    public static final int LOG_TRACE = 4;
    public static final int LOG_WARNING = 3;
    public static final int LOG_INFO = 2;
    public static final int LOG_ERROR = 1;

    public static final int ERROR_FILE_ACCESS = -100;
    public static final int OK = 0;

    private int m_nLogLevel = LOG_ERROR;

    public Logger() {

    }

    public int setLogFile( String sLogFile ) {

	try {
	    m_oOut = new PrintStream( new FileOutputStream( sLogFile ) );
	}
	catch( FileNotFoundException e ) {
	    return ERROR_FILE_ACCESS;
	}

	return OK;

    }

    public boolean isAvailable() {

	if ( m_oOut != null )
	    return true;

	return false;

    }


    public void setLogLevel( int n ) { m_nLogLevel = n; }
    
    public void trace( String sMsg ) {
	if ( m_nLogLevel >= LOG_TRACE )
	    m_oOutDebug.println( sMsg );
    }
    
    public void debug( String sMsg ) {
	if ( m_nLogLevel == LOG_DEBUG )
	    m_oOutDebug.println( sMsg );
    }

    public void info( String sMsg ) {
	if ( m_nLogLevel >= LOG_INFO )
	    m_oOutDebug.println( sMsg );
    }

    public void error( String sMsg ) {
	if ( m_nLogLevel >= LOG_ERROR )
	    m_oOutDebug.println( sMsg );
    }

    public void log( String sMsg ) {

	if ( m_oOut != null )
	    m_oOut.println( sMsg );
	
	if ( m_bLogToScreen )
	    System.out.println( sMsg );
    }
    



}
