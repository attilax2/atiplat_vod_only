package log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.attilax.exception.ExUtil;
import com.attilax.io.filex;

public class Log {
	public String LOGFILENAME_S = "c:\\0log\\log_S" + filex.getUUidName()
			+ ".txt";
	public String LOGFILENAME_C = "c:\\0log\\log_C" + filex.getUUidName()
			+ ".txt";

	public OutputStream log_S = null; // 日志输出流
	public OutputStream log_C = null; // 日志输出流
	public boolean inied = false;
	
	public static void main(String[] args) {
		Log l=new Log();
		l.writeLog(71, true);	l.writeLog(72, true);l.writeLog(73, true);
		l.flush();
		System.out.println("--f");
	}

	public void ini() {
		try {
			filex.createAllPath(LOGFILENAME_S);
			filex.createAllPath(LOGFILENAME_C);
			log_S = new FileOutputStream(new File(LOGFILENAME_S),true);

			log_C = new FileOutputStream(new File(LOGFILENAME_C),true);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ExUtil.throwEx(e);
		}
	}

	public void writeLog(int c, boolean browser) {
		if (!inied)
			ini();
		
		try {
			if (browser)
				log_C.write((char) c);
			else
				log_S.write((char) c);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void writeLog(byte[] bytes, int offset, int len, boolean browser) {
		for (int i = 0; i < len; i++)
			writeLog((int) bytes[offset + i], browser);

		flush();
	}

	public void flush() {
		try {
			log_C.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			log_S.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			log_C.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			log_S.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
