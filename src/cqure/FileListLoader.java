/* 
   Cifs Password Scanner
   Copyright (C) Patrik Karlsson 2004
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

package cqure;

import java.io.*;
import java.util.Vector;

public class FileListLoader extends Vector {

    public static final int ERROR_FILE_NOT_FOUND = -1000;
    public static final int ERROR_READ_FAILURE = -1001;
    public static final int OK = 0;

    private int m_nPos = 0;

    public FileListLoader() {
	super();
    }

    public int getItemCount() { return this.size(); }

    public void reset() {
	m_nPos = 0;
    }

    public String getNextItem() {

	String sItem = null;

	if ( m_nPos < this.size() ) {
	    sItem = (String)this.get(m_nPos);
	    m_nPos ++;
	}

	return sItem;

    }

    public int loadFromFile( String sFilename ) {

	String sItem;
	LineNumberReader oReader = null;
	int nCommentPos = -1;


	try {
	    oReader = new LineNumberReader( new FileReader( sFilename ) );

	    while ( ( sItem = oReader.readLine() ) != null ) {

		nCommentPos = sItem.indexOf( "#" );

		if ( nCommentPos > 0 )
		    add( sItem.substring( 0, nCommentPos ) );
		else if ( nCommentPos == 0 )
		    continue;
		else
		    add( sItem );
	    }

	    oReader.close();

	}
	catch( Exception e ) {
	    return ERROR_READ_FAILURE;
	}

	return OK;
	
    }


}
