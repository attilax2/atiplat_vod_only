/*
Navicat MySQL Data Transfer

Source Server         : loc
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : zz_plat

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2016-06-13 23:09:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unionid` varchar(255) NOT NULL DEFAULT '',
  `promoter` bigint(20) unsigned NOT NULL DEFAULT '0',
  `plat` varchar(64) NOT NULL DEFAULT '',
  `totalRmb` int(10) unsigned NOT NULL DEFAULT '0',
  `createdTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `unionid` (`unionid`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('1000', 'C8-0A-A9-95-B4-65', '888', '', '0', '0000-00-00 00:00:00');
INSERT INTO `account` VALUES ('1001', '98-90-96-D0-11-94', '888', '', '0', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for agent
-- ----------------------------
DROP TABLE IF EXISTS `agent`;
CREATE TABLE `agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agent
-- ----------------------------
INSERT INTO `agent` VALUES ('1', '888', 'ati', '0', '000');
INSERT INTO `agent` VALUES ('9', '1000', null, '888', '111111');

-- ----------------------------
-- Table structure for promoter
-- ----------------------------
DROP TABLE IF EXISTS `promoter`;
CREATE TABLE `promoter` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createdTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promoter
-- ----------------------------

-- ----------------------------
-- Table structure for recharge
-- ----------------------------
DROP TABLE IF EXISTS `recharge`;
CREATE TABLE `recharge` (
  `orderid` varchar(120) NOT NULL COMMENT '订单ID',
  `accountId` bigint(20) unsigned NOT NULL,
  `platform` int(10) unsigned NOT NULL COMMENT '充值平台',
  `rmb` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值金额',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`orderid`),
  KEY `accountId` (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recharge
-- ----------------------------
INSERT INTO `recharge` VALUES ('', '1000', '0', '150', '2016-06-09 17:03:43');
INSERT INTO `recharge` VALUES ('1', '1000', '0', '2', '2016-06-09 17:04:26');
